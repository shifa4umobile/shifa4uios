//
//  ConnectingVC.swift
//  AppDesign
//
//  Created by UDHC on 31/10/2018.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class ConnectingVC: UIViewController , UITableViewDelegate , UITableViewDataSource {
   
    @IBOutlet weak var TimerLabel: UILabel!
    @IBOutlet weak var TableView: UITableView!
    @IBOutlet weak var SecondView: UIView!
    @IBOutlet weak var SubView: UIView!
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var EndCall: UIButton!
    
    var startTime = TimeInterval()
    var timer:Timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (!timer.isValid) {
            let aSelector : Selector = #selector(ConnectingVC.updateTime)
            timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: aSelector, userInfo: nil, repeats: true)
            startTime = NSDate.timeIntervalSinceReferenceDate
        }
        Common.SetShadowwithCorneronView(view: MainView, cornerradius: 10.0, color: UIColor.lightGray)
        Common.SetShadowwithCorneronView(view: SecondView, cornerradius: 10.0, color: UIColor.lightGray)
        Common.SetShadowwithCorneronView(view: SubView, cornerradius: 10.0, color: UIColor.lightGray)
      Common.SetShadowwithCorneronView(view: EndCall, cornerradius: 10.0, color: UIColor.lightGray)
        TableView.tableFooterView = UIView()
        let when = DispatchTime.now() + 15
        DispatchQueue.main.asyncAfter(deadline: when){
            let madicalVC = AppUtility.STORY_BOARD_SECOND().instantiateViewController(withIdentifier: "OnlineDoctorsWVC") as! OnlineDoctorsWVC
            AppUtility.shared.mainNavController.pushViewController(madicalVC, animated: true)
            ISMessages.show("Doctor Found")
        }

        // Do any additional setup after loading the view.
    }
    
    @objc func updateTime() {
        var currentTime = NSDate.timeIntervalSinceReferenceDate
        
        //Find the difference between current time and start time.
        var elapsedTime: TimeInterval = currentTime - startTime
        let hours = UInt8(elapsedTime / 3600)
        
        //calculate the minutes in elapsed time.
        let minutes = UInt8(elapsedTime / 60.0)
        elapsedTime -= (TimeInterval(minutes) * 60)
        
        //calculate the seconds in elapsed time.
        let seconds = UInt8(elapsedTime)
        elapsedTime -= TimeInterval(seconds)
        
        //find out the fraction of milliseconds to be displayed.
        
        
        //add the leading zero for minutes, seconds and millseconds and store them as string constants
        
        let strMinutes = String(format: "%02d", hours)
        let strSeconds = String(format: "%02d", minutes)
        let strFraction = String(format: "%02d", seconds)
        
        //concatenate minuets, seconds and milliseconds as assign it to the UILabel
        TimerLabel.text = "\(strMinutes):\(strSeconds):\(strFraction)"
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func BackBtn(_ sender: Any)
    {
        AppUtility.shared.mainNavController.popViewController(animated: true)
        timer.invalidate()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = TableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let mainview = cell.contentView.viewWithTag(1)
        var imgmain = cell.contentView.viewWithTag(2) as! UIImageView
        let lbltitle = cell.contentView.viewWithTag(3) as! UILabel
        let lblprice = cell.contentView.viewWithTag(4) as! UILabel
        let lblothername = cell.contentView.viewWithTag(5) as! UILabel
        let btnorder = cell.contentView.viewWithTag(6) as! UIView
        let btnordernew = cell.contentView.viewWithTag(7) as! UIButton
        let submittView = cell.contentView.viewWithTag(8) as! UIView
        let btnsubmitt = cell.contentView.viewWithTag(9) as! UIButton
        
        Common.SetShadowwithCorneronView(view: mainview!, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: imgmain, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: btnorder, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: btnordernew, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: submittView, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: btnsubmitt, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        return cell

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 150
    }
    
}
