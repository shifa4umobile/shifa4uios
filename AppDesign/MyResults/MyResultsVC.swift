//
//  MyResultsVC.swift
//  AppDesign
//
//  Created by sachin on 15/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class MyResultsVC: UIViewController
{
    @IBOutlet weak var btnlabresult: UIButton!
    @IBOutlet weak var btnradiology: UIButton!
    @IBOutlet weak var lineleadig: NSLayoutConstraint!
    @IBOutlet weak var linewidth: NSLayoutConstraint!
    @IBOutlet weak var tbllist: UITableView!
    var arrLabTestResult:NSMutableArray = NSMutableArray()
    
    var currentTab = String()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        btnradiology.titleLabel?.textColor = UIColor.lightGray
        
        currentTab = "1"
        if UserDefaults.standard.object(forKey: "arrlabtestresults") != nil
        {
            self.arrLabTestResult = NSMutableArray(array: UserDefaults.standard.object(forKey: "arrlabtestresults") as! NSArray)
            self.tbllist.reloadData()
            
            if self.arrLabTestResult.count != 0
            {
               apicall_LabTest(K.URL.labtestresults, is_firsttime: "")
            }
            else
            {
               apicall_LabTest(K.URL.labtestresults, is_firsttime: "1")
            }
        }
        else
        {
            apicall_LabTest(K.URL.labtestresults, is_firsttime: "1")
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnback(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnlabresult(_ sender: Any)
    {
        currentTab = "1"
        if UserDefaults.standard.object(forKey: "arrlabtestresults") != nil
        {
            self.arrLabTestResult = NSMutableArray(array: UserDefaults.standard.object(forKey: "arrlabtestresults") as! NSArray)
            self.tbllist.reloadData()
        }
        
        btnradiology.titleLabel?.textColor = UIColor.lightGray
        btnlabresult.titleLabel?.textColor = UIColor.white
        
        lineleadig.constant = btnlabresult.frame.origin.x
        linewidth.constant = btnlabresult.frame.size.width
    }
    
    @IBAction func btnradiologyresult(_ sender: Any)
    {
        currentTab = "2"
        if UserDefaults.standard.object(forKey: "arrradilogyresults") != nil
        {
            self.arrLabTestResult = NSMutableArray(array: UserDefaults.standard.object(forKey: "arrradilogyresults") as! NSArray)
            self.tbllist.reloadData()
        }
        
        btnlabresult.titleLabel?.textColor = UIColor.lightGray
        btnradiology.titleLabel?.textColor = UIColor.white
        
        lineleadig.constant = btnradiology.frame.origin.x
        linewidth.constant = btnradiology.frame.size.width
    }
    
    @IBAction func btnCell_DownloadPDF(_ sender: Any) {
    
        
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tbllist)
        let indexPath = self.tbllist.indexPathForRow(at: buttonPosition)
        
        let obj = arrLabTestResult[(indexPath?.row)!] as! NSDictionary
        let filename = (obj.object(forKey: "Paths") as! String)

        if currentTab == "1"
        {
            let strstr = K.URL.DisplayFilebyFileName + filename
            if let url = URL(string: strstr as String) {
                UIApplication.shared.open(url)
            }
        }
        else
        {
            let strstr = K.URL.DisplayFilebyFileName + filename
            if let url = URL(string: strstr as String) {
                UIApplication.shared.open(url)
            }
        }
    }
}

//MARK:- Tableview Delegate Method
extension MyResultsVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrLabTestResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let lbltitle = cell.contentView.viewWithTag(1) as! UILabel
        let lblOrderRefNumber = cell.contentView.viewWithTag(2) as! UILabel
        let lblOrderdate = cell.contentView.viewWithTag(3) as! UILabel
        let viewdownload = cell.contentView.viewWithTag(4)
        let viewmain = cell.contentView.viewWithTag(5)
        let obj = arrLabTestResult[indexPath.row] as! NSDictionary
        
        lbltitle.text = (obj.object(forKey: "ReportDetail") as! String)
        lblOrderdate.text = (obj.object(forKey: "OrderDate") as! String)
        lblOrderRefNumber.text = (obj.object(forKey: "OrderId") as! String)
        
        Common.SetShadowwithCorneronView(view: viewmain!, cornerradius: 10.0, color:UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: viewdownload!, cornerradius: 10.0, color:UIColor.clear)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 145
        
    }
    func apicall_LabTest( _ serviceURL:String, is_firsttime: String)
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        if is_firsttime == "1" {
            SVProgressHUD.show()
        }
        
        let token = UserDefaults.standard.object(forKey: "token") as! String
        let url = URL(string: serviceURL)
        var urlRequest = URLRequest(url: url!)
        urlRequest.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        urlRequest.timeoutInterval = 60
        urlRequest.httpMethod = "GET"
        urlRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, errorResponse) in
            
            SVProgressHUD.dismiss()
            if errorResponse != nil {
                print(errorResponse ?? "Erroer Found")
            }
            else{
                    do {
                        // gunzip
                        let decompressedData: Data
                        if (data?.isGzipped)! {
                            decompressedData = (try! data?.gunzipped())!
                        } else {
                            decompressedData = data!
                        }
            
                    if let dictionary = try JSONSerialization.jsonObject(with: decompressedData, options: .mutableContainers) as? [Any]
                    {
                        
                        DispatchQueue.main.async {
                            print(dictionary)
                            
                            let arrLabTestListTemp = NSMutableArray(array: dictionary)
                            let labArray:NSMutableArray = NSMutableArray()
                            for i in 0..<arrLabTestListTemp.count
                            {
                                let dictemp = arrLabTestListTemp.object(at: i) as! Dictionary<AnyHashable, Any>
                                let labDetails11 =  dictemp.nullKeyRemoval()
                                labArray.add(labDetails11)
                            }
                            if is_firsttime == "1" {
                                self.arrLabTestResult = labArray
                                self.tbllist.reloadData()
                            }
                            
                            UserDefaults.standard.set(labArray, forKey: "arrlabtestresults")
                            UserDefaults.standard.synchronize()
                        }
                        
                    }
                    
                }
                catch let exeptionError as Error? {
                    print(exeptionError!.localizedDescription)
                    SVProgressHUD.dismiss()
                }
            }
            
            self.apicall_Radiology(K.URL.radilogyresults, is_firsttime: "")
        })
        task.resume()
    }
    
    func apicall_Radiology( _ serviceURL:String, is_firsttime: String)
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        if is_firsttime == "1" {
            SVProgressHUD.show()
        }
        
        let token = UserDefaults.standard.object(forKey: "token") as! String
        let url = URL(string: serviceURL)
        var urlRequest = URLRequest(url: url!)
        urlRequest.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        urlRequest.timeoutInterval = 60
        urlRequest.httpMethod = "GET"
        urlRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, errorResponse) in
            
            SVProgressHUD.dismiss()
            if errorResponse != nil {
                print(errorResponse ?? "Erroer Found")
                
            }
            else{
                
                do {
                    let decompressedData: Data
                    if (data?.isGzipped)! {
                        decompressedData = (try! data?.gunzipped())!
                    } else {
                        decompressedData = data!
                    }
                    if let dictionary = try JSONSerialization.jsonObject(with: decompressedData, options: .mutableContainers) as? [Any]
                    {
                        DispatchQueue.main.async {
                            print(dictionary)
                            
                            let arrLabTestListTemp = NSMutableArray(array: dictionary)
                            
                            let labArray:NSMutableArray = NSMutableArray()
                            for i in 0..<arrLabTestListTemp.count
                            {
                                let dictemp = arrLabTestListTemp.object(at: i) as! Dictionary<AnyHashable, Any>
                                let labDetails11 =  dictemp.nullKeyRemoval()
                                labArray.add(labDetails11)
                            }
                            if is_firsttime == "1" {
                                self.arrLabTestResult = labArray
                                self.tbllist.reloadData()
                            }
                            
                            UserDefaults.standard.set(labArray, forKey: "arrradilogyresults")
                            UserDefaults.standard.synchronize()
                        }
                        
                    }
                }
                catch let exeptionError as Error? {
                    print(exeptionError!.localizedDescription)
                    SVProgressHUD.dismiss()
                }
            }
            
        })
        task.resume()
    }
    
}

