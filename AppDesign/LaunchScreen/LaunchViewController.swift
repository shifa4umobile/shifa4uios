//
//  LaunchViewController.swift
//  AppDesign
//
//  Created by Mahavir Makwana on 04/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD
import FirebasePerformance

class LaunchViewController: UIViewController {
    
    
    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var btnCity: UIButton!
    
    var countryID: Int = -1
    var cityID: Int = -1
    let chooseArticleDropDown = DropDown()
    var arrCountryList = NSArray()
    var arrCityList = NSArray()
    
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        
        _ = Performance.startTrace(name: "test trace")
        //trace?.incrementMetric("retry", by: 1)
        // trace?.start()
        // trace?.stop()
        self.getAllCountry()
    }
    
    //MARK: - IBActions
    @IBAction func btnCountryTapped(_ sender: UIButton)
    {
        let lblNotificationText = sender
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText.bounds.height))
        
        let arrLabsTemp = NSMutableArray()
        for i in 0..<arrCountryList.count
        {
            let labname = (arrCountryList.object(at: i) as! NSDictionary).object(forKey: "CountryName")
            arrLabsTemp.add(labname as Any)
        }
        
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            
            lblNotificationText.setTitle(item, for: .normal)
            
            if let value = (self?.arrCountryList.object(at: index) as! NSDictionary).object(forKey: "CountryName") as? String {
                self?.countryID = (self?.arrCountryList.object(at: index) as! NSDictionary).object(forKey: "CountryId") as! Int
                self?.btnCountry.setTitle(value, for: .normal)
                
                UserDefaults.standard.set("\(self?.countryID ?? -1)", forKey: K.Key.Country)
                UserDefaults.standard.synchronize()
                
                self?.getCity()
            }
            
            self?.chooseArticleDropDown.hide()
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
        }
        
        chooseArticleDropDown.show()
    }
    
    func getAllCountry() {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        SVProgressHUD.show()
        
        let url = URL(string: K.URL.GET_ALL_COUNTRY)
        let request = URLRequest(url: url!)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        print(url)
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            SVProgressHUD.dismiss()
            if data != nil
            {
                // gunzip
                let decompressedData: Data
                if (data?.isGzipped)! {
                    decompressedData = (try! data?.gunzipped())!
                } else {
                    decompressedData = data!
                }
                
                let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
                
                if let arrData = dictionary as? NSArray
                {
                    //print(dictionary!)
                    self.arrCountryList = NSArray(array: arrData)
                }
                else
                {
                    ISMessages.show("Countries not found, try again later")
                }
            }
        });
        task.resume()
    }

    /*func getCountryList()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        AppUtility.shared.getAllCountry { (success, error) in
            if success == true {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                    self.showCountryPicker()
                })
            }
            else
            {
                ISMessages.show("Countries not found, try again later")
            }
        }
    }*/
    
    @IBAction func btnCityTapped(_ sender: UIButton)
    {
        if countryID == -1 {
            ISMessages.show("Please select country")
            return
        }
        
        let lblNotificationText = sender
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText.bounds.height))
        
        let arrLabsTemp = NSMutableArray()
        for i in 0..<arrCityList.count
        {
            let labname = (arrCityList.object(at: i) as! NSDictionary).object(forKey: "CityName")
            arrLabsTemp.add(labname as Any)
        }
        
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            
            lblNotificationText.setTitle(item, for: .normal)
            
            if let value = (self?.arrCityList.object(at: index) as! NSDictionary).object(forKey: "CityName") as? String {
                self?.cityID = (self?.arrCityList.object(at: index) as! NSDictionary).object(forKey: "CityId") as! Int
                self?.btnCity.setTitle(value, for: .normal)
                
                UserDefaults.standard.set(value, forKey: K.Key.City)
                UserDefaults.standard.synchronize()
                
            }
            self?.chooseArticleDropDown.hide()
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
        }
        chooseArticleDropDown.show()
    }
    
    
    /*{
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        if countryID == -1 {
            ISMessages.show("Please select country")
        }
        AppUtility.shared.getCity(countryID: countryID, completion: { (success, error, arrayList) in
            if success == true {
                DispatchQueue.main.async {
                    if arrayList != nil {
                        self.showCityPicker(arrayList: arrayList!)
                    }
                }
            }
        })
    }*/
    
    func getCity()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        SVProgressHUD.show()
        
        let url = URL(string: K.URL.GET_CITY + "countryId=\(self.countryID)")
        let request = URLRequest(url: url!)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            SVProgressHUD.dismiss()
            // gunzip
            let decompressedData: Data
            if (data?.isGzipped)! {
                decompressedData = (try! data?.gunzipped())!
            } else {
                decompressedData = data!
            }
            
            let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
            
            if let arrData = dictionary as? NSArray {
                print(dictionary!)
                self.arrCityList = NSArray(array: arrData)
            }
            else {
                ISMessages.show("City not found, try again later")
            }
        });
        task.resume()
    }
    
    @IBAction func btnGetStartedTapped(_ sender: UIButton) {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        setGlobalCityWebservice()
    }
    
    //MARK: - Other Actions
    func initialSetup() {
        
        if UserDefaults.standard.object(forKey: "guest") != nil {
            if UserDefaults.standard.object(forKey: "guest") as! String == "1"
            {
                AppUtility.sharedAppDelegate.setMainNavigationController()
                return
            }
        }
        if UserDefaults.standard.object(forKey: "selectCityid") != nil {
            let loginVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            AppUtility.shared.loginNavController.pushViewController(loginVC, animated: false)
        }
    }
    
    func showCountryPicker() {
        RMPickerViewController.show(withTitle: nil, selectedIndex:0, array: AppUtility.shared.arrCountry.map({$0["CountryName"]}) as! [String], doneBlock: { (index, value) in
            DispatchQueue.main.async {
                if let value = AppUtility.shared.arrCountry[index]["CountryName"] as? String {
                    self.countryID = AppUtility.shared.arrCountry[index]["CountryId"] as? Int ?? -1
                    self.btnCountry.setTitle(value, for: .normal)
                    
                    UserDefaults.standard.set("\(self.countryID)", forKey: K.Key.Country)
                    UserDefaults.standard.synchronize()
                }
            }
        }, cancel: {
            
        })
    }
    
    func showCityPicker(arrayList: Array<Dictionary<String,Any>>) {
        if arrayList.count == 0 {
            return
        }
        RMPickerViewController.show(withTitle: nil, selectedIndex:0, array: arrayList.map({$0["CityName"]}) as! [String], doneBlock: { (index, value) in
            DispatchQueue.main.async {
                if let value = arrayList[index]["CityName"] as? String {
                    self.cityID = arrayList[index]["CityId"] as? Int ?? -1
                    self.btnCity.setTitle(value, for: .normal)
                    UserDefaults.standard.set(value, forKey: K.Key.City)
                    UserDefaults.standard.synchronize()
                }
            }
        }, cancel: {
            
        })
    }
    
    //MARK: - UITextField Delegate
    
    
    //MARK: - Webservice
    func setGlobalCityWebservice() {
        if cityID == -1 {
            ISMessages.show("Please select city")
            return
        }
        SVProgressHUD.show()
        
        let para = ["cityId" : cityID] as? Dictionary<String,Any>
        
        let url = URL(string: K.URL.SET_GLOBAL_CITY)
        let request = NSMutableURLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: para as Any, options: .prettyPrinted)
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request as URLRequest) {(data, response, error) in
            
            DispatchQueue.main.async {
                // gunzip
                let decompressedData: Data
                if (data?.isGzipped)! {
                    decompressedData = (try! data?.gunzipped())!
                } else {
                    decompressedData = data!
                }
                // DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
                if dictionary != nil {
                    print(dictionary!)
                }
                
                if self.cityID == -1
                {
                    UserDefaults.standard.set(nil, forKey: "selectcountryid")
                    UserDefaults.standard.set(nil, forKey: "selectCityid")
                    UserDefaults.standard.synchronize()
                }
                else
                {
                    UserDefaults.standard.set("\(self.countryID)", forKey: "selectcountryid")
                    UserDefaults.standard.set("\(self.cityID)", forKey: "selectCityid")
                    UserDefaults.standard.synchronize()
                }
                
                let loginVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                AppUtility.shared.loginNavController.pushViewController(loginVC, animated: true)
                
                SVProgressHUD.dismiss()
            }
            
        }
        
        task.resume()
    }
}
