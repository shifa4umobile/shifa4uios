//
//  FollowUsonSocialMediaVC.swift
//  AppDesign
//
//  Created by sachin on 16/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class FollowUsonSocialMediaVC: BaseViewController
{
    @IBOutlet weak var viewfacebook: UIView!
    @IBOutlet weak var viewtwitter: UIView!
    @IBOutlet weak var viewgoogle: UIView!
    @IBOutlet weak var viewlinkedin: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Add menu button
        addSlideMenuButton()
        addCartButton()

        Common.SetShadowwithCorneronView(view: viewfacebook, cornerradius: 10.0, color: UIColor.init(red: 210/255, green: 211/255, blue: 212/255, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: viewtwitter, cornerradius: 10.0, color: UIColor.init(red: 210/255, green: 211/255, blue: 212/255, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: viewgoogle, cornerradius: 10.0, color: UIColor.init(red: 210/255, green: 211/255, blue: 212/255, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: viewlinkedin, cornerradius: 10.0, color: UIColor.init(red: 210/255, green: 211/255, blue: 212/255, alpha: 1.0))

    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnfacebook(_ sender: Any)
    {
        guard let url = URL(string: "https://www.facebook.com/Shifa4Uonline/") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func btntwitter(_ sender: Any)
    {
        guard let url = URL(string: "https://twitter.com/hashtag/shifa4u") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    
    }
    
    @IBAction func btnlinkedin(_ sender: Any)
    {
        guard let url = URL(string: "https://www.linkedin.com/company/shifa4u") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func btngoogle(_ sender: Any)
    {
        guard let url = URL(string: "https://plus.google.com/+Shifa4U") else {
            return //be safe
            }
    
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    override func viewDidLayoutSubviews()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                removeView()
                addCartButton()
            }
            else
            {
                removeView()
                
                addCartButton()
            }
            
        } else {
            print("Portrait")
            //Image Height
            removeView()
            addCartButton()
        }
    }

}
