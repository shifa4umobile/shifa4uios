//
//  MyAccountVC.swift
//  AppDesign
//
//  Created by sachin on 10/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class MyAccountVC: BaseViewController {

    @IBOutlet weak var viewMyAccount: UIView!
    @IBOutlet weak var viewLabResult: UIView!
    @IBOutlet weak var viewMyAppointments: UIView!
    @IBOutlet weak var viewYourOrder: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commonData()
        //Add menu button
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addSlideMenuButton()
        addCartButton()
    }
    
    override func viewDidLayoutSubviews()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                removeView()
                addCartButton()
            }
            else
            {
                removeView()
                
                addCartButton()
            }
            
        } else {
            print("Portrait")
            //Image Height
            removeView()
            addCartButton()
        }
    }

    func commonData()  {
        Common.SetShadowwithCorneronView(view: viewMyAccount, cornerradius: 10.0, color: UIColor.init(red: 210/255, green: 211/255, blue: 212/255, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: viewLabResult, cornerradius: 10.0, color: UIColor.init(red: 210/255, green: 211/255, blue: 212/255, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: viewYourOrder, cornerradius: 10.0, color: UIColor.init(red: 210/255, green: 211/255, blue: 212/255, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: viewMyAppointments, cornerradius: 10.0, color: UIColor.init(red: 210/255, green: 211/255, blue: 212/255, alpha: 1.0))
        
    }
    

    @IBAction func btnMyProfileTapped(_ sender: UIButton) {
        let profileVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        AppUtility.shared.mainNavController.pushViewController(profileVC, animated: true)
    }

    @IBAction func btnLabTapped(_ sender: UIButton) {
        let labVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "MyResultsVC") as! MyResultsVC
        AppUtility.shared.mainNavController.pushViewController(labVC, animated: true)
    }

    @IBAction func btnAppointmentTapped(_ sender: UIButton) {
       let appointmentVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "MyAppointmentsVC") as! MyAppointmentsVC
     AppUtility.shared.mainNavController.pushViewController(appointmentVC, animated: true)
    }

    @IBAction func btnOrdersTapped(_ sender: UIButton)
    {
        let myOrderVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderVC
        AppUtility.shared.mainNavController.pushViewController(myOrderVC, animated: true)
        
    }

}
