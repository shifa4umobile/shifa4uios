//
//  AppDelegate.swift
//  AppDesign
//
//  Created by sachin on 07/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

//----------------------
import Firebase
import FirebasePerformance
import Fabric
import Crashlytics
//-----------------------


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , MobileRTCAuthDelegate {
    
    
    
    var rtc_appkey      = "ZTDjJRcjLoLmygRiZy4gXVnijETxlctkSrkB";
    var rtc_appsecret   = "7UoIcAUKcbzhiuNiBgvfpTVy3nGzWpUHE4cw";
    var rtc_domain      = "zoom.us";
    
    
    
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//        if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
//            statusbar.backgroundColor = UIColor.init(red: 8/255, green: 29/255, blue: 106/255, alpha: 1.0)
//        }
        
        
       
        //-----------------------
        FirebaseApp.configure()
        Fabric.sharedSDK().debug = true
        Fabric.with([Crashlytics.self])

        //-----------------------
        
        
        if User.loggedInUser() == nil {
            setLoginNavigationController()
        }
        else {
            setMainNavigationController()
        }
        IQKeyboardManager.shared.enable = true
        sdkAuth()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    //MARK: - Set Main Navigation Controller for Logged In User
    func setMainNavigationController() {
        AppUtility.shared.mainNavController = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "MainNavigationController") as? MainNavigationController
        window?.rootViewController = AppUtility.shared.mainNavController
    }

    func setLoginNavigationController() {
        AppUtility.shared.loginNavController = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "LoginNavigationController") as? LoginNavigationController
        window?.rootViewController = AppUtility.shared.loginNavController
    }
    
    func sdkAuth(){
        MobileRTC.shared().setMobileRTCDomain(rtc_domain);
        
        let service = MobileRTC.shared().getAuthService();
        if (service != nil) {
            service?.delegate = self;
            
            service?.clientKey = rtc_appkey;
            service?.clientSecret = rtc_appsecret;
            
            service?.sdkAuth()
        }
    }
    
    func onMobileRTCAuthReturn(_ returnValue: MobileRTCAuthError){
        print("onMobileRTCAuthReturn....  \(returnValue)");
    }
}

