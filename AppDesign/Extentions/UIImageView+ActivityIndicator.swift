
//
//  UIImageView+ActivityIndicator.swift
//  WebClient-Swift
//
//  Created on 19/11/16.
//
//

import Foundation
import Alamofire
import AlamofireImage
import AVFoundation

var TAG_ACTIVITY_INDICATOR = 156456

public enum ActivityIndicatorViewStyle : Int {
    
    case whiteLarge
    
    case white
    
    case gray
    
    case none
}

private let cacheDirectoryPath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]

extension UIImageView {
    
    func imageFromCache(strURL : String?) -> UIImage? {
        if strURL != nil {
            let path = cacheDirectoryPath + "/" + (strURL?.lastPathComponent)!
            if FileManager.default.fileExists(atPath: path) {
                return UIImage(contentsOfFile: path)
            }
        }
        return nil
    }
    
    func cacheImage(strURL : String, image : UIImage?) -> Void {
        if image != nil {
            let data : NSData = UIImageJPEGRepresentation(image!, 1)! as NSData
            let path = cacheDirectoryPath + "/" + (strURL.lastPathComponent)
            data.write(toFile: path, atomically: true)            
        }
    }
    
    func setImageWithURL(strUrl: String?, placeHolderImage: UIImage? = nil, activityIndicatorViewStyle: ActivityIndicatorViewStyle? = .gray) {
        setImageWithURL(strUrl, placeHolderImage: placeHolderImage, activityIndicatorViewStyle: activityIndicatorViewStyle, completionBlock: nil)
    }
  
    func setImageWithURL(_ strUrl: String?, placeHolderImage: UIImage?, activityIndicatorViewStyle: ActivityIndicatorViewStyle?, completionBlock:((_ image: UIImage?, _ error: Error?) -> Void)?) {
        self.image = placeHolderImage
       
        if let image = imageFromCache(strURL: strUrl) {
            self.image = image;
            completionBlock?(image, nil)
        }else {
            if strUrl != nil {
                if activityIndicatorViewStyle != .none {
                    self.addActivityIndicator(activityStyle: activityIndicatorViewStyle!)
                }
                
                Alamofire.request(strUrl!).responseImage { response in
                    if let image = response.result.value {
                        self.image = image;
                        self.cacheImage(strURL: strUrl!, image: image)
                        completionBlock?(image, nil)
                    }
                    else {
                        self.image = placeHolderImage
                        completionBlock?(nil, response.result.error)
                    }
                    self.removeActivityIndicator()
                }
            }
        }
    }

    func getVideoThumb(_ strUrl: String?, placeHolderImage: UIImage?, activityIndicatorViewStyle: ActivityIndicatorViewStyle?, completionBlock: ((_ image: UIImage?, _ error: Error?) -> Void)?) {
        self.image = placeHolderImage

        if let image = self.imageFromCache(strURL: strUrl) {
            self.image = image
            completionBlock?(image, nil)
        }else {
            if strUrl != nil {
                if activityIndicatorViewStyle != .none {
                    self.addActivityIndicator(activityStyle: activityIndicatorViewStyle!)
                }
                DispatchQueue.global().async {
                    
                    let asset = AVAsset(url: URL(string: strUrl!)!)
                    let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
                    assetImgGenerate.appliesPreferredTrackTransform = true
                    let time = CMTimeMake(1, 2)
                    let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
                    if img != nil {
                        let frameImg  = UIImage(cgImage: img!)
                        DispatchQueue.main.async(execute: {
                            self.image = frameImg
                            self.cacheImage(strURL: strUrl!, image: frameImg)
                            completionBlock?(frameImg, nil)
                        })
                    }
                    else {
                        completionBlock?(placeHolderImage, nil)
                    }
                }
            }

        }
    }


    func addActivityIndicator(activityStyle: ActivityIndicatorViewStyle) {
        var activityIndicator = self.viewWithTag(TAG_ACTIVITY_INDICATOR) as? UIActivityIndicatorView
        if activityIndicator == nil {
            var indicatiorStyle :  UIActivityIndicatorViewStyle = .white
            switch activityStyle {
            case .white:
                indicatiorStyle = .white
                break
            case .whiteLarge:
                indicatiorStyle = .whiteLarge
                break
            case .gray:
                indicatiorStyle = .gray
                break
            default:
                indicatiorStyle = .white
                
            }
            activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: indicatiorStyle)
            activityIndicator!.center = CGPoint(x: CGFloat(self.frame.size.width / 2), y: CGFloat(self.frame.size.height / 2))
            activityIndicator!.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin, .flexibleRightMargin, .flexibleBottomMargin]
            activityIndicator!.hidesWhenStopped = true
            activityIndicator!.tag = TAG_ACTIVITY_INDICATOR
            self.addSubview(activityIndicator!)
        }
        activityIndicator?.startAnimating()
    }
    
    func removeActivityIndicator() {
        let activityIndicator = self.viewWithTag(TAG_ACTIVITY_INDICATOR) as? UIActivityIndicatorView
        if activityIndicator != nil{
            activityIndicator!.removeFromSuperview()
        }
    }
    
}

