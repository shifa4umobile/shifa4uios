//
//  MenuViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    /**
    *  Array to display menu options
    */
    @IBOutlet weak var City: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet var tblMenuOptions : UITableView!
    
    /**
    *  Transparent button to hide menu
    */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
    *  Array containing menu options
    */
    var arrayMenuOptions = [Dictionary<String,String>]()
    
    /**
    *  Menu button which was tapped to display the menu
    */
    var btnMenu : UIButton!
    
    /**
    *  Delegate of the MenuVC
    */
    var delegate : SlideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblMenuOptions.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnDeposite(_ sender: Any)
    {
        let btn = UIButton(type: UIButtonType.custom)
        btn.tag = 14
        self.onCloseMenuClick(btn)
    }
    
    @IBAction func btnWithdrawal(_ sender: Any)
    {
        let btn = UIButton(type: UIButtonType.custom)
        btn.tag = 15
        self.onCloseMenuClick(btn)
    }
    
    @IBAction func btnLogout(_ sender: Any)
    {
        let btn = UIButton(type: UIButtonType.custom)
        btn.tag = arrayMenuOptions.count
        self.onCloseMenuClick(btn)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if User.loggedInUser() == nil {
            updateArrayMenuOptions_beforelogin()
            lblUserName.text = "Guest"
            lblEmail.text = ""
            City.text = ""
        }else{
            updateArrayMenuOptions()
            let user = User.loggedInUser()
            lblUserName.text = user!.firstName + " " + user!.lastName
            lblEmail.text = user?.loginName
            City.text = user?.city
        }
    }
    
    func updateArrayMenuOptions(){
        arrayMenuOptions.append(["title":"Home", "icon":"home"])
        arrayMenuOptions.append(["title":"My Account", "icon":"myaccount"])
        arrayMenuOptions.append(["title":"Change City", "icon":"changecity"])
        arrayMenuOptions.append(["title":"Order Medicines", "icon":"online-pharmacy"])
        arrayMenuOptions.append(["title":"Latest Blog", "icon":"latestblog"])
        arrayMenuOptions.append(["title":"Live Chat", "icon":"chat"])
        arrayMenuOptions.append(["title":"Follow Us on Social Media", "icon":"social"])
        arrayMenuOptions.append(["title":"Upload Prescription", "icon":"upload-prescription"])
        arrayMenuOptions.append(["title":"Contact Us", "icon":"contactus"])
        arrayMenuOptions.append(["title":"Privacy Policy", "icon":"privacypolicy"])
        arrayMenuOptions.append(["title":"Terms and Conditions", "icon":"termsAndService"])
        arrayMenuOptions.append(["title": User.loggedInUser() == nil ? "Login" : "Logout", "icon":"logout"])
        tblMenuOptions.reloadData()
    }
    func updateArrayMenuOptions_beforelogin(){
        arrayMenuOptions.append(["title":"Home", "icon":"home"])
        arrayMenuOptions.append(["title":"Change City", "icon":"changecity"])
        arrayMenuOptions.append(["title":"Order Medicines", "icon":"online-pharmacy"])
        arrayMenuOptions.append(["title":"Latest Blog", "icon":"latestblog"])
        arrayMenuOptions.append(["title":"Live Chat", "icon":"chat"])
        arrayMenuOptions.append(["title":"Follow Us on Social Media", "icon":"social"])
        arrayMenuOptions.append(["title":"Upload Prescription", "icon":"upload-prescription"])
        arrayMenuOptions.append(["title":"Contact Us", "icon":"contactus"])
        arrayMenuOptions.append(["title":"Privacy Policy", "icon":"privacypolicy"])
        arrayMenuOptions.append(["title":"Terms and Conditions", "icon":"termsAndService"])
        arrayMenuOptions.append(["title": User.loggedInUser() == nil ? "Login" : "Logout", "icon":"logout"])
        tblMenuOptions.reloadData()
    }
    
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        btnMenu.tag = 0
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
        })
    }
                                                                                                                                                      
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellmenu")!
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.layoutMargins = UIEdgeInsets.zero
            cell.preservesSuperviewLayoutMargins = false
            cell.backgroundColor = UIColor.clear
            
            let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel
            let imgicon : UIImageView = cell.contentView.viewWithTag(102) as! UIImageView
            
            
            lblTitle.text = arrayMenuOptions[indexPath.row]["title"]!
            imgicon.image = UIImage (named: arrayMenuOptions[indexPath.row]["icon"]!)
            return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
            let btn = UIButton(type: UIButtonType.custom)
            btn.tag = indexPath.row
            self.onCloseMenuClick(btn)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
            return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
