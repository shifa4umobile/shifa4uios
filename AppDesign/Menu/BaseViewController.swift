
//
//  BaseViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, SlideMenuDelegate {
    
    let btnShowMenu = UIButton()
    let viewmaindata = UIView()
    var count:Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        let topViewController : UIViewController = self.navigationController!.topViewController!
        print("View Controller is : \(topViewController) \n", terminator: "")
        if User.loggedInUser() == nil {
            switch(index){
            case 0:
                self.openViewControllerBasedOnIdentifier("HomeVC")
                break
            case 1:
                self.openViewControllerBasedOnIdentifier("ChangeCityVC")
                break
            case 2:
                self.openViewControllerBasedOnIdentifier("OnlineMedicinesVC")
                break
            case 3:
                self.openViewControllerBasedOnIdentifier("LatestBLogVC")
                break
            case 4:
                self.openViewControllerBasedOnIdentifier("LiveCHatVC")
                break
            case 5:
                self.openViewControllerBasedOnIdentifier("FollowUsonSocialMediaVC")
                break
            case 6:
                self.openViewControllerBasedOnIdentifier("UploadPrescriptionVC")
                break
            case 7:
                self.openViewControllerBasedOnIdentifier("ContactUsVC")
                break
            case 8:
                self.openViewControllerBasedOnIdentifier("PrivacyPolicyVC")
                break
            case 9:
                self.openViewControllerBasedOnIdentifier("TermsAndConditoinVC")
                break
            case 10:
                self.openViewControllerBasedOnIdentifier("LoginViewController")
    
                break
            default:
                print("default\n", terminator: "")
            }
            
        }else{
            switch(index){
            case 0:
                print("Home\n", terminator: "")
                
                self.openViewControllerBasedOnIdentifier("HomeVC")
                
                break
            case 1:
                print("MyAccountVC\n", terminator: "")
                
                self.openViewControllerBasedOnIdentifier("MyAccountVC")
                
                break
            case 2:
                print("ChangeCityVC\n", terminator: "")
                
                self.openViewControllerBasedOnIdentifier("ChangeCityVC")
                
                break
            case 3:
                print("PharmacyVC\n", terminator: "")
                
                self.openViewControllerBasedOnIdentifier("OnlineMedicinesVC")
                
                break
            case 4:
                print("LatestBLogVC\n", terminator: "")
                
                self.openViewControllerBasedOnIdentifier("LatestBLogVC")
                
                break
            case 5:
                print("LiveCHatVC\n", terminator: "")
                
                self.openViewControllerBasedOnIdentifier("LiveCHatVC")
                
                break
            case 6:
                print("FollowUsonSocialMediaVC\n", terminator: "")
                
                self.openViewControllerBasedOnIdentifier("FollowUsonSocialMediaVC")
                
                break
            case 7:
                print("UploadPrescriptionVC\n", terminator: "")
                
                self.openViewControllerBasedOnIdentifier("UploadPrescriptionVC")
                
                break
            case 8:
                print("ContactUsVC\n", terminator: "")
                
                self.openViewControllerBasedOnIdentifier("ContactUsVC")
                
                break
            case 9:
                print("PrivacyPolicyVC\n", terminator: "")
                
                self.openViewControllerBasedOnIdentifier("PrivacyPolicyVC")
                
                break
            case 10:
                print("TermsAndConditoinVC\n", terminator: "")
                
                self.openViewControllerBasedOnIdentifier("TermsAndConditoinVC")
                
                break
            case 11:
                print("LoginVC\n", terminator: "")
                
                
                let actionSheetController: UIAlertController = UIAlertController(title: "Shifa4U", message: "Are you sure you want to LogOut", preferredStyle: .alert)
                let Yes: UIAlertAction = UIAlertAction(title: "YES", style: .default) { action -> Void in
                    let domain = Bundle.main.bundleIdentifier!
                    UserDefaults.standard.removePersistentDomain(forName: domain)
                    UserDefaults.standard.synchronize()
                    print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
                    //self.openViewControllerBasedOnIdentifier("HomeVC")
                    
                    if User.loggedInUser() != nil {
                        User.delete()
                    }
                    AppUtility.sharedAppDelegate.setLoginNavigationController()
                }
                let cancelAction: UIAlertAction = UIAlertAction(title: "NO", style: .cancel) { action -> Void in
                    
                }
                actionSheetController.addAction(Yes)
                actionSheetController.addAction(cancelAction)
                self.present(actionSheetController, animated: true, completion: nil)
                break
            default:
                print("default\n", terminator: "")
            }
        }
        
    }
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
        
        let topViewController : UIViewController = self.navigationController!.topViewController!
        
        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
            print("Same VC")
        } else {
            self.navigationController!.pushViewController(destViewController, animated: true)
        }
    }
    
    func addSlideMenuButton(){
        let height = UIApplication.shared.statusBarFrame.height

        btnShowMenu.setImage(#imageLiteral(resourceName: "menu"), for: UIControlState.normal)
        btnShowMenu.frame = CGRect(x: 0, y: height+8, width: 44, height: 44)
        btnShowMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        self.view.addSubview(btnShowMenu)
        
    }

    func addCartButton()
    {
        let height = UIApplication.shared.statusBarFrame.height
        viewmaindata.frame = CGRect(x: self.view.frame.size.width - 44, y: height + 8, width: 44, height: 44)
        viewmaindata.backgroundColor = UIColor.clear
        
        
        let btncart = UIButton()
        btncart.setImage(#imageLiteral(resourceName: "shopping-cart"), for: UIControlState.normal)
        btncart.frame = CGRect(x: 2, y: 7, width: 35, height: 35)
        btncart.addTarget(self, action: #selector(BaseViewController.onCartbuttonClicked(_:)), for: UIControlEvents.touchUpInside)
        viewmaindata.addSubview(btncart)
        
        //=== count item
        var totalItem:Int = 0
        //LabTest
        if UserDefaults.standard.object(forKey: "arrlabtestcart") != nil
        {
           totalItem = totalItem + (UserDefaults.standard.value(forKey: "arrlabtestcart") as! NSArray).count

        }
        
        //Medical Package
        if UserDefaults.standard.object(forKey: "arrmedicalpackagecart") != nil
        {
            totalItem = totalItem + (UserDefaults.standard.value(forKey: "arrmedicalpackagecart") as! NSArray).count
        }
        
        //Radiology
        if UserDefaults.standard.object(forKey: "arrradiologycart") != nil
        {
            totalItem = totalItem + (UserDefaults.standard.value(forKey: "arrradiologycart") as! NSArray).count
        }
        
        //HomeCare
        if UserDefaults.standard.object(forKey: "arrhomecarecart") != nil
        {
            totalItem = totalItem + (UserDefaults.standard.value(forKey: "arrhomecarecart") as! NSArray).count
        }
        
        
        //===
        let lblcount = UILabel()
        lblcount.backgroundColor = UIColor.red
        lblcount.font = UIFont.systemFont(ofSize: 12.0, weight: .semibold)
        lblcount.textColor = UIColor.white
        lblcount.frame = CGRect(x: 15, y: 0, width: 20, height: 15)
        lblcount.text = NSString(format: "%d", totalItem) as String
        lblcount.textAlignment = NSTextAlignment.center
        lblcount.numberOfLines = 1;
        lblcount.adjustsFontSizeToFitWidth = true;
       // lblcount.sizeToFit()
        Common.setCornerOnView(view: lblcount, cornerradius: lblcount.frame.size.height/2.0)
        viewmaindata.addSubview(lblcount)
        self.view.addSubview(viewmaindata)
    }
    
    func removeView()
    {
        viewmaindata.removeFromSuperview()
    }
    @objc func onCartbuttonClicked(_ sender : UIButton?)
{
        if (UserDefaults.standard.object(forKey: "arrlabtestcart") == nil) &&
           (UserDefaults.standard.object(forKey: "arrhomecarecart") == nil) &&
           (UserDefaults.standard.object(forKey: "arrradiologycart") == nil) &&
           (UserDefaults.standard.object(forKey: "arrmedicalpackagecart") == nil)
        {
            ISMessages.show("No item exist in your order.")

        }
        else
        {
            var totalItem:Int = 0
            //LabTest
            if UserDefaults.standard.object(forKey: "arrlabtestcart") != nil
            {
                totalItem = totalItem + (UserDefaults.standard.value(forKey: "arrlabtestcart") as! NSArray).count
                
            }
            
            //Medical Package
            if UserDefaults.standard.object(forKey: "arrmedicalpackagecart") != nil
            {
                totalItem = totalItem + (UserDefaults.standard.value(forKey: "arrmedicalpackagecart") as! NSArray).count
            }  
            
            //Radiology
            if UserDefaults.standard.object(forKey: "arrradiologycart") != nil
            {
                totalItem = totalItem + (UserDefaults.standard.value(forKey: "arrradiologycart") as! NSArray).count
            }
            
            //HomeCare
            if UserDefaults.standard.object(forKey: "arrhomecarecart") != nil
            {
                totalItem = totalItem + (UserDefaults.standard.value(forKey: "arrhomecarecart") as! NSArray).count
            }
            if  totalItem == 0
            {
                 ISMessages.show("No item exist in your order.")
            }
            else
            {
                let viewcontrollerVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "ViewController") as! ViewController
                AppUtility.shared.mainNavController.pushViewController(viewcontrollerVC, animated: true)
            }
        }


//         if (UserDefaults.standard.value(forKey: "arrhomecarecart") as! NSArray).count + (UserDefaults.standard.value(forKey: "arrradiologycart") as! NSArray).count + (UserDefaults.standard.value(forKey: "arrmedicalpackagecart") as! NSArray).count +
//            (UserDefaults.standard.value(forKey: "arrlabtestcart") as! NSArray).count != 0
//        {
//        let viewcontrollerVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "ViewController") as! ViewController
//        AppUtility.shared.mainNavController.pushViewController(viewcontrollerVC, animated: true)
//        }
//        else
//        {
//            ISMessages.show("Cart is Empty")
//        }
}


    
    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)
        
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
       
        return defaultMenuImage;
    }
    
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        
        let menuVC : MenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
            }, completion:nil)
    }
}
