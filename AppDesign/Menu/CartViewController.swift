//
//  CartViewController.swift
//  AppDesign
//
//  Created by Mahavir Makwana on 20/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class CartViewController: BaseViewController
{

    @IBOutlet weak var tblList: UITableView!
    var storedOffsets = [Int: CGFloat]()
    var arrdata = NSMutableArray()
    let chooseArticleDropDown = DropDown()

    @IBOutlet weak var viewfooter: UIView!
    @IBOutlet weak var txtcouponcode: UITextField!
    @IBOutlet weak var btnuse: UIButton!
    @IBOutlet weak var lblItems: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblmainPrice: UILabel!
    @IBOutlet weak var lblExtraOff: UILabel!
    @IBOutlet weak var lbldiscountPrice: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var viewdialogue: UIView!
    @IBOutlet weak var btncontinueOrder: UIButton!
    @IBOutlet weak var btncheckout: UIButton!
    @IBOutlet weak var btndelete: UIButton!


    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown
        ]
    }()

    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()

        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }


    override func viewDidLoad()
    {
        super.viewDidLoad()


        Common.SetShadowwithCorneronView(view: btncontinueOrder, cornerradius: 10.0, color: UIColor.clear)
        Common.SetShadowwithCorneronView(view: btncheckout, cornerradius: 10.0, color: UIColor.clear)
        Common.SetShadowwithCorneronView(view: btnuse, cornerradius: 10.0, color: UIColor.clear)
        Common.SetShadowwithCorneronView(view: btnuse, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))

        Common.setCornerOnView(view: viewdialogue, cornerradius: 10.0)



        //Add menu button
        addSlideMenuButton()

        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }

    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

   
    @IBAction func btnlab(_ sender: Any, forEvent event: UIEvent)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tblList)
        let indexPath = self.tblList.indexPathForRow(at: buttonPosition)

        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        chooseArticleDropDown.dataSource = [
            "Lab1",
            "Lab2",
            "Lab3",
            "Lab4",
            "Lab5"
        ]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            lblNotificationText?.setTitle(item, for: .normal)
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            //            if items.isEmpty {
            //                lblNotificationText?.setTitle("", for: .normal)
            //            }
        }

        chooseArticleDropDown.show()
    }

    @IBAction func btncollectionType(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tblList)
        let indexPath = self.tblList.indexPathForRow(at: buttonPosition)

        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        chooseArticleDropDown.dataSource = [
            "Coll Type1",
            "Coll Type2",
            "Coll Type3",
            "Coll Type4",
            "Coll Type5"
        ]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            lblNotificationText?.setTitle(item, for: .normal)
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            if items.isEmpty {
                lblNotificationText?.setTitle("", for: .normal)
            }
        }

        chooseArticleDropDown.show()
    }

    @IBAction func btnprovider(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tblList)
        let indexPath = self.tblList.indexPathForRow(at: buttonPosition)

        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        chooseArticleDropDown.dataSource = [
            "provider1",
            "provider2",
            "provider3",
            "provider4",
            "provider5"
        ]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            lblNotificationText?.setTitle(item, for: .normal)
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            if items.isEmpty {
                lblNotificationText?.setTitle("", for: .normal)
            }
        }

        chooseArticleDropDown.show()
    }

    @IBAction func btntype(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tblList)
        let indexPath = self.tblList.indexPathForRow(at: buttonPosition)

        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        chooseArticleDropDown.dataSource = [
            "Type1",
            "Type2",
            "Type3",
            "Type4",
            "Type5"
        ]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            lblNotificationText?.setTitle(item, for: .normal)
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            if items.isEmpty {
                lblNotificationText?.setTitle("", for: .normal)
            }
        }

        chooseArticleDropDown.show()
    }

    @IBAction func btnlocation(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tblList)
        let indexPath = self.tblList.indexPathForRow(at: buttonPosition)

        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        chooseArticleDropDown.dataSource = [
            "Location1",
            "Location2",
            "Location3",
            "Location4",
            "Location5"
        ]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            lblNotificationText?.setTitle(item, for: .normal)
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            if items.isEmpty {
                lblNotificationText?.setTitle("", for: .normal)
            }
        }

        chooseArticleDropDown.show()
    }

    @IBAction func btndeleteaction(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tblList)
        let indexPath = self.tblList.indexPathForRow(at: buttonPosition)
        let cell = self.tblList.cellForRow(at: indexPath!) as? CurrentOrderCell

        let index = cell?.tbldetail.tag
        //let indexPath1 = cell?.tbldetail.indexPathForRow(at: buttonPosition)
        let arrdatadetail = arrdata[index!] as? NSDictionary
        var arr = arrdatadetail!["detail"] as! NSArray
        let arrdatalist = NSMutableArray()
        arrdatalist.addObjects(from: arr as! [Any])
        arrdatalist.removeObject(at: (sender as AnyObject).tag)
        arr = arrdatalist
        arrdatadetail?.setValue(arr, forKey: "detail")
        arrdata.replaceObject(at: index!, with: arrdatadetail!)
        tblList.reloadData()

        cell?.tbldetail.reloadData()
    
        
    }
}

//MARK:- Tableview Delegate Method
extension CartViewController: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblList
        {
            return AppUtility.shared.getTotalTypesOfProduct()
        }
        else
        {
            if AppUtility.shared.arrCartLab.count > 0 {
                return AppUtility.shared.arrCartLab.count
            }
            else if AppUtility.shared.arrCartLab.count > 0 {
                return AppUtility.shared.arrCartDiet.count
            }
            else {
                return 0
            }
            //            if let dict = AppUtility.shared.arrCart[tableView.tag] as? Dictionary<String,Any> {
            //                let arr = dict["products"] as! NSArray
            //                return arr.count
            //            }
            //            return 0
            //            let dict = arrdata[tableView.tag] as? NSDictionary
            //            let arr = dict!["detail"] as! NSArray
            //            return arr.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblList
        {
            var strcellname = String()
            var strcellTitle: String = ""
            var productCount = 0

            if AppUtility.shared.arrCartLab.count > 0 {
                strcellTitle = K.Key.CartLab
                productCount = AppUtility.shared.arrCartLab.count
                strcellname = "CurrentOrderCell"
            }
            else if AppUtility.shared.arrCartDiet.count > 0 {
                strcellTitle = K.Key.CartDiet
                productCount = AppUtility.shared.arrCartDiet.count
                strcellname = "CurrentOrderCell"
            }
            else {
                strcellname = "CurrentOrderCell"
                strcellTitle = "Other"
                productCount = 0
                strcellname = "CurrentOrderCell"
            }
            //                if let arrLab = AppUtility.shared.dictCartData[K.Key.CartLab] as? Array<LabDetails> {
            //                    strcellname = "CurrentOrderCellRadiology"
            //                    strcellTitle = K.Key.CartLab
            //                    productCount = arrLab.count
            //                }
            //                else if let arrDiet = AppUtility.shared.dictCartData[K.Key.CartDiet] as? Array<HomeCare> {
            //                    strcellname = "CurrentOrderCellRadiology"
            //                    strcellTitle = K.Key.CartDiet
            //                    productCount = arrDiet.count
            //                }
            //                else {
            //                    strcellname = "CurrentOrderCell"
            //                    strcellTitle = "Other"
            //                    productCount = 0
            //                }

            //                let dict = arrdata[indexPath.row] as? NSDictionary
            //                var strcellname = String()
            //                if dict!["type"] as! String == "3"
            //                {
            //                    strcellname = "CurrentOrderCellRadiology"
            //                }
            //                else
            //                {
            //                    strcellname = "CurrentOrderCell"
            //                }
            let cell = tableView.dequeueReusableCell(withIdentifier: strcellname, for: indexPath) as! CurrentOrderCell

            //let arr = dict!["detail"] as! NSArray
            // cell.lblprice.text = dict!["price"] as? String
            cell.lbltitle.text = strcellTitle
            Common.SetShadowwithCorneronView(view: cell.viewmain, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
            if strcellTitle  == "Radiology"
            {
                cell.tblheight.constant = CGFloat(productCount * 201)
            }
            else
            {
                cell.tblheight.constant = CGFloat(productCount * 127)
            }

            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath) as! DetailCell

            //                cell.viewLab.isHidden = false
            //                cell.viewCollectionType.isHidden = false
            var strcellPrice: String = ""

            if AppUtility.shared.arrCartLab.count >  0 {
                strcellPrice = "0.0"
                let product = AppUtility.shared.arrCartLab[indexPath.row]
                let selectedItem = product.arrLabs.filter({$0.isSelected == true}).first
                if selectedItem != nil {
                    strcellPrice = "\(selectedItem?.price))"
                }
            }
            else if AppUtility.shared.arrCartDiet.count >  0 {
                cell.viewLab.isHidden = true
                cell.viewCollectionType.isHidden = true

                strcellPrice = "0.0"
                let product = AppUtility.shared.arrCartDiet[indexPath.row]
                strcellPrice = "\(product.price)"
            }

            cell.backgroundColor = UIColor.green
            //                if let dict = AppUtility.shared.arrCart[tableView.tag] as? Dictionary<String,Any> {
            //                    if (dict["title"] as? String ?? "") == K.Key.CartLab {
            //                        strcellPrice = "0.0"
            //
            //                        if let arrLab = dict["products"] as? Array<LabDetails> {
            //                            if let labDetail = arrLab[indexPath.row] as? LabDetails {
            //                                cell.lbltitle.text = labDetail.name
            //                            }
            //                        }
            //                    }
            //                    else if (dict["title"] as? String ?? "") == K.Key.CartDiet {
            //                        strcellPrice = "0.0"
            //
            //                        cell.viewLab.isHidden = true
            //                        cell.viewCollectionType.isHidden = true
            //
            //                        if let arrLab = dict["products"] as? Array<HomeCare> {
            //                            if let homeCare = arrLab[indexPath.row] as? HomeCare {
            //                                cell.lbltitle.text = homeCare.name
            //                            }
            //                        }
            //
            //                    }
            //                }

            cell.viewLab.addTapGesture { (gesture) in

                if let dict = AppUtility.shared.arrCart[tableView.tag] as? Dictionary<String,Any> {
                    if (dict["title"] as? String ?? "") == K.Key.CartLab {
                        if let arrLab = dict["products"] as? Array<LabDetails> {
                            self.chooseArticleDropDown.dataSource = arrLab.map({$0.name})
                        }
                    }
                }
                self.chooseArticleDropDown.anchorView = cell.btnDropDown
                self.chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (cell.btnDropDown?.bounds.height)!)

                self.chooseArticleDropDown.selectionAction = { [weak self] (index, item) in

                    //                        self?.selectedLab = self?.labDeatil.arrLabs[index]
                    //                        lblNotificationText?.setTitle(item, for: .normal)
                    //                        self?.lblprice.text = "\(K.kCurrency) \(self?.selectedLab?.price ?? 0)/-"

                    self?.chooseArticleDropDown.hide()

                    cell.btnDropDown.setTitle(item, for: .normal)
                }

                self.chooseArticleDropDown.show()
            }

            //                else if let arrDiet = AppUtility.shared.dictCartData[K.Key.CartDiet] as? Array<HomeCare> {
            //                    if let diet = arrDiet[indexPath.row] as? HomeCare {
            //                        cell.lbltitle.text = diet.name
            //                    }
            //                }

            //                let dict = arrdata[tableView.tag] as? NSDictionary
            //                let arr = dict!["detail"] as! NSArray
            //
            //                cell.lbltitle.text = arr[indexPath.row] as? String

            cell.btncolectionType.tag = indexPath.row

            return cell
        }
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if tableView == tblList
        {
            guard let tableViewCell = cell as? CurrentOrderCell else { return }

            storedOffsets[indexPath.row] = tableViewCell.tableViewOffset
        }
        else
        {

        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == tblList
        {
            //            let dict = arrdata[indexPath.row] as? NSDictionary
            //            let arr = dict!["detail"] as! NSArray

            var productCount = 0
            var removeDropDownHeight = CGFloat(0)

            if AppUtility.shared.arrCartLab.count > 0 {
                removeDropDownHeight = 0
                productCount = AppUtility.shared.arrCartLab.count
            }
            else if AppUtility.shared.arrCartDiet.count > 0 {
                removeDropDownHeight = 40
                productCount = AppUtility.shared.arrCartDiet.count
            }
            //            if let dict = AppUtility.shared.arrCart[tableView.tag] as? Dictionary<String,Any> {
            //                if (dict["title"] as? String ?? "") == K.Key.CartLab {
            //                    removeDropDownHeight = 0
            //                    if let arrLab = dict["products"] as? Array<LabDetails> {
            //                        productCount = arrLab.count
            //                    }
            //                }
            //                else if (dict["title"] as? String ?? "") == K.Key.CartDiet {
            //                    removeDropDownHeight = 40
            //                    if let arrLab = dict["products"] as? Array<HomeCare> {
            //                        productCount = arrLab.count
            //                    }
            //                }
            //            }

            //            if let arrLab = AppUtility.shared.dictCartData[K.Key.CartLab] as? Array<LabDetails> {
            //                productCount = arrLab.count
            //            }
            //            else if let arrDiet = AppUtility.shared.dictCartData[K.Key.CartDiet] as? Array<HomeCare> {
            //                productCount = arrDiet.count
            //            }
            //            else {
            //                productCount = 0
            //            }

            if indexPath.row == 0
            {
                if productCount == 0
                {
                    return 0
                }
                return CGFloat((productCount * 127) + 100)
            }
            else if indexPath.row == 1
            {
                if productCount == 0
                {
                    return 0
                }
                var height = CGFloat((productCount * 127) + 100)
                height = height - removeDropDownHeight
                return height
            }
            else if indexPath.row == 2
            {
                if productCount == 0
                {
                    return 0
                }
                var height = CGFloat((productCount * 201) + 73)
                height = height - removeDropDownHeight
                return height
            }
            else
            {
                return 200 //UITableViewAutomaticDimension
            }
        }
        else
        {
            //            let dict = arrdata[tableView.tag] as? NSDictionary
            //            if dict!["title"] as? String  == "Radiology"

            var title = ""

            if AppUtility.shared.arrCartLab.count > 0 {
                title = K.Key.CartLab
            }
            else if AppUtility.shared.arrCartDiet.count > 0 {
                title = K.Key.CartDiet
            }

            //            if let dict = AppUtility.shared.arrCart[tableView.tag] as? Dictionary<String,Any> {
            //                if (dict["title"] as? String ?? "") == K.Key.CartLab {
            //                    title = K.Key.CartLab
            //                }
            //                else if (dict["title"] as? String ?? "") == K.Key.CartDiet {
            //                    title = K.Key.CartDiet
            //                }
            //            }

            //            if let arrLab = AppUtility.shared.dictCartData[K.Key.CartLab] as? Array<LabDetails> {
            //                title = K.Key.CartLab
            //            }
            //            else if let arrDiet = AppUtility.shared.dictCartData[K.Key.CartDiet] as? Array<HomeCare> {
            //                title = K.Key.CartDiet
            //            }
            //            else {
            //                title = ""
            //            }

            if title == "Radiology"
            {
                return 201
            }
            else{

                return 127
            }
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if tableView == tblList
        {
            guard let tableViewCell = cell as? CurrentOrderCell else { return }

            tableViewCell.settableViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.tableViewOffset = storedOffsets[indexPath.row] ?? 0
        }
        else
        {

        }
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {

        print("scrollViewWillBeginDragging")
    }
}

