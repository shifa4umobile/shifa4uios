//
//  LoginViewController.swift
//  AppDesign
//
//  Created by Mahavir Makwana on 31/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD
import Crashlytics

class LoginViewController: UIViewController {

    @IBOutlet weak var txtEmailUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSocialGoogle: UIButton!
    @IBOutlet weak var btnSocialFacebook: UIButton!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnGuestUser: UIButton!

    var is_comefromcartvc = "0"
    var iconClick = true
    
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        
//        let button = UIButton(type: .roundedRect)
//        button.frame = CGRect(x: 20, y: 100, width: 100, height: 30)
//        button.setTitle("Crash", for: [])
//        button.addTarget(self, action: #selector(self.crashButtonTapped(_:)), for: .touchUpInside)
//        view.addSubview(button)
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

//    @IBAction func crashButtonTapped(_ sender: AnyObject) {
//        Crashlytics.sharedInstance().crash()
//    }
    
    //MARK: - IBActions
    @IBAction func btnForgotPasswordTapped(_ sender: UIButton) {
        
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "ForgotPasswordViewController")
        let topViewController : UIViewController = self.navigationController!.topViewController!
        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
            print("Same VC")
        } else {
            self.navigationController!.pushViewController(destViewController, animated: true)
        }
        
//        let forgotPasswordVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
//        AppUtility.shared.loginNavController.pushViewController(forgotPasswordVC, animated: true)
    }

    @IBAction func EyeView(_ sender: Any) {
        if(iconClick == true) {
            txtPassword.isSecureTextEntry = false
        } else {
            txtPassword.isSecureTextEntry = true
        }
        iconClick = !iconClick
    }
    @IBAction func btnLoginTapped(_ sender: UIButton) {
        let email = isValidEmail(testStr: txtEmailUserName.text!)
        if txtEmailUserName.text?.isValid == false {
            view.endEditing(true)
            ISMessages.show(K.Message.enterUserName)
        }
        else if email == false {
            view.endEditing(true)
            ISMessages.show("Enter Valid Email")
        }
        else if txtPassword.text?.isValid == false {
            view.endEditing(true)
            ISMessages.show(K.Message.enterPassword)
        }
        else if (txtPassword.text?.length)! < 6 {
            view.endEditing(true)
            ISMessages.show(K.Message.enterminimumsixdigitpwd)
        }
        else {
            view.endEditing(true)
            self.loginWebservice()
        }
    }

    @IBAction func btnSocialLoginTapped(_ sender: UIButton) {

    }
    

    @IBAction func btnSignupTapped(_ sender: UIButton) {
        
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "RegisterViewController")
        let topViewController : UIViewController = self.navigationController!.topViewController!
        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
            print("Same VC")
        } else {
            self.navigationController!.pushViewController(destViewController, animated: true)
        }
        //let registerVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        //AppUtility.shared.loginNavController.pushViewController(registerVC, animated: true)
    }

    @IBAction func btnGuestUserTapped(_ sender: UIButton) {
        
        UserDefaults.standard.setValue("1", forKeyPath: "guest")
        UserDefaults.standard.synchronize()
        
        AppUtility.sharedAppDelegate.setMainNavigationController()
    }


    //MARK: - Other Actions
    func initialSetup() {

       // txtEmailUserName.text = "shahid.mrd@hotmail.com"
        //txtPassword.text = "shahid321"

        let attributedString = NSMutableAttributedString(string: "Don't have an account? Sign up", attributes: [
            .font: UIFont.systemFont(ofSize: 15),
            .foregroundColor: UIColor.white
            ])
        attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 15), range: NSRange(location: 23, length: 7))
        btnSignup.setAttributedTitle(attributedString, for: .normal)
    }


    //MARK: - UITextField Delegate


    //MARK: - Webservice
    func loginWebservice() {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        SVProgressHUD.show()

        let reqParam = ["grant_type" : "password",
                        "password" : txtPassword.text?.trimmed() ?? "",
                        "username" : txtEmailUserName.text?.trimmed() ?? "",
                        "client_id" : K.API_CLIENT_ID] as Dictionary<String, Any>

        _ = WebClient.requestWithUrl(url: K.URL.LOGIN, parameters: reqParam, method: "POST") { (responseObject, error) in
            SVProgressHUD.dismiss()
            if error == nil
            {
                //let dict = responseObject as! Dictionary<String, Any>
                print("Data: \(responseObject!)")

                //Store User Data In It's Model And
                //let user = User(dict: responseObject?[K.Key.Data] as! Dictionary)

                if let token = (responseObject as! Dictionary<String,Any>)["access_token"] as? String {
                    
                    if token.length != 0
                    {
                        let user = User(dict: nil)
                        user.accessToken = token
                        user.save()
                        print(token)
                        
                        UserDefaults.standard.remove("guest")
                        UserDefaults.standard.setValue(token, forKey: "token")
                        UserDefaults.standard.set(true, forKey: K.Key.IsLoggedInUser)
                        UserDefaults.standard.synchronize()
                        
                        if self.is_comefromcartvc == "1"
                        {
                            AppUtility.shared.mainNavController.popViewController(animated: true)
                        }
                        else
                        {
                            AppUtility.sharedAppDelegate.setMainNavigationController()
                        }
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Incorrect Credentials", message:"Your user name or password is incorrect", preferredStyle: .alert)
                        let later = UIAlertAction(title: "OK", style: .default, handler: nil)
                        
                        alert.addAction(later)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                else {
                    let alert = UIAlertController(title: "Incorrect Credentials", message:"Your user name or password is incorrect", preferredStyle: .alert)
                    let later = UIAlertAction(title: "OK", style: .default, handler: nil)
                    
                    alert.addAction(later)
                    self.present(alert, animated: true, completion: nil)
                }

            }
            else
            {
                let alert = UIAlertController(title: "Incorrect Credentials", message:"Your user name or password is incorrect", preferredStyle: .alert)
                let later = UIAlertAction(title: "OK", style: .default, handler: nil)
                
                alert.addAction(later)
                self.present(alert, animated: true, completion: nil)
            }
            
        }
    }

}
