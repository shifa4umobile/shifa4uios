//
//  Constant.swift

//
//  Created on 15/11/16.
//
//

import UIKit
import Foundation


//Staging
//public let BASE_URL = "http://www.33junaid.xyz/"
  public let BASE_URL = "https://api.shifa4u.com/"
//public let BASE_URL = "http://192.168.0.10:9091/"
 // public let BASE_URL = "http://www.shifa4u.net/"

struct K {
    
    static let ITUNE_APP_ID     = ""
    static let GOOGLE_API_KEY   = ""
    static let API_CLIENT_ID   = "aad4dc0739b64c529ab86c2126ed341c"


    static let kCurrency : String = "PKR"
    static let kCornerRadius : CGFloat = 5.0
    static let kCornerRadiusSmall : CGFloat = 3.0
    static let PopViewControllerAnimationDuration : CGFloat = 0.5

    struct URL {
        
        static let ITUNE_APP_URL       = "itms-apps://itunes.apple.com/app/id\(K.ITUNE_APP_ID)"

        static let UPDATE_DEVICE_TOKEN = BASE_URL + "update_token"
        static let LOGIN = BASE_URL + "token"
        static let REGISTRATION = BASE_URL + "api/account/v1/MobileRegistration"
        static let GET_MY_PROFILE = BASE_URL + "api/customeraccount/v1/customerprofile"
        static let FORGETPASSWORD = BASE_URL + "api/account/v1/ForgotPasswordMobile"
        static let EDITPROFILE = BASE_URL + "api/customeraccount/v1/updatecustomerprofile"
        
        static let GET_ALL_COUNTRY = BASE_URL + "api/common/v1/getallcountries"
        static let GET_CITY = BASE_URL + "api/common/v1/getcitiesforcountry?"
        static let GET_AREA = BASE_URL + "api/common/v1/getareasforcity?cityId="
        static let SET_GLOBAL_CITY = BASE_URL + "api/common/v1/setglobalCity"
        static let GET_HOMECARE_DATA = BASE_URL + "api/homecare/getpaginatablehomecareservices"
        static let uploadprescription = BASE_URL + "api/common/v1/uploadprescription"
        static let saveorder = BASE_URL + "api/cart/v1/saveorder"
        
        static let GetAllImages = BASE_URL + "api/common/v1/displayimagebyimageid"
        
        static let GetLabImagingsBySearchKeyWord = BASE_URL + "api/Imaging/v1/GetLabImagingsBySearchKeyWord"
        static let GetAllLabImagings = BASE_URL + "api/Imaging/v1/paginateradilogy"//"api/Imaging/v1/GetAllLabImagings"
        static let radiologydetailbyid = BASE_URL + "api/Imaging/v1/radiologydetailbyid"
        
        static let GetAllLabPackages = BASE_URL + "api/LabPackage/v1/GetAllLabPackages"
        
        static let GetAllProvidors = BASE_URL + "api/AmericanTeleClinic/v1/GetAllATPPhysicians"
        static let GetAllSpecialities = BASE_URL + "api/AmericanTeleClinic/v1/getallmedicalspecialities"
        static let GetAllBranches = BASE_URL + "api/AmericanTeleClinic/v1/getallmedicalBranches"
        static let SubmittRequest = BASE_URL + "api/AmericanTeleClinic/v1/OrderOfflineConsultancy"
        static let GetAllBlog = BASE_URL + "api/blog/v1/paginateblog"
        static let GetAllFilteredBlog = BASE_URL + "api/blog/v1/paginateandfilter"
        static let GetAllBlogCategories = BASE_URL + "api/blog/v1/paginateblogcategories"
        
        static let GetAllCategories = BASE_URL + "api/Imaging/v1/GetAllImagingCategories"
        static let GetAllLocations = BASE_URL + "api/Imaging/v1/BodyLocationByImagingCategoryId"
        static let GetAllSubLocations = BASE_URL + "api/Imaging/v1/BodyLocationPartByBodyLocationAndCategoryId"
        static let GetAllType = BASE_URL + "api/Imaging/v1/BodyLocationPartTypeByCategoryAndOthers"
        static let GetAllSearch = BASE_URL + "api/Imaging/v1/paginateandfilterradiology"

        
        static let getlabtestdetailbyid = BASE_URL + "api/LabTest/v1/getlabtestdetailbyid"
        static let GetAllLabTests = BASE_URL + "api/LabTest/v1/paginatelabtests"//"api/LabTest/v1/GetAllLabTests"
        static let searchlabtestbyword = BASE_URL + "api/LabTest/v1/searchlabtestbyword"
        
        static let GetCustomerPersonalContacts = BASE_URL + "api/account/v1/GetCustomerPersonalContacts"
        
        static let labtestresults = BASE_URL + "api/customeraccount/v1/labtestresults"
        static let radilogyresults = BASE_URL + "api/customeraccount/v1/radilogyresults"
        
        static let allCustomerOrders = BASE_URL + "api/customeraccount/v1/allCustomerOrders"
        static let getphysiotherapyservicetypes = BASE_URL + "api/homecare/getphysiotherapyservicetypes"
        
        static let displayfilebyfileid = BASE_URL + "api/common/v1/displayfilebyfileid?id="
        static let displayfilebyfileName = BASE_URL + "api/common/v1/downloadFile?fileName="
        
        static let DownloadfilebyfileName = BASE_URL + "api/common/v1/downloadImage?fileName="
        
        static let DisplayFilebyFileName = BASE_URL + "api/common/v1/displayfilebyfilename?fileName="
        static let ResetPassword = BASE_URL + "api/account/v1/changemobileuserpassword"
        
        static let SaveContactNumber = BASE_URL + "api/customeraccount/v1/saveusercontactnumbers"
        static let SaveAddress = BASE_URL + "api/customeraccount/v1/saveuseraddresses"
        static let SaveName = BASE_URL + "api/customeraccount/v1/saveuserfamilymembers"
        
        static let GetAddress = BASE_URL + "api/customeraccount/v1/getuseraddresses"
        static let GetNames = BASE_URL + "api/customeraccount/v1/getuserfamilymembers"
        static let GetContactNumber = BASE_URL + "api/customeraccount/v1/getusercontactnumbers"
        static let OnlineMedicinesUpload = BASE_URL + "api/common/v1/uploadprescriptionpatientcommunication"
    
    }

    //struct: Notification Observer Name
    struct Notification {
    }
    
    //struct: Contains All notification types
    struct NotificationType {
        static let ViewAppear                   = "kViewAppear"
    }

    //struct: Contains Keys
    struct Key {
        static let Data                         = "data"
        static let Country                      = "country"
        static let Message                      = "message"
        static let TotalCount                   = "totalcount"
        static let LoggedInUser                 = "kLoggedInUserKey"
        static let IsLoggedInUser               = "kIsLoggedInUserKey"
        static let DeviceToken                  = "kDeviceToken"
        static let FirstName                    = "kFirstName"
        static let LastName                     = "kLastName"
        static let Email                        = "kEmail"
        static let Password                     = "kPassword"
        static let Gender                       = "kGender"
        static let Birthday                     = "kBirthday"
        static let LoginName                    = "kLoginName"
        static let FatherName                   = "kFatherName"
        static let PhoneNumber                  = "kPhoneNumber"
        static let Address                      = "kAddress"
        static let AreaID                       = "kAreaID"
        static let City                         = "kCity"
        static let MobileNo                     = "kMobileNo"


        //CART
        static let CartLab                      = "Lab Tests"
        static let CartDiet                     = "Dietician"
        static let CartLabPackages              = "Lab Packages"
        static let CartRadiology                = "Radiology"
        static let Cart                         = ""


    }

    
    //struct: Contains All the messages shown to user
    struct Message {
        //Buttons
        static let OK                                   = "OK"
        static let Cancel                               = "Cancel"
        static let YES                                  = "Yes"
        static let NO                                   = "No"
        static let LATER                                = "Later"
        static let NOW                                  = "Now"


        //Common
        static let Terms                                = "By clicking sign up you confirm that you have read and agreed with the Terms and Conditions and the Privacy Policy."
        static let SignUp                               = "By clicking sign up, you agree to our \nTerms & Conditions you confrim that you have \nread our Terms & Conditions and Privacy Policy"

        
        //Signup
        static let enterName                            = "Please enter name"
        static let enterFirstName                       = "Please enter first name"
        static let enterLastName                        = "Please enter last name"
        static let enterUserName                        = "Please enter email or username"

        static let enterEmail                           = "Please enter email"
        static let enterValidEmail                      = "Please enter valid email"
        static let selectGender                         = "Please select gender"
        static let selectBirthday                       = "Please select birthday"
        static let enterPassword                        = "Please enter password"
        static let enterConfirmPassword                 = "Please enter confirm password"
        static let passwordDoesNotMatch                 = "Password doesn't match"
        static let mobile                 = "Please enter mobile"
        static let country                 = "Please select your country"
        static let city                 = "Please select your city"
        static let address                 = "Please enter your address"
        static let enterminimumsixdigitpwd                 = "Please enter minimum 6 digits password"

    }

    //struct: Contains Color used in application
    struct Color {
        static let themeBlue                  = UIColor(red: 0/255, green: 188/255, blue: 212/255, alpha: 1)
        static let grayBoxBord                = UIColor(red: 221/255, green: 221/255, blue: 221/255, alpha: 1)
        static let grayText                   = UIColor(red: 128/255, green: 128/255, blue: 128/255, alpha: 1)
        static let blue                       = UIColor(red: 0/255, green: 40/255, blue: 104/255, alpha: 1)
        static let red                        = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1)
        static let green                        = UIColor(red: 11/255, green: 102/255, blue: 35/255, alpha: 1)
    }

    //struct: Contains all date and time format
    struct DateFormat {
        static let dateTime           = "yyyy-MM-dd HH:mm:ss"
        static let time               = "hh:mm a"
        static let time24Hours        = "HH:mm"
        static let HomeScreenListDate = "MMMM dd"
        static let dateFormatMMMDD    = "MMM dd"
        static let dateFormatDDMMYYYY = "dd/MM/yyyy"
        static let dateFormatMMDDYYY = "MM/dd/yyyy"

    }

    struct TappableTitleType {
        static let TermsAndCondition  = "TermsAndConditions"
        static let Privacy  = "Privacy"
    }

    struct TappableValueType {
        static let TermsAndCondition  = "Terms and Conditions"
        static let Privacy  = "Privacy Policy"
    }

    struct SideMenuOptionType {
        static let Home           = "Home"
        static let MyProfile      = "My Profile"
        static let MyPost         = "My Post"
        static let FavouritePost  = "Favorite Posts"
        static let ConnectedBox   = "Connected Box"
        static let Alert          = "Alert"
        static let HelpAndSupport = "Help & Support"
    }

    struct ImageSize {
        static let imageOne = CGSize(width: AppUtility.SCREEN_WIDTH()-40, height: 180)
        static let imageTwo = CGSize(width: (AppUtility.SCREEN_WIDTH()-45)/2, height: 180)
    }
}

enum PostType : Int {
    case information = 1
    case social      = 2
    case trending    = 3
}

enum FavoriteType : Int {
    case favorite  = 1
    case fan       = 2
    case connected = 3
}

enum ChildViewType : Int {
    case favorite     = 1
    case notification = 2
    case none         = 0
}

enum NotificationType : Int {
    case image     = 1
    case video     = 2
    case text      = 3
    case none      = 0
}


