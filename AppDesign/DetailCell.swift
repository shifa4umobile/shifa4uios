//
//  DetailCell.swift
//  AppDesign
//
//  Created by sachin on 12/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {

    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var viewLab: UIView!
    @IBOutlet weak var viewCollectionType: UIView!
    
 @IBOutlet weak var btncolectionType: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
