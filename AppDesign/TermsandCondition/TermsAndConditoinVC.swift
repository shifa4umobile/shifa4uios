//
//  TermsAndConditoinVC.swift
//  AppDesign
//
//  Created by sachin on 10/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class TermsAndConditoinVC: BaseViewController , UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var textview: UITextView!
    var isFromLogin: Bool = false

    //MARK: -
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.textview.scrollRangeToVisible(NSMakeRange(0, 0))
      

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        btnBack.isHidden = false
        if isFromLogin == false {
            btnBack.isHidden = true
            //Add menu button
            addSlideMenuButton()
            addCartButton()
        }
    }
    
    func SetupData()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        webView.delegate = self
        webView.scalesPageToFit = true
        webView.contentMode = .scaleAspectFit
        webView.clipsToBounds = true
        SVProgressHUD.show()
        let url = URL(string: "https://www.shifa4u.com/Home/TermsAndConditions")
        let request = URLRequest(url: url!)
        print(request)
        self.webView.loadRequest(request)
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        SVProgressHUD.show()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        SVProgressHUD.dismiss()
       
    }

    //MARK: - IBActions
    @IBAction func btnBackTapped(_ sender: UIButton) {
        AppUtility.shared.loginNavController.popViewController(animated: true)
    }
    
    override func viewDidLayoutSubviews()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                removeView()
                addCartButton()
            }
            else
            {
                removeView()
                
                addCartButton()
            }
            
        } else {
            print("Portrait")
            //Image Height
            removeView()
            addCartButton()
        }
    }
}
