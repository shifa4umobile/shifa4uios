//
//  ForgotPasswordViewController.swift
//  AppDesign
//
//  Created by Mahavir Makwana on 02/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//rogressHUD
import Alamofire

class ForgotPasswordViewController: UIViewController {


    @IBOutlet weak var txtForgotPassword: UITextField!
    var Txt:String = ""
    
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }


    //MARK: - IBActions
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        //AppUtility.shared.loginNavController.popViewController(animated: true)
    }

    @IBAction func btnSendTapped(_ sender: UIButton) {
        if txtForgotPassword.text?.isValid == false {
            view.endEditing(true)
            ISMessages.show(K.Message.enterEmail)
        }
        else if txtForgotPassword.text?.isEmail == false {
            view.endEditing(true)
            ISMessages.show(K.Message.enterValidEmail)
        }
        else {
            view.endEditing(true)
            forgotPassowordWebservice()
            }
        
    }

    //MARK: - Other Actions
    func initialSetup() {

    }


    //MARK: - UITextField Delegate


    //MARK: - Webservice
    func forgotPassowordWebservice()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        SVProgressHUD.show()
            
            let headers = [
                "content-type": "application/json"
            ]
            Txt = txtForgotPassword.text!
            let parameters = ["LoginName": "\(Txt)"]
            let url = URL(string: K.URL.FORGETPASSWORD)
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
            request.httpBody = httpBody
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            
            let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
                
                
                if error != nil {
                    print(error ?? "Error Found")
                    
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    print(httpResponse?.statusCode)
                    if httpResponse?.statusCode == 200
                    {
                        UserDefaults.standard.synchronize()
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Successful", message:"Verification Code has been sent to your email and contact number , please check and reset your password", preferredStyle: .alert)
                            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
                        let vc = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "passwordResetVC") as! passwordResetVC
                        vc.Username = self.txtForgotPassword.text!
                            self.navigationController!.pushViewController(vc, animated: true)
                            alert.addAction(later)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message:"Error Occured , Please try again and enter registered email.", preferredStyle: .alert)
                        let later = UIAlertAction(title: "OK", style: .default, handler: nil)
                        
                        alert.addAction(later)
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                    
                    SVProgressHUD.dismiss()
                }
            })
            task.resume()
    }

}
