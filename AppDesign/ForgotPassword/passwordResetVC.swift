//
//  passwordResetVC.swift
//  AppDesign
//
//  Created by UDHC on 03/10/2018.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class passwordResetVC: UIViewController {

    @IBOutlet weak var SendButton: UIButton!
    @IBOutlet weak var ConfirmPassword: UITextField!
    @IBOutlet weak var Password: UITextField!
    @IBOutlet weak var SecurityCode: UITextField!
    
    var Username:String = ""
    var PasswordForChange:String = ""
    var VerificationCode:String = ""
    
    var status:Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func SendBtn(_ sender: Any) {
        
        if Password.text?.isValid == false {
            view.endEditing(true)
            ISMessages.show("Enter Password")
        }
        else if (Password.text?.length)! < 6 {
            view.endEditing(true)
            ISMessages.show(K.Message.enterminimumsixdigitpwd)
        }
        else if ConfirmPassword.text?.isValid == false {
            view.endEditing(true)
            ISMessages.show("please Confirm Password")
        }
        else if SecurityCode.text?.isValid == false {
            view.endEditing(true)
            ISMessages.show("Enter Code")
        }
        else if Password.text != ConfirmPassword.text {
            view.endEditing(true)
            ISMessages.show("Password not matched")
        }
        else{
        self.PasswordForChange = Password.text!
        self.VerificationCode = SecurityCode.text!
        view.endEditing(true)
        forgotPassowordWebservice()
        }
        
    }
    
    @IBAction func BackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func forgotPassowordWebservice()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        } 
        SVProgressHUD.show()
        
        do
        {
            
            let parameters = ["Username": "\(Username)" , "VerificationCode": "\(VerificationCode)" , "Password": "\(PasswordForChange)"]
            print(parameters)
            
            let url = URL(string: K.URL.ResetPassword)
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
            request.httpBody = httpBody
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            
            let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
                
                
                if error != nil {
                    print(error ?? "Error Found")
                    
                }
                else
                {
                    let httpResponse = response as? HTTPURLResponse
                    print(httpResponse?.statusCode)
                    if httpResponse?.statusCode == 200
                    {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Successful", message:"Password Successfully Changed", preferredStyle: .alert)
                            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
                            let vc = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                            self.navigationController!.pushViewController(vc, animated: true)
                            alert.addAction(later)
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Alert", message:"Error Occured , Enter Valid Code", preferredStyle: .alert)
                        let later = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alert.addAction(later)
                        self.present(alert, animated: true, completion: nil)
                    }
                    SVProgressHUD.dismiss()
                }
            });
            task.resume()
            
        }
        
    }
}
