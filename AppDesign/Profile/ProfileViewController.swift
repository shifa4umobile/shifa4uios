//
//  ProfileViewController.swift
//  AppDesign
//
//  Created by Mahavir Makwana on 02/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD
import Alamofire

class ProfileViewController: UIViewController , UITextFieldDelegate {
    
    @IBOutlet weak var btnEditProfile: UIButton!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtBirthDay: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    
    
    @IBOutlet weak var botumOfBtnSubmitt: NSLayoutConstraint!
    @IBOutlet weak var btnSubmitt: UIButton!
    @IBOutlet weak var heightOfBtnSubmitt: NSLayoutConstraint!
    var dictUserData: Dictionary<String,Any> = [:]
    var countryID: Int = -1
    var cityID: Int = -1
    var arrCountry:[String] = ["India" , "Pakistan" , "Uk" , "USA" , "AUS"]
    var arrCity:[String] = ["Delhi" , "Karachi" , "Landon" , "Californiya" , "Sydni"]
    let arrGender = ["Male","Female"]
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        self.setData()
        getMyProfileWebservice()
        
    }
    
    //MARK: - UITextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtBirthDay {
            showDatePicker()
            return false
        }
        else if textField == txtCountry{
            textField.text = ""
            countryID = -1
            AppUtility.shared.getAllCountry { (success, error) in
                if success == true {
                    DispatchQueue.main.async {
                        self.showCountryPicker()
                        
                    }
                }
            }
            return false
        }
        else if textField == txtCity
        {
            if countryID == -1 {
                ISMessages.show("Please select country")
            }
            cityID = -1
            textField.text = ""
            AppUtility.shared.getCity(countryID: countryID, completion: { (success, error, arrayList) in
                if success == true {
                    DispatchQueue.main.async {
                        if arrayList != nil {
                            self.showCityPicker(arrayList: arrayList!)
                        }
                    }
                }
            })
            return false
        }
        else if textField == txtGender
        {
            showGenderPicker(arrList: arrGender, textField: textField)
            return false
        }
        return true
    }
    
    //MARK: - IBActions
    
    @IBAction func btnEditProfileTapped(_ sender: UIButton)
    {
        setupEditPrfile()
    }
    
    @IBAction func btnSubmittTapped(_ sender: UIButton)
    {
        //initialSetup()
        validate()
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton)
    {
        AppUtility.shared.mainNavController.popViewController(animated: true)
        
    }
    //MARK: - Other Actions
    
    func validate()
    {
        if txtName.text?.isValid == false {
            ISMessages.show(K.Message.enterFirstName)
        }
        else if txtLastName.text?.isValid == false {
            ISMessages.show(K.Message.enterLastName)
        }
        else if txtBirthDay.text?.isValid == false {
            ISMessages.show(K.Message.selectBirthday)
        }
        else if txtGender.text?.isValid == false {
            ISMessages.show(K.Message.selectGender)
        }
        else if txtEmail.text?.isValid == false {
            ISMessages.show(K.Message.enterEmail)
        }
        else if txtEmail.text?.isEmail == false {
            ISMessages.show(K.Message.enterValidEmail)
        }
        else if txtMobile.text?.isValid == false {
            ISMessages.show(K.Message.mobile)
        }
        else if txtAddress.text?.isValid == false {
            ISMessages.show(K.Message.address)
        }
        else
        {
//            let parameters: [String: Any] =
//                [
//                    "FirstName" : txtName.text?.trimmed(),
//                    "LastName" : txtLastName.text?.trimmed(),
//                    "DateofBirth" : txtBirthDay.text?.trimmed(),
//                    "CountryId" : "\(countryID)",
//                    "CityId" : "\(cityID)",
//                    "Address" : txtAddress.text?.trimmed(),
//                    "MobileNo" : txtMobile.text?.trimmed(),
//                    "Email" :txtEmail.text?.trimmed(),
//                    "Gender": txtGender.text?.trimmed()
//            ]
            apicall()
            
        }
        
    }
    
    func apicall()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        SVProgressHUD.show()
        
        do {
            
            let headers = [
                "Content-Type": "application/json"
            ]
            
            let parameters:NSMutableDictionary = NSMutableDictionary()
            parameters.setValue(txtName.text?.trimmed(), forKeyPath: "FirstName")
            parameters.setValue(txtLastName.text?.trimmed(), forKeyPath: "LastName")
            parameters.setValue(txtBirthDay.text?.trimmed(), forKeyPath: "DateofBirth")
            parameters.setValue("\(countryID)", forKeyPath: "CountryId")
            parameters.setValue("\(cityID)", forKeyPath: "CityId")
            parameters.setValue(txtAddress.text?.trimmed(), forKeyPath: "Address")
            parameters.setValue(txtMobile.text?.trimmed(), forKeyPath: "MobileNo")
            parameters.setValue(txtEmail.text?.trimmed(), forKeyPath: "Email")
            parameters.setValue(txtGender.text?.trimmed(), forKeyPath: "Gender")
            
            
            
            
            
            
            let postData = try! JSONSerialization.data(withJSONObject: parameters, options: [])
            
            let request = NSMutableURLRequest(url: NSURL(string: K.URL.EDITPROFILE)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            let token = UserDefaults.standard.object(forKey: "token") as! String
            request.addValue(NSString(format: "bearer %@", token) as String, forHTTPHeaderField: "Authorization")
    
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                
                SVProgressHUD.dismiss()
                if error != nil {
                    print(error ?? "Error Found")
                    
                }
                else{
                    let httpResponse = response as? HTTPURLResponse
                    print(httpResponse)
                    if httpResponse?.statusCode == 200
                    {
                        do {
                            // gunzip
                            let decompressedData: Data
                            if (data?.isGzipped)! {
                                decompressedData = (try! data?.gunzipped())!
                            } else {
                                decompressedData = data!
                            }
                                    DispatchQueue.main.async {
                                        ISMessages.show("Profile updated sucessfully")
                                        self.getMyProfileWebservice()
                                        self.initialSetup()
                                    }
                        }
                        catch let exeptionError as Error? {
                            print(exeptionError!.localizedDescription)
                            SVProgressHUD.dismiss()
                        }
                    }
                    else
                    {
                        ISMessages.show("An error has occurred.")
                    }
                }
            })
            dataTask.resume()
        }
        catch
        {
            print(error)
        }
        

        
        /*let para = params as? Dictionary<String,Any>
        
        let url = URL(string: K.URL.EDITPROFILE)
        let request = NSMutableURLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: para, options: .prettyPrinted)
       
        let token = UserDefaults.standard.object(forKey: "token") as! String
        request.addValue(NSString(format: "Bearer %@", token) as String, forHTTPHeaderField: "Authorization")
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request as URLRequest) {(data, response, error) in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                
                // gunzip
                let decompressedData: Data
                if (data?.isGzipped)! {
                    decompressedData = (try! data?.gunzipped())!
                } else {
                    decompressedData = data!
                }
                let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
                if dictionary != nil {
                    print(dictionary!)
                }
                ISMessages.show("Profile updated sucessfully")
                self.initialSetup()
               
                
                SVProgressHUD.dismiss()
            })
            
        }
        
        task.resume()*/
    }
       /*
        
        Alamofire.request(K.URL.EDITPROFILE, method: .post, parameters: params, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response)
                switch response.result {
                case .success:
                    print(response.result)
                    SVProgressHUD.dismiss()
                    self.initialSetup(); AppUtility.shared.mainNavController.popViewController(animated: true)
                    break
                
                case .failure(let error):
                    print(error)
                    break
                    
                }
        }*/
    
    func initialSetup() {
        txtName.isEnabled = false
        txtLastName.isEnabled = false
        txtEmail.isEnabled = false
        txtMobile.isEnabled = false
        txtBirthDay.isEnabled = false
        txtCountry.isEnabled = false
        txtCity.isEnabled = false
        txtAddress.isEnabled = false
        btnSubmitt.isHidden = true
        txtGender.isEnabled = false
        botumOfBtnSubmitt.constant = 0
        heightOfBtnSubmitt.constant = 0
        
    }
    func setupEditPrfile()
    {
        let alert = UIAlertController(title: "Alert", message:"Profile Editing Enabled. You can't edit email.", preferredStyle: .alert)
        let later = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(later)
        self.present(alert, animated: true, completion: nil)
        txtName.isEnabled = true
        txtLastName.isEnabled = true
        txtEmail.isEnabled = false
        txtEmail.textColor = K.Color.red
        txtMobile.isEnabled = true
        txtBirthDay.isEnabled = true
        txtCountry.isEnabled = true
        txtCity.isEnabled = true
        txtAddress.isEnabled = true
        txtGender.isEnabled = true
        btnSubmitt.isHidden = false
        botumOfBtnSubmitt.constant = 35
        heightOfBtnSubmitt.constant = 40
        self.setData()
    }
    
    func setData() {
        let user = User.loggedInUser()
        print(user)
        txtName.text = user?.firstName
        txtLastName.text = user?.lastName
        txtEmail.text = user?.loginName
        txtMobile.text = user?.mobileNo
        txtBirthDay.text = user?.dateofBirth
        txtCountry.text = user?.country
        txtCity.text = user?.city
        txtAddress.text = user?.address
        txtGender.text = user?.gender
        cityID = (user?.cityId)!
        countryID = (user?.countryId)!
        
    }
    
    
    //MARK: - Other Actions
    func showGenderPicker(arrList: Array<String>, textField: UITextField) {
        var selectedIndex = 0
        if txtGender.text?.isValid == true {
            let indexes = arrGender.filter({$0 == txtGender.text})
            if indexes.count > 0 {
                selectedIndex = 0
            }
        }
        RMPickerViewController.show(withTitle: nil, selectedIndex:selectedIndex, array: arrGender, doneBlock: { (index, value) in
            textField.text = value!
        }, cancel: {
            
        })
    }
    func showCountryPicker() {
        RMPickerViewController.show(withTitle: nil, selectedIndex:0, array: AppUtility.shared.arrCountry.map({$0["CountryName"]}) as! [String], doneBlock: { (index, value) in
            if let value = AppUtility.shared.arrCountry[index]["CountryName"] as? String {
                self.countryID = AppUtility.shared.arrCountry[index]["CountryId"] as? Int ?? -1
                self.txtCountry.text = value
               // self.btnCountry.setTitle(value, for: .normal)
                
                UserDefaults.standard.set(value, forKey: K.Key.Country)
                UserDefaults.standard.synchronize()
            }
        }, cancel: {
            
        })
    }
    
    func showCityPicker(arrayList: Array<Dictionary<String,Any>>) {
        if arrayList.count == 0 {
            return
        }
        RMPickerViewController.show(withTitle: nil, selectedIndex:0, array: arrayList.map({$0["CityName"]}) as! [String], doneBlock: { (index, value) in
            if let value = arrayList[index]["CityName"] as? String {
                self.cityID = arrayList[index]["CityId"] as? Int ?? -1
                self.txtCity.text = value
                //self.btnCity.setTitle(value, for: .normal)
                UserDefaults.standard.set(value, forKey: K.Key.City)
                UserDefaults.standard.synchronize()
            }
        }, cancel: {
            
        })
    }
    
    func showDatePicker() {
        var selectedDate = Date(timeIntervalSinceReferenceDate: 0)
        if txtBirthDay.text?.isValid == true {
            let date = Date(fromString: (txtBirthDay.text?.trimmed())!, format: K.DateFormat.dateFormatDDMMYYYY)
            if date != nil {
                selectedDate = date!
            }
        }
        
        let selectAction = RMAction<UIDatePicker>(title: "Select", style: RMActionStyle.done) { controller in
            print("Successfully selected date: ", controller.contentView.date)
            
            self.txtBirthDay.text = controller.contentView.date.toString(format: K.DateFormat.dateFormatDDMMYYYY)
        }
        
        let cancelAction = RMAction<UIDatePicker>(title: "Cancel", style: RMActionStyle.cancel) { _ in
            print("Date selection was canceled")
        }
        
        let actionController = RMDateSelectionViewController(style: .white, title: "Test", message: "This is a test message.\nPlease choose a date and press 'Select' or 'Cancel'.", select: selectAction, andCancel: cancelAction)!;
        
        //You can access the actual UIDatePicker via the datePicker property
        actionController.datePicker.datePickerMode = .date
        //actionController.datePicker.minuteInterval = 5
        actionController.datePicker.backgroundColor = UIColor.white
        actionController.datePicker.tintColor = UIColor.blue
        
        actionController.datePicker.maximumDate = Date()
        actionController.datePicker.date = selectedDate
        
        present(actionController, animated: true, completion: nil)
    }
    
    
    
    //MARK: - Webservice
    func getMyProfileWebservice() {
        
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        SVProgressHUD.show()
        
        let url = URL(string: K.URL.GET_MY_PROFILE)
        var urlRequest = URLRequest(url: url!)
        urlRequest.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        urlRequest.timeoutInterval = 60 * 5
        urlRequest.httpMethod = "GET"
        //urlRequest.allHTTPHeaderFields = headers
        urlRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let token = UserDefaults.standard.object(forKey: "token") as! String
        urlRequest.addValue(NSString(format: "Bearer %@", token) as String, forHTTPHeaderField: "Authorization")
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, errorResponse) in
            SVProgressHUD.dismiss()
            if errorResponse != nil {
                print(errorResponse ?? "Error Found")
                
            }
            else{
                
                do {
                    // gunzip
                    let decompressedData: Data
                    if (data?.isGzipped)! {
                        decompressedData = (try! data?.gunzipped())!
                    } else {
                        decompressedData = data!
                    }
                    //if  let json = try! JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves) as? NSDictionary
                    //{
                    if let json = try! JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves) as? NSDictionary
                    {
                        // self.arrLabs.removeAll(keepingCapacity: false)
                        DispatchQueue.main.async {
                            //print(dictionary)
                            let data1: Dictionary<String,Any> = (json as! Dictionary<String,Any>)
                            print("here is profile")
                            print(data1)
                            let user = User(dict: data1)
                            user.save()
                            
                            self.setData()

                        }
                        SVProgressHUD.dismiss()
                    }
                }
                catch let exeptionError as Error? {
                    print(exeptionError!.localizedDescription)
                    SVProgressHUD.dismiss()
                }
            }
        })
        task.resume()
        
        /*_ = WebClient.requestWithUrl(url: K.URL.GET_MY_PROFILE, parameters: nil, method: "GET") { (responseObject, error) in
            if error == nil {
                //let dict = responseObject as! Dictionary<String, Any>
                print("Data: \(responseObject!)")
           
                
                if let data: Dictionary<String,Any> = (responseObject as! Dictionary<String,Any>) {
                    
                    let user = User(dict: data)
                    user.save()
                    
                    self.setData()
                    
                }
                
            }else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        }*/
    }
}
