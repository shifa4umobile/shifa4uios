//
//  CurrentOrderCell.swift
//  AppDesign
//
//  Created by sachin on 07/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class CurrentOrderCell: UITableViewCell {

    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var tbldetail: UITableView!
    @IBOutlet weak var btnlab: UIButton!
    @IBOutlet weak var imglab: UIImageView!
    @IBOutlet weak var btncolectionType: UIButton!
    @IBOutlet weak var imgcolectionType: UIImageView!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var viewmain: UIView!
    @IBOutlet weak var tblheight: NSLayoutConstraint!
    @IBOutlet weak var btnselectlocation: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func settableViewDataSourceDelegate<D: UITableViewDataSource & UITableViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        tbldetail.delegate = dataSourceDelegate
        tbldetail.dataSource = dataSourceDelegate
        tbldetail.tag = row
        tbldetail.setContentOffset(tbldetail.contentOffset, animated:false) // Stops collection view if it was scrolling.
        tbldetail.reloadData()
    }
    
    var tableViewOffset: CGFloat {
        set { tbldetail.contentOffset.x = newValue }
        get { return tbldetail.contentOffset.x }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
