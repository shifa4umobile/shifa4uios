//
//  LabsDetailVC.swift
//  AppDesign
//
//  Created by sachin on 19/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class LabsDetailVC: BaseViewController
{
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var viewdetail: UIView!
    @IBOutlet weak var viewpric: UIView!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var lbldescription: UILabel!
    @IBOutlet weak var lbltestname: UILabel!
    @IBOutlet weak var lblalsoKnown: UILabel!
    @IBOutlet weak var lblSampleRequired: UILabel!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var vieworder: UIView!
    @IBOutlet weak var btnchooselab: UIButton!

    var labDeatil: LabDetails!
    var selectedLab: Lab? = nil
    
    let chooseArticleDropDown = DropDown()
    
    var dicSelectLabTest:NSMutableDictionary = NSMutableDictionary()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown
        ]
    }()
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
        Common.SetShadowwithCorneronView(view: viewdetail, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: vieworder, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: viewpric, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))


        // (dicSelectLabTest.object(forKey: "") as! String)
        
        lbltitle.text = (dicSelectLabTest.object(forKey: "Name") as! String)
        lblsubtitle.text = (dicSelectLabTest.object(forKey: "Name") as! String)
        lbldescription.text = (dicSelectLabTest.object(forKey: "ShortDescription") as! String)
        lbltestname.attributedText = Common.setattributetextColor(strtext:"Test Name: \((dicSelectLabTest.object(forKey: "Name") as! String))",rangeString:"Test Name:")
        lblalsoKnown.attributedText = Common.setattributetextColor(strtext:"Also known as: \((dicSelectLabTest.object(forKey: "OtherNames") as! String))",rangeString:"Also known as:")
        lblSampleRequired.attributedText = Common.setattributetextColor(strtext:"Sample Required: \((dicSelectLabTest.object(forKey: "SampleType") as! String))",rangeString:"Sample Required")
        btnchooselab.setTitle("Select Lab", for: .normal)
        lblprice.text = "0"
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnback(_ sender: Any)
    {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    
    @IBAction func btnchooselab(_ sender: Any)
    {
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        
        let arrLabsTemp = NSMutableArray()
        for i in 0..<(dicSelectLabTest.object(forKey: "Labs") as! NSArray).count
        {
            let labname = ((dicSelectLabTest.object(forKey: "Labs") as! NSArray).object(at: i) as! NSDictionary).object(forKey: "LabName")
            arrLabsTemp.add(labname as Any)
        }
        
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        
        //chooseArticleDropDown.dataSource = labDeatil.arrLabs.map({$0.labName})
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            //self?.selectedLab = self?.labDeatil.arrLabs[index]
            
            lblNotificationText?.setTitle(item, for: .normal)
            //self?.lblprice.text = "\(K.kCurrency) \(self?.selectedLab?.price ?? 0)/-"
            let arr = self?.dicSelectLabTest.object(forKey: "Labs") as! NSMutableArray
            let dic = arr.object(at: index) as! NSDictionary
            self?.lblprice.text = NSString(format: "Rs. %@",Utility.setCurrencyFormate(NSString(format: "%d",dic.object(forKey: "YourPrice") as! Int) as String)) as String
            
            for i in 0..<arr.count
            {
                let dictemp = NSMutableDictionary(dictionary: arr.object(at: i) as! NSDictionary)
                if i == index
                {
                    dictemp.setValue("1", forKey: "isselect")
                }
                else
                {
                    dictemp.setValue("0", forKey: "isselect")
                }
                arr.removeObject(at: i)
                arr.insert(dictemp, at: i)
            }
            self?.dicSelectLabTest.setValue(arr, forKey: "Labs")
            self?.chooseArticleDropDown.hide()
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
        }
        
        chooseArticleDropDown.show()
    }
    
    @IBAction func btnorder(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "arrlabtestcart") == nil
        {
            var labDetails1 = Dictionary<AnyHashable, Any>()
            labDetails1 = dicSelectLabTest as! [AnyHashable : Any]
            let labDetails11 =  labDetails1.nullKeyRemoval()
            
            let arrLabTestCart:NSMutableArray = NSMutableArray()
            arrLabTestCart.add(labDetails11)
            UserDefaults.standard.set(arrLabTestCart, forKey: "arrlabtestcart")
            UserDefaults.standard.synchronize()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                self.onCartbuttonClicked(nil)
            })
        }
        else
        {
            let arrLabTestCart:NSMutableArray = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrlabtestcart") as! NSArray)
            var istrue = "yes"
            for i in 0..<arrLabTestCart.count
            {
                let dicLab = arrLabTestCart[i] as! NSDictionary
                let labidtemp = dicLab.object(forKey: "LabTestId") as! Int
                let labidtemp11 = dicSelectLabTest.object(forKey: "LabTestId") as! Int
                
                if labidtemp11 == labidtemp
                {
                    istrue = "no"
                }
            }
            if istrue == "yes"
            {
                var labDetails1 = Dictionary<AnyHashable, Any>()
                labDetails1 = dicSelectLabTest as! [AnyHashable : Any]
                let labDetails11 =  labDetails1.nullKeyRemoval()
                
                arrLabTestCart.add(labDetails11)
                UserDefaults.standard.set(arrLabTestCart, forKey: "arrlabtestcart")
                UserDefaults.standard.synchronize()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    self.onCartbuttonClicked(nil)
                })
            }
            else
            {
                ISMessages.show("Product Already Exits in Cart")
            }
        }
    }
}
