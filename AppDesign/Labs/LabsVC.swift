//
//  LabsVC.swift
//  AppDesign
//
//  Created by sachin on 19/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class LabsVC: BaseViewController
{
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tbllist: UITableView!
    @IBOutlet weak var btnSearchTop: UIButton!
    @IBOutlet weak var viewSearchTop: UIView!
    
    //var arrLabs: Array<LabDetails> = []
    //var arrLabsDisplay: Array<LabDetails> = []

    var arrLabTestList:NSMutableArray = NSMutableArray()
    var arrSearch_LabTestList:NSMutableArray = NSMutableArray()
    
    var TotalRecords:Int = 0
    var page_Limit: Int = 0
    
    
    var activityIndicator: UIActivityIndicatorView?
    
    //MARK:: -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Add menu button
        
        //
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        
        //addSlideMenuButton()
        addCartButton()
        
        viewSearchTop.layer.cornerRadius = 4
        viewSearchTop.clipsToBounds = true
        
        Common.SetShadowwithCorneronView(view: viewSearch, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))

        if UserDefaults.standard.object(forKey: "arrGetAllLabTests") != nil
        {
            
            self.arrLabTestList = NSMutableArray(array: UserDefaults.standard.object(forKey: "arrGetAllLabTests") as! NSArray)
            
            self.arrSearch_LabTestList = NSMutableArray(array:UserDefaults.standard.object(forKey: "arrGetAllLabTests") as! NSArray)
            
            self.tbllist.reloadData()
            if self.arrLabTestList.count != 0
            {
                apicall(is_firsttime: "0", SearchKeyWord: "")
            }
            else
            {
                apicall(is_firsttime: "1", SearchKeyWord: "")
            }
        }
        else
        {
            apicall(is_firsttime: "1", SearchKeyWord: "")
        }
    }

    
    override func viewDidLayoutSubviews()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                removeView()
                addCartButton()
            }
            else
            {
                removeView()
                
                addCartButton()
            }
            
        } else {
            print("Portrait")
            //Image Height
            removeView()
            addCartButton()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        addCartButton()
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }


    //MARK: - IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    @IBAction func btnfilter(_ sender: Any)
    {
    
    }
    
    @IBAction func btninfo(_ sender: Any)
    {
    
    }
    
    @IBAction func btnaddtocart(_ sender: Any)
    {
    
    }

    @IBAction func btnSearchTop(_ sender: Any) {
        page_Limit = 0
        arrLabTestList = NSMutableArray()
        arrSearch_LabTestList = NSMutableArray()
        tbllist.reloadData()
        apicall(is_firsttime: "1", SearchKeyWord: txtSearch.text!)
    }
    
    @IBAction func btnAddCartCell(_ sender: Any) {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tbllist)
        let indexPath = self.tbllist.indexPathForRow(at: buttonPosition)
        let labDetails = NSMutableDictionary(dictionary: arrLabTestList[(indexPath?.row)!] as! NSDictionary)

        if UserDefaults.standard.value(forKey: "arrlabtestcart") == nil
        {
            var labDetails1 = Dictionary<AnyHashable, Any>()
            labDetails1 = labDetails as! [AnyHashable : Any]
            let labDetails11 =  labDetails1.nullKeyRemoval()
            
            let selectLabId = "\(labDetails.object(forKey: "LabTestId") ?? 0)"
            getDetail(is_firsttime: "1", selectid: selectLabId, selectDic: labDetails11 as NSDictionary, isDetail: "0")
        }
        else
        {
            //var arrLabTestCart = NSArray()
            let arrLabTestCart:NSMutableArray = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrlabtestcart") as! NSArray)
            var istrue = "yes"
            for i in 0..<arrLabTestCart.count
            {
                let dicLab = arrLabTestCart[i] as! NSDictionary
                let labidtemp = dicLab.object(forKey: "LabTestId") as! Int
                let labidtemp11 = labDetails.object(forKey: "LabTestId") as! Int
                
                if labidtemp11 == labidtemp
                {
                    istrue = "no"
                }
            }
            if istrue == "yes"
            {
                var labDetails1 = Dictionary<AnyHashable, Any>()
                labDetails1 = labDetails as! [AnyHashable : Any]
                let labDetails11 =  labDetails1.nullKeyRemoval()
                
                
                let selectLabId = "\(labDetails.object(forKey: "LabTestId") ?? 0)"
                getDetail(is_firsttime: "1", selectid: selectLabId, selectDic: labDetails11 as NSDictionary, isDetail: "0")
            }
            else
            {
                ISMessages.show("Product Already Exits in Cart")
            }
        }
    }
    
    
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        setData()
    }


    //MARK: - Other Action
    func setData() {
       // arrLabTestList.removeAll(keepingCapacity: false)
//        if txtSearch.text?.isValid == true {
//            arrLabTestList = self.arrLabTestList.filter(using: {$0.name.contains((txtSearch.text?)!, compareOption: NSString.CompareOptions.caseInsensitive)})
//        }
//        else {
//            arrLabTestList.append(contentsOf: self.arrLabTestList)
//        }
//        tbllist.reloadData()
    }

}


//MARK:- UITextField Delegate
extension LabsVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //setData()
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        //arrLabTestList = NSMutableArray(array: arrSearch_LabTestList)
        //tbllist.reloadData()
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let newString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        if textField == txtSearch
        {
            if newString?.count == 0
            {
                arrLabTestList = NSMutableArray()
                arrSearch_LabTestList = NSMutableArray()
                page_Limit = 0
                tbllist.reloadData()
                apicall(is_firsttime: "1", SearchKeyWord: "")
            }
            else if (newString?.count)! > 1
            {
//                page_Limit = 0
//                arrLabTestList = NSMutableArray()
//                arrSearch_LabTestList = NSMutableArray()
//                apicall(is_firsttime: "1", SearchKeyWord: newString!)
                /*var tmpary = [AnyHashable]()
                let predicate = NSPredicate(format: "SELF.Name contains[c] %@", newString!)
                tmpary = arrSearch_LabTestList.filtered(using: predicate) as! [AnyHashable]
                arrLabTestList = NSMutableArray(array: tmpary)
                tbllist.reloadData()*/
            }
        }
        return true
    }
    
    //MARK: - scrol page
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView == tbllist
        {
            let visibleRows = tbllist.indexPathsForVisibleRows
            let indexPath: IndexPath? = visibleRows?.last
            if indexPath?.row == arrLabTestList.count-1
            {
                if arrLabTestList.count < TotalRecords
                {
                    page_Limit = page_Limit + 1
                    self.apicall(is_firsttime: "", SearchKeyWord: "")
                }
            }
        }
    }
}


//MARK:- Tableview Delegate Method
extension LabsVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrLabTestList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let viewmain = cell.contentView.viewWithTag(1)
        let lbltitle = cell.contentView.viewWithTag(2) as! UILabel
        let lblrecommended = cell.contentView.viewWithTag(3) as! UILabel
        let lbldesc = cell.contentView.viewWithTag(4) as! UILabel
        let viewbtnaddtocart = cell.contentView.viewWithTag(5)
        //let btnaddtocart = cell.contentView.viewWithTag(6) as! UIButton
        let viewbtndetail = cell.contentView.viewWithTag(7)
        //let btndetail = cell.contentView.viewWithTag(8) as! UIButton
        let labDetails = NSMutableDictionary(dictionary: arrLabTestList[indexPath.row] as! NSDictionary)

        lbltitle.text = (labDetails.object(forKey: "Name") as! String)
        if let ShortDescription = labDetails.object(forKey: "ShortDescription")
        {
            lbldesc.text = (ShortDescription as! String)
        }
        else
        {
            lbldesc.text = ""
        }
        Common.SetShadowwithCorneronView(view: viewmain!, cornerradius: 10.0, color:UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.setCornerOnView(view: lblrecommended, cornerradius: 4.0)
        Common.SetShadowwithCorneronView(view: viewbtnaddtocart!, cornerradius: 10.0, color:UIColor.clear)
        Common.SetShadowwithCorneronView(view: viewbtndetail!, cornerradius: 10.0, color:UIColor.clear)

        /*btnaddtocart.addTapGesture { (gesture) in
            //AppUtility.shared.addProductToCart(type: K.Key.CartLab, product: labDetails)
            //AppUtility.shared.addProductToLab(item: labDetails)
            
            //add item in cart array
            
            if UserDefaults.standard.value(forKey: "arrlabtestcart") == nil
            {
//                var labDetails1 = Dictionary<AnyHashable, Any>()
//                labDetails1 = labDetails as! [AnyHashable : Any]
//                let labDetails11 =  labDetails1.nullKeyRemoval()
//
//                let arrLabTestCart:NSMutableArray = NSMutableArray()
//                arrLabTestCart.add(labDetails11)
//                UserDefaults.standard.set(arrLabTestCart, forKey: "arrlabtestcart")
//                UserDefaults.standard.synchronize()
                self.selectLabId = Int("\(labDetails.object(forKey: "LabTestId") ?? 0)")!
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    self.onCartbuttonClicked(nil)
                })
            }
            else
            {
                //var arrLabTestCart = NSArray()
                let arrLabTestCart:NSMutableArray = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrlabtestcart") as! NSArray)
                var istrue = "yes"
                for i in 0..<arrLabTestCart.count
                {
                    let dicLab = arrLabTestCart[i] as! NSDictionary
                    let labidtemp = dicLab.object(forKey: "LabTestId") as! Int
                    let labidtemp11 = labDetails.object(forKey: "LabTestId") as! Int
                    
                    if labidtemp11 == labidtemp
                    {
                      istrue = "no"
                    }
                }
                if istrue == "yes"
                {
//                    var labDetails1 = Dictionary<AnyHashable, Any>()
//                    labDetails1 = labDetails as! [AnyHashable : Any]
//                    let labDetails11 =  labDetails1.nullKeyRemoval()
//
//                    arrLabTestCart.add(labDetails11)
//                    UserDefaults.standard.set(arrLabTestCart, forKey: "arrlabtestcart")
//                    UserDefaults.standard.synchronize()
                    self.selectLabId = Int("\(labDetails.object(forKey: "LabTestId") ?? 0)")!
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                        self.onCartbuttonClicked(nil)
                    })
                }
                else
                {
                    ISMessages.show("Product Already Exits in Cart")
                }
            }
 
        }*/

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let labDetails = arrLabTestList[indexPath.row] as! NSMutableDictionary
        let selectLabId = "\(labDetails.object(forKey: "LabTestId") ?? 0)"
        self.getDetail(is_firsttime: "1", selectid: selectLabId, selectDic: labDetails, isDetail: "1")
        
            }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
        
    }

    //MARK: - Webservice
    func getDetail(is_firsttime: String, selectid:String, selectDic:NSDictionary, isDetail:String)
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        if is_firsttime == "1" {
            SVProgressHUD.show()
        }
        
        let parameters:NSMutableDictionary = NSMutableDictionary()
        parameters.setValue(selectid, forKeyPath: "MedicalProductId")//medicalProductId
        
        if UserDefaults.standard.object(forKey: "selectCityid") != nil {
            let CityId = UserDefaults.standard.object(forKey: "selectCityid") as! String
            parameters.setValue(CityId, forKeyPath: "CityId")
        }
        
        let postData = try! JSONSerialization.data(withJSONObject: parameters, options: [])

        let request = NSMutableURLRequest(url: NSURL(string: K.URL.getlabtestdetailbyid)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 120.0)
        request.httpMethod = "POST"
        let headers = [
            "content-type": "application/json"
        ]
        request.allHTTPHeaderFields = headers
        
        request.httpMethod = "POST"
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            SVProgressHUD.dismiss()
            if error != nil {
                print(error ?? "Error Found")
            }
            else
            {
                do {
                    // gunzip
                    let decompressedData: Data
                    if (data?.isGzipped)! {
                        decompressedData = (try! data?.gunzipped())!
                    } else {
                        decompressedData = data!
                    }
                    if let dictionary = try JSONSerialization.jsonObject(with: decompressedData, options: .mutableContainers) as? [Any]
                    {
                        DispatchQueue.main.async {
                            let arrdropdown =  NSMutableArray(array: dictionary)
                            for j in 0..<arrdropdown.count
                            {
                                let dictemp1 = arrdropdown.object(at: j) as! Dictionary<AnyHashable, Any>
                                let labDetails111 =  dictemp1.nullKeyRemoval()
                                arrdropdown.removeObject(at: j)
                                arrdropdown.insert(labDetails111, at: j)
                            }
                            if isDetail == "1"
                            {
                                let mutableDelectDic = NSMutableDictionary(dictionary: selectDic)
                                mutableDelectDic.setValue(arrdropdown, forKey: "Labs")

                                let labDetailVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "LabsDetailVC") as! LabsDetailVC
                                
                                labDetailVC.dicSelectLabTest = NSMutableDictionary(dictionary: mutableDelectDic)
                                AppUtility.shared.mainNavController.pushViewController(labDetailVC, animated: true)
                            }
                            else
                            {
                                if UserDefaults.standard.value(forKey: "arrlabtestcart") == nil
                                {
                                    let arrLabTestCart:NSMutableArray = NSMutableArray()
                                    let mutableDelectDic = NSMutableDictionary(dictionary: selectDic)
                                    mutableDelectDic.setValue(arrdropdown, forKey: "Labs")
                                    arrLabTestCart.add(mutableDelectDic)
                                    UserDefaults.standard.set(arrLabTestCart, forKey: "arrlabtestcart")
                                    UserDefaults.standard.synchronize()
                                }
                                else
                                {
                                    let arrLabTestCart:NSMutableArray = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrlabtestcart") as! NSArray)
                                    
                                    let mutableDelectDic = NSMutableDictionary(dictionary: selectDic)
                                    mutableDelectDic.setValue(arrdropdown, forKey: "Labs")
                                    arrLabTestCart.add(mutableDelectDic)
                                    UserDefaults.standard.set(arrLabTestCart, forKey: "arrlabtestcart")
                                    UserDefaults.standard.synchronize()
                                }
                                self.onCartbuttonClicked(nil)
                            }
                            
                        }
                    }
                }
                catch let exeptionError as Error? {
                    print(exeptionError!.localizedDescription)
                    SVProgressHUD.dismiss()
                }
            }
            
        })
        dataTask.resume()
    }
        
    func apicall(is_firsttime: String,SearchKeyWord:String)
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        if is_firsttime == "1"
        {
            SVProgressHUD.show()
        }
        else
        {
            activityIndicator?.startAnimating()
            tbllist.tableFooterView = activityIndicator
        }
        
        let parameters:NSMutableDictionary = NSMutableDictionary()
        parameters.setValue(page_Limit, forKeyPath: "SkipPages")
        parameters.setValue("10", forKeyPath: "Records")
        parameters.setValue(SearchKeyWord, forKeyPath: "SearchKeyWord")
        if UserDefaults.standard.object(forKey: "selectCityid") != nil {
            let CityId = UserDefaults.standard.object(forKey: "selectCityid") as! String
            parameters.setValue(CityId, forKeyPath: "CityId")
        }
        
        let postData = try! JSONSerialization.data(withJSONObject: parameters, options: [])

        var strUrl = String()
        if SearchKeyWord.length != 0
        {
            strUrl = K.URL.searchlabtestbyword
        }
        else
        {
            strUrl = K.URL.GetAllLabTests
        }
        
        let request = NSMutableURLRequest(url: NSURL(string: strUrl)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 120.0)
        request.httpMethod = "POST"
        let headers = [
            "content-type": "application/json"
        ]
        request.allHTTPHeaderFields = headers
        
        request.httpMethod = "POST"
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            self.activityIndicator?.stopAnimating()
            self.tbllist.tableFooterView = nil
            SVProgressHUD.dismiss()
            if let httpResponse = response as? HTTPURLResponse {
                //print("Status code: (\(httpResponse.allHeaderFields["Content-Encoding"] ?? ""))")
                if httpResponse.statusCode == 200
                {
                    do {
                        // gunzip
                        let decompressedData: Data
                        if (data?.isGzipped)! {
                            decompressedData = (try! data?.gunzipped())!
                        } else {
                            decompressedData = data!
                        }
                        //if  let json = try! JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves) as? NSDictionary
                        //{
                        //if  let json = try! JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves) as? NSArray
                        let json1 = try JSONSerialization.jsonObject(with: decompressedData) as? [String: Any]
                        print(json1 as Any)
                        
                        if let json = try! JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves) as? NSDictionary
                        {
                            DispatchQueue.main.async {
                                
                                self.TotalRecords = Int("\(json["TotalRecords"] ?? 0)")!
                                
                                if let data11 = json["LabTestList"]
                                {
                                    if (!(data11 as AnyObject).isKind(of: NSNull.self))
                                    {
                                        let arrLabTestListTemp =  NSMutableArray(array: json["LabTestList"] as! NSArray)//NSMutableArray(array: json)
                                        
                                        let labArray:NSMutableArray = NSMutableArray()
                                        for i in 0..<arrLabTestListTemp.count
                                        {
                                            let dictemp = arrLabTestListTemp.object(at: i) as! Dictionary<AnyHashable, Any>
                                            let labDetails11 =  dictemp.nullKeyRemoval()
                                            /*let Labstemp = NSMutableArray(array:labDetails11["Labs"] as! NSArray)
                                             for j in 0..<Labstemp.count
                                             {
                                             let dictemp1 = Labstemp.object(at: j) as! Dictionary<AnyHashable, Any>
                                             let labDetails111 =  dictemp1.nullKeyRemoval()
                                             Labstemp.insert(labDetails111, at: j)
                                             }
                                             for k in 0..<Labstemp.count
                                             {
                                             let dic = NSMutableDictionary(dictionary: Labstemp.object(at: k) as! NSDictionary)
                                             
                                             
                                             if let OtherNames = dic.object(forKey: "OtherNames")
                                             {
                                             if dic["OtherNames"] is NSNull
                                             {
                                             dic.setValue("", forKeyPath: "OtherNames")
                                             }
                                             else
                                             {
                                             dic.setValue(OtherNames, forKeyPath: "OtherNames")
                                             }
                                             }
                                             else
                                             {
                                             dic.setValue("", forKeyPath: "OtherNames")
                                             }
                                             
                                             if let ShortDescription = dic.object(forKey: "ShortDescription")
                                             {
                                             if dic["ShortDescription"] is NSNull
                                             {
                                             dic.setValue("", forKeyPath: "ShortDescription")
                                             }
                                             else
                                             {
                                             dic.setValue(ShortDescription, forKeyPath: "ShortDescription")
                                             }
                                             }
                                             else
                                             {
                                             dic.setValue("", forKeyPath: "ShortDescription")
                                             }
                                             
                                             
                                             Labstemp.removeObject(at: k)
                                             Labstemp.insert(dic, at: k)
                                             }
                                             tempdic.setValue(Labstemp, forKey: "Labs")
                                             */
                                            let tempdic = NSMutableDictionary(dictionary: labDetails11)
                                            
                                            labArray.add(tempdic)
                                        }
                                        
                                        if labArray.count != 0
                                        {
                                            for dic in labArray
                                            {
                                                let dicTemp = NSMutableDictionary(dictionary: dic as! NSDictionary)
                                                let strUserID = dicTemp.object(forKey: "LabTestId")
                                                let arrUserID = self.arrLabTestList.value(forKey: "LabTestId") as! NSArray
                                                if arrUserID.contains(strUserID ?? "")
                                                {
                                                    //nothing
                                                }
                                                else
                                                {
                                                    self.arrLabTestList.add(dic)
                                                    self.arrSearch_LabTestList.add(dic)
                                                }
                                            }
                                            self.tbllist.reloadData()
                                        }
                                        
                                        //                                if labArray.count != 0
                                        //                                {
                                        //                                    self.arrLabTestList = labArray
                                        //                                    self.arrSearch_LabTestList = labArray
                                        //
                                        //
                                        //
                                        //                                    // UserDefaults.standard.set(self.arrLabTestList, forKey: "arrGetAllLabTests")
                                        //                                    // UserDefaults.standard.synchronize()
                                        //                                }
                                        print(data11)
                                    }
                                }
                                else
                                {
                                    
                                }
                                
                                //                            self.arrLabs = LabDetails.modelsFromDictionaryArray(array: dictionary as! Array<Dictionary<String,Any>>)
                                //                            self.setData()
                         
                                if self.arrLabTestList.count == 0
                                {
                                    ISMessages.show("This service is not available in your current selected city")
                                }

                            }
                            
                        }
                        
                    }
                    catch let exeptionError as Error? {
                        print(exeptionError!.localizedDescription)
                        SVProgressHUD.dismiss()
                    }
                }
                else
                {
                    ISMessages.show("This service is not available in your current selected city")
                }
            }
        })
        dataTask.resume()
    }
}


