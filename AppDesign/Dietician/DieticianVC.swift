//
//  DieticianVC.swift
//  AppDesign
//
//  Created by sachin on 15/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class DieticianVC: BaseViewController
{
    @IBOutlet weak var tbllist: UITableView!
    
    //var arrHomeCare: Array<HomeCare> = []
    
    var arrDieticianList:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        addCartButton()
        
        if UserDefaults.standard.object(forKey: "arrDieticianList") != nil
        {
            self.arrDieticianList = NSMutableArray(array: UserDefaults.standard.object(forKey: "arrDieticianList") as! NSArray)
            self.tbllist.reloadData()
            
            if self.arrDieticianList.count != 0
            {
                getHomeCareDataWebservice(is_firsttime: "")
            }
            else
            {
                getHomeCareDataWebservice(is_firsttime: "1")
            }
        }
        else
        {
            getHomeCareDataWebservice(is_firsttime: "1")
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnback(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnorder(_ sender: Any) {
    }
    
    override func viewDidLayoutSubviews()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                removeView()
                addCartButton()
            }
            else
            {
                removeView()
                
                addCartButton()
            }
            
        } else {
            print("Portrait")
            //Image Height
            removeView()
            addCartButton()
        }
    }
    
    
    //MARK: - Webservice
    func getHomeCareDataWebservice(is_firsttime: String)
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        if is_firsttime == "1" {
            SVProgressHUD.show()
        }
        do {
            
            
           
            let parameters:NSMutableDictionary = NSMutableDictionary()
            parameters.setValue("All", forKeyPath: "SelectedLetter")
            parameters.setValue("0", forKeyPath: "SkipPages")
            parameters.setValue("20", forKeyPath: "Records")
            parameters.setValue("0", forKeyPath: "MedicalSpecialityId")
            parameters.setValue("DIET", forKeyPath: "ProductTypePrefix")
            
            print(parameters)
            
            let postData = try! JSONSerialization.data(withJSONObject: parameters, options: [])
            
            let request = NSMutableURLRequest(url: NSURL(string: K.URL.GET_HOMECARE_DATA)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            let headers = [
                "content-type": "application/json"
            ]
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                
                SVProgressHUD.dismiss()
                if error != nil {
                    print(error ?? "Erroer Found")
                    
                }
                else{
                    
                    do {
                        // gunzip
                        let decompressedData: Data
                        if (data?.isGzipped)! {
                            decompressedData = (try! data?.gunzipped())!
                        } else {
                            decompressedData = data!
                        }
                        if  let json = try! JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves) as? NSDictionary
                        {
                            DispatchQueue.main.async {
                                print(json)
                                
                                let arrLabTestListTemp = NSMutableArray(array: json["Procedures"] as! NSArray)
                                
                                let labArray:NSMutableArray = NSMutableArray()
                                for i in 0..<arrLabTestListTemp.count
                                {
                                    let dictemp = arrLabTestListTemp.object(at: i) as! Dictionary<AnyHashable, Any>
                                    let labDetails11 =  dictemp.nullKeyRemoval()
                                    labArray.add(labDetails11)
                                }
                                
                                self.arrDieticianList = labArray
                                self.tbllist.reloadData()
                                
                                UserDefaults.standard.set(self.arrDieticianList, forKey: "arrDieticianList")
                                UserDefaults.standard.synchronize()
                            }
                    
                        }
                    }
                    catch let exeptionError as Error? {
                        print(exeptionError!.localizedDescription)
                        SVProgressHUD.dismiss()
                    }
                }
            })
            dataTask.resume()
        }
        catch
        {
            print(error)
        }
        
        /* _ = WebClient.requestWithUrl(url: K.URL.GET_HOMECARE_DATA, parameters: reqParam, method: "POST") { (responseObject, error) in
         if error == nil {
         print("Data: \(responseObject!)")
         if let _ = responseObject {
         if let dictData = (responseObject) {
         let arrLabTestListTemp = NSMutableArray(array: dictData["Procedures"] as! NSArray)
         
         let labArray:NSMutableArray = NSMutableArray()
         for i in 0..<arrLabTestListTemp.count
         {
         let dictemp = arrLabTestListTemp.object(at: i) as! Dictionary<AnyHashable, Any>
         let labDetails11 =  dictemp.nullKeyRemoval()
         labArray.add(labDetails11)
         }
         
         self.arrDieticianList = labArray
         self.tbllist.reloadData()
         
         UserDefaults.standard.set(self.arrDieticianList, forKey: "arrDieticianList")
         UserDefaults.standard.synchronize()
         
         }
         }
         
         }else {
         ISMessages.show(error?.localizedDescription)
         }
         SVProgressHUD.dismiss()
         }
         }*/
        
    }
}
//MARK:- Tableview Delegate Method
extension DieticianVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrDieticianList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let mainview = cell.contentView.viewWithTag(1)
        let imgmain = cell.contentView.viewWithTag(2) as! UIImageView
        let lbltitle = cell.contentView.viewWithTag(3) as! UILabel
        let lblprice = cell.contentView.viewWithTag(4) as! UILabel
        let lblothername = cell.contentView.viewWithTag(5) as! UILabel
        let vieworder = cell.contentView.viewWithTag(6)
        let btnaddtocart = cell.contentView.viewWithTag(7) as! UIButton
        
        Common.SetShadowwithCorneronView(view: mainview!, cornerradius: 10.0, color:UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: vieworder!, cornerradius: 10.0,color: UIColor.clear)
        
        let homeCare = NSMutableDictionary(dictionary: arrDieticianList[indexPath.row] as! NSDictionary)
        
        lbltitle.text = (homeCare.object(forKey: "ProductLine_ProductName") as! String)
        lblprice.text = NSString(format: "Price: Rs. %@",Utility.setCurrencyFormate(NSString(format: "%d", homeCare.object(forKey: "ProductLine_DiscountedPrice") as! Int) as String as String)) as String
        lblothername.text = NSString(format: "Other Name: %@", homeCare.object(forKey: "MedicalProductOtherNames") as! String) as String
        
        imgmain.image = #imageLiteral(resourceName: "bannerPlaceholder")
        AppUtility.shared.getImage(imageID: Int(homeCare.object(forKey: "MedicalProductImageUrlWeb") as! String)!, completion: { (success, error, image) in
            if success == true {
                if image != nil {
                    imgmain.image = image
                }
            }
        })
        
        btnaddtocart.addTapGesture { (gesture) in
            if UserDefaults.standard.value(forKey: "arrhomecarecart") == nil
            {
                var labDetails1 = Dictionary<AnyHashable, Any>()
                labDetails1 = homeCare as! [AnyHashable : Any]
                let labDetails11 =  labDetails1.nullKeyRemoval()
                
                let arrLabTestCart:NSMutableArray = NSMutableArray()
                arrLabTestCart.add(labDetails11)
                UserDefaults.standard.set(arrLabTestCart, forKey: "arrhomecarecart")
                UserDefaults.standard.synchronize()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    self.onCartbuttonClicked(nil)
                })
            }
            else
            {
                //var arrLabTestCart = NSArray()
                let arrLabTestCart:NSMutableArray = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrhomecarecart") as! NSArray)
                var istrue = "yes"
                for i in 0..<arrLabTestCart.count
                {
                    let dicLab = arrLabTestCart[i] as! NSDictionary
                    let labidtemp = dicLab.object(forKey: "ProductLineId") as! Int
                    let labidtemp11 = homeCare.object(forKey: "ProductLineId") as! Int
                    
                    if labidtemp11 == labidtemp
                    {
                        istrue = "no"
                    }
                }
                if istrue == "yes"
                {
                    var labDetails1 = Dictionary<AnyHashable, Any>()
                    labDetails1 = homeCare as! [AnyHashable : Any]
                    let labDetails11 =  labDetails1.nullKeyRemoval()
                    
                    arrLabTestCart.add(labDetails11)
                    UserDefaults.standard.set(arrLabTestCart, forKey: "arrhomecarecart")
                    UserDefaults.standard.synchronize()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                        self.onCartbuttonClicked(nil)
                    })
                }
                else
                {
                    ISMessages.show("Product Already Exits in Cart")
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 129
        
    }
}


