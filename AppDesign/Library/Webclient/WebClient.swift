//
//  WebClient.swift
//  WebClient-Swift
//
//  Created by Harry on 19/11/16.
//
//

import UIKit
import Alamofire

var kInternetConnectionMessage = "Please check your internet connection"
var kTryAgainLaterMessage = "Please try again later"
var kErrorDomain = "Error"
var kDefaultErrorCode = 1234

var kSuccessKey = "status"
var kMessageKey = "message"
var kDataKey = "data"

/**
# MultipartFormDataBlock
 
**Snippet**
multipartFormData.appendBodyPart(data: name : )
multipartFormData.appendBodyPart(data: name : mimeType:)

**Example**

    
    multipartFormData: { multipartFormData in
    multipartFormData.appendBodyPart(data: self.name.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"article[name]")
    for var i = 0; i < self.photoArray.count; i++ {
        let imageData = UIImageJPEGRepresentation(self.photoArray[i], 1)

        multipartFormData.appendBodyPart(data: imageData!, name: "article[article_images_attributes][][image]", mimeType: "image/jpeg")
 
    }
*/
typealias MultipartFormDataBlock = (_ multipartFormData : MultipartFormData?) -> Void
typealias CompletionBlock = (_ response: Dictionary<String, Any>?, _ error: Error?) -> Void
typealias DownloadCompletionBlock = (_ filePath: String?, _ fileData: Data?, _ error: Error?) -> Void
typealias ResponseHandler = (_ response:DataResponse<Any>) -> Void

class WebClient: NSObject {
    
    //MARK: - Request
    class func requestWithUrl(url: String, parameters: Dictionary<String, Any>?, method: String, completion: CompletionBlock?) -> DataRequest? {
        
        var param = parameters ?? Dictionary()
        //param["auth_key"] = User.loggedInUser()?.authKey
//        print("Param = \(param)")


        var headers: HTTPHeaders =  [
            "Content-Type":"application/x-www-form-urlencoded",
            "Accept":"application/json",
//            "Transfer-Encoding":"chunked"
        ]

        if User.loggedInUser()?.accessToken.isValid == true {
            headers["Authorization"] = "Bearer \(User.loggedInUser()?.accessToken ?? "")"
        }
        
        
        return Alamofire.request(url, method: HTTPMethod(rawValue: method)!, parameters: param, encoding: URLEncoding.httpBody, headers: headers)
            .validate()            
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    handleResponse(response: response, completion: completion)
                case .failure(let error):
                    print(error.localizedDescription)
                    completion?(nil, error)
                }
        }
    }
    
    //MARK: - Upload requests
    
/**
#   Multipart uploads
     
*[MultipartFormDataBlock](@MultipartFormDataBlock)
     
    - parameters:
     - url: The url in string where the data needs to be uploaded
     - multiPartFormDataBlock: Is a block of type MultipartFormDataBlock
     - completion: Is a block of type CompletionBlock
*/
    
    class func multiPartRequestWithUrl(url: String, parameters: Dictionary<String,Any>?, multiPartFormDataBlock: MultipartFormDataBlock?, completion: CompletionBlock?) -> Void {
        var param : Dictionary! = parameters ?? Dictionary()
//        param["auth_key"] = User.loggedInUser()?.authKey
        //        print("Param = \(param)")
        Alamofire.upload(multipartFormData: { (multiPartFormData) in
            multiPartFormDataBlock?(multiPartFormData)            
            if param != nil {
                for (key, value) in param! {
                    multiPartFormData.append("\(value)".data(using: .utf8)!, withName: key)
                }
            }
            
        }, to: url, encodingCompletion:{encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    switch response.result {
                    case .success(let value):
                        handleResponse(response: value, completion: completion)
                    case .failure(let error):
                        print(error.localizedDescription)
                        completion?(nil, error)
                    }
                }
            case .failure(let encodingError):
                completion?(nil, encodingError)
            }
            
        })
    }
    /*
    //MARK: - Upload Media
    
    class func uploadMedia(url: String = K.URL.UPLOAD_FILE, folderName: String, image : UIImage?, videoURL : URL? = nil ,needToCreateThumb : Bool = true, completion: CompletionBlock?) -> Void {
      
            var param : Dictionary<String,Any> = Dictionary()
            param["dir_name"] = folderName
            param["thumb_create"] = needToCreateThumb ? 1 : 0
            let imageToUpload = image?.fixOrientation()
            print("Param = \(param)")
            Alamofire.upload(multipartFormData: { (multiPartFormData) in
                
                if videoURL != nil {
                    do {
                        let videoData = try Data(contentsOf: videoURL!)
                         multiPartFormData.append(videoData, withName: "file", fileName: "video.mp4", mimeType: "mp4")
                        
                    } catch let error as NSError {
                        print("Error: \(error.localizedDescription)")
                    }
                }else if imageToUpload != nil {
                     multiPartFormData.append(UIImageJPEGRepresentation(imageToUpload!, 0.7)!, withName: "file", fileName: "image.jpeg", mimeType: "image/jpeg")
                }
               
                for (key, value) in param {
                    multiPartFormData.append("\(String(describing: value))".data(using: .utf8)!, withName: key)
                }
                
            }, to: url, encodingCompletion:{encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        switch response.result {
                        case .success(let value):
                            let dictData = value as! NSDictionary
                            let success = dictData[kSuccessKey] as! Bool
                            if success {
                                let filename = dictData.value(forKeyPath: "data.filename")
                                completion?(filename as? Dictionary<String, Any>, nil)
                            }
                            else {
                                completion?(nil, NSError(domain: "", code: kDefaultErrorCode, userInfo: [NSLocalizedDescriptionKey :dictData[kMessageKey] as! String ]))
                            }
                            
                        case .failure(let error):
                            print(error.localizedDescription)
                            completion?(nil, error)
                        }
                    }
                case .failure(let encodingError):
                    completion?(nil, encodingError)
                }
            })
        
    }
    
    class func uploadImage(url: String = K.URL.UPLOAD_FILE, folderName: String, image:UIImage?, completion: CompletionBlock?) -> Void {
        uploadMedia(url: url, folderName: folderName, image: image, completion: completion)
        
        if image == nil {
            completion?(nil, nil)
        }else {
            var param : Dictionary<String,Any> = Dictionary()            
            param["dir_name"] = folderName
            print("Param = \(param)")
            let imageToUpload = image?.fixOrientation()
            Alamofire.upload(multipartFormData: { (multiPartFormData) in
                if imageToUpload != nil {
                     multiPartFormData.append(UIImageJPEGRepresentation(imageToUpload!, 0.5)!, withName: "file", fileName: "image.jpeg", mimeType: "image/jpeg")
                }
                for (key, value) in param {
                    multiPartFormData.append("\(String(describing: value))".data(using: .utf8)!, withName: key)
                }
                
            }, to: url, encodingCompletion:{encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        switch response.result {
                        case .success(let value):
                            let dictData = value as! NSDictionary
                            let success = dictData[kSuccessKey] as! Bool
                            if success {
                                let filename = dictData.value(forKeyPath: "data.filename")
                                completion?(filename as? Dictionary<String, Any>, nil)
                            }
                            else {
                                completion?(nil, NSError(domain: "", code: kDefaultErrorCode, userInfo: [NSLocalizedDescriptionKey :dictData[kMessageKey] as! String ]))
                            }
                            
                        case .failure(let error):
                            print(error.localizedDescription)
                            completion?(nil, error)
                        }
                    }
                case .failure(let encodingError):
                    completion?(nil, encodingError)
                }
            })
        }
    }
    
    
    // main queue by default
    class func upload(file:URL,url:String, progressHandler:@escaping Request.ProgressHandler, responseHandler:@escaping ResponseHandler){
        Alamofire.upload(file, to: url)
            .uploadProgress { progress in
                progressHandler(progress)
            }
            .downloadProgress { progress in
                progressHandler(progress)
            }
            .responseJSON { response in
                responseHandler(response)
        }
    }
 */
 
    //MARK: - Handle Response
    
    class func handleResponse(response: Any?, completion: CompletionBlock?) {
        //print(response ?? "Response nil")
        let error = NSError(domain: kErrorDomain, code: kDefaultErrorCode, userInfo: [NSLocalizedDescriptionKey: kTryAgainLaterMessage])
        if !(response is Any) {
            completion?(nil, error)
        }
        else {
            
            //let success = (response as! [AnyHashable: Any])[kSuccessKey] as! Int
            let success = (response as? DataResponse<Any>)?.response?.statusCode ?? 0
            if success == 200 {
                switch (response as? DataResponse<Any>)!.result {
                case .success(let value):
                    completion?((value as? Dictionary<String, Any>), nil)
                case .failure(let error):
                    print(error.localizedDescription)
                    completion?(nil, error)
                }
            }
            else {
                completion?(nil, NSError(domain: "", code: 1, userInfo: [NSLocalizedDescriptionKey : "Error occured, Please try again"]))
            }
/*            //let success = (response as! [AnyHashable: Any])["success"] as? Dictionary<String,Any>
            
            //if success == 1 {
            if (success?["access_token"] as? String ?? "").isValid == true {
                completion?(response as? Dictionary<String, Any>, nil)
            }
            else if let data = success?["data"] as? Dictionary<String,Any> {
                completion?(response as? Dictionary<String, Any>, nil)
            }
            else if let array = success?["country"] as? Array<Dictionary<String,Any>> {
                completion?(success as? Dictionary<String, Any>, nil)
            }
            else {
//            else if success == -1 {
//                completion?(nil, NSError(domain: "", code: (response as! [AnyHashable: Any])[kSuccessKey] as! Int, userInfo: [NSLocalizedDescriptionKey :(response as! [AnyHashable: Any])[kMessageKey] as! String ]))
//
//            }else {
//                completion?(nil, NSError(domain: "", code: (response as! [AnyHashable: Any])[kSuccessKey] as! Int, userInfo: [NSLocalizedDescriptionKey :(response as! [AnyHashable: Any])[kMessageKey] as! String ]))
            completion?(nil, NSError(domain: "", code: 1, userInfo: [NSLocalizedDescriptionKey : "Error occured, Please try again"]))
            }*/
        }
    }
 
    
    // MARK: - Download File
    
    class func downloadFile(url: String, downloadCompletionBlock: DownloadCompletionBlock?) -> DownloadRequest? {
        return Alamofire.download(url)
            .downloadProgress(closure: { progress in
                
            })
            .responseData { response in
                switch response.result {
                case .success(let value):
                    downloadCompletionBlock?(nil, value, nil)
                case .failure(let error):
                    print(error.localizedDescription)
                    downloadCompletionBlock?(nil, nil, error)
                }
        }
    }
    
    class func downloadFile(url: String, atUrl: URL, downloadCompletionBlock: DownloadCompletionBlock?) -> DownloadRequest? {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (atUrl, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        return Alamofire.download(url, to: destination).response { response in
            if response.error == nil, let imagePath = response.destinationURL?.path {
                downloadCompletionBlock?(imagePath, nil, nil)
            }
            else {
                downloadCompletionBlock?(nil, nil, response.error)
            }
        }
    }
}


