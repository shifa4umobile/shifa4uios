//
//  ChangeCityVC.swift
//  AppDesign
//
//  Created by sachin on 10/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class ChangeCityVC: BaseViewController {
    
    @IBOutlet weak var viewmain: UIView!
    @IBOutlet weak var btnselectlocation: UIButton!
    @IBOutlet weak var btnsave: UIButton!
    var dictUserData: Dictionary<String,Any> = [:]
    var countryID: Int = -1
    var cityID: Int = -1
    
    let chooseArticleDropDown = DropDown()
    var arrCityList = NSArray()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown
        ]
    }()
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }
    
    
    override func viewDidLoad()
    {
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
    
        super.viewDidLoad()
        //Add menu button
        addSlideMenuButton()
        addCartButton()
        
        Common.SetShadowwithCorneronView(view: btnsave, cornerradius: 10.0,color:UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.setCornerOnView(view: viewmain, cornerradius: 10.0)
        let val = UserDefaults.standard.value(forKey: K.Key.City) as? String
        btnselectlocation.setTitle(val, for: .normal)
        
    }
    
    override func viewDidLayoutSubviews()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                removeView()
                addCartButton()
            }
            else
            {
                removeView()
                
                addCartButton()
            }
            
        } else {
            print("Portrait")
            //Image Height
            removeView()
            addCartButton()
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getCityData()
    }
    
    func getCityData()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        if let data1 = UserDefaults.standard.value(forKey: K.Key.Country)
        {
            countryID = Int(UserDefaults.standard.object(forKey: "\(K.Key.Country)") as! String)!
            if countryID == -1 {
                ISMessages.show("Please select country")
            }
            cityID = -1
            
            AppUtility.shared.getCity(countryID: countryID, completion: { (success, error, arrayList) in
                if success == true {
                    DispatchQueue.main.async {
                        if arrayList != nil {
                            self.arrCityList = NSArray(array: arrayList!)
                        }
                    }
                }
            })
        }
    }
    
    
    @IBAction func btnsave(_ sender: Any)
    {
        setGlobalCityWebservice()
    }
    
    @IBAction func btnlocation(_ sender: Any)
    {

        chooseArticleDropDown.anchorView = btnselectlocation
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (btnselectlocation?.bounds.height)!)
        
        let arrLabsTemp = NSMutableArray()
        for i in 0..<arrCityList.count
        {
            let labname = (arrCityList.object(at: i) as! NSDictionary).object(forKey: "CityName")
            arrLabsTemp.add(labname as Any)
        }
        
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            
            self!.btnselectlocation?.setTitle(item, for: .normal)
            
            if let value = (self?.arrCityList.object(at: index) as! NSDictionary).object(forKey: "CityName") as? String {
                self?.cityID = (self?.arrCityList.object(at: index) as! NSDictionary).object(forKey: "CityId") as? Int ?? -1
                self!.btnselectlocation?.setTitle(value, for: .normal)
                //self.btnCity.setTitle(value, for: .normal)
                UserDefaults.standard.set(value, forKey: K.Key.City)
                UserDefaults.standard.synchronize()
            }
            
            self?.chooseArticleDropDown.hide()
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
        }
        
        chooseArticleDropDown.show()
    }
    
    func showCityPicker(arrayList: Array<Dictionary<String,Any>> , sender:Any) {
        if arrayList.count == 0 {
            return
        }
        let lblNotificationText = sender as? UIButton
        
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (btnselectlocation?.bounds.height)!)
        RMPickerViewController.show(withTitle: nil, selectedIndex:0, array: arrayList.map({$0["CityName"]}) as! [String], doneBlock: { (index, value) in
            DispatchQueue.main.async {
                if let value = arrayList[index]["CityName"] as? String {
                    self.cityID = arrayList[index]["CityId"] as? Int ?? -1
                    lblNotificationText?.setTitle(value, for: .normal)
                    //self.btnCity.setTitle(value, for: .normal)
                    UserDefaults.standard.set(value, forKey: K.Key.City)
                    UserDefaults.standard.synchronize()
                }
            }
        }, cancel: {
            
        })
    }
    func setGlobalCityWebservice() {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        if cityID == -1 {
            ISMessages.show("Please select city")
            return
        }
        SVProgressHUD.show()
        
        let para = ["cityId" : cityID]
        let url = URL(string: K.URL.SET_GLOBAL_CITY)
        
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: para as Any as Any, options: .prettyPrinted)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request as URLRequest) {(data, response, error) in
            SVProgressHUD.dismiss()
            DispatchQueue.main.async {
                if let httpResponse = response as? HTTPURLResponse {
                    print("Status code: (\(httpResponse.statusCode))")
                    if httpResponse.statusCode == 200
                    {
                        
                        // gunzip
                        let decompressedData: Data
                        if (data?.isGzipped)! {
                            decompressedData = (try! data?.gunzipped())!
                        } else {
                            decompressedData = data!
                        }
                        let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
                        if dictionary != nil {
                            print(dictionary!)
                        }
                        
                        if self.cityID == -1
                        {
                            UserDefaults.standard.set(nil, forKey: "selectCityid")
                            UserDefaults.standard.synchronize()
                        }
                        else
                        {
                            UserDefaults.standard.set("\(self.cityID)", forKey: "selectCityid")
                            UserDefaults.standard.synchronize()
                        }
                            DispatchQueue.main.async {
                       ISMessages.show("Change city successfully!")
                       AppUtility.sharedAppDelegate.setMainNavigationController()
                        }
                    }
                    else
                    {
                        ISMessages.show("Error occurred, Please try again")
                    }
                }
                else
                {
                    ISMessages.show("Error occurred, Please try again")
                }
            }
        }
        task.resume()
    }
}
