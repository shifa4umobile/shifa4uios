//
//  MedicalPackagesVC.swift
//  AppDesign
//
//  Created by sachin on 20/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class MedicalPackagesVC: BaseViewController
{
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tbllist: UITableView!
    
    //    var arrMedicalPackage: Array<MedicalDetail> = []
    //    var arrMedicalPackageDisplay: Array<MedicalDetail> = []
    
    var arrPackageList:NSMutableArray = NSMutableArray()
    var arrSearch_PackageList:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Add menu button
        //addSlideMenuButton()
        
        
        Common.SetShadowwithCorneronView(view: viewSearch, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        
        if UserDefaults.standard.object(forKey: "GetAllLabPackages") != nil
        {
            
            self.arrPackageList = NSMutableArray(array: UserDefaults.standard.object(forKey: "GetAllLabPackages") as! NSArray)
            
            self.arrSearch_PackageList = NSMutableArray(array:UserDefaults.standard.object(forKey: "GetAllLabPackages") as! NSArray)
            
            self.tbllist.reloadData()
            if self.arrPackageList.count != 0
            {
                apicall(is_firsttime: "0")
            }
            else
            {
                apicall(is_firsttime: "1")
            }
        }
        else
        {
            apicall(is_firsttime: "1")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addCartButton()
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                removeView()
                addCartButton()
            }
            else
            {
                removeView()
                
                addCartButton()
            }
            
        } else {
            print("Portrait")
            //Image Height
            removeView()
            addCartButton()
        }
    }
    
    
    //MARK: - IBActions
    
    @IBAction func btnBack(_ sender: Any) {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    
    @IBAction func btnfilter(_ sender: Any)
    {
        
    }
    
    @IBAction func btninfo(_ sender: Any)
    {
        
    }
    
    @IBAction func btnaddtocart(_ sender: Any)
    {
        
    }
    
    
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        setData()
    }
    
    
    //MARK: - Other Action
    func setData() {
        //        arrMedicalPackageDisplay.removeAll(keepingCapacity: false)
        //        if txtSearch.text?.isValid == true {
        //            arrMedicalPackageDisplay = self.arrMedicalPackage.filter({$0.name.contains((txtSearch.text?.trim())!, compareOption: NSString.CompareOptions.caseInsensitive)})
        //        }
        //        else {
        //            arrMedicalPackageDisplay.append(contentsOf: self.arrMedicalPackage)
        //        }
        tbllist.reloadData()
    }
}


//MARK:- UITextField Delegate
extension MedicalPackagesVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //arrPackageList = NSMutableArray(array: arrSearch_PackageList)
        //tbllist.reloadData()
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let newString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        if textField == txtSearch
        {
            if newString?.count == 0
            {
                arrPackageList = NSMutableArray(array: arrSearch_PackageList)
                tbllist.reloadData()
            }
            else
            {
                var tmpary = [AnyHashable]()
                let predicate = NSPredicate(format: "SELF.Name contains[c] %@", newString!)
                tmpary = arrSearch_PackageList.filtered(using: predicate) as! [AnyHashable]
                arrPackageList = NSMutableArray(array: tmpary)
                tbllist.reloadData()
            }
        }
        return true
    }
}


//MARK:- Tableview Delegate Method
extension MedicalPackagesVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrPackageList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let viewmain = cell.contentView.viewWithTag(1)
        let lbltitle = cell.contentView.viewWithTag(2) as! UILabel
        let lblrecommended = cell.contentView.viewWithTag(3) as! UILabel
        let lbldesc = cell.contentView.viewWithTag(4) as! UILabel
        
        let viewbtnaddtocart = cell.contentView.viewWithTag(5)
        let btnaddtocart = cell.contentView.viewWithTag(6) as! UIButton
        
        let viewbtndetail = cell.contentView.viewWithTag(7)
        //let btndetail = cell.contentView.viewWithTag(8) as! UIButton
        
        Common.SetShadowwithCorneronView(view: viewmain!, cornerradius: 10.0, color:UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.setCornerOnView(view: lblrecommended, cornerradius: 4.0)
        Common.SetShadowwithCorneronView(view: viewbtnaddtocart!, cornerradius: 10.0, color:UIColor.clear)
        Common.SetShadowwithCorneronView(view: viewbtndetail!, cornerradius: 10.0, color:UIColor.clear)
        
        let dicmadicalPackage = NSMutableDictionary(dictionary: arrPackageList[indexPath.row] as! NSDictionary)
        
        lbldesc.text = (dicmadicalPackage.object(forKey: "ShortDescription") as! String)
        lbltitle.text = (dicmadicalPackage.object(forKey: "Name") as! String)
        
        
        btnaddtocart.addTapGesture { (gesture) in
            //add item in cart array
            if UserDefaults.standard.value(forKey: "arrmedicalpackagecart") == nil
            {
                var labDetails1 = Dictionary<AnyHashable, Any>()
                labDetails1 = dicmadicalPackage as! [AnyHashable : Any]
                let labDetails11 =  labDetails1.nullKeyRemoval()
                
                let arrLabTestCart:NSMutableArray = NSMutableArray()
                arrLabTestCart.add(labDetails11)
                UserDefaults.standard.set(arrLabTestCart, forKey: "arrmedicalpackagecart")
                UserDefaults.standard.synchronize()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    self.onCartbuttonClicked(nil)
                })
            }
            else
            {
                //var arrLabTestCart = NSArray()
                let arrLabTestCart:NSMutableArray = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrmedicalpackagecart") as! NSArray)
                var istrue = "yes"
                for i in 0..<arrLabTestCart.count
                {
                    let dicLab = arrLabTestCart[i] as! NSDictionary
                    let labidtemp = dicLab.object(forKey: "LabPackageId") as! Int
                    let labidtemp11 = dicmadicalPackage.object(forKey: "LabPackageId") as! Int
                    
                    if labidtemp11 == labidtemp
                    {
                        istrue = "no"
                    }
                }
                if istrue == "yes"
                {
                    var labDetails1 = Dictionary<AnyHashable, Any>()
                    labDetails1 = dicmadicalPackage as! [AnyHashable : Any]
                    let labDetails11 =  labDetails1.nullKeyRemoval()
                    
                    arrLabTestCart.add(labDetails11)
                    UserDefaults.standard.set(arrLabTestCart, forKey: "arrmedicalpackagecart")
                    UserDefaults.standard.synchronize()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                        self.onCartbuttonClicked(nil)
                    })
                }
                else
                {
                    ISMessages.show("Product Already Exits in Cart")
                }
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dicmadicalPackage = NSMutableDictionary(dictionary: arrSearch_PackageList[indexPath.row] as! NSDictionary)
        var labDetails1 = Dictionary<AnyHashable, Any>()
        labDetails1 = dicmadicalPackage as! [AnyHashable : Any]
        let labDetails11 =  labDetails1.nullKeyRemoval()
        
        
        let medicalPackageDetailVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "MedicalPackagesDetailVC") as! MedicalPackagesDetailVC
        medicalPackageDetailVC.dicSelectPackage = NSMutableDictionary(dictionary: labDetails11)
        AppUtility.shared.mainNavController.pushViewController(medicalPackageDetailVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
        
    }
    
    func apicall(is_firsttime: String)
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        if is_firsttime == "1" {
            SVProgressHUD.show()
        }
        
        var CityId = String()
        if UserDefaults.standard.object(forKey: "selectCityid") != nil {
            CityId = UserDefaults.standard.object(forKey: "selectCityid") as! String
        }
        
        let strUrl = NSString(format: "%@?cityId=%@", K.URL.GetAllLabPackages,CityId)
        
        let url = URL(string: strUrl as String)
        var urlRequest = URLRequest(url: url!)
        urlRequest.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        urlRequest.timeoutInterval = 60 * 5
        urlRequest.httpMethod = "GET"
        urlRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        //urlRequest.addValue("Bearer \(User.loggedInUser()?.accessToken ?? "")", forHTTPHeaderField: "Authorization")
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, errorResponse) in
            SVProgressHUD.dismiss()
            if let httpResponse = response as? HTTPURLResponse {
                print("Status code: (\(httpResponse.statusCode))")
                if httpResponse.statusCode == 200
                {
                    do {
                        // gunzip
                        let decompressedData: Data
                        if (data?.isGzipped)! {
                            decompressedData = (try! data?.gunzipped())!
                        } else {
                            decompressedData = data!
                        }
                        if let dictionary = try JSONSerialization.jsonObject(with: decompressedData, options: .mutableContainers) as? [Any]
                        {
                            //self.arrMedicalPackage.removeAll(keepingCapacity: false)
                            DispatchQueue.main.async {
                                // print(dictionary)
                                //self.arrMedicalPackage = MedicalDetail.modelsFromDictionaryArray(array: dictionary as! Array<Dictionary<String,Any>>)
                                //self.setData()
                                
                                let arrLabTestListTemp = NSMutableArray(array: dictionary)
                                
                                let labArray:NSMutableArray = NSMutableArray()
                                for i in 0..<arrLabTestListTemp.count
                                {
                                    let dictemp = arrLabTestListTemp.object(at: i) as! Dictionary<AnyHashable, Any>
                                    let labDetails11 =  dictemp.nullKeyRemoval()
                                    labArray.add(labDetails11)
                                }
                                if labArray.count != 0
                                {
                                    self.arrPackageList = labArray
                                    self.arrSearch_PackageList = labArray
                                    self.tbllist.reloadData()
                                    
                                    // UserDefaults.standard.set(self.arrPackageList, forKey: "GetAllLabPackages")
                                    // UserDefaults.standard.synchronize()
                                }
                                
                                if self.arrPackageList.count == 0
                                {
                                    ISMessages.show("This service is not available in your current selected city")
                                }
                                
                            }
                        }
                    }
                    catch let exeptionError as Error? {
                        print(exeptionError!.localizedDescription)
                        SVProgressHUD.dismiss()
                    }
                }
                else
                {
                    ISMessages.show("This service is not available in your current selected city")
                }
            }
        })
        task.resume()
    }
}



