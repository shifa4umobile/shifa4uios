//
//  MedicalPackagesDetailVC.swift
//  AppDesign
//
//  Created by sachin on 20/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class MedicalPackagesDetailVC: BaseViewController
{
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var viewdetail: UIView!
    @IBOutlet weak var viewpric: UIView!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var lbldescription: UILabel!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var vieworder: UIView!
    @IBOutlet weak var btnchooselab: UIButton!
    @IBOutlet weak var tbllabtest: UITableView!
    @IBOutlet weak var heightviewdetail: NSLayoutConstraint!//130
    
    @IBOutlet weak var scrollMain: UIScrollView!
    
    var arrdata = NSMutableArray()
    
    let chooseArticleDropDown = DropDown()
    var medicalPackage: MedicalDetail? = nil
    var selectedLab: LabPackagesComparison? = nil

    var dicSelectPackage = NSMutableDictionary()
    var arrLabTests = NSMutableArray()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown
        ]
    }()
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
        
        Common.SetShadowwithCorneronView(view: viewdetail, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: vieworder, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: viewpric, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))

        
        lbltitle.text = (dicSelectPackage.object(forKey: "Name") as! String)
        lblsubtitle.text = (dicSelectPackage.object(forKey: "Name") as! String)
        lbldescription.text = (dicSelectPackage.object(forKey: "ShortDescription") as! String)

        
        btnchooselab.setTitle("Select Lab", for: .normal)
        lblprice.text = "0"

        arrLabTests = NSMutableArray(array: dicSelectPackage.object(forKey: "LabTests") as! NSArray)
        tbllabtest.reloadData()
    }

    override func viewDidLayoutSubviews()
    {
        //let height1 = Common.calculateHeight(inString: (lbldescription.text)!, fontSize: 11.0, frame: UIScreen.main.bounds.size.width/2)

        heightviewdetail.constant = 100 + 18 + lblsubtitle.frame.size.height + lbldescription.frame.size.height + tbllabtest.contentSize.height
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnback(_ sender: Any)
    {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    
    @IBAction func btnchooselab(_ sender: Any)
    {
        
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        
        let arrLabsTemp = NSMutableArray()
        for i in 0..<(dicSelectPackage.object(forKey: "LabPackagesComparison") as! NSArray).count
        {
            let labname = ((dicSelectPackage.object(forKey: "LabPackagesComparison") as! NSArray).object(at: i) as! NSDictionary).object(forKey: "LabName")
            arrLabsTemp.add(labname as Any)
        }
        
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in

            lblNotificationText?.setTitle(item, for: .normal)
            
            let arr = self?.dicSelectPackage.object(forKey: "LabPackagesComparison") as! NSMutableArray
            let dic = arr.object(at: index) as! NSDictionary
            self?.lblprice.text = NSString(format: "Rs. %@",Utility.setCurrencyFormate(NSString(format: "%d",dic.object(forKey: "YourPrice") as! Int) as String)) as String

            for i in 0..<arr.count
            {
                let dictemp = arr.object(at: i) as! NSMutableDictionary
                if i == index
                {
                    dictemp.setValue("1", forKey: "isselect")
                }
                else
                {
                    dictemp.setValue("0", forKey: "isselect")
                }
                arr.removeObject(at: i)
                arr.insert(dictemp, at: i)
            }
            self?.dicSelectPackage.setValue(arr, forKey: "LabPackagesComparison")
            
            self?.chooseArticleDropDown.hide()
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
//            if items.isEmpty {
//                lblNotificationText?.setTitle("", for: .normal)
//            }
        }
        
        chooseArticleDropDown.show()
    }
    
    @IBAction func btnorder(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "arrmedicalpackagecart") == nil
        {
            var labDetails1 = Dictionary<AnyHashable, Any>()
            labDetails1 = dicSelectPackage as! [AnyHashable : Any]
            let labDetails11 =  labDetails1.nullKeyRemoval()
            
            let arrLabTestCart:NSMutableArray = NSMutableArray()
            arrLabTestCart.add(labDetails11)
            UserDefaults.standard.set(arrLabTestCart, forKey: "arrmedicalpackagecart")
            UserDefaults.standard.synchronize()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                self.onCartbuttonClicked(nil)
            })
        }
        else
        {
            //var arrLabTestCart = NSArray()
            let arrLabTestCart:NSMutableArray = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrmedicalpackagecart") as! NSArray)
            var istrue = "yes"
            for i in 0..<arrLabTestCart.count
            {
                let dicLab = arrLabTestCart[i] as! NSDictionary
                let labidtemp = dicLab.object(forKey: "LabPackageId") as! Int
                let labidtemp11 = dicSelectPackage.object(forKey: "LabPackageId") as! Int
                
                if labidtemp11 == labidtemp
                {
                    istrue = "no"
                }
            }
            if istrue == "yes"
            {
                var labDetails1 = Dictionary<AnyHashable, Any>()
                labDetails1 = dicSelectPackage as! [AnyHashable : Any]
                let labDetails11 =  labDetails1.nullKeyRemoval()
                
                arrLabTestCart.add(labDetails11)
                UserDefaults.standard.set(arrLabTestCart, forKey: "arrmedicalpackagecart")
                UserDefaults.standard.synchronize()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    self.onCartbuttonClicked(nil)
                })
            }
            else
            {
                ISMessages.show("Product Already Exits in Cart")
            }
        }
        
        
    }
}


//MARK:- Tableview Delegate Method
extension MedicalPackagesDetailVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrLabTests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let lbltitle = cell.contentView.viewWithTag(1) as! UILabel
        
        let dicData = arrLabTests.object(at: indexPath.row) as! NSDictionary
        
        lbltitle.text = (dicData.object(forKey: "LabTestName") as! String)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
        
    }
}
