//
//  AddFamilyDetailsNameVC.swift
//  
//
//  Created by UDHC on 22/10/2018.
//

import UIKit
//import SVProgressHUD

class AddFamilyDetailsNameVC: UIViewController , UITextFieldDelegate {


    @IBOutlet weak var relationship: UIButton!
    @IBOutlet weak var Gender: UIButton!
    @IBOutlet weak var DOB: UITextField!
    @IBOutlet weak var name: UITextField!
    
    
    var TotalRecords:Int = 0
    var page_Limit: Int = 0
    
    var Relationshop:String = ""
    var Gen:String = ""
    
    var activityIndicator: UIActivityIndicatorView?
    
    let chooseArticleDropDown = DropDown()
    //var arrRadiologyList: Array<Radiology> = []
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown
        ]
    }()
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    }

    

    @IBAction func senbbtn(_ sender: Any) {
        SaveName()
    }
    @IBAction func backbtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showGender(_ sender: Any)
    {
            let lblNotificationText = sender as? UIButton
            chooseArticleDropDown.anchorView = lblNotificationText
            chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
            chooseArticleDropDown.dataSource = ["Male" , "Female"]
            chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
                lblNotificationText?.setTitle(item, for: .normal)
                self!.Gen = item as String
                self?.chooseArticleDropDown.hide()
            }
            
            chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
                print("Muti selection action called with: \(items)")
                
                if items.isEmpty {
                    lblNotificationText?.setTitle("", for: .normal)
                    
                }
            }
            
            chooseArticleDropDown.show()
    }
    @IBAction func showRelationship(_ sender: Any) {
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        chooseArticleDropDown.dataSource = ["Mother" , "Father" , "Brother" , "Sister" , "Son" , "Daughter" , "Wife"]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            lblNotificationText?.setTitle(item, for: .normal)
            if item == "Mother"
            {
                self!.Relationshop = "MTH"
            }
            else if item == "Father"
            {
                self!.Relationshop = "FTH"
            }
            else if item == "Wife"
            {
                self!.Relationshop = "WWF"
            }
            else if item == "Son"
            {
                self!.Relationshop = "SON"
            }
            else if item == "Daughter"
            {
                self!.Relationshop = "DTR"
            }
            else if item == "Brother"
            {
                self!.Relationshop = "BTR"
            }
            else if item == "Sister"
            {
                self!.Relationshop = "SSR"
            }
            self?.chooseArticleDropDown.hide()
        }
        
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
    
            
            if items.isEmpty {
                lblNotificationText?.setTitle("", for: .normal)
                
            }
        }
        
        chooseArticleDropDown.show()
    }
    func SaveName()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        SVProgressHUD.show()
        do {
            
            let headers = [
                "content-type": "application/json"
            ]
            let reqParam = ["Name" : name.text,
                            "RelationshipPrefix" : Relationshop,
                            "DateOfBirth": "1998-02-01T00:00:00",
                            "Gender": Gen] as [String : Any]
            
            
            
            let postData = try! JSONSerialization.data(withJSONObject: reqParam, options: [])
            
            let request = NSMutableURLRequest(url: NSURL(string: K.URL.SaveName)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            let token = UserDefaults.standard.object(forKey: "token") as! String
            request.addValue(NSString(format: "bearer %@", token) as String, forHTTPHeaderField: "Authorization")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                SVProgressHUD.dismiss()
                if (error != nil) {
                    print(error as Any as Any)
                    ISMessages.show("Error occurred, Please try again")
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200
                    {
                        
                        ISMessages.show("Name Sucessfully Added")
                        
                        DispatchQueue.main.async
                            {
                                self.dismiss(animated: true, completion: nil)
                                
                        }
                    }
                    else
                    {
                        ISMessages.show("Cant add name at this time")
                    }
                }
            })
            dataTask.resume()
        }
        catch
        {
            print(error)
        }
    }
    
}








