//
//  OnlineDoctorVC.swift
//  AppDesign
//
//  Created by UDHC on 25/10/2018.
//  Copyright © 2018 Bilal. All rights reserved.
//

import UIKit

class OnlineDoctorVC: UIViewController {

    @IBOutlet weak var ProceedBtn: UIButton!
    @IBOutlet weak var PaymentViewNew: UIView!
    @IBOutlet weak var MainView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Common.SetShadowwithCorneronView(view: MainView, cornerradius: 10.0, color: UIColor.lightGray)
        Common.SetShadowwithCorneronView(view: PaymentViewNew, cornerradius: 10.0, color: UIColor.lightGray)
        Common.SetShadowwithCorneronView(view: ProceedBtn, cornerradius: 10.0, color: UIColor.clear)
    }
    
    @IBAction func btnback(_ sender: Any)
    {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    
    @IBAction func Proceed(_ sender: Any) {
        let VC = AppUtility.STORY_BOARD_SECOND().instantiateViewController(withIdentifier: "ConnectingVC") as! ConnectingVC
        AppUtility.shared.mainNavController.pushViewController(VC, animated: true)
        
    }

}
