//
//  TeleclinicVC.swift
//  AppDesign
//
//  Created by UDHC on 01/11/2018.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class TeleclinicVC: UIViewController {

    @IBOutlet weak var Connectionbutton: UIButton!
    @IBOutlet weak var TextView: UITextView!
    @IBOutlet weak var MainView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Common.setCornerOnView(view: TextView, cornerradius: 10.0)
        Common.setBorderOnView(view: TextView, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: MainView, cornerradius: 10.0, color: UIColor.lightGray)
        //Common.SetShadowwithCorneronView(view: TextView, cornerradius: 10.0, color: UIColor.lightGray)
        Common.SetShadowwithCorneronView(view: Connectionbutton, cornerradius: 10.0, color: UIColor.clear)

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
     @IBAction func Connection(_ sender: Any) {
        let VC = AppUtility.STORY_BOARD_SECOND().instantiateViewController(withIdentifier: "OnlineDoctorVC") as! OnlineDoctorVC
        AppUtility.shared.mainNavController.pushViewController(VC, animated: true)
        
     }
 
    @IBAction func backbtn(_ sender: Any) {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
}
