//
//  OrderPreviewCell.swift
//  AppDesign
//
//  Created by sachin on 18/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class OrderPreviewCell: UITableViewCell
{

    @IBOutlet weak var lblLabTest: UILabel!
    @IBOutlet weak var lblPreferreddate: UILabel!
    @IBOutlet weak var lblSelectLab: UILabel!
    @IBOutlet weak var lblCollectiontype: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    
    @IBOutlet weak var lblLabTestPackage: UILabel!
    @IBOutlet weak var lblPreferreddatePackage: UILabel!
    @IBOutlet weak var lblSelectLabPackage: UILabel!
    @IBOutlet weak var lblCollectiontypePackage: UILabel!
    @IBOutlet weak var lblPricePackage: UILabel!

    @IBOutlet weak var lbltestdetail: UILabel!
    @IBOutlet weak var lblRadiologyTest: UILabel!
    @IBOutlet weak var lblPriceRadiology: UILabel!
    @IBOutlet weak var lblDiagnosticCenter: UILabel!
    @IBOutlet weak var lbltype: UILabel!
    @IBOutlet weak var lbllocation: UILabel!
    @IBOutlet weak var lblPreferreddateRadiology: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
}
