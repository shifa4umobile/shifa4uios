//
//  OrderPreviewVC.swift
//  AppDesign
//
//  Created by sachin on 17/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class OrderPreviewVC: UIViewController
{
    var Count:Int = 0
    
    @IBOutlet weak var Mobile: UIButton!
    @IBOutlet weak var Address: UIButton!
    @IBOutlet weak var Patient: UIButton!
    @IBOutlet weak var viewcontactnfo: UIView!
    @IBOutlet weak var viewlabtest: UIView!
    @IBOutlet weak var tbllabtest: UITableView!
    @IBOutlet weak var vieelabpackages: UIView!
    @IBOutlet weak var tbllabPackages: UITableView!
    @IBOutlet weak var viewradiology: UIView!
    @IBOutlet weak var tblradiology: UITableView!
    @IBOutlet weak var btnprevious: UIButton!
    @IBOutlet weak var btnnext: UIButton!
    @IBOutlet weak var lbltotal: UILabel!
    @IBOutlet weak var heightlabtest: NSLayoutConstraint!
    @IBOutlet weak var heightlabpackages: NSLayoutConstraint!
    @IBOutlet weak var heightradiology: NSLayoutConstraint!
    @IBOutlet weak var btnbackblur: UIButton!
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var viewdatepicker: UIView!
    @IBOutlet weak var viewHomeCare: UIView!
    @IBOutlet weak var viewHomecare_Height: NSLayoutConstraint!
    @IBOutlet weak var tblHomecareList: UITableView!
    
    
    //
    var labPackageIndexPath = IndexPath()
    var radiologyIndexpath = IndexPath()
    var LabTestIndexpath = IndexPath()
    var selectedTableview = String()
    
    var arrCategoryList = NSArray()
    var arrContactList = NSArray()
    var arrNameList = NSArray()
    var NumberId:String = ""
    var AddressId:String = ""

    //
    var arrLabTestList:NSMutableArray = NSMutableArray()
    var arrLabPackage:NSMutableArray = NSMutableArray()
    var arrRadiologyList:NSMutableArray = NSMutableArray()
    var arrHomeCareList:NSMutableArray = NSMutableArray()
    
    //
    var YourPrice:Int = 0
    var arrOrderDetails:NSMutableArray = NSMutableArray()
    
    var TotalRecords:Int = 0
    var page_Limit: Int = 0
    var activityIndicator: UIActivityIndicatorView?
    
    let chooseArticleDropDown = DropDown()
    //var arrRadiologyList: Array<Radiology> = []
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown
        ]
    }()
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        
        Common.SetShadowwithCorneronView(view: viewcontactnfo, cornerradius: 10.0, color: UIColor.lightGray)
        Common.SetShadowwithCorneronView(view: viewlabtest, cornerradius: 10.0, color: UIColor.lightGray)
        Common.SetShadowwithCorneronView(view: vieelabpackages, cornerradius: 10.0, color: UIColor.lightGray)
        Common.SetShadowwithCorneronView(view: viewradiology, cornerradius: 10.0, color: UIColor.lightGray)
        Common.SetShadowwithCorneronView(view: viewHomeCare, cornerradius: 10.0, color: UIColor.lightGray)
        
        Common.setCornerOnButton(buttton: btnprevious, cornerradius: 10.0)
        Common.setCornerOnButton(buttton: btnnext, cornerradius: 10.0)
        Patient.setTitle("Select Patient", for: .normal)
        Mobile.setTitle( "Select Mobile Number", for: .normal)
        Address.setTitle("Select Address", for: .normal)
        hideView()
        GetContactNumbers()
        GetAddress()
        GetPatientName()
        setAllCart_Array()
    }
    override func viewDidAppear(_ animated: Bool) {
        GetContactNumbers()
        GetAddress()
        GetPatientName()
    }

    
    override func viewDidLayoutSubviews()
    {
        //LabTest
        if UserDefaults.standard.object(forKey: "arrlabtestcart") != nil
        {
            if (UserDefaults.standard.object(forKey: "arrlabtestcart") as! NSArray).count == 0
            {
                heightlabtest.constant=0
                viewlabtest.isHidden = true
            }
            else
            {
                heightlabtest.constant = tbllabtest.contentSize.height + 55
            }
        }
        else
        {
            heightlabtest.constant=0
            viewlabtest.isHidden = true
        }
        
        //Medical Package
        if UserDefaults.standard.object(forKey: "arrmedicalpackagecart") == nil
        {
            heightlabpackages.constant=0
            vieelabpackages.isHidden = true
        }
        else
        {
            if (UserDefaults.standard.object(forKey: "arrmedicalpackagecart") as! NSArray).count == 0
            {
                heightlabpackages.constant=0
                vieelabpackages.isHidden = true
            }
            else
            {
                heightlabpackages.constant = tbllabPackages.contentSize.height + 55
            }
        }
        
        //Radiology
        if UserDefaults.standard.object(forKey: "arrradiologycart") == nil
        {
            heightradiology.constant=0
            viewradiology.isHidden = true
        }
        else
        {
            if (UserDefaults.standard.object(forKey: "arrradiologycart") as! NSArray).count == 0
            {
                heightradiology.constant=0
                viewradiology.isHidden = true
            }
            else
            {
                heightradiology.constant = tblradiology.contentSize.height + 55
            }
        }
        
        //HomeCare
        if UserDefaults.standard.object(forKey: "arrhomecarecart") == nil
        {
            viewHomecare_Height.constant=0
            viewHomeCare.isHidden = true
        }
        else
        {
            if (UserDefaults.standard.object(forKey: "arrhomecarecart") as! NSArray).count == 0
            {
                viewHomecare_Height.constant=0
                viewHomeCare.isHidden = true
            }
            else
            {
                viewHomecare_Height.constant=tblHomecareList.contentSize.height + 55
            }
        }
        
        self.view.layoutIfNeeded()
    }
   
    @IBAction func PatientAddButton(_ sender: Any) {
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    

    func GetContactNumbers()
        {
            
            if Utility.isInterNetConnectionIsActive() == false
            {
                let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
                let later = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(later)
                self.present(alert, animated: true, completion: nil)
                return;
            }
            SVProgressHUD.show()
            
            let url = URL(string: K.URL.GetContactNumber)
            var request = URLRequest(url: url!)
            let token = UserDefaults.standard.object(forKey: "token") as! String
            request.addValue(NSString(format: "bearer %@", token) as String, forHTTPHeaderField: "Authorization")
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            
            let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
                SVProgressHUD.dismiss()
                // gunzip
                let decompressedData: Data
                if (data?.isGzipped)! {
                    decompressedData = (try! data?.gunzipped())!
                } else {
                    decompressedData = data!
                }
                
                let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
                
                if let arrData = dictionary as? NSArray {
                    print(dictionary!)
                    self.arrContactList = NSArray(array: arrData)
                }
                else {
                    ISMessages.show("Numbers not found")
                }
            });
            task.resume()
        }
    func GetAddress()
    {
        
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        SVProgressHUD.show()
        
        let url = URL(string: K.URL.GetAddress)
        var request = URLRequest(url: url!)
        let token = UserDefaults.standard.object(forKey: "token") as! String
        request.addValue(NSString(format: "bearer %@", token) as String, forHTTPHeaderField: "Authorization")
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            SVProgressHUD.dismiss()
            // gunzip
            let decompressedData: Data
            if (data?.isGzipped)! {
                decompressedData = (try! data?.gunzipped())!
            } else {
                decompressedData = data!
            }
            
            let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
            
            if let arrData = dictionary as? NSArray {
                print(dictionary!)
                self.arrCategoryList = NSArray(array: arrData)
            }
            else {
                ISMessages.show("Addresses not found")
            }
        });
        task.resume()
    }
    func GetPatientName()
    {
        
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        SVProgressHUD.show()
        
        let url = URL(string: K.URL.GetNames)
        var request = URLRequest(url: url!)
        let token = UserDefaults.standard.object(forKey: "token") as! String
        request.addValue(NSString(format: "bearer %@", token) as String, forHTTPHeaderField: "Authorization")
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            SVProgressHUD.dismiss()
            // gunzip
            let decompressedData: Data
            if (data?.isGzipped)! {
                decompressedData = (try! data?.gunzipped())!
            } else {
                decompressedData = data!
            }
            
            let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
            
            if let arrData = dictionary as? NSArray {
                print(dictionary!)
                self.arrNameList = NSArray(array: arrData)
            }
            else {
                ISMessages.show("Names not found")
            }
        });
        task.resume()
    }
    func hideView()
    {
        btnbackblur.isHidden = true
        viewdatepicker.isHidden = true
    }
    
    func ShowView()
    {
        btnbackblur.isHidden = false
        viewdatepicker.isHidden = false
    }
    
    
    func setAllCart_Array()
    {
        //LabTest
        if  UserDefaults.standard.object(forKey: "arrlabtestcart") == nil
        {
            viewlabtest.isHidden = true
        }
        else
        {
            if (UserDefaults.standard.object(forKey: "arrlabtestcart") as! NSArray).count == 0
            {
                viewlabtest.isHidden = true
            }
            else
            {
                arrLabTestList = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrlabtestcart") as! NSArray)
                tbllabtest.reloadData()
            }
        }
        
        //Medical Package
        if  UserDefaults.standard.object(forKey: "arrmedicalpackagecart") == nil
        {
            vieelabpackages.isHidden = true
        }
        else
        {
            if (UserDefaults.standard.object(forKey: "arrmedicalpackagecart") as! NSArray).count == 0
            {
                vieelabpackages.isHidden = true
            }
            else
            {
                arrLabPackage = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrmedicalpackagecart") as! NSArray)
                tbllabPackages.reloadData()
            }
        }
        
        //Radiology
        if  UserDefaults.standard.object(forKey: "arrradiologycart") == nil
        {
            viewradiology.isHidden = true
        }
        else
        {
            if (UserDefaults.standard.object(forKey: "arrradiologycart") as! NSArray).count == 0
            {
                viewradiology.isHidden = true
            }
            else
            {
                arrRadiologyList = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrradiologycart") as! NSArray)
                tblradiology.reloadData()
            }
        }
        
        //HomeCare
        if UserDefaults.standard.object(forKey: "arrhomecarecart") == nil
        {
            viewHomeCare.isHidden = true
        }
        else
        {
            if (UserDefaults.standard.object(forKey: "arrhomecarecart") as! NSArray).count == 0
            {
                viewHomeCare.isHidden = true
            }
            else
            {
                arrHomeCareList = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrhomecarecart") as! NSArray)
                tblHomecareList.reloadData()
            }
        }
        setFinalPriceCalculate()
       //performSegue(withIdentifier: "segue", sender: self)
        DispatchQueue.main.async {
            self.viewDidLayoutSubviews()
        }
    }
    
    func setFinalPriceCalculate()
    {
        //LabTest
        for j in 0..<arrLabTestList.count
        {
            let labDetails = arrLabTestList[j] as! NSMutableDictionary
            let arrsublabtemp = labDetails.object(forKey: "Labs") as! NSArray
            for i in 0..<arrsublabtemp.count
            {
                let dictemp = arrsublabtemp.object(at: i) as! NSDictionary
                if let val = dictemp["isselect"] {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        YourPrice = YourPrice + Int(dictemp.object(forKey: "YourPrice") as! Int)
                        
                        //set order array
                        let dicOrder:NSMutableDictionary = NSMutableDictionary()
                        dicOrder.setValue(dictemp.object(forKey: "LabCenterProductId"), forKey: "ProductLineId")
                        dicOrder.setValue(dictemp.object(forKey: "LabId"), forKey: "LabId")
                        dicOrder.setValue("0", forKey: "VendorLocationId")
                        dicOrder.setValue("true", forKey: "IsHomeSampleCollection")
                        arrOrderDetails.add(dicOrder)
                    }
                }
            }
        }
        
        //package
        for j in 0..<arrLabPackage.count
        {
            let labDetails = arrLabPackage[j] as! NSMutableDictionary
            let arrsublabtemp = labDetails.object(forKey: "LabPackagesComparison") as! NSArray
            for i in 0..<arrsublabtemp.count
            {
                let dictemp = arrsublabtemp.object(at: i) as! NSDictionary
                if let val = dictemp["isselect"] {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        YourPrice = YourPrice + Int(dictemp.object(forKey: "YourPrice") as! Int)
                        
                        //set order array
                        let dicOrder:NSMutableDictionary = NSMutableDictionary()
                        dicOrder.setValue(dictemp.object(forKey: "PackageId"), forKey: "ProductLineId")
                        dicOrder.setValue(dictemp.object(forKey: "labId"), forKey: "LabId")
                        dicOrder.setValue("0", forKey: "VendorLocationId")
                        dicOrder.setValue("true", forKey: "IsHomeSampleCollection")
                        arrOrderDetails.add(dicOrder)
                    }
                }
            }
        }
        
        //radiology
        for j in 0..<arrRadiologyList.count
        {
            let labDetails = arrRadiologyList[j] as! NSMutableDictionary
            let arrRadiologyLabsComparison = labDetails.object(forKey: "RadiologyLabsComparison") as! NSArray
            for i in 0..<arrRadiologyLabsComparison.count
            {
                let dictemp = arrRadiologyLabsComparison.object(at: i) as! NSDictionary
                if let val = dictemp["isselect"] {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        YourPrice = YourPrice + Int(dictemp.object(forKey: "YourPrice") as! Int)
                        
                        //set order array
                        let dicOrder:NSMutableDictionary = NSMutableDictionary()
                        
                        //get Locationid
                        let Vendors = NSMutableArray(array: labDetails.object(forKey: "Vendors") as! NSArray)
                        for j in 0..<Vendors.count
                        {
                            //2
                            let ImagingId = dictemp.object(forKey: "ImagingId") as! Int
                            let dictempInner = NSMutableDictionary(dictionary: Vendors.object(at: j) as! NSDictionary)
                            let ImagingId_inner = dictempInner.object(forKey: "ImagingId") as! Int
                            if ImagingId == ImagingId_inner
                            {
                                let Locations = NSMutableArray(array: dictempInner.object(forKey: "Locations") as! NSArray)
                                for c in 0..<Locations.count
                                {
                                    //3
                                    let dictempInner_sub = NSMutableDictionary(dictionary: Locations.object(at: c) as! NSDictionary)
                                    if let val = dictempInner_sub["isselect"]
                                    {
                                        if NSString(format: "%@", val as! CVarArg) == "1"
                                        {
                                            dicOrder.setValue(dictempInner_sub.object(forKey: "VendorLocationId"), forKey: "VendorLocationId")
                                        }
                                    }
                                }
                            }
                        }
                        
                        dicOrder.setValue(dictemp.object(forKey: "ProductLineId"), forKey: "ProductLineId")
                        dicOrder.setValue(dictemp.object(forKey: "LabId"), forKey: "LabId")
                        dicOrder.setValue("false", forKey: "IsHomeSampleCollection")
                        arrOrderDetails.add(dicOrder)
                    }
                }
            }
        }
        
        //homecare
        for j in 0..<arrHomeCareList.count
        {
            let labDetails = arrHomeCareList[j] as! NSMutableDictionary
            
            YourPrice = YourPrice + Int(labDetails.object(forKey: "ProductLine_DiscountedPrice") as! Int)
            
            //set order array
            let dicOrder:NSMutableDictionary = NSMutableDictionary()
            dicOrder.setValue(labDetails.object(forKey: "ProductLineId"), forKey: "ProductLineId")
            dicOrder.setValue(labDetails.object(forKey: "MedicalProductId"), forKey: "LabId")
            dicOrder.setValue("0", forKey: "VendorLocationId")
            dicOrder.setValue("true", forKey: "IsHomeSampleCollection")
            arrOrderDetails.add(dicOrder)
        }
        
        lbltotal.text = NSString(format: "Rs. %@",Utility.setCurrencyFormate(NSString(format: "%d", YourPrice) as String)) as String
    
        
    }
    //MARK:- UIButton Action
    @IBAction func btnbackblur(_ sender: Any)
    {
        hideView()
    }
    
    @IBAction func btnback(_ sender: Any)
    {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    
    @IBAction func btnpatient(_ sender: Any)
    {
        let AddressDetails = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "AddFamilyDetailsNameVC") as! AddFamilyDetailsNameVC
        AddressDetails.modalTransitionStyle = .flipHorizontal
        present(AddressDetails, animated: true)
    }
    
    @IBAction func btnaddress(_ sender: Any)
    {
        let AddressDetails = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "AddFamilyDetailsAddress") as! AddFamilyDetailsAddress
        AddressDetails.modalTransitionStyle = .flipHorizontal
        present(AddressDetails, animated: true)
    }
    
    @IBAction func btnmobile(_ sender: Any)
    {
        let MobileDetails = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "AddFamilyDetailsNumberVC") as! AddFamilyDetailsNumberVC
        MobileDetails.modalTransitionStyle = .flipHorizontal
        present(MobileDetails, animated: true)
    }
    
    @IBAction func MobileShow(_ sender: Any) {
        
        let arrLabsTemp = NSMutableArray()
        for i in 0..<arrContactList.count
        {
            let labname = (arrContactList.object(at: i) as! NSDictionary).object(forKey: "Description")
            arrLabsTemp.add(labname as Any)
        }
        
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            lblNotificationText?.setTitle(item, for: .normal)
            //self?.NumberId = (self?.arrContactList.object(at: index) as! NSDictionary).object(forKey: "ContactId") as! String
            let labDetails = self!.arrContactList[index] as! NSDictionary
            self!.NumberId = String(labDetails.object(forKey: "ContactId") as! Int)
            print(self!.NumberId)
            self?.chooseArticleDropDown.hide()
        }
        
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            
            if items.isEmpty {
                lblNotificationText?.setTitle("", for: .normal)
                
            }
        }
        
        chooseArticleDropDown.show()
    }
    @IBAction func AddressShow(_ sender: Any) {
            let arrLabsTemp = NSMutableArray()
            for i in 0..<arrCategoryList.count
            {
                let labname = (arrCategoryList.object(at: i) as! NSDictionary).object(forKey: "Description")
                arrLabsTemp.add(labname as Any)
            }
            
            let lblNotificationText = sender as? UIButton
            chooseArticleDropDown.anchorView = lblNotificationText
            chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
            chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
            chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
                lblNotificationText?.setTitle(item, for: .normal)
                //self?.AddressId = (self?.arrCategoryList.object(at: index) as! NSDictionary).object(forKey: "ContactId") as Any as! String
                let labDetails = self!.arrCategoryList[index] as! NSDictionary
                self!.AddressId = String(labDetails.object(forKey: "ContactId") as! Int)
                print(self!.AddressId)
                self?.chooseArticleDropDown.hide()
            }
            
            chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
                print("Muti selection action called with: \(items)")
                
                if items.isEmpty {
                    lblNotificationText?.setTitle("", for: .normal)
                    
                }
            }
            
            chooseArticleDropDown.show()
    }
    @IBAction func PatientShow(_ sender: Any) {
        
        let arrLabsTemp = NSMutableArray()
        for i in 0..<arrNameList.count
        {
            let labname = (arrNameList.object(at: i) as! NSDictionary).object(forKey: "Name")
            arrLabsTemp.add(labname as Any)
        }
        
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            lblNotificationText?.setTitle(item, for: .normal)
            self?.chooseArticleDropDown.hide()
        }
        
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            
            if items.isEmpty {
                lblNotificationText?.setTitle("", for: .normal)
                
            }
        }
        
        chooseArticleDropDown.show()
    }
    
    @IBAction func btnlabpackagesedit(_ sender: Any)
    {
    
    }
    
    @IBAction func btnlabTestEdit(_ sender: Any)
    {
    
    }
    
    @IBAction func btnradiologyEdit(_ sender: Any)
    {
    
    }
    
    
    @IBAction func btnlabtestCalender(_ sender: Any)
    {
        ShowView()
        
        selectedTableview = "labtest"
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tbllabtest)
        LabTestIndexpath = self.tbllabtest.indexPathForRow(at: buttonPosition)!
    }

    @IBAction func btnlabpackagesCalender(_ sender: Any)
    {
        ShowView()
        
        selectedTableview = "labpackeges"
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tbllabPackages)
        labPackageIndexPath = self.tbllabPackages.indexPathForRow(at: buttonPosition)!
    }
    
    @IBAction func btnradiologyCalander(_ sender: Any)
    {
        ShowView()
        
        selectedTableview = "radiology"
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tblradiology)
        radiologyIndexpath = self.tblradiology.indexPathForRow(at: buttonPosition)!
    }
    
    @IBAction func btnnext(_ sender: Any)
    {
//        if Patient.currentTitle == "Select Patient"
//        {
//            ISMessages.show("Select patient , You can add new patient as well")
//        }
//        else if Mobile.currentTitle == "Select Mobile Number"
//        {
//            ISMessages.show("Select Mobile , You can add new mobile number as well")
//        }
//        else if Address.currentTitle == "Select Address"
//        {
//            ISMessages.show("Select Address , You can add new address as well")
//        }
//        else
//        {
        let vc = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "PaymentModeVC") as! PaymentModeVC
        //vc.straddress = txtaddress.text!
        //vc.strmobile = txtmobile.text!
        vc.stratotalprice = NSString(format: "%d", YourPrice) as String
        vc.arrOrderDetails = NSMutableArray(array: arrOrderDetails)
        vc.strmobile = self.NumberId
        vc.straddress = self.AddressId
        AppUtility.shared.mainNavController.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnprevious(_ sender: Any)
    {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    @IBAction func btndone(_ sender: Any)
    {
        hideView()
        if selectedTableview == "labpackeges"
        {
            let cell = tbllabPackages.cellForRow(at: labPackageIndexPath) as! OrderPreviewCell
            cell.lblPreferreddatePackage.text = Common.convertdateFormat(date: datepicker.date, format: "dd/MM/yyyy")
        }
        if selectedTableview == "labtest"
        {
            let cell = tbllabtest.cellForRow(at: LabTestIndexpath) as! OrderPreviewCell
            cell.lblPreferreddate.text = Common.convertdateFormat(date: datepicker.date, format: "dd/MM/yyyy")
        }
        else if selectedTableview == "radiology"
        {
            let cell = tblradiology.cellForRow(at: radiologyIndexpath) as! OrderPreviewCell
            cell.lblPreferreddateRadiology.text = Common.convertdateFormat(date: datepicker.date, format: "dd/MM/yyyy")
        }
    }
    
    @IBAction func btncancel(_ sender: Any)
    {
        hideView()
    }
}


//MARK:- Tableview Delegate Method
extension OrderPreviewVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tbllabtest
        {
            return arrLabTestList.count
        }
        else if tableView == tbllabPackages
        {
            return arrLabPackage.count
        }
        else if tableView == tblradiology
        {
            return arrRadiologyList.count
        }
        else if tableView == tblHomecareList
        {
            return arrHomeCareList.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tbllabtest
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrderPreviewCell
            
            let labDetails = arrLabTestList[indexPath.row] as! NSMutableDictionary
            
            let arrsublabtemp = labDetails.object(forKey: "Labs") as! NSArray
            for i in 0..<arrsublabtemp.count
            {
                let dictemp = arrsublabtemp.object(at: i) as! NSDictionary
                if let val = dictemp["isselect"] {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        cell.lblPrice.text = NSString(format: "Rs. %@",Utility.setCurrencyFormate(NSString(format:"%d",dictemp.object(forKey: "YourPrice") as! Int) as String)) as String
                        cell.lblSelectLab.text = (dictemp.object(forKey: "LabName") as! String)
                    }
                }
            }
            
            cell.lblLabTest.text = (labDetails.object(forKey: "Name") as! String)
            cell.lblCollectiontype.text = "Home Collection"
        
            return cell
        }
        else if tableView == tbllabPackages
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! OrderPreviewCell
            
            let labDetails = arrLabPackage[indexPath.row] as! NSMutableDictionary
            cell.lblLabTestPackage.text =  (labDetails.object(forKey: "Name") as! String)
            
            let arrsublabtemp = labDetails.object(forKey: "LabPackagesComparison") as! NSArray
            for i in 0..<arrsublabtemp.count
            {
                let dictemp = arrsublabtemp.object(at: i) as! NSDictionary
                if let val = dictemp["isselect"] {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        cell.lblPricePackage.text = NSString(format: "Rs. %@",Utility.setCurrencyFormate(NSString(format: "%d",dictemp.object(forKey: "YourPrice") as! Int) as String)) as String
                        cell.lblSelectLabPackage.text = (dictemp.object(forKey: "LabName") as! String)
                    }
                }
            }

            cell.lbltestdetail.text = "Home Collection"
            
            return cell
        }
        else if tableView == tblradiology
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! OrderPreviewCell
            
            let labDetails = arrRadiologyList[indexPath.row] as! NSMutableDictionary
            let arrRadiologyLabsComparison = labDetails.object(forKey: "RadiologyLabsComparison") as! NSArray
            for i in 0..<arrRadiologyLabsComparison.count
            {
                let dictemp = arrRadiologyLabsComparison.object(at: i) as! NSDictionary
                if let val = dictemp["isselect"] {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        cell.lblPriceRadiology.text = NSString(format: "Rs. %@",Utility.setCurrencyFormate(NSString(format: "%d",dictemp.object(forKey: "YourPrice") as! Int) as String)) as String
                        cell.lblDiagnosticCenter.text = (dictemp.object(forKey: "LabName") as! String)
                    }
                }
            }
            
            cell.lblRadiologyTest.text = (labDetails.object(forKey: "ImagingName") as! String)
            cell.lbltype.text = "Waking"
            
            return cell
        }
        else if tableView == tblHomecareList
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_row", for: indexPath)
            
            let lblTitle = cell.contentView.viewWithTag(1) as! UILabel
            let lblPrice = cell.contentView.viewWithTag(2) as! UILabel
            
            let labDetails = arrHomeCareList[indexPath.row] as! NSMutableDictionary
            lblTitle.text =  (labDetails.object(forKey: "ProductLine_ProductName") as! String)
            
            lblPrice.text = NSString(format: "Rs. %@",Utility.setCurrencyFormate(NSString(format: "%d",labDetails.object(forKey: "ProductLine_DiscountedPrice") as! Int) as String)) as String
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
  
}


