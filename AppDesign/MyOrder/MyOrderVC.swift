//
//  MyOrderVC.swift
//  AppDesign
//
//  Created by sachin on 15/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD
import ObjectMapper



enum OrderTab : Int {
    case none = 0,
    openOrder,
    closeOrder,
    cancleOrder
}
class MyOrderVC: UIViewController
{

    @IBOutlet weak var CancelOrderBtn: UIButton!
    @IBOutlet weak var btnopenorder: UIButton!
    @IBOutlet weak var btnclosedorder: UIButton!
    @IBOutlet weak var btncancelorder: UIButton!
    @IBOutlet weak var lineleadig: NSLayoutConstraint!
    @IBOutlet weak var linewidth: NSLayoutConstraint!
    @IBOutlet weak var tbllist: UITableView!
    var currentTag : OrderTab = .openOrder
    // var arrAllOrder: Array<OrderModel> = []
    var arrAllOrder:OrederModel1?
    
    
    var arrDisplayOrder:NSMutableArray = NSMutableArray()
    
    //var arrDisplayOrder: Array<OrderDetailModel> = []
    
    override func viewDidLoad()
    {
    
        super.viewDidLoad()
        //btnopenorder.sendActions(for: .touchUpInside)
        btnopenorder.titleLabel?.textColor = UIColor.lightGray
        btncancelorder.titleLabel?.textColor = UIColor.lightGray
        
        if UserDefaults.standard.object(forKey: "OpenOrdersArray") != nil
        {
            self.arrDisplayOrder = NSMutableArray(array: UserDefaults.standard.object(forKey: "OpenOrdersArray") as! NSArray)

            self.tbllist.reloadData()
            
            if self.arrDisplayOrder.count != 0
            {
                apicall(is_firsttime: "")
            }
            else
            {
                apicall(is_firsttime: "1")
            }
        }
        else
        {
            apicall(is_firsttime: "1")
        }
       
    }

    @IBAction func CancelOrder(_ sender: Any)
        {
            let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tbllist)
            let indexPath = self.tbllist.indexPathForRow(at: buttonPosition)
            DispatchQueue.main.async {
                let actionSheetController: UIAlertController = UIAlertController(title: "Shifa4U", message: "Are you sure you want to remove order", preferredStyle: .alert)
                let Yes: UIAlertAction = UIAlertAction(title: "YES", style: .default) { action -> Void in
                    self.arrDisplayOrder.removeObject(at: (indexPath?.row)!)
                    
                    UserDefaults.standard.set(self.arrDisplayOrder, forKey: "canceledArray")
                    UserDefaults.standard.synchronize()
                    self.tbllist.reloadData()
                    ISMessages.show("Lab Test removed")
                    self.viewDidLayoutSubviews()
                }
                let cancelAction: UIAlertAction = UIAlertAction(title: "NO", style: .cancel) { action -> Void in
                }
                actionSheetController.addAction(Yes)
                actionSheetController.addAction(cancelAction)
                self.present(actionSheetController, animated: true, completion: nil)
                
            }
            
            
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    @IBAction func btnbcak(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btncloseorder(_ sender: Any)
    {
        if UserDefaults.standard.object(forKey: "ClosedOrdersArray") != nil
        {
            //self.CancelOrderBtn.isHidden = true
            self.arrDisplayOrder = NSMutableArray(array: UserDefaults.standard.object(forKey: "ClosedOrdersArray") as! NSArray)
            self.tbllist.reloadData()
        }
        
        btnopenorder.titleLabel?.textColor = UIColor.lightGray
        btncancelorder.titleLabel?.textColor = UIColor.lightGray
        btnclosedorder.titleLabel?.textColor = UIColor.white
        //currentTag = .closeOrder
        lineleadig.constant = btnclosedorder.frame.origin.x
        linewidth.constant = btnclosedorder.frame.size.width
        
        //setData()
    }
    
    @IBAction func btnopenorder(_ sender: Any)
    {
        if UserDefaults.standard.object(forKey: "OpenOrdersArray") != nil
        {
            self.arrDisplayOrder = NSMutableArray(array: UserDefaults.standard.object(forKey: "OpenOrdersArray") as! NSArray)
            self.tbllist.reloadData()
        }
        
        btnclosedorder.titleLabel?.textColor = UIColor.lightGray
        btncancelorder.titleLabel?.textColor = UIColor.lightGray
        btnopenorder.titleLabel?.textColor = UIColor.white
        //currentTag  = .openOrder
        lineleadig.constant = btnopenorder.frame.origin.x
        linewidth.constant = btnopenorder.frame.size.width
        
        //setData()
    }
    
    @IBAction func btncancelorder(_ sender: Any)
    {
        if UserDefaults.standard.object(forKey: "canceledArray") != nil
        {
            self.arrDisplayOrder = NSMutableArray(array: UserDefaults.standard.object(forKey: "canceledArray") as! NSArray)
            self.tbllist.reloadData()
        }
        
        btnopenorder.titleLabel?.textColor = UIColor.lightGray
        btnclosedorder.titleLabel?.textColor = UIColor.lightGray
        btncancelorder.titleLabel?.textColor = UIColor.white
       // currentTag  = .cancleOrder
        lineleadig.constant = btncancelorder.frame.origin.x
        linewidth.constant = btncancelorder.frame.size.width
        
        //setData()
    }
    
    func setData()
    {
        /*if arrAllOrder != nil
        {
            if currentTag == .openOrder
            {
                if (arrAllOrder?.arrOpenOrders?.count ?? 0) > 0 {
                    arrDisplayOrder = (arrAllOrder?.arrOpenOrders)!
                }

                tbllist.reloadData()
            }
            else if currentTag == .closeOrder
            {
                if (arrAllOrder?.arrClosedOrders?.count ?? 0) > 0 {
                    arrDisplayOrder = (arrAllOrder?.arrClosedOrders)!
                }

                tbllist.reloadData()
            }
            else if currentTag == .cancleOrder
            {
                if (arrAllOrder?.arrCanceledOrders?.count ?? 0) > 0 {
                    arrDisplayOrder = (arrAllOrder?.arrCanceledOrders)!
                }

                tbllist.reloadData()
            }
        }*/
    }
}


//MARK:- Tableview Delegate Method
extension MyOrderVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrDisplayOrder.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let obj = arrDisplayOrder[indexPath.row] as! NSDictionary
        let lbltitle = cell.contentView.viewWithTag(1) as! UILabel
        let lblorderid = cell.contentView.viewWithTag(2) as! UILabel
        let lblOrderdate = cell.contentView.viewWithTag(3) as! UILabel
        let lblpaymenttMode = cell.contentView.viewWithTag(4) as! UILabel
        let lblPaymentStatus = cell.contentView.viewWithTag(5) as! UILabel
        let lblprice = cell.contentView.viewWithTag(6) as! UILabel
        let viewmain = cell.contentView.viewWithTag(7)
        
        let OrderDetailsVM = obj.object(forKey: "OrderDetailsVM") as! NSArray
        if OrderDetailsVM.count > 1
        {
            lbltitle.text = (obj.object(forKey: "ItemName") as! String) + " & Other"
        }
        else
        {
            lbltitle.text = (obj.object(forKey: "ItemName") as! String)
        }
        
        lblOrderdate.text = (obj.object(forKey: "OrderDate") as! String)
        lblpaymenttMode.text = (obj.object(forKey: "PaymentMethod") as! String)
        lblPaymentStatus.text = (obj.object(forKey: "OrderStatus") as! String)
        lblprice.text = "Rs: " + (obj.object(forKey: "TotalAmount") as! String)
        lblorderid.text = (obj.object(forKey: "OrderId") as! String)
        
        Common.SetShadowwithCorneronView(view: viewmain!, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let details = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "MyOrderDetailsVC") as! MyOrderDetailsVC
        details.dicSelectOrderDetail = arrDisplayOrder.object(at: indexPath.row) as! NSDictionary
        AppUtility.shared.mainNavController.pushViewController(details, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 145
        
    }
    

    func apicall(is_firsttime: String)
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        if is_firsttime == "1" {
            SVProgressHUD.show()
        }
        let token = UserDefaults.standard.object(forKey: "token") as! String
        let url = URL(string: K.URL.allCustomerOrders)
        var urlRequest = URLRequest(url: url!)
        urlRequest.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        urlRequest.timeoutInterval = 60
        urlRequest.httpMethod = "GET"
        // urlRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, errorResponse) in
            if errorResponse != nil {
                print(errorResponse ?? "Erroer Found")
                SVProgressHUD.dismiss()
            }
            else{
                
                do {
                    // gunzip
                    let decompressedData: Data
                    if (data?.isGzipped)! {
                        decompressedData = (try! data?.gunzipped())!
                    } else {
                        decompressedData = data!
                    }
                    if  let json = try! JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves) as? NSDictionary
                    {
                        
                        DispatchQueue.main.async {
                           // print(json)

                            let arrCanceledOrdersTemp = NSMutableArray(array: json.object(forKey: "CanceledOrders") as! NSArray)
                            let canceledArray:NSMutableArray = NSMutableArray()
                            for i in 0..<arrCanceledOrdersTemp.count
                            {
                                let dictemp = arrCanceledOrdersTemp.object(at: i) as! Dictionary<AnyHashable, Any>
                                let labDetails11 =  dictemp.nullKeyRemoval()
                                
                                let OrderDetailsVM = labDetails11["OrderDetailsVM"] as! NSArray
                                let OrderDetailsVMtemp:NSMutableArray = NSMutableArray()
                                for j in 0..<OrderDetailsVM.count
                                {
                                    let dictemp22 = OrderDetailsVM.object(at: j) as! Dictionary<AnyHashable, Any>
                                    let labDetails22 =  dictemp22.nullKeyRemoval()
                                    OrderDetailsVMtemp.add(labDetails22)
                                }
                               let labDetails11_temp = NSMutableDictionary(dictionary: labDetails11)
                                labDetails11_temp.setValue(OrderDetailsVMtemp, forKey: "OrderDetailsVM")
                                canceledArray.add(labDetails11_temp)
                            }
                            if is_firsttime == "1" {
                                self.arrDisplayOrder = canceledArray
                                self.tbllist.reloadData()
                            }
                            if is_firsttime == "" {
                                self.arrDisplayOrder = canceledArray
                                self.tbllist.reloadData()
                            }
                            
                            UserDefaults.standard.set(canceledArray, forKey: "canceledArray")
                            UserDefaults.standard.synchronize()
                            
                            //======
                            let arrOpenOrdersTemp = json.object(forKey: "OpenOrders") as! NSArray
                            let OpenOrdersArray:NSMutableArray = NSMutableArray()
                            for i in 0..<arrOpenOrdersTemp.count
                            {
                                let dictemp = arrOpenOrdersTemp.object(at: i) as! Dictionary<AnyHashable, Any>
                                let labDetails11 =  dictemp.nullKeyRemoval()
                                
                                let OrderDetailsVM = labDetails11["OrderDetailsVM"] as! NSArray
                                let OrderDetailsVMtemp:NSMutableArray = NSMutableArray()
                                for j in 0..<OrderDetailsVM.count
                                {
                                    let dictemp22 = OrderDetailsVM.object(at: j) as! Dictionary<AnyHashable, Any>
                                    let labDetails22 =  dictemp22.nullKeyRemoval()
                                    OrderDetailsVMtemp.add(labDetails22)
                                }
                                let labDetails11_temp = NSMutableDictionary(dictionary: labDetails11)
                                labDetails11_temp.setValue(OrderDetailsVMtemp, forKey: "OrderDetailsVM")
                                OpenOrdersArray.add(labDetails11_temp)
                            }
                            if is_firsttime == "1" {
                                self.arrDisplayOrder = OpenOrdersArray
                                self.tbllist.reloadData()
                            }
                            if is_firsttime == "" {
                                self.arrDisplayOrder = OpenOrdersArray
                                self.tbllist.reloadData()
                            }
                            UserDefaults.standard.set(OpenOrdersArray, forKey: "OpenOrdersArray")
                            UserDefaults.standard.synchronize()
                            
                            //======
                            let arrClosedOrdersTemp = json.object(forKey: "ClosedOrders") as! NSArray
                            let ClosedOrdersArray:NSMutableArray = NSMutableArray()
                            for i in 0..<arrClosedOrdersTemp.count
                            {
                                let dictemp = arrClosedOrdersTemp.object(at: i) as! Dictionary<AnyHashable, Any>
                                let labDetails11 =  dictemp.nullKeyRemoval()
                                
                                let OrderDetailsVM = labDetails11["OrderDetailsVM"] as! NSArray
                                let OrderDetailsVMtemp:NSMutableArray = NSMutableArray()
                                for j in 0..<OrderDetailsVM.count
                                {
                                    let dictemp22 = OrderDetailsVM.object(at: j) as! Dictionary<AnyHashable, Any>
                                    let labDetails22 =  dictemp22.nullKeyRemoval()
                                    OrderDetailsVMtemp.add(labDetails22)
                                }
                                let labDetails11_temp = NSMutableDictionary(dictionary: labDetails11)
                                labDetails11_temp.setValue(OrderDetailsVMtemp, forKey: "OrderDetailsVM")
                                ClosedOrdersArray.add(labDetails11_temp)
                            }
                            UserDefaults.standard.set(ClosedOrdersArray, forKey: "ClosedOrdersArray")
                            UserDefaults.standard.synchronize()
                            
                           // self.arrAllOrder = Mapper<OrederModel1>().map(JSONObject: dictionary)
                          //  self.setData()
                        }

                        SVProgressHUD.dismiss()
                    }
                }
                catch let exeptionError as Error? {
                    print(exeptionError!.localizedDescription)
                    SVProgressHUD.dismiss()
                }
            }
            
        })
        task.resume()
    }
}

