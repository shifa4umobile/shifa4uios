//
//  MyOrderDetailsVC.swift
//  AppDesign
//
//  Created by UDHC on 10/08/2018.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class MyOrderDetailsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var lblOrderPrice: UILabel!
    
    
    var arrorderDetailList:NSMutableArray = NSMutableArray()
    var dicSelectOrderDetail:NSDictionary  = NSDictionary()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commonData()
    }

  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func commonData()
    {
        arrorderDetailList = NSMutableArray(array: dicSelectOrderDetail.object(forKey: "OrderDetailsVM") as! NSArray)
        tblList.reloadData()
        
        lblOrderDate.text = "\(dicSelectOrderDetail.object(forKey: "OrderDate") ?? "")"
        lblOrderId.text = "\(dicSelectOrderDetail.object(forKey: "OrderId") ?? "")"
        lblPaymentMode.text = "\(dicSelectOrderDetail.object(forKey: "PaymentMethod") ?? "")"
        lblPaymentStatus.text = "\(dicSelectOrderDetail.object(forKey: "PaymentStatus") ?? "")"
        lblOrderPrice.text = "Rs: " + "\(dicSelectOrderDetail.object(forKey: "TotalAmount") ?? "")"
        
    }
    
    //MARK:- UITableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrorderDetailList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let viewmain = cell.contentView.viewWithTag(201)
        
        let obj = arrorderDetailList[indexPath.row] as! NSDictionary
        
        let lblItemName = cell.contentView.viewWithTag(101) as! UILabel
        let lblOrderDetailId = cell.contentView.viewWithTag(102) as! UILabel
        let lblOrderStatus = cell.contentView.viewWithTag(103) as! UILabel
        let lblVendorName = cell.contentView.viewWithTag(104) as! UILabel
        let lblCategory = cell.contentView.viewWithTag(105) as! UILabel
        let lblprice = cell.contentView.viewWithTag(106) as! UILabel
        
        lblItemName.text = (obj.object(forKey: "ItemName") as! String)
        lblOrderDetailId.text = "\(obj.object(forKey: "OrderDetailId") ?? 0)"
        lblOrderStatus.text = (obj.object(forKey: "OrderStatus") as! String)
        lblVendorName.text = (obj.object(forKey: "VendorName") as! String)
        lblCategory.text = (obj.object(forKey: "Category") as! String)
        lblprice.text = "Rs: " + (obj.object(forKey: "Price") as! String)
        
        Common.SetShadowwithCorneronView(view: viewmain!, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    //MARK:- UIButton Action
    @IBAction func bckbtn(_ sender: Any) {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }

}
