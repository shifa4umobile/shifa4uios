//
//  OnlineMedicinesVC.swift
//  AppDesign
//
//  Created by UDHC on 11/23/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
import EzPopup

class OnlineMedicinesVC: BaseViewController {
    
    @IBOutlet weak var Upload: UIButton!
    @IBOutlet weak var MedicinesNames: UIButton!
    @IBOutlet weak var ORLabel: UILabel!
    @IBOutlet weak var TwoSidedButton: UIButton!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var DontKnow: UIButton!
    @IBOutlet weak var Know: UIButton!
    @IBOutlet weak var MainView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
         addSlideMenuButton()
         //initialSetup()
        
        TwoSidedButton.isHidden = true
        textLabel.isHidden = true
        Know.backgroundColor = K.Color.blue
        DontKnow.backgroundColor = K.Color.blue
        ORLabel.isHidden = true
        MedicinesNames.isHidden = true

        //textLabel.text = "Click one of the above buttons"
        
   Common.SetShadowwithCorneronView(view: MainView, cornerradius: 10.0, color: UIColor.gray)
   Common.SetShadowwithCorneronView(view: Know, cornerradius: 10.0, color: UIColor.gray)
        Common.SetShadowwithCorneronView(view: DontKnow, cornerradius: 10.0, color: UIColor.gray)
    }
    

    override func viewDidLayoutSubviews()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                removeView()
                addCartButton()
            }
            else
            {
                removeView()
                
                addCartButton()
            }
            
        } else {
            print("Portrait")
            //Image Height
            removeView()
            addCartButton()
        }
    }
    
   
    @IBAction func Names(_ sender: Any) {
        if User.loggedInUser() == nil
        {
            ISMessages.show("Login Please to avail this service")
            let loginVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            loginVC.is_comefromcartvc = "1"
            AppUtility.shared.mainNavController.pushViewController(loginVC, animated: true)
            return;
        }
        else
        {
        let customAlertVC = AppUtility.STORY_BOARD_SECOND().instantiateViewController(withIdentifier: "MedicinesPopupVC") as! MedicinesPopupVC
        let popupVC = PopupViewController(contentController: customAlertVC, popupWidth: 350)
        popupVC.cornerRadius = 5
        self.present(popupVC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func Know(_ sender: Any) {
        Know.backgroundColor = K.Color.green
        DontKnow.backgroundColor = K.Color.blue
        textLabel.isHidden = false
        textLabel.text = "If you know the name of your desired medinice, click below to visit Fazaldin family's website"
        TwoSidedButton.isHidden = false
        Common.SetShadowwithCorneronView(view: TwoSidedButton, cornerradius: 10.0, color: UIColor.clear)
        TwoSidedButton.backgroundColor = K.Color.green
        TwoSidedButton.setTitle("Go to Website", for: .normal)
        ORLabel.isHidden = true
        MedicinesNames.isHidden = true
    TwoSidedButton.addTapGesture { (gesture) in
         if User.loggedInUser() == nil
        {
            ISMessages.show("Login Please to avail this service.")
            let loginVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            loginVC.is_comefromcartvc = "1"
            AppUtility.shared.mainNavController.pushViewController(loginVC, animated: true)
            return;
        }
        else
         {
            //ISMessages.show("User is login")
            guard let url = URL(string: "https://sehat.com.pk/?affRefr=Shifa4u") else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
         }
        }
    }
    
    @IBAction func Dont(_ sender: Any) {
        DontKnow.backgroundColor = K.Color.green
        Know.backgroundColor = K.Color.blue
        textLabel.isHidden = false
        textLabel.text = "In case you are unable to read the prescription, or just don't feel like ordering yourself, just send us your prescription and we'll do the rest."
        TwoSidedButton.isHidden = false
        Common.SetShadowwithCorneronView(view: TwoSidedButton, cornerradius: 10.0, color: UIColor.clear)
        TwoSidedButton.backgroundColor = K.Color.red
        TwoSidedButton.setTitle("Upload Prescription", for: .normal)
        ORLabel.isHidden = false
        MedicinesNames.isHidden = false
        Common.SetShadowwithCorneronView(view: MedicinesNames, cornerradius: 10.0, color: UIColor.clear)
        TwoSidedButton.addTapGesture { (gesture) in
            if User.loggedInUser() == nil
            {
                ISMessages.show("Login Please to avail this service.")
                let loginVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                loginVC.is_comefromcartvc = "1"
                AppUtility.shared.mainNavController.pushViewController(loginVC, animated: true)
                return;
            }
            else
            {
                let customAlertVC = AppUtility.STORY_BOARD_SECOND().instantiateViewController(withIdentifier: "UploadViewController") as! UploadViewController
                let popupVC = PopupViewController(contentController: customAlertVC, popupWidth: 350)
                popupVC.cornerRadius = 5
                self.present(popupVC, animated: true, completion: nil)
              
            }
            
            
        }
    }
    
}
