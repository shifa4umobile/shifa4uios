//
//  ViewController.swift
//  AppDesign
//
//  Created by sachin on 07/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class ViewController: BaseViewController
{
    
    var storedOffsets = [Int: CGFloat]()
    var arrdata = NSMutableArray()
    let chooseArticleDropDown = DropDown()
    var arrTotalPrice:NSMutableArray = NSMutableArray()
    
    //labTest
    var arrLabTestList:NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var viewLabTests: UIView!
    @IBOutlet weak var viewLabTest_Height: NSLayoutConstraint! // 203
    @IBOutlet weak var tblLabTests: UITableView!
    
    
    //LabPackage
    var arrLabPackage:NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var viewLabPackage: UIView!
    @IBOutlet weak var viewLabPackage_Height: NSLayoutConstraint!
    @IBOutlet weak var tblLabPackage: UITableView!
    
    
    //Radiology
    var arrRadiologyList:NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var viewRadiology: UIView!
    @IBOutlet weak var viewRadiology_Height: NSLayoutConstraint!
    @IBOutlet weak var tblRadiologyList: UITableView!
    
    
    //Homecare
    var arrHomeCareList:NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var viewHomeCare: UIView!
    @IBOutlet weak var viewHomeCare_Height: NSLayoutConstraint!//124
    
    @IBOutlet weak var tblHomeCareList: UITableView!
    
    
    //final count
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var txtCouponCode: UITextField!
    @IBOutlet weak var btnApplyCouponCode: UIButton!
    
    
    @IBOutlet weak var lblTotalItem: UILabel!
    @IBOutlet weak var lblYourPrice_total: UILabel!
    @IBOutlet weak var lblVendorPrice_total: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblFinalTotal: UILabel!
    
    @IBOutlet weak var btnContinueOrder: UIButton!
    @IBOutlet weak var btnCheckout: UIButton!
    
    
    
    var is_selectallitemlab:Int = 0
    
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown
        ]
    }()
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //=====
        Common.SetShadowwithCorneronView(view: viewLabTests, cornerradius: 10.0, color: UIColor.init(red: 210/255, green: 211/255, blue: 212/255, alpha: 1.0))
        Common.setCornerOnView(view: tblLabTests, cornerradius: 10.0)
        Common.SetShadowwithCorneronView(view: viewLabPackage, cornerradius: 10.0, color: UIColor.init(red: 210/255, green: 211/255, blue: 212/255, alpha: 1.0))
        Common.setCornerOnView(view: tblLabPackage, cornerradius: 10.0)
        Common.SetShadowwithCorneronView(view: viewRadiology, cornerradius: 10.0, color: UIColor.init(red: 210/255, green: 211/255, blue: 212/255, alpha: 1.0))
        Common.setCornerOnView(view: tblRadiologyList, cornerradius: 10.0)
        Common.SetShadowwithCorneronView(view: viewHomeCare, cornerradius: 10.0, color: UIColor.init(red: 210/255, green: 211/255, blue: 212/255, alpha: 1.0))
        Common.setCornerOnView(view: tblHomeCareList, cornerradius: 10.0)
        
        //====
        Common.SetShadowwithCorneronView(view: btnContinueOrder, cornerradius: 10.0, color: UIColor.clear)
        Common.SetShadowwithCorneronView(view: btnCheckout, cornerradius: 10.0, color: UIColor.clear)
        Common.SetShadowwithCorneronView(view: btnApplyCouponCode, cornerradius: 10.0, color: UIColor.clear)
        //Common.setCornerOnView(view: viewdialogue, cornerradius: 10.0)
        
        
        //Add menu button
        //addSlideMenuButton()
        
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
        setAllCart_Array()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    override func viewDidLayoutSubviews()
    {
        //LabTest
        if UserDefaults.standard.object(forKey: "arrlabtestcart") != nil
        {
            if (UserDefaults.standard.object(forKey: "arrlabtestcart") as! NSArray).count == 0
            {
                viewLabTest_Height.constant=0
                viewLabTests.isHidden = true
            }
            else
            {
                viewLabTest_Height.constant=tblLabTests.contentSize.height + 44 + 10
            }
        }
        else
        {
            viewLabTest_Height.constant=0
            viewLabTests.isHidden = true
        }
        
        //Medical Package
        if UserDefaults.standard.object(forKey: "arrmedicalpackagecart") == nil
        {
            viewLabPackage_Height.constant=0
            viewLabPackage.isHidden = true
        }
        else
        {
            if (UserDefaults.standard.object(forKey: "arrmedicalpackagecart") as! NSArray).count == 0
            {
                viewLabPackage_Height.constant=0
                viewLabPackage.isHidden = true
            }
            else
            {
                viewLabPackage_Height.constant=tblLabPackage.contentSize.height + 44 + 10
            }
        }
        
        //Radiology
        if UserDefaults.standard.object(forKey: "arrradiologycart") == nil
        {
            viewRadiology_Height.constant=0
            viewRadiology.isHidden = true
        }
        else
        {
            if (UserDefaults.standard.object(forKey: "arrradiologycart") as! NSArray).count == 0
            {
                viewRadiology_Height.constant=0
                viewRadiology.isHidden = true
            }
            else
            {
                viewRadiology_Height.constant=tblRadiologyList.contentSize.height + 44 + 10
            }
        }
        
        //HomeCare
        if UserDefaults.standard.object(forKey: "arrhomecarecart") == nil
        {
            viewHomeCare_Height.constant=0
            viewHomeCare.isHidden = true
        }
        else
        {
            if (UserDefaults.standard.object(forKey: "arrhomecarecart") as! NSArray).count == 0
            {
                viewHomeCare_Height.constant=0
                viewHomeCare.isHidden = true
            }
            else
            {
                viewHomeCare_Height.constant=tblHomeCareList.contentSize.height + 44 + 10
            }
        }
        
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    func Total() -> String
    {
        return lblFinalTotal.text!
    }
    
    func setAllCart_Array()
    {
        //LabTest
        if  UserDefaults.standard.object(forKey: "arrlabtestcart") == nil
        {
            viewLabTests.isHidden = true
        }
        else
        {
            if (UserDefaults.standard.object(forKey: "arrlabtestcart") as! NSArray).count == 0
            {
                viewLabTests.isHidden = true
            }
            else
            {
                arrLabTestList = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrlabtestcart") as! NSArray)
                tblLabTests.reloadData()
            }
        }
        
        //Medical Package
        if  UserDefaults.standard.object(forKey: "arrmedicalpackagecart") == nil
        {
            viewLabPackage.isHidden = true
        }
        else
        {
            if (UserDefaults.standard.object(forKey: "arrmedicalpackagecart") as! NSArray).count == 0
            {
                viewLabPackage.isHidden = true
            }
            else
            {
                arrLabPackage = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrmedicalpackagecart") as! NSArray)
                tblLabPackage.reloadData()
            }
        }
        
        //Radiology
        if  UserDefaults.standard.object(forKey: "arrradiologycart") == nil
        {
            viewRadiology.isHidden = true
        }
        else
        {
            if (UserDefaults.standard.object(forKey: "arrradiologycart") as! NSArray).count == 0
            {
                viewRadiology.isHidden = true
            }
            else
            {
                arrRadiologyList = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrradiologycart") as! NSArray)
                tblRadiologyList.reloadData()
            }
        }
        
        //HomeCare
        if UserDefaults.standard.object(forKey: "arrhomecarecart") == nil
        {
            viewHomeCare.isHidden = true
        }
        else
        {
            if (UserDefaults.standard.object(forKey: "arrhomecarecart") as! NSArray).count == 0
            {
                viewHomeCare.isHidden = true
            }
            else
            {
                arrHomeCareList = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrhomecarecart") as! NSArray)
                tblHomeCareList.reloadData()
            }
        }
        
        setFinalPriceCalculate()
        checkSelectLabSame()
        DispatchQueue.main.async {
            self.viewDidLayoutSubviews()
        }
    }
    
    func setFinalPriceCalculate()
    {
        var YourPrice:Int = 0
        var VendorPrice:Int = 0
        is_selectallitemlab = 0
        //LabTest
        for j in 0..<arrLabTestList.count
        {
            let labDetails = arrLabTestList[j] as! NSMutableDictionary
            let arrsublabtemp = labDetails.object(forKey: "Labs") as! NSArray
            for i in 0..<arrsublabtemp.count
            {
                let dictemp = arrsublabtemp.object(at: i) as! NSDictionary
                if let val = dictemp["isselect"] {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        YourPrice = YourPrice + Int(dictemp.object(forKey: "YourPrice") as! Int)
                        VendorPrice = VendorPrice + Int(dictemp.object(forKey: "VendorPrice") as! Int)
                        is_selectallitemlab = is_selectallitemlab + 1
                    }
                }
            }
        }
        
        //package
        for j in 0..<arrLabPackage.count
        {
            let labDetails = arrLabPackage[j] as! NSMutableDictionary
            let arrsublabtemp = labDetails.object(forKey: "LabPackagesComparison") as! NSArray
            for i in 0..<arrsublabtemp.count
            {
                let dictemp = arrsublabtemp.object(at: i) as! NSDictionary
                if let val = dictemp["isselect"] {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        YourPrice = YourPrice + Int(dictemp.object(forKey: "YourPrice") as! Int)
                        VendorPrice = VendorPrice + Int(dictemp.object(forKey: "VendorPrice") as! Int)
                        is_selectallitemlab = is_selectallitemlab + 1
                    }
                }
            }
        }
        
        //radiology
        for j in 0..<arrRadiologyList.count
        {
            let labDetails = arrRadiologyList[j] as! NSMutableDictionary
            let arrRadiologyLabsComparison = labDetails.object(forKey: "RadiologyLabsComparison") as! NSArray
            for i in 0..<arrRadiologyLabsComparison.count
            {
                let dictemp = arrRadiologyLabsComparison.object(at: i) as! NSDictionary
                if let val = dictemp["isselect"] {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        YourPrice = YourPrice + Int(dictemp.object(forKey: "YourPrice") as! Int)
                        VendorPrice = VendorPrice + Int(dictemp.object(forKey: "VendorPrice") as! Int)
                        is_selectallitemlab = is_selectallitemlab + 1
                    }
                }
                
            }
        }
        
        //homecare
        for j in 0..<arrHomeCareList.count
        {
            let labDetails = arrHomeCareList[j] as! NSMutableDictionary
            
            YourPrice = YourPrice + Int(labDetails.object(forKey: "ProductLine_DiscountedPrice") as! Int)
            VendorPrice = VendorPrice + Int(labDetails.object(forKey: "ProductLine_SalePrice") as! Int)
            is_selectallitemlab = is_selectallitemlab + 1
        }
        
        lblYourPrice_total.text = NSString(format: "Rs. %@",Utility.setCurrencyFormate(NSString(format: "%d", VendorPrice) as String)) as String
        lblDiscount.text = NSString(format: "Rs. %@",Utility.setCurrencyFormate(NSString(format: "%d", (VendorPrice - YourPrice)) as String)) as String
        lblFinalTotal.text = NSString(format: "Rs. %@",Utility.setCurrencyFormate(NSString(format: "%d", YourPrice) as String)) as String
        
        
        
        
        let totalIteminCart = arrLabTestList.count + arrLabPackage.count + arrRadiologyList.count + arrHomeCareList.count
        lblTotalItem.text = NSString(format: "%d Items", totalIteminCart) as String
        
        self.viewDidLayoutSubviews()
    }
    
    
    //MARK:- UIButton Action
    
    @IBAction func btnBack(_ sender: Any) {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    
    @IBAction func btnContinueOrder(_ sender: Any)
    {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    @IBAction func btnApplyCouponCode(_ sender: Any)
    {
        let alert = UIAlertController(title: "Currently Unavaliable", message:"This Service is currently Unavaliable.", preferredStyle: .alert)
        let later = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(later)
        self.present(alert, animated: true, completion: nil)
        
    }
    @IBAction func btnCheckout(_ sender: Any)
    {
        let totalIteminCart = arrLabTestList.count + arrLabPackage.count + arrRadiologyList.count + arrHomeCareList.count
        if totalIteminCart == 0
        {
            ISMessages.show("Please select a Lab for each Item first and Try Again.")
            return;
        }
        
        if User.loggedInUser() == nil
        {
            let loginVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            loginVC.is_comefromcartvc = "1"
            AppUtility.shared.mainNavController.pushViewController(loginVC, animated: true)
        }
        else
        {
            
            if totalIteminCart != is_selectallitemlab
            {
                ISMessages.show("Please select a Lab for each Item first and Try Again.")
                return;
            }
            
            let vc = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "OrderPreviewVC") as! OrderPreviewVC
            AppUtility.shared.mainNavController.pushViewController(vc, animated: true)
        }
    }
    
    
    @IBAction func btncollectionType(_ sender: Any)
    {
    }
    
    @IBAction func btnprovider(_ sender: Any)
    {
    }
    
    @IBAction func btntype(_ sender: Any)
    {
    }
    
    @IBAction func btnlocation(_ sender: Any)
    {
    }
    
    @IBAction func btndeleteaction(_ sender: Any)
    {
    }
    
    
    //MARK:- Check Select Lab Same?
    func checkSelectLabSame()
    {
        var countsameid:Int = 0
        //LabTest
        let arr_selectlabid:NSMutableArray = NSMutableArray()
        //let arrLabTestList_selectlabid:NSMutableArray = NSMutableArray()
        for j in 0..<(self.arrLabTestList.count)
        {
            let labDetails = self.arrLabTestList[j] as! NSMutableDictionary
            let arrsublabtemp = labDetails.object(forKey: "Labs") as! NSArray
            for i in 0..<arrsublabtemp.count
            {
                let dictemp = arrsublabtemp.object(at: i) as! NSDictionary
                if let val = dictemp["isselect"] {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        if arr_selectlabid.contains("\(dictemp.object(forKey: "LabId") ?? "0")")
                        {
                            
                        }
                        else
                        {
                            arr_selectlabid.add("\(dictemp.object(forKey: "LabId") ?? "0")")
                        }
                    }
                }
            }
        }
        
        //Lab Package
        //let arrLabPackageList_selectlabid:NSMutableArray = NSMutableArray()
        for j in 0..<(self.arrLabPackage.count)
        {
            let labDetails = self.arrLabPackage[j] as! NSMutableDictionary
            let arrsublabtemp = labDetails.object(forKey: "LabPackagesComparison") as! NSArray
            for i in 0..<arrsublabtemp.count
            {
                let dictemp = arrsublabtemp.object(at: i) as! NSDictionary
                if let val = dictemp["isselect"] {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        if arr_selectlabid.contains("\(dictemp.object(forKey: "labId") ?? "0")")
                        {
                            
                        }
                        else
                        {
                            arr_selectlabid.add("\(dictemp.object(forKey: "labId") ?? "0")")
                        }
                        /*let LabId =  "\(dictemp.object(forKey: "LabId") ?? "0")"
                         for h in 0..<(arrLabPackageList_selectlabid.count)
                         {
                         let strid = arrLabPackageList_selectlabid.object(at: h) as! String
                         if Int(LabId) != Int(strid)
                         {
                         countsameid = countsameid + 1
                         }
                         }*/
                    }
                }
            }
        }
        
        //RadioLogy
        //let arrRadiologyList_selectlabid:NSMutableArray = NSMutableArray()
        for j in 0..<(self.arrRadiologyList.count)
        {
            let labDetails = self.arrRadiologyList[j] as! NSMutableDictionary
            let arrsublabtemp = labDetails.object(forKey: "RadiologyLabsComparison") as! NSArray
            for i in 0..<arrsublabtemp.count
            {
                let dictemp = arrsublabtemp.object(at: i) as! NSDictionary
                if let val = dictemp["isselect"] {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        if arr_selectlabid.contains("\(dictemp.object(forKey: "LabId") ?? "0")")
                        {
                            
                        }
                        else
                        {
                            arr_selectlabid.add("\(dictemp.object(forKey: "LabId") ?? "0")")
                        }
                    }
                }
            }
        }
        
        //        var LabId = String()
        //        for h in 0..<(arr_selectlabid.count)
        //        {
        //            LabId = arr_selectlabid.object(at: h) as! String
        //            let strid = arr_selectlabid.object(at: h) as! String
        //            if Int(LabId) != Int(strid)
        //            {
        //                countsameid = countsameid + 1
        //            }
        //        }
        
        //--
        //if countsameid > 0
        if arr_selectlabid.count > 1
        {
            let alert = UIAlertController(title: "Warning", message:"Please note that different tests from different labs will require a separate sample for each lab. While Radiology service is for walking Only", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
        }
    }
}

//MARK:- LabTest cell button action
extension ViewController
{
    @IBAction func btnRemove_LabTest(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tblLabTests)
        let indexPath = self.tblLabTests.indexPathForRow(at: buttonPosition)
        DispatchQueue.main.async {
            let actionSheetController: UIAlertController = UIAlertController(title: "Shifa4U", message: "Are you sure you want to remove order", preferredStyle: .alert)
            let Yes: UIAlertAction = UIAlertAction(title: "YES", style: .default) { action -> Void in
                self.arrLabTestList.removeObject(at: (indexPath?.row)!)
                
                UserDefaults.standard.set(self.arrLabTestList, forKey: "arrlabtestcart")
                UserDefaults.standard.synchronize()
                
                self.tblLabTests.reloadData()
                self.setFinalPriceCalculate()
                ISMessages.show("Lab Test removed")
                if (self.arrLabTestList.count + self.arrLabPackage.count + self.arrRadiologyList.count + self.arrHomeCareList.count) == 0
                {
                
                    AppUtility.sharedAppDelegate.setMainNavigationController()
                }
                else
                {
                    
                }
                self.viewDidLayoutSubviews()
            }
            let cancelAction: UIAlertAction = UIAlertAction(title: "NO", style: .cancel) { action -> Void in
            }
            actionSheetController.addAction(Yes)
            actionSheetController.addAction(cancelAction)
            self.present(actionSheetController, animated: true, completion: nil)
            
        }

        
    }
    @IBAction func btnLabTest_LabDropDown(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tblLabTests)
        let indexPath = self.tblLabTests.indexPathForRow(at: buttonPosition)
        
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        
        let labDetails = arrLabTestList[(indexPath?.row)!] as! NSMutableDictionary
        
        
        let arrLabsTemp = NSMutableArray()
        for i in 0..<(labDetails.object(forKey: "Labs") as! NSArray).count
        {
            let labname = ((labDetails.object(forKey: "Labs") as! NSArray).object(at: i) as! NSDictionary).object(forKey: "LabName")
            arrLabsTemp.add(labname as Any)
        }
        
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            lblNotificationText?.setTitle(item, for: .normal)
            
            
            //set select lab price
            let labDetails1 = NSMutableDictionary(dictionary: self?.arrLabTestList[(indexPath?.row)!] as! NSDictionary)
            
            var arrLabsTemp1:NSMutableArray = NSMutableArray()
            arrLabsTemp1 = NSMutableArray(array:labDetails1.object(forKey: "Labs") as! NSArray)
            
            for i in 0..<arrLabsTemp1.count
            {
                if i == index
                {
                    let dicsublab = NSMutableDictionary(dictionary: arrLabsTemp1.object(at: i) as! NSDictionary)
                    dicsublab.setValue("1", forKey: "isselect")
                    arrLabsTemp1.removeObject(at: i)
                    arrLabsTemp1.insert(dicsublab, at: i)
                }
                else
                {
                    let dicsublab = NSMutableDictionary(dictionary: arrLabsTemp1.object(at: i) as! NSDictionary)
                    dicsublab.setValue("0", forKey: "isselect")
                    arrLabsTemp1.removeObject(at: i)
                    arrLabsTemp1.insert(dicsublab, at: i)
                }
            }
            labDetails1.setValue(arrLabsTemp1, forKey: "Labs")
            self?.arrLabTestList.removeObject(at: (indexPath?.row)!)
            self?.arrLabTestList.insert(labDetails1, at: (indexPath?.row)!)
            self?.tblLabTests.reloadData()
            
            UserDefaults.standard.set(self?.arrLabTestList, forKey: "arrlabtestcart")
            UserDefaults.standard.synchronize()
            
            self?.chooseArticleDropDown.hide()
            DispatchQueue.main.async {
                self?.checkSelectLabSame()
                self?.setFinalPriceCalculate()
            }
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            if items.isEmpty {
                lblNotificationText?.setTitle("", for: .normal)
            }
        }
        
        chooseArticleDropDown.show()
    }
    @IBAction func btnLabTest_CollectionDropDown(_ sender: Any) {
        
    }
}

//MARK:- LabPackage cell button action
extension ViewController
{
    @IBAction func btnRemove_labPackage(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tblLabPackage)
        let indexPath = self.tblLabPackage.indexPathForRow(at: buttonPosition)

        DispatchQueue.main.async {
            let actionSheetController: UIAlertController = UIAlertController(title: "Shifa4U", message: "Are you sure you want to remove order", preferredStyle: .alert)
            let Yes: UIAlertAction = UIAlertAction(title: "YES", style: .default) { action -> Void in
                self.arrLabPackage.removeObject(at: (indexPath?.row)!)
                
                UserDefaults.standard.set(self.arrLabPackage, forKey: "arrmedicalpackagecart")
                UserDefaults.standard.synchronize()
                
                self.tblLabPackage.reloadData()
                self.setFinalPriceCalculate()
                ISMessages.show("Lab Package removed")
                if (self.arrLabTestList.count + self.arrLabPackage.count + self.arrRadiologyList.count + self.arrHomeCareList.count) == 0
                {
                    AppUtility.sharedAppDelegate.setMainNavigationController()
                }
                else
                {
                    
                }
                self.viewDidLayoutSubviews()
            }
            let cancelAction: UIAlertAction = UIAlertAction(title: "NO", style: .cancel) { action -> Void in
            }
            actionSheetController.addAction(Yes)
            actionSheetController.addAction(cancelAction)
            self.present(actionSheetController, animated: true, completion: nil)
            
        }

        
    }
    
    @IBAction func btnlabPackage_LabDropDown(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tblLabPackage)
        let indexPath = self.tblLabPackage.indexPathForRow(at: buttonPosition)
        
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        
        let labDetails = arrLabPackage[(indexPath?.row)!] as! NSMutableDictionary
        
        let arrLabsTemp = NSMutableArray()
        for i in 0..<(labDetails.object(forKey: "LabPackagesComparison") as! NSArray).count
        {
            let labname = ((labDetails.object(forKey: "LabPackagesComparison") as! NSArray).object(at: i) as! NSDictionary).object(forKey: "LabName")
            arrLabsTemp.add(labname as Any)
        }
        
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            lblNotificationText?.setTitle(item, for: .normal)
            
            
            //set select lab price
            let labDetails1 = NSMutableDictionary(dictionary: self?.arrLabPackage[(indexPath?.row)!] as! NSDictionary)
            
            var arrLabsTemp1:NSMutableArray = NSMutableArray()
            arrLabsTemp1 = NSMutableArray(array:labDetails1.object(forKey: "LabPackagesComparison") as! NSArray)
            
            for i in 0..<arrLabsTemp1.count
            {
                if i == index
                {
                    let dicsublab = NSMutableDictionary(dictionary: arrLabsTemp1.object(at: i) as! NSDictionary)
                    dicsublab.setValue("1", forKey: "isselect")
                    arrLabsTemp1.removeObject(at: i)
                    arrLabsTemp1.insert(dicsublab, at: i)
                }
                else
                {
                    let dicsublab = NSMutableDictionary(dictionary: arrLabsTemp1.object(at: i) as! NSDictionary)
                    dicsublab.setValue("0", forKey: "isselect")
                    arrLabsTemp1.removeObject(at: i)
                    arrLabsTemp1.insert(dicsublab, at: i)
                }
            }
            labDetails1.setValue(arrLabsTemp1, forKey: "LabPackagesComparison")
            self?.arrLabPackage.removeObject(at: (indexPath?.row)!)
            self?.arrLabPackage.insert(labDetails1, at: (indexPath?.row)!)
            self?.tblLabPackage.reloadData()
            
            UserDefaults.standard.set(self?.arrLabPackage, forKey: "arrmedicalpackagecart")
            UserDefaults.standard.synchronize()
            
            self?.chooseArticleDropDown.hide()
            
            DispatchQueue.main.async {
                self?.checkSelectLabSame()
                self?.setFinalPriceCalculate()
            }
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            if items.isEmpty {
                lblNotificationText?.setTitle("", for: .normal)
            }
        }
        
        chooseArticleDropDown.show()
    }
    @IBAction func btnCollectionType_LabPackage_dropdown(_ sender: Any) {
    }
}

//MARK:- Radiology cell button action
extension ViewController
{
    @IBAction func btnRemove_Radiology(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tblRadiologyList)
        let indexPath = self.tblRadiologyList.indexPathForRow(at: buttonPosition)
        
        DispatchQueue.main.async {
            let actionSheetController: UIAlertController = UIAlertController(title: "Shifa4U", message: "Are you sure you want to remove order", preferredStyle: .alert)
            let Yes: UIAlertAction = UIAlertAction(title: "YES", style: .default) { action -> Void in
                self.arrRadiologyList.removeObject(at: (indexPath?.row)!)
                
                UserDefaults.standard.set(self.arrRadiologyList, forKey: "arrradiologycart")
                UserDefaults.standard.synchronize()
                
                self.tblRadiologyList.reloadData()
                self.setFinalPriceCalculate()
                ISMessages.show("Radiology service removed")
                if (self.arrLabTestList.count + self.arrLabPackage.count + self.arrRadiologyList.count + self.arrHomeCareList.count) == 0
                {
                    
                    AppUtility.sharedAppDelegate.setMainNavigationController()
                }
                else
                {
                    
                }
                self.viewDidLayoutSubviews()
            }
            let cancelAction: UIAlertAction = UIAlertAction(title: "NO", style: .cancel) { action -> Void in
            }
            actionSheetController.addAction(Yes)
            actionSheetController.addAction(cancelAction)
            self.present(actionSheetController, animated: true, completion: nil)
            
        }

        
    }
    @IBAction func btnRadiology_ProviderDropDown(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tblRadiologyList)
        let indexPath = self.tblRadiologyList.indexPathForRow(at: buttonPosition)
        
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        
        let labDetails = NSMutableDictionary(dictionary: arrRadiologyList[(indexPath?.row)!] as! NSDictionary)
        
        let arrLabsTemp = NSMutableArray()
        for i in 0..<(labDetails.object(forKey: "RadiologyLabsComparison") as! NSArray).count
        {
            let labname = ((labDetails.object(forKey: "RadiologyLabsComparison") as! NSArray).object(at: i) as! NSDictionary).object(forKey: "LabName")
            arrLabsTemp.add(labname as Any)
        }
        
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            lblNotificationText?.setTitle(item, for: .normal)
            
            
            //set select lab price
            let labDetails1 = NSMutableDictionary(dictionary: self?.arrRadiologyList[(indexPath?.row)!] as! NSDictionary)
            
            var arrLabsTemp1:NSMutableArray = NSMutableArray()
            arrLabsTemp1 = NSMutableArray(array:labDetails1.object(forKey: "RadiologyLabsComparison") as! NSArray)
            
            for i in 0..<arrLabsTemp1.count
            {
                if i == index
                {
                    let dicsublab = NSMutableDictionary(dictionary: arrLabsTemp1.object(at: i) as! NSDictionary)
                    dicsublab.setValue("1", forKey: "isselect")
                    arrLabsTemp1.removeObject(at: i)
                    arrLabsTemp1.insert(dicsublab, at: i)
                }
                else
                {
                    let dicsublab = NSMutableDictionary(dictionary: arrLabsTemp1.object(at: i) as! NSDictionary)
                    dicsublab.setValue("0", forKey: "isselect")
                    arrLabsTemp1.removeObject(at: i)
                    arrLabsTemp1.insert(dicsublab, at: i)
                }
            }
            labDetails1.setValue(arrLabsTemp1, forKey: "RadiologyLabsComparison")
            self?.arrRadiologyList.removeObject(at: (indexPath?.row)!)
            self?.arrRadiologyList.insert(labDetails1, at: (indexPath?.row)!)
            
            do {
                let labDetails = NSMutableDictionary(dictionary: self?.arrRadiologyList[(indexPath?.row)!] as! NSDictionary)
                let arrRadiologyLabsComparison = labDetails.object(forKey: "RadiologyLabsComparison") as! NSArray
                for i in 0..<arrRadiologyLabsComparison.count
                {
                    //1
                    let dictemp = NSMutableDictionary(dictionary: arrRadiologyLabsComparison.object(at: i) as! NSDictionary)
                    if let val = dictemp["isselect"]
                    {
                        if NSString(format: "%@", val as! CVarArg) == "1"
                        {
                            let Vendors = NSMutableArray(array: labDetails.object(forKey: "Vendors") as! NSArray)
                            for j in 0..<Vendors.count
                            {
                                //2
                                let ImagingId = dictemp.object(forKey: "ImagingId") as! Int
                                let dictempInner = NSMutableDictionary(dictionary: Vendors.object(at: j) as! NSDictionary)
                                let ImagingId_inner = dictempInner.object(forKey: "ImagingId") as! Int
                                if ImagingId == ImagingId_inner
                                {
                                    let Locations = NSMutableArray(array: dictempInner.object(forKey: "Locations") as! NSArray)
                                    for c in 0..<Locations.count
                                    {
                                        //3
                                        let dictempInner_sub = NSMutableDictionary(dictionary: Locations.object(at: c) as! NSDictionary)
                                        
                                        dictempInner_sub.setValue("0", forKey: "isselect")
                                        Locations.removeObject(at: c)
                                        Locations.insert(dictempInner_sub, at: c)
                                    }
                                    //
                                    dictempInner.setValue(Locations, forKey: "Locations")
                                    Vendors.removeObject(at: j)
                                    Vendors.insert(dictempInner, at: j)
                                }
                            }
                            //
                            labDetails.setValue(Vendors, forKey: "Vendors")
                            self?.arrRadiologyList.removeObject(at: (indexPath?.row)!)
                            self?.arrRadiologyList.insert(labDetails, at: (indexPath?.row)!)
                        }
                    }
                }
            }
            
            self?.tblRadiologyList.reloadData()
            
            UserDefaults.standard.set(self?.arrRadiologyList, forKey: "arrradiologycart")
            UserDefaults.standard.synchronize()
            
            
            self?.chooseArticleDropDown.hide()
            DispatchQueue.main.async {
                self?.checkSelectLabSame()
                self?.setFinalPriceCalculate()
            }
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            if items.isEmpty {
                lblNotificationText?.setTitle("", for: .normal)
            }
        }
        
        chooseArticleDropDown.show()
    }
    @IBAction func btnRadiology_TypeDropDown(_ sender: Any) {
    }
    @IBAction func btnLocation_RadiologyDropdown(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tblRadiologyList)
        let indexPath = self.tblRadiologyList.indexPathForRow(at: buttonPosition)
        
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        
        let labDetails = NSMutableDictionary(dictionary: arrRadiologyList[(indexPath?.row)!] as! NSDictionary)
        
        let arrRadiologyLabsComparison = labDetails.object(forKey: "RadiologyLabsComparison") as! NSArray
        let arrLabsTemp = NSMutableArray()
        for i in 0..<arrRadiologyLabsComparison.count
        {
            let dictemp = arrRadiologyLabsComparison.object(at: i) as! NSDictionary
            if let val = dictemp["isselect"] {
                if NSString(format: "%@", val as! CVarArg) == "1"
                {
                    let Vendors = labDetails.object(forKey: "Vendors") as! NSArray
                    for j in 0..<Vendors.count
                    {
                        let ImagingId = dictemp.object(forKey: "LabId") as! Int
                        let dictempInner = Vendors.object(at: j) as! NSDictionary
                        let ImagingId_inner = dictempInner.object(forKey: "LabId") as! Int
                        if ImagingId == ImagingId_inner
                        {
                            let Locations = NSMutableArray(array: dictempInner.object(forKey: "Locations") as! NSArray)
                            for c in 0..<Locations.count
                            {
                                let dictempInner_sub = Locations.object(at: c) as! NSDictionary
                                let Contacts = dictempInner_sub.object(forKey: "Contacts") as! NSArray
                                if Contacts.count != 0
                                {
                                    let diccontact = Contacts.object(at: 0) as! NSDictionary
                                    let labname = diccontact.object(forKey: "Description")
                                    arrLabsTemp.add(labname as Any)
                                }
                            }
                        }
                    }
                }
            }
            
        }
        
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            lblNotificationText?.setTitle(item, for: .normal)
            
            let labDetails = NSMutableDictionary(dictionary: self?.arrRadiologyList[(indexPath?.row)!] as! NSDictionary)
            let arrRadiologyLabsComparison = labDetails.object(forKey: "RadiologyLabsComparison") as! NSArray
            for i in 0..<arrRadiologyLabsComparison.count
            {
                //1
                let dictemp = NSMutableDictionary(dictionary: arrRadiologyLabsComparison.object(at: i) as! NSDictionary)
                if let val = dictemp["isselect"]
                {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        let Vendors = NSMutableArray(array: labDetails.object(forKey: "Vendors") as! NSArray)
                        for j in 0..<Vendors.count
                        {
                            //2
                            let ImagingId = dictemp.object(forKey: "LabId") as! Int
                            let dictempInner = NSMutableDictionary(dictionary: Vendors.object(at: j) as! NSDictionary)
                            let ImagingId_inner = dictempInner.object(forKey: "LabId") as! Int
                            if ImagingId == ImagingId_inner
                            {
                                let Locations = NSMutableArray(array: dictempInner.object(forKey: "Locations") as! NSArray)
                                for c in 0..<Locations.count
                                {
                                    //3
                                    let dictempInner_sub = NSMutableDictionary(dictionary: Locations.object(at: c) as! NSDictionary)
                                    
                                    if c == index
                                    {
                                        dictempInner_sub.setValue("1", forKey: "isselect")
                                        Locations.removeObject(at: c)
                                        Locations.insert(dictempInner_sub, at: c)
                                    }
                                    else
                                    {
                                        dictempInner_sub.setValue("0", forKey: "isselect")
                                        Locations.removeObject(at: c)
                                        Locations.insert(dictempInner_sub, at: c)
                                    }
                                }
                                //
                                dictempInner.setValue(Locations, forKey: "Locations")
                                Vendors.removeObject(at: j)
                                Vendors.insert(dictempInner, at: j)
                            }
                        }
                        //
                        labDetails.setValue(Vendors, forKey: "Vendors")
                        self?.arrRadiologyList.removeObject(at: (indexPath?.row)!)
                        self?.arrRadiologyList.insert(labDetails, at: (indexPath?.row)!)
                    }
                }
            }
            self?.tblRadiologyList.reloadData()
            
            UserDefaults.standard.set(self?.arrRadiologyList, forKey: "arrradiologycart")
            UserDefaults.standard.synchronize()
            
            self?.chooseArticleDropDown.hide()
            
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            if items.isEmpty {
                lblNotificationText?.setTitle("", for: .normal)
            }
        }
        
        chooseArticleDropDown.show()
    }
}

//MARK:- HomeCare cell button action
extension ViewController
{
    @IBAction func btnRemove_HomeCare(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tblHomeCareList)
        let indexPath = self.tblHomeCareList.indexPathForRow(at: buttonPosition)
        DispatchQueue.main.async {
            let actionSheetController: UIAlertController = UIAlertController(title: "Shifa4U", message: "Are you sure you want to remove order", preferredStyle: .alert)
            let Yes: UIAlertAction = UIAlertAction(title: "YES", style: .default) { action -> Void in
                self.arrHomeCareList.removeObject(at: (indexPath?.row)!)
                
                UserDefaults.standard.set(self.arrHomeCareList, forKey: "arrhomecarecart")
                UserDefaults.standard.synchronize()
                
                self.tblHomeCareList.reloadData()
                self.setFinalPriceCalculate()
                ISMessages.show("Home care service removed")
                if (self.arrLabTestList.count + self.arrLabPackage.count + self.arrRadiologyList.count + self.arrHomeCareList.count) == 0
                {
                    
                    AppUtility.sharedAppDelegate.setMainNavigationController()
                }
                else
                {
                    
                }
                self.viewDidLayoutSubviews()
            }
            let cancelAction: UIAlertAction = UIAlertAction(title: "NO", style: .cancel) { action -> Void in
            }
            actionSheetController.addAction(Yes)
            actionSheetController.addAction(cancelAction)
            self.present(actionSheetController, animated: true, completion: nil)
            
        }

    }
}

//MARK:- Tableview Delegate Method
extension ViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblLabTests
        {
            return arrLabTestList.count
        }
        else if tableView == tblLabPackage
        {
            return arrLabPackage.count
        }
        else if tableView == tblRadiologyList
        {
            return arrRadiologyList.count
        }
        else if tableView == tblHomeCareList
        {
            return arrHomeCareList.count;
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblLabTests
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_labtest", for: indexPath)
            let lblTitle = cell.contentView.viewWithTag(1) as! UILabel
            let lblPrice = cell.contentView.viewWithTag(7) as! UILabel
            lblPrice.text = ""
            
            let btnLabName = cell.contentView.viewWithTag(4) as! UIButton
            
            let btnCollectionType = cell.contentView.viewWithTag(6) as! UIButton
            btnCollectionType.setTitle("Home Collection", for: .normal)
            
            let labDetails = arrLabTestList[indexPath.row] as! NSMutableDictionary
            lblTitle.text =  (labDetails.object(forKey: "Name") as! String)
            
            let arrsublabtemp = labDetails.object(forKey: "Labs") as! NSArray
            for i in 0..<arrsublabtemp.count
            {
                let dictemp = arrsublabtemp.object(at: i) as! NSDictionary
                if let val = dictemp["isselect"] {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        lblPrice.text = NSString(format: "Rs. %@", Utility.setCurrencyFormate(NSString(format: "%d",dictemp.object(forKey: "YourPrice") as! Int) as String)) as String
                        
                        btnLabName.setTitle((dictemp.object(forKey: "LabName") as! String), for: .normal)
                    }
                }
                
            }
            
            return cell
        }
        else if tableView == tblLabPackage
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_labpackage", for: indexPath)
            
            let lblTitle = cell.contentView.viewWithTag(1) as! UILabel
            let lblPrice = cell.contentView.viewWithTag(7) as! UILabel
            lblPrice.text = ""
            let btnLabName = cell.contentView.viewWithTag(4) as! UIButton
            let btnCollectionType = cell.contentView.viewWithTag(6) as! UIButton
            btnCollectionType.setTitle("Home Collection", for: .normal)
            
            let labDetails = arrLabPackage[indexPath.row] as! NSMutableDictionary
            lblTitle.text =  (labDetails.object(forKey: "Name") as! String)
            
            let arrsublabtemp = labDetails.object(forKey: "LabPackagesComparison") as! NSArray
            for i in 0..<arrsublabtemp.count
            {
                let dictemp = arrsublabtemp.object(at: i) as! NSDictionary
                if let val = dictemp["isselect"] {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        lblPrice.text = NSString(format: "Rs. %@",Utility.setCurrencyFormate(NSString(format: "%d",dictemp.object(forKey: "YourPrice") as! Int) as String)) as String
                        btnLabName.setTitle((dictemp.object(forKey: "LabName") as! String), for: .normal)
                    }
                }
            }
            
            return cell
        }
        else if tableView == tblRadiologyList
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_radiology", for: indexPath)
            let lblTitle = cell.contentView.viewWithTag(1) as! UILabel
            let lblPrice = cell.contentView.viewWithTag(9) as! UILabel
            lblPrice.text = ""
            
            let btnProvier = cell.contentView.viewWithTag(4) as! UIButton
            btnProvier.setTitle("Provider", for: .normal)
            
            let btnCollectionType = cell.contentView.viewWithTag(6) as! UIButton
            btnCollectionType.setTitle("Walking", for: .normal)
            
            let btnLocationdropdown = cell.contentView.viewWithTag(8) as! UIButton
            
            let labDetails = arrRadiologyList[indexPath.row] as! NSMutableDictionary
            lblTitle.text =  (labDetails.object(forKey: "ImagingName") as! String)
            
            let arrRadiologyLabsComparison = labDetails.object(forKey: "RadiologyLabsComparison") as! NSArray
            for i in 0..<arrRadiologyLabsComparison.count
            {
                let dictemp = arrRadiologyLabsComparison.object(at: i) as! NSDictionary
                if let val = dictemp["isselect"] {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        lblPrice.text = NSString(format: "Rs. %@",Utility.setCurrencyFormate(NSString(format: "%d",dictemp.object(forKey: "YourPrice") as! Int) as String)) as String
                        btnProvier.setTitle((dictemp.object(forKey: "LabName") as! String), for: .normal)
                    }
                }
            }
            
            var isSelectLocation:String = "no"
            for i in 0..<arrRadiologyLabsComparison.count
            {
                //1
                let dictemp = NSMutableDictionary(dictionary: arrRadiologyLabsComparison.object(at: i) as! NSDictionary)
                if let val = dictemp["isselect"]
                {
                    if NSString(format: "%@", val as! CVarArg) == "1"
                    {
                        let Vendors = NSMutableArray(array: labDetails.object(forKey: "Vendors") as! NSArray)
                        for j in 0..<Vendors.count
                        {
                            //2
                            let ImagingId = dictemp.object(forKey: "ImagingId") as! Int
                            let dictempInner = NSMutableDictionary(dictionary: Vendors.object(at: j) as! NSDictionary)
                            let ImagingId_inner = dictempInner.object(forKey: "ImagingId") as! Int
                            if ImagingId == ImagingId_inner
                            {
                                let Locations = NSMutableArray(array: dictempInner.object(forKey: "Locations") as! NSArray)
                                for c in 0..<Locations.count
                                {
                                    //3
                                    let dictempInner_sub = NSMutableDictionary(dictionary: Locations.object(at: c) as! NSDictionary)
                                    if let val = dictempInner_sub["isselect"]
                                    {
                                        if NSString(format: "%@", val as! CVarArg) == "1"
                                        {
                                            isSelectLocation = "yes"
                                            let Contacts = dictempInner_sub.object(forKey: "Contacts") as! NSArray
                                            if Contacts.count != 0
                                            {
                                                let diccontact = Contacts.object(at: 0) as! NSDictionary
                                                let labname = diccontact.object(forKey: "Description") as! String
                                                btnLocationdropdown.setTitle(labname, for: .normal)
                                            }
                                            else
                                            {
                                                btnLocationdropdown.setTitle((dictempInner_sub.object(forKey: "Name") as! String), for: .normal)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            let btnlocation = cell.contentView.viewWithTag(8) as! UIButton
            if isSelectLocation == "no"
            {
                btnlocation.setTitle("Select Location", for: .normal)
            }
            
            return cell
        }
        else if tableView == tblHomeCareList
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_homecare", for: indexPath)
            
            let lblTitle = cell.contentView.viewWithTag(1) as! UILabel
            let lblPrice = cell.contentView.viewWithTag(3) as! UILabel
            
            let labDetails = arrHomeCareList[indexPath.row] as! NSMutableDictionary
            lblTitle.text =  (labDetails.object(forKey: "ProductLine_ProductName") as! String)
            
            lblPrice.text = NSString(format: "Rs. %@",Utility.setCurrencyFormate(NSString(format: "%d",labDetails.object(forKey: "ProductLine_DiscountedPrice") as! Int) as String)) as String
            
            return cell
        }
        return UITableViewCell()
    }
    
    
    /*func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
     {
     if tableView == tblList
     {
     return AppUtility.shared.getTotalTypesOfProduct()
     }
     else
     {
     if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartLab.count > 0 && tableView.tag == 0) {
     return AppUtility.shared.arrCartLab.count
     }
     else if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartDiet.count > 0 && tableView.tag == 1){
     return AppUtility.shared.arrCartDiet.count
     }
     else if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartOrthopedic.count > 0 && tableView.tag == 2) {
     return AppUtility.shared.arrCartOrthopedic.count
     }
     else if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartNeuro.count > 0 && tableView.tag == 3){
     return AppUtility.shared.arrCartNeuro.count
     }
     else if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartRadiology.count > 0 && tableView.tag == 4){
     return AppUtility.shared.arrCartRadiology.count
     }
     else {
     return 0
     }
     //            if let dict = AppUtility.shared.arrCart[tableView.tag] as? Dictionary<String,Any> {
     //                let arr = dict["products"] as! NSArray
     //                return arr.count
     //            }
     //            return 0
     //            let dict = arrdata[tableView.tag] as? NSDictionary
     //            let arr = dict!["detail"] as! NSArray
     //            return arr.count
     }
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
     {
     if tableView == tblList
     {
     var strcellname = String()
     var strcellTitle: String = ""
     var productCount = 0
     
     if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartLab.count > 0 && tableView.tag == 0) {
     strcellTitle = K.Key.CartLab
     productCount = AppUtility.shared.arrCartLab.count
     strcellname = "CurrentOrderCell"
     }
     else if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartDiet.count > 0 && tableView.tag == 1) {
     strcellTitle = K.Key.CartDiet
     productCount = AppUtility.shared.arrCartDiet.count
     strcellname = "CurrentOrderCell"
     }
     else if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartOrthopedic.count > 0 && tableView.tag == 2) {
     strcellTitle = K.Key.CartLab
     productCount = AppUtility.shared.arrCartOrthopedic.count
     strcellname = "CurrentOrderCell"
     }
     else if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartNeuro.count > 0 && tableView.tag == 3) {
     strcellTitle = K.Key.CartDiet
     productCount = AppUtility.shared.arrCartNeuro.count
     strcellname = "CurrentOrderCell"
     }
     else if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartNeuro.count > 0 && tableView.tag == 4) {
     strcellTitle = K.Key.CartRadiology
     productCount = AppUtility.shared.arrCartRadiology.count
     strcellname = "CurrentOrderCellRadiology"
     }
     else {
     strcellname = "CurrentOrderCell"
     strcellTitle = "Other"
     productCount = 0
     strcellname = "CurrentOrderCell"
     }
     
     //                let dict = arrdata[indexPath.row] as? NSDictionary
     //                var strcellname = String()
     //                if dict!["type"] as! String == "3"
     //                {
     //                    strcellname = "CurrentOrderCellRadiology"
     //                }
     //                else
     //                {
     //                    strcellname = "CurrentOrderCell"
     //                }
     let cell = tableView.dequeueReusableCell(withIdentifier: strcellname, for: indexPath) as! CurrentOrderCell
     
     //let arr = dict!["detail"] as! NSArray
     // cell.lblprice.text = dict!["price"] as? String
     cell.lbltitle.text = strcellTitle
     Common.SetShadowwithCorneronView(view: cell.viewmain, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
     if strcellTitle  == K.Key.CartRadiology
     {
     cell.tblheight.constant = CGFloat(productCount * 201)
     }
     else
     {
     cell.tblheight.constant = CGFloat(productCount * 127)
     }
     
     return cell
     }
     else
     {
     let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath) as! DetailCell
     
     cell.viewLab.isHidden = false
     cell.viewCollectionType.isHidden = false
     var strcellName: String = ""
     var strcellPrice: String = ""
     
     if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartLab.count >  0 && tableView.tag == 0) {
     strcellPrice = "0.0"
     cell.btnDropDown.setTitle("Select Lab", for: .normal)
     
     let product = AppUtility.shared.arrCartLab[indexPath.row]
     
     let selectedItem = product.arrLabs.filter({$0.isSelected == true}).first
     if selectedItem != nil {
     strcellPrice = "\(K.kCurrency) \(selectedItem?.price ?? 0)/-"
     cell.btnDropDown.setTitle(selectedItem?.labName, for: .normal)
     }
     
     
     cell.btncolectionType.addTapGesture { (gesture) in
     AppUtility.shared.removeProductFromLab(item: product)
     self.tblList.reloadData()
     }
     
     cell.viewLab.addTapGesture { (gesture) in
     
     self.chooseArticleDropDown.dataSource = product.arrLabs.map({$0.labName})
     self.chooseArticleDropDown.anchorView = cell.btnDropDown
     self.chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (cell.btnDropDown?.bounds.height)!)
     self.chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
     
     let obj = AppUtility.shared.arrCartLab[indexPath.row]
     let lab = obj.arrLabs[index]
     lab.isSelected = true
     AppUtility.shared.arrCartLab[indexPath.row] = obj
     
     //                            lblNotificationText?.setTitle(item, for: .normal)
     //                            self?.lblprice.text = "\(K.kCurrency) \(self?.selectedLab?.price ?? 0)/-"
     
     //cell.lbltitle.text = strcellName
     cell.lblprice.text = "\(K.kCurrency) \(lab.price ?? 0)/-"
     
     self?.chooseArticleDropDown.hide()
     
     cell.btnDropDown.setTitle(item, for: .normal)
     }
     
     self.chooseArticleDropDown.show()
     }
     }
     else if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartDiet.count >  0 && tableView.tag == 1) {
     cell.viewLab.isHidden = true
     cell.viewCollectionType.isHidden = true
     
     strcellPrice = "0.0"
     let product = AppUtility.shared.arrCartDiet[indexPath.row]
     strcellName = product.name
     strcellPrice = "\(product.price ?? 0)"
     
     cell.btncolectionType.addTapGesture { (gesture) in
     AppUtility.shared.removeProductFromDiet(item: product)
     self.tblList.reloadData()
     }
     }
     else if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartRadiology.count >  0 && tableView.tag == 2) {
     cell.viewLab.isHidden = true
     cell.viewCollectionType.isHidden = true
     
     strcellPrice = "0.0"
     let product = AppUtility.shared.arrCartDiet[indexPath.row]
     strcellName = product.name
     strcellPrice = "\(product.price ?? 0)"
     
     cell.btncolectionType.addTapGesture { (gesture) in
     AppUtility.shared.removeProductFromDiet(item: product)
     self.tblList.reloadData()
     }
     }
     
     cell.lbltitle.text = strcellName
     cell.lblprice.text = strcellPrice
     
     
     
     
     
     //                else if let arrDiet = AppUtility.shared.dictCartData[K.Key.CartDiet] as? Array<HomeCare> {
     //                    if let diet = arrDiet[indexPath.row] as? HomeCare {
     //                        cell.lbltitle.text = diet.name
     //                    }
     //                }
     
     //                let dict = arrdata[tableView.tag] as? NSDictionary
     //                let arr = dict!["detail"] as! NSArray
     //
     //                cell.lbltitle.text = arr[indexPath.row] as? String
     
     cell.btncolectionType.tag = indexPath.row
     
     return cell
     }
     }
     
     func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath)
     {
     if tableView == tblList
     {
     guard let tableViewCell = cell as? CurrentOrderCell else { return }
     
     storedOffsets[indexPath.row] = tableViewCell.tableViewOffset
     }
     else
     {
     
     }
     }
     
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
     {
     }
     
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
     {
     if tableView == tblList
     {
     var productCount = 0
     var removeDropDownHeight = CGFloat(0)
     
     if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartLab.count > 0 && tableView.tag == 0) {
     removeDropDownHeight = 0
     productCount = AppUtility.shared.arrCartLab.count
     }
     else if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartDiet.count > 0 && tableView.tag == 1) {
     removeDropDownHeight = 40
     productCount = AppUtility.shared.arrCartDiet.count
     }
     
     if indexPath.row == 0
     {
     if productCount == 0
     {
     return 0
     }
     var height =  CGFloat((productCount * 127) + 100)
     height = height - removeDropDownHeight
     return height
     }
     else if indexPath.row == 1
     {
     if productCount == 0
     {
     return 0
     }
     var height = CGFloat((productCount * 127) + 100)
     height = height - removeDropDownHeight
     return height
     }
     else if indexPath.row == 2
     {
     if productCount == 0
     {
     return 0
     }
     var height: CGFloat = CGFloat((productCount * 201) + 73)
     height = height - removeDropDownHeight
     return height
     }
     else
     {
     return UITableViewAutomaticDimension
     }
     }
     else
     {
     //            let dict = arrdata[tableView.tag] as? NSDictionary
     //            if dict!["title"] as? String  == "Radiology"
     
     var title = ""
     
     if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartLab.count > 0 && tableView.tag == 0) {
     title = K.Key.CartLab
     }
     else if (AppUtility.shared.getTotalTypesOfProduct() > 0 && AppUtility.shared.arrCartDiet.count > 0 && tableView.tag == 1) {
     title = K.Key.CartDiet
     }
     
     if title == "Radiology"
     {
     return 201
     }
     else{
     
     return 127
     }
     }
     }
     
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
     {
     if tableView == tblList
     {
     guard let tableViewCell = cell as? CurrentOrderCell else { return }
     
     tableViewCell.settableViewDataSourceDelegate(self, forRow: indexPath.row)
     tableViewCell.tableViewOffset = storedOffsets[indexPath.row] ?? 0
     }
     else
     {
     
     }
     }
     
     func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
     {
     
     print("scrollViewWillBeginDragging")
     }*/
}
//MARK:-
