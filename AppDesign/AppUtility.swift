            
//
//  AppUtility.swift

//
//  Created on 15/11/16.
//
//

import UIKit
import Foundation
//import SVProgressHUD
import Gzip

class AppUtility: NSObject {
    
    
    //MARK: -
    static let shared = AppUtility()
    static let sharedAppDelegate: AppDelegate = (UIApplication.shared.delegate as? AppDelegate)!
    var mainNavController: MainNavigationController!
    var loginNavController: LoginNavigationController!
    
    var arrCountry: Array<Dictionary<String,Any>> = []
    var arrCart: Array<Dictionary<String,Any>> = []
    var arrCartItems: Array<CartItems> = []
    var dictCartData: Dictionary<String,Any> = [:]

    var arrCartLab: Array<LabDetails> = []
    var arrCartDiet: Array<HomeCare> = []
    var arrCartOrthopedic: Array<HomeCare> = []
    var arrCartNeuro: Array<HomeCare> = []
    var arrCartRadiology: Array<Radiology> = []

    //MARK: -
    override init() {
        super.init()
        print("Initialize shared Utility")
        intializeOnce()
    }
    
    func intializeOnce() -> Void {
        UIApplication.shared.statusBarStyle = .lightContent
        
    }
    
    
    //MARK: - Class Functions
    class func STORY_BOARD() -> UIStoryboard {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard
    }
    class func STORY_BOARD_SECOND() -> UIStoryboard {
        let storyboard = UIStoryboard(name: "Second_Storyboard", bundle: nil)
        return storyboard
    }
    
    class func SCREEN_WIDTH() -> CGFloat {
        return UIScreen.main.bounds.size.width
    }
    
    class func SCREEN_HEIGHT() -> CGFloat {
        return UIScreen.main.bounds.size.height
    }
    
    class func RGBACOLOR(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: (red) / 255.0, green: (green) / 255.0, blue: (blue) / 255.0, alpha: alpha)
    }
    
    class func RGBCOLOR(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return RGBACOLOR(red: red, green: green, blue: blue, alpha: 1)
    }
    
    class func redirectToTermsAndCondition(from viewController: UIViewController, type: String, isTerms: Bool) {
        if isTerms == true {
            let termsVC = self.STORY_BOARD().instantiateViewController(withIdentifier: "TermsAndConditoinVC") as! TermsAndConditoinVC
            termsVC.isFromLogin = true
            viewController.navigationController?.pushViewController(termsVC, animated: true)
        }
        else {
            let privacyVC = self.STORY_BOARD().instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
            privacyVC.isFromLogin = true
            viewController.navigationController?.pushViewController(privacyVC, animated: true)
        }
    }
    
    
    //MARK: - Get Common Data (i.e: Interest list of News/Blog/Article and Country list)
    func getAllCountry(shouldRefreshData: Bool? = false,completion: ((Bool?, Error?) -> ())?) {
        
        if arrCountry.count > 0  {
            completion?(true, nil)
            return
        }

        SVProgressHUD.show()

        let url = URL(string: K.URL.GET_ALL_COUNTRY)
        let request = URLRequest(url: url!)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            SVProgressHUD.dismiss()
            if data != nil
            {
                // gunzip
                let decompressedData: Data
                if (data?.isGzipped)! {
                    decompressedData = (try! data?.gunzipped())!
                } else {
                    decompressedData = data!
                }
                
                let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
                
                if let arrData = dictionary as? Array<Dictionary<String,Any>> {
                    print(dictionary!)
                    
                    for dict in arrData {
                        self.arrCountry.append(dict)
                    }
                    completion?(true, nil)
                }
                else {
                    completion?(false, error)
                }
            }
        });
        task.resume()
    }
    
    func getCity(countryID: Int? = 0,completion: ((Bool?, Error?, Array<Dictionary<String,Any>>?) -> ())?) {
        
        if countryID == 0  {
            completion?(false, nil, nil)
            return
        }
        
        let url = URL(string: K.URL.GET_CITY + "countryId=\(countryID!)")
        let request = URLRequest(url: url!)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            
            // gunzip
            let decompressedData: Data
            if (data?.isGzipped)! {
                decompressedData = (try! data?.gunzipped())!
            } else {
                decompressedData = data!
            }
            
            let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)

            if let arrData = dictionary as? Array<Dictionary<String,Any>> {
                print(dictionary!)
                completion?(true, nil, arrData)
            }
            else {
                completion?(false, error, nil)
            }
            
        });
        task.resume()
    }

    func getImage(imageID: Int,completion: ((Bool?, Error?, UIImage?) -> ())?)
    {
        var token:String = ""
        if UserDefaults.standard.object(forKey: "token") != nil
        {
            token = UserDefaults.standard.object(forKey: "token") as! String
        }
        
        
        let headers = [
            "Authorization": NSString(format: "bearer %@", token)
        ]
        
        let str1 = NSString(format: "%@%d", K.URL.displayfilebyfileid, imageID)
       // let str1 = NSString(format: "%@120", K.URL.displayfilebyfileid)
        let str = str1.replacingOccurrences(of: " ", with: "%20")
        let url = URL(string: str as String)

        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers as [String : String]

        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) {(data, response, error) in
            DispatchQueue.main.async {
                let httpResponse = response as? HTTPURLResponse
                if httpResponse?.statusCode == 200{
                    if let newData = data, newData.count > 0
                    {
                        let decompressedData: Data
                        if (data?.isGzipped)! {
                            decompressedData = (try! data?.gunzipped())!
                        } else {
                            decompressedData  = data!
                        }
//                        let datastring = "\(data: decompressedData, encoding: String.Encoding.utf8.rawValue)"
//                        print(datastring as Any)

                        var backToString = String(data: decompressedData, encoding: String.Encoding.utf8) as String?
                        //print(backToString)
                        
                        // let myArray = [UInt8](decompressedData)
                        // print(myArray)
                        
                        // let strBase64:String = decompressedData.base64EncodedString()
                        //print(strBase64)
                        //var base64String: String!
                        
                        //base64String = decompressedData.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: NSData.Base64EncodingOptions.RawValue(0))) as String?
                        //print(base64String)
                        backToString = backToString?.replacing("\"", with: "")
                        let imageData = NSData(base64Encoded: backToString!, options: NSData.Base64DecodingOptions(rawValue: 0))
                        
                        
                        
                        let image = UIImage(data: imageData! as Data)
                        
                        
                        if image != nil {
                            completion?(true, nil, image)
                        }
                        else {
                            completion?(false, nil, #imageLiteral(resourceName: "bannerPlaceholder"))
                        }
                    }
                    else {
                        completion?(false, nil, #imageLiteral(resourceName: "bannerPlaceholder"))
                    }
                }
            }

        }

        task.resume()
    }
    
    func getImageByName(imageName: String,completion: ((Bool?, Error?, UIImage?) -> ())?)
    {
        var token:String = ""
        if UserDefaults.standard.object(forKey: "token") != nil
        {
            token = UserDefaults.standard.object(forKey: "token") as! String
        }
        
        
        let headers = [
            "Authorization": NSString(format: "bearer %@", token)
        ]
    
        let str1 = K.URL.displayfilebyfileName + imageName
        print(str1)
        print(imageName)
        let url = URL(string: str1 as String)
        
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers as [String : String]
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) {(data, response, error) in
            DispatchQueue.main.async {
                let httpResponse = response as? HTTPURLResponse
                if httpResponse?.statusCode == 200{
                    if let newData = data, newData.count > 0
                    {
                        let decompressedData: Data
                        if (data?.isGzipped)! {
                            decompressedData = (try! data?.gunzipped())!
                        } else {
                            decompressedData  = data!
                        }
                        //                        let datastring = "\(data: decompressedData, encoding: String.Encoding.utf8.rawValue)"
                        //                        print(datastring as Any)
                        
                        var backToString = String(data: decompressedData, encoding: String.Encoding.utf8) as String?
                        //print(backToString)
                        
                        // let myArray = [UInt8](decompressedData)
                        // print(myArray)
                        
                        // let strBase64:String = decompressedData.base64EncodedString()
                        //print(strBase64)
                        //var base64String: String!
                        
                        //base64String = decompressedData.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: NSData.Base64EncodingOptions.RawValue(0))) as String?
                        //print(base64String)
                        backToString = backToString?.replacing("\"", with: "")
                        let imageData = NSData(base64Encoded: backToString!, options: NSData.Base64DecodingOptions(rawValue: 0))
                        
                        
                        
                        let image = UIImage(data: imageData! as Data)
                        
                        
                        if image != nil {
                            completion?(true, nil, image)
                        }
                        else {
                            completion?(false, nil, #imageLiteral(resourceName: "bannerPlaceholder"))
                        }
                    }
                    else {
                        completion?(false, nil, #imageLiteral(resourceName: "bannerPlaceholder"))
                    }
                }
            }
            
        }
        
        task.resume()
    }
    
    
    func getImageByNameDownload(imageName: String,completion: ((Bool?, Error?, UIImage?) -> ())?)
    {
        var token:String = ""
        if UserDefaults.standard.object(forKey: "token") != nil
        {
            token = UserDefaults.standard.object(forKey: "token") as! String
        }
        
        
        let headers = [
            "Authorization": NSString(format: "bearer %@", token)
        ]
        
        let str1 = K.URL.DownloadfilebyfileName + imageName
        print(str1)
        print(imageName)
        let url = URL(string: str1 as String)
        
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers as [String : String]
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) {(data, response, error) in
            DispatchQueue.main.async {
                let httpResponse = response as? HTTPURLResponse
                if httpResponse?.statusCode == 200{
                    if let newData = data, newData.count > 0
                    {
                        let decompressedData: Data
                        if (data?.isGzipped)! {
                            decompressedData = (try! data?.gunzipped())!
                        } else {
                            decompressedData  = data!
                        }
                        //                        let datastring = "\(data: decompressedData, encoding: String.Encoding.utf8.rawValue)"
                        //                        print(datastring as Any)
                        
                        //var backToString = String(data: decompressedData, encoding: String.Encoding.utf8) as String?
                        //print(backToString)
                        
                        // let myArray = [UInt8](decompressedData)
                        // print(myArray)
                        
                        // let strBase64:String = decompressedData.base64EncodedString()
                        //print(strBase64)
                        //var base64String: String!
                        
                        //base64String = decompressedData.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: NSData.Base64EncodingOptions.RawValue(0))) as String?
                        //print(base64String)
                        //backToString = backToString?.replacing("\"", with: "")
                        //let imageData = NSData(base64Encoded: backToString!, options: NSData.Base64DecodingOptions(rawValue: 0))
                        
                        
                        
                        let image = UIImage(data: decompressedData as Data)
                        
                        
                        if image != nil {
                            completion?(true, nil, image)
                        }
                        else {
                            completion?(false, nil, #imageLiteral(resourceName: "bannerPlaceholder"))
                        }
                    }
                    else {
                        completion?(false, nil, #imageLiteral(resourceName: "bannerPlaceholder"))
                    }
                }
            }
            
        }
        
        task.resume()
    }
    
    //MARK: - CART
    func getTotalTypesOfProduct() -> Int {
        var count = 0
        if AppUtility.shared.arrCartLab.count > 0 {
            count = count + 1
        }
        if AppUtility.shared.arrCartDiet.count > 0 {
            count = count + 1
        }
        if AppUtility.shared.arrCartOrthopedic.count > 0 {
            count = count + 1
        }
        if AppUtility.shared.arrCartNeuro.count > 0 {
            count = count + 1
        }
        return count
    }


    //Add To Cart
    func addProductToDiet(item: HomeCare) {
        if AppUtility.shared.arrCartDiet.contains(item) == false {
            AppUtility.shared.arrCartDiet.append(item)
        }
    }

    func addProductToLab(item: LabDetails) {
        if AppUtility.shared.arrCartLab.contains(item) == false {
            AppUtility.shared.arrCartLab.append(item)
        }
    }

    func addProductToOrthopedic(item: HomeCare) {
        if AppUtility.shared.arrCartOrthopedic.contains(item) == false {
            AppUtility.shared.arrCartOrthopedic.append(item)
        }
    }

    func addProductToNuero(item: HomeCare) {
        if AppUtility.shared.arrCartNeuro.contains(item) == false {
            AppUtility.shared.arrCartNeuro.append(item)
        }
    }

    func addProductToRadiology(item: Radiology) {
        if AppUtility.shared.arrCartRadiology.contains(item) == false {
            AppUtility.shared.arrCartRadiology.append(item)
        }
    }


    //Remove From Cart
    func removeProductFromLab(item: LabDetails) {
        AppUtility.shared.arrCartLab.remove(element: item)
    }

    func removeProductFromDiet(item: HomeCare) {
        AppUtility.shared.arrCartDiet.remove(element: item)
    }

    func removeProductFromOrthopedic(item: HomeCare) {
        AppUtility.shared.arrCartOrthopedic.remove(element: item)
    }

    func removeProductFromNeuro(item: HomeCare) {
        AppUtility.shared.arrCartNeuro.remove(element: item)
    }

    func removeProductFromRadiology(item: Radiology) {
        AppUtility.shared.arrCartRadiology.remove(element: item)
    }

//Working for Diet
//    func addProductToCart(type: String, product:Any) {
//        var isFound: Bool = false
//        for (index, var obj) in AppUtility.shared.arrCart.enumerated() {
//            if (obj["title"] as? String ?? "") == type {
//                isFound = true
//                if (obj["title"] as? String) == K.Key.CartLab {
//                    if var arr = obj["products"] as? Array<LabDetails> {
//                        if arr.contains(product as! LabDetails) == false {
//                            arr.append(product as! LabDetails)
//                            obj["products"] = arr
//                            AppUtility.shared.arrCart[index] = obj
//                        }
//
//                    }
//                    else {
//                        AppUtility.shared.arrCart.append(["title" : K.Key.CartLab, "products" : [product as? LabDetails]])
//                    }
//                }
//                else if (obj["title"] as? String) == K.Key.CartDiet {
//                    if var arr = obj["products"] as? Array<HomeCare> {
//                        if arr.contains(product as! HomeCare) == false {
//                            arr.append(product as! HomeCare)
//                            obj["products"] = arr
//                            AppUtility.shared.arrCart[index] = obj
//                        }
//                    }
//                    else {
//                        AppUtility.shared.arrCart.append(["title" : K.Key.CartDiet, "products" : [product as? HomeCare]])
//                    }
//                }
//            }
//        }
//
//        if isFound == false {
//            if type == K.Key.CartLab {
//                AppUtility.shared.arrCart.append(["title" : K.Key.CartLab, "products" : [product as? LabDetails]])
//            }
//            else if type == K.Key.CartDiet {
//                AppUtility.shared.arrCart.append(["title" : K.Key.CartDiet, "products" : [product as? HomeCare]])
//            }
//        }
//
//    }
//    func addProductToCart(type: String, product:Any) {
//        if type == K.Key.CartLab {
//            if var arr = dictCartData[K.Key.CartLab] as? Array<LabDetails> {
//                if arr.contains(product as! LabDetails) == false {
//                    arr.append(product as! LabDetails)
//                    dictCartData[K.Key.CartLab] = arr
//                }
//            }
//            else {
//                dictCartData[K.Key.CartLab] = [product as? LabDetails]
//            }
//        }
//        else if type == K.Key.CartDiet {
//            if var arr = dictCartData[K.Key.CartDiet] as? Array<HomeCare> {
//                if arr.contains(product as! HomeCare) == false {
//                    arr.append(product as! HomeCare)
//                    dictCartData[K.Key.CartDiet] = arr
//                }
//            }
//            else {
//                dictCartData[K.Key.CartDiet] = [product as? HomeCare]
//            }
//
//        }
//    }

}
