//
//  PharmacyViewController.swift
//  AppDesign
//
//  Created by Mahavir Makwana on 02/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class PharmacyViewController: BaseViewController {

    @IBOutlet weak var tblList: UITableView!
    var arrList: Array<PharmacyModel> = []
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSlideMenuButton()
        initialSetup()
    }

    override func viewDidLayoutSubviews()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                removeView()
                addCartButton()
            }
            else
            {
                removeView()

                addCartButton()
            }

        } else {
            print("Portrait")
            //Image Height
            removeView()
            addCartButton()
        }
    }


    //MARK: - IBActions

    @IBAction func btnback(_ sender: Any) {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    
    //MARK: - Other Actions
    func initialSetup() {
       //r addSlideMenuButton()


        arrList.removeAll(keepingCapacity: false)
        //arrList.append(PharmacyModel(dict: ["id":1 ,"icon": #imageLiteral(resourceName: "ImgPharmacy1"), "LinkUrl": "https://sehat.com.pk/?affRefr=Shifa4u"]))
        arrList.append(PharmacyModel(dict: ["id":2 ,"icon": #imageLiteral(resourceName: "imgPharmacy2"), "LinkUrl": "https://sehat.com.pk/?affRefr=Shifa4u"]))



        tblList.reloadData()

    }


    //MARK: - UITextField Delegate


    //MARK: - Webservice

}
//MARK:- Tableview Delegate Method
extension PharmacyViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        let viewmain = cell.contentView.viewWithTag(1) as! UIView
        let imgImage = cell.contentView.viewWithTag(3) as! UIImageView
        let btnaddtocart = cell.contentView.viewWithTag(4) as! UIView

        let obj = arrList[indexPath.row]
        imgImage.image = obj.icon

        Common.SetShadowwithCorneronView(view: viewmain, cornerradius: 10.0, color:UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))

        Common.SetShadowwithCorneronView(view: btnaddtocart, cornerradius: 10.0, color:UIColor.clear)


        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        openUrl(Url: arrList[indexPath.row].LinkUrl ?? "")
        //        let labDetail = arrLabs[indexPath.row]
        //
        //        let labDetailVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "LabsDetailVC") as! LabsDetailVC
        //        labDetailVC.labDeatil = labDetail
        //        AppUtility.shared.mainNavController.pushViewController(labDetailVC, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension

    }
    func openUrl(Url:String)
    {
        guard let url = URL(string: Url) else {
            return //be safe
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }



}

