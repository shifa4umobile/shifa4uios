//
//  NursingVC.swift
//  AppDesign
//
//  Created by sachin on 15/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class NursingVC: BaseViewController
{
    @IBOutlet weak var viewmain: UIView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        addCartButton()
        
        Common.setCornerOnView(view: viewmain, cornerradius: 10.0)
        Common.SetShadowwithCorneronView(view: viewmain, cornerradius: 10.0, color:UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))

    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnbcak(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLayoutSubviews()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                removeView()
                addCartButton()
            }
            else
            {
                removeView()
                
                addCartButton()
            }
            
        } else {
            print("Portrait")
            //Image Height
            removeView()
            addCartButton()
        }
    }

}
