//
//  PaymentModeVC.swift
//  AppDesign
//
//  Created by sachin on 16/05/18.
//  Copyright © 2018 Bilal Iqbal. All rights reserved.
//

import UIKit
//import SVProgressHUD
import Foundation
import RNCryptor

class PaymentModeVC: UIViewController
{
    
    @IBOutlet weak var viewDetail_BankInfo: UIView!

    @IBOutlet weak var viewBankDetail_Border_Height: NSLayoutConstraint! // 1
    @IBOutlet weak var viewBankTransfer_Height: NSLayoutConstraint!//370 == 86
    
    @IBOutlet weak var viewcashondelivery: UIView!
    @IBOutlet weak var imgcashondelivery: UIImageView!
    @IBOutlet weak var btncashondelivery: UIButton!
    @IBOutlet weak var vieweasypay: UIView!
    @IBOutlet weak var imgeasypay: UIImageView!
    @IBOutlet weak var viewbanktransfer: UIView!
    @IBOutlet weak var imgbanktransfer: UIImageView!
    @IBOutlet weak var btnprevious: UIButton!
    @IBOutlet weak var btnconfirmorder: UIButton!
    
    @IBOutlet weak var lblnote: UILabel!
    
    var PaymentMethodPrefix:String = ""
    var straddress:String = ""
    var strmobile:String = ""
    var PostData:String = ""
    var stratotalprice:String = ""
    var arrOrderDetails:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Common.SetShadowwithCorneronView(view: viewcashondelivery, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        Common.SetShadowwithCorneronView(view: vieweasypay, cornerradius: 10.0,color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        Common.SetShadowwithCorneronView(view: viewbanktransfer, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        Common.SetShadowwithCorneronView(view: btnconfirmorder, cornerradius: 10.0, color: UIColor.clear)
        Common.SetShadowwithCorneronView(view: btnprevious, cornerradius: 10.0, color: UIColor.clear)
        
        if strmobile.isValid == false
        {
            apicall()
        }
        else if straddress.isValid == false
        {
            apicall()
        }
        PaymentMethodPrefix = "BCOD"
        lblnote.text = NSString(format: "Payment will be collected at the time of home sampling. Kindly Keep Rs. %@ Ready", stratotalprice) as String
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btneasypay(_ sender: Any)
    {
                PaymentMethodPrefix = "BEP"
                imgeasypay.image = #imageLiteral(resourceName: "checkmark")
                imgbanktransfer.image = #imageLiteral(resourceName: "uncheck")
                imgcashondelivery.image = #imageLiteral(resourceName: "uncheck")

        viewBankTransfer_Height.constant = 86
        viewBankDetail_Border_Height.constant = 1

        Common.SetShadowwithCorneronView(view: vieweasypay, cornerradius: 10.0, color: UIColor(red: 8.0/255.0, green: 30.0/255.0, blue: 107.0/255.0, alpha: 1.0))

        Common.SetShadowwithCorneronView(view: viewcashondelivery, cornerradius: 10.0,color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))

        Common.SetShadowwithCorneronView(view: viewbanktransfer, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
            lblnote.isHidden = true
    }
    
    @IBAction func btnbanktransfer(_ sender: Any)
    {
        PaymentMethodPrefix = "BNKTFR"
        imgbanktransfer.image = #imageLiteral(resourceName: "checkmark")
        imgeasypay.image = #imageLiteral(resourceName: "uncheck")
        imgcashondelivery.image = #imageLiteral(resourceName: "uncheck")
        
        viewBankTransfer_Height.constant = 370
        viewBankDetail_Border_Height.constant = 1
        
        Common.SetShadowwithCorneronView(view: viewcashondelivery, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        Common.SetShadowwithCorneronView(view: vieweasypay, cornerradius: 10.0,color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        Common.SetShadowwithCorneronView(view: viewbanktransfer, cornerradius: 10.0, color: UIColor(red: 8.0/255.0, green: 30.0/255.0, blue: 107.0/255.0, alpha: 1.0))
        lblnote.isHidden = true
        
        
    }
    
    @IBAction func btncashondelivery(_ sender: Any)
    {
        PaymentMethodPrefix = "BCOD"
        imgcashondelivery.image = #imageLiteral(resourceName: "checkmark")
        imgbanktransfer.image = #imageLiteral(resourceName: "uncheck")
        imgeasypay.image = #imageLiteral(resourceName: "uncheck")
        
        viewBankTransfer_Height.constant = 86
        viewBankDetail_Border_Height.constant = 0
        Common.SetShadowwithCorneronView(view: viewcashondelivery, cornerradius: 10.0, color: UIColor(red: 8.0/255.0, green: 30.0/255.0, blue: 107.0/255.0, alpha: 1.0))
        
        Common.SetShadowwithCorneronView(view: vieweasypay, cornerradius: 10.0,color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        Common.SetShadowwithCorneronView(view: viewbanktransfer, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        lblnote.isHidden = false
        
    }
    
    @IBAction func btnprevious(_ sender: Any)
    {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    
    @IBAction func btnback(_ sender: Any)
    {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    
    @IBAction func btnconfirmorder(_ sender: Any)
    {
        apiCall_saveorder()
    }
    
    //MARK: - Webservice
//
    func apicall()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }

        SVProgressHUD.show()
        let token = UserDefaults.standard.object(forKey: "token") as! String
        let headers = [
            "authorization": "Bearer \(token)",
            "cache-control": "no-cache"
        ]

        let request = NSMutableURLRequest(url: NSURL(string: K.URL.GetCustomerPersonalContacts)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            SVProgressHUD.dismiss()
            if (error != nil) {
                print(error as Any)
            } else {
                let httpResponse = response as? HTTPURLResponse
                if httpResponse?.statusCode == 200
                {
                    do {
                        //var err: NSError?

                        if  let json = try! JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? NSDictionary
                        {
                            print(json)
                            DispatchQueue.main.async {
                                let mobile = json.object(forKey: "PhoneNumberContactId")
                                self.strmobile = mobile as Any as! String
                                let addresstemp = json.object(forKey: "AddressContactId")
                                self.straddress = addresstemp as Any as! String

                            }
                        }
                    }
                }
            }
        })

        dataTask.resume()
    }
    
    func apiCall_saveorder()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        SVProgressHUD.show()
        do {
            let token = UserDefaults.standard.object(forKey: "token") as! String
            let headers = [
                "content-type": "application/json",
                "authorization": "Bearer \(token)"
            ]
            
            let parameters:NSMutableDictionary = NSMutableDictionary()
            parameters.setValue(PaymentMethodPrefix, forKeyPath: "PaymentMethodPrefix")
            parameters.setValue(straddress, forKeyPath: "CustomerAddress")
            parameters.setValue(strmobile, forKeyPath: "CustomerContact")
            parameters.setValue(arrOrderDetails, forKeyPath: "OrderDetails")
            
            print(parameters)
            
            let postData = try! JSONSerialization.data(withJSONObject: parameters, options: [])
            
            let request = NSMutableURLRequest(url: NSURL(string: K.URL.saveorder)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                SVProgressHUD.dismiss()
                if (error != nil) {
                    print(error as Any as Any)                
                    ISMessages.show("Error occurred, 00")
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200
                    {
                        do {
                            // gunzip
                            let decompressedData: Data
                            if (data?.isGzipped)! {
                                decompressedData = (try! data?.gunzipped())!
                            } else {
                                decompressedData = data!
                            }
                            if  let json = try! JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves) as? NSDictionary
                            {
                                print(json)
                                if json["Status"] as! String == "Success"
                                {
                                    UserDefaults.standard.remove("arrlabtestcart")
                                    UserDefaults.standard.remove("arrmedicalpackagecart")
                                    UserDefaults.standard.remove("arrradiologycart")
                                    UserDefaults.standard.remove("arrhomecarecart")
                                    UserDefaults.standard.synchronize()
                                    if self.PaymentMethodPrefix != "BEP"
                                    {
                                    DispatchQueue.main.async {
                                        let vc = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "OrderPlacedVC") as! OrderPlacedVC
                                        vc.dicOrderFinal = NSMutableDictionary(dictionary: json)
                                        AppUtility.shared.mainNavController.pushViewController(vc, animated: true)
                                    }
                                    }
                                    else
                                    {
                                        DispatchQueue.main.async {
                                        let pharmacyVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "PaymentModuleVC") as! PaymentModuleVC
                                            pharmacyVC.Order = NSMutableDictionary(dictionary: json)
                                        self.navigationController!.pushViewController(pharmacyVC, animated: true)
                                    }
                                    }
                                }
                                else
                                {
                                    ISMessages.show("Please check your Address or contact is missing")
                                }
                                
                            }
                            else
                            {
                                ISMessages.show("Error occurred, 2")
                            }
                        }
                    }
                    else
                    {
                        ISMessages.show("Error occurred, 3")
                
                    }
                }
            })
            dataTask.resume()
        }
        catch
        {
            print(error)
        }
    }
}
