//
//  PaymentModuleVC.swift
//
//
//  Created by UDHC on 15/10/2018.
//

import UIKit
import RNCryptor
import CryptoSwift
//import SVProgressHUD


class PaymentModuleVC: UIViewController {
    
    var HashKey: String = "94KS420MZVU4E7JY";
    var Order:NSDictionary = NSDictionary()
    var Token:String = ""
    var Price:String = ""
    var OrderNumber = ""
    var requestBack = URLRequest(url: URL(string:"https://easypay.easypaisa.com.pk/easypay/Confirm.jsf")!)
    var request = URLRequest(url: URL(string:"https://easypay.easypaisa.com.pk/easypay/Index.jsf")!)

    
    @IBOutlet weak var MyWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Price = Order.object(forKey: "NetTotal") as! String
        OrderNumber = Order.object(forKey: "OrderId") as! String
        if Price.range(of:".") != nil {
            print("exists")
        }
        else
        {
            Price = Price + ".0"
        }
        print(Price)
        print(OrderNumber)
        apiCall()
}
    
    
    func aesEncryption(value: String, key: String) -> String
    {
        let encrypted = try! AES(key: key.bytes, blockMode: ECB(), padding: .pkcs5).encrypt(value.bytes)
        let encryptedData = Data(encrypted)
        return encryptedData.base64EncodedString()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnback(_ sender: Any)
    {
        AppUtility.sharedAppDelegate.setMainNavigationController()
    }
    
    func apiCall()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        else
        {
            SVProgressHUD.show()
            
            let postString = "amount=" + "\(Price)" + "&autoRedirect=1&orderRefNum=" + "\(OrderNumber)" + "&postBackURL=https://www.shifa4u.com/cart/easypayconfirm&storeId=7954"
            var str = try? aesEncryption(value: postString, key: HashKey)
            print(str)
            self.request.httpMethod = "POST"
            let allowed = NSMutableCharacterSet.alphanumeric()
            let unreserved = "-._~"
            allowed.addCharacters(in: unreserved)
            var escapedString = str?.addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet)
            print(escapedString!)
            let strstr = postString + "&" + "merchantHashedReq=" + escapedString!
            request.httpBody = strstr.data(using: .utf8)
            MyWebView.loadRequest(self.request)
            request.httpShouldHandleCookies = true
            let session = URLSession.shared
            let task = session.dataTask(with: request, completionHandler: { (data, response, errorResponse) in
                self.Token = (response?.url?.absoluteString)!
                print(self.Token)
                let start = self.Token.index(self.Token.startIndex, offsetBy: 44)
                let end = self.Token.index(self.Token.endIndex, offsetBy: -66)
                let range = start..<end
                let mySubstring = self.Token[range]
                print(mySubstring)
                HTTPCookieStorage.shared.cookieAcceptPolicy = HTTPCookie.AcceptPolicy.always
                self.requestBack.httpMethod = "POST"
                let PostBackURL = "&postBackURL=https://www.shifa4u.com/cart/easypayconfirm"
                let strstr = mySubstring + PostBackURL
                self.requestBack.httpBody = strstr.data(using: .utf8)
                self.requestBack.httpShouldHandleCookies = true
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.MyWebView.loadRequest(self.requestBack)
                }
            })
            task.resume()
           
        }
        
        
    }
}
