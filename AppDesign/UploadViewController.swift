//
//  UploadViewController.swift
//  AppDesign
//
//  Created by UDHC on 12/4/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class UploadViewController: BaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate {
    @IBOutlet weak var SendBtn: UIButton!
    @IBOutlet weak var UploadBtn: UIButton!
    @IBOutlet weak var Label: UILabel!
    @IBOutlet weak var SecondView: UIView!
    @IBOutlet weak var MainView: UIView!
    var selectImage = UIImage()
    var Img:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Common.SetShadowwithCorneronView(view: MainView, cornerradius: 10.0, color: UIColor.gray)
        Common.SetShadowwithCorneronView(view: SecondView, cornerradius: 10.0, color: UIColor.gray)
        Common.SetShadowwithCorneronView(view: UploadBtn, cornerradius: 10.0, color: UIColor.clear)
        Common.SetShadowwithCorneronView(view: SendBtn, cornerradius: 10.0, color: UIColor.clear)

        // Do any additional setup after loading the view.
    }
    func uploadPrescription()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        SVProgressHUD.show()
        
        let strUrl = K.URL.OnlineMedicinesUpload
        print(strUrl)
        let dic = NSMutableDictionary()
        let when = DispatchTime.now() + 10
        DispatchQueue.main.asyncAfter(deadline: when){
            Utility.apicallforParams_Image(strUrl  as String?, imgParam: "PrescriptionUrl", image: self.selectImage, authorization_Header: "", parms: dic) { (response: NSMutableDictionary?,error:Error?) in
                print(error)
                SVProgressHUD.dismiss()
                ISMessages.show("Upload Prescription successfully!")
                self.Label.text = nil
                
            }
            ISMessages.show("Internet Connection is slow , Still Uploading...")
        }
    }
    
    // MARK:- actionsheet delegate
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        if buttonIndex == 2
        {
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                let pickerView = UIImagePickerController()
                pickerView.allowsEditing = true
                pickerView.delegate = self
                pickerView.sourceType = .camera
                Label.isHidden = true
                self.present(pickerView, animated: true, completion: {
                    
                });
            }
        }
        else if buttonIndex == 3
        {
            let pickerView = UIImagePickerController()
            pickerView.allowsEditing = true
            pickerView.delegate = self
            pickerView.sourceType = .photoLibrary
            Label.isHidden = false;
            self.present(pickerView, animated: true, completion: {
                
            });
        }
    }
    
    // MARK: - PickerDelegates
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if picker.sourceType == .camera
        {
            Label.text = "CDt6-89766-okp09.jpeg"
        }
        else
        {
            if #available(iOS 11.0, *) {
                let imageUrl          = info[UIImagePickerControllerImageURL] as! NSURL
                let imgName         = imageUrl.lastPathComponent
                print("Here is the image name")
                print(imgName)
                Label.text = imgName
            }
            else {
                // Fallback on earlier versions
            }
        }
        dismiss(animated: true, completion: {() -> Void in
        })
        //        selectImage = (info[UIImagePickerControllerEditedImage] as? UIImage)!
        //        let alert = UIAlertController(title: "Successful", message:"Photo selected", preferredStyle: .alert)
        //        let later = UIAlertAction(title: "OK", style: .default, handler: nil)
        //        alert.addAction(later)
        //        self.present(alert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func UploadBtn(_ sender: Any)
    {
        
        let alertController = UIAlertController(title: "Choose Photo", message: "", preferredStyle: .actionSheet)
        
        let sendButton = UIAlertAction(title: "Take photo", style: .default, handler: { (action) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                let pickerView = UIImagePickerController()
                pickerView.allowsEditing = true
                pickerView.delegate = self
                pickerView.sourceType = .camera
                self.present(pickerView, animated: true, completion: {
                });
            }
            print("Ok button tapped")
        })
        
        let  deleteButton = UIAlertAction(title: "Choose from library", style: .destructive, handler: { (action) -> Void in
            let pickerView = UIImagePickerController()
            pickerView.allowsEditing = true
            pickerView.delegate = self
            pickerView.sourceType = .photoLibrary
            self.present(pickerView, animated: true, completion: {
                
            });
            print("Delete button tapped")
            self.Label.text = self.Img
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
        })
        
        
        alertController.addAction(sendButton)
        alertController.addAction(deleteButton)
        alertController.addAction(cancelButton)
        
        let popPresenter: UIPopoverPresentationController? = alertController.popoverPresentationController
        popPresenter?.sourceView = self.UploadBtn
        popPresenter?.sourceRect = self.UploadBtn.bounds
        present(alertController, animated: true)
        
    }
    @IBAction func SendBtn(_ sender: Any) {
        uploadPrescription()
    }
    
    @IBAction func Back(_ sender: Any) {
        dismiss(animated: true , completion: nil)
    }
    
}
