//
//  AddFamilyDetailsAddress.swift
//  AppDesign
//
//  Created by UDHC on 22/10/2018.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class AddFamilyDetailsAddress: UIViewController {

    @IBOutlet weak var Address: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func SendBtn(_ sender: Any) {
        SaveAddress()
    }
    
    @IBAction func BackBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func SaveAddress()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        SVProgressHUD.show()
        do {
            
            let headers = [
                "content-type": "application/json"
            ]
            
            let reqParam = ["Description" : Address.text]
            
            
            
            let postData = try! JSONSerialization.data(withJSONObject: reqParam, options: [])
            
            let request = NSMutableURLRequest(url: NSURL(string: K.URL.SaveAddress)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            let token = UserDefaults.standard.object(forKey: "token") as! String
            request.addValue(NSString(format: "bearer %@", token) as String, forHTTPHeaderField: "Authorization")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                SVProgressHUD.dismiss()
                if (error != nil) {
                    print(error as Any as Any)
                    ISMessages.show("Error occurred, Please try again")
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200
                    {
                        
                        ISMessages.show("Address Sucessfully Added")
                        
                        DispatchQueue.main.async
                            {
                                self.dismiss(animated: true, completion: nil)
                            }
                    }
                    else
                    {
                        ISMessages.show("Cant add address at this time")
                    }
                }
            })
            dataTask.resume()
        }
        catch
        {
            print(error)
        }
    }

}
