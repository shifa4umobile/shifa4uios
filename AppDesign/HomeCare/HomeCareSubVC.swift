//
//  HomeCareSubVC.swift
//  AppDesign
//
//  Created by Mahavir Makwana on 10/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class HomeCareSubVC: UIViewController {

    @IBOutlet weak var tbllist: UITableView!
    @IBOutlet weak var viewsearch: UIView!
    @IBOutlet weak var txtsearch: UITextField!

    var arrHomeCare: Array<HomeCareSubModel> = []
    var arrHomeCareDisplay: Array<HomeCareSubModel> = []


    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        Common.setCornerOnView(view: viewsearch, cornerradius: viewsearch.frame.size.height/2.0)
        Common.SetShadowwithCorneronView(view: viewsearch, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        apiCall2()
        tbllist.estimatedRowHeight = 89
        tbllist.rowHeight = UITableViewAutomaticDimension
    }


    @IBAction func btnback(_ sender: Any)
    {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }

    @IBAction func textFieldDidChange(_ sender: UITextField) {
        setData()
    }


    //MARK: - Other Action
    func setData() {
        arrHomeCareDisplay.removeAll(keepingCapacity: false)
        if txtsearch.text?.isValid == true {
            arrHomeCareDisplay = self.arrHomeCare.filter({($0.name as? String ?? "").contains((txtsearch.text?.trim())!, compareOption: NSString.CompareOptions.caseInsensitive)})
        }
        else {
            arrHomeCareDisplay.append(contentsOf: self.arrHomeCare)
        }
        tbllist.reloadData()
    }

    //MARK: - Webservice
    func apiCall2()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        SVProgressHUD.show()
         let url = URL(string: K.URL.getphysiotherapyservicetypes)
        var urlRequest = URLRequest(url: url!)
        urlRequest.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        urlRequest.timeoutInterval = 60
        urlRequest.httpMethod = "GET"
        urlRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("Bearer \(User.loggedInUser()?.accessToken ?? "")", forHTTPHeaderField: "Authorization")
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, errorResponse) in
            if errorResponse != nil {
                print(errorResponse ?? "Erroer Found")
                SVProgressHUD.dismiss()
            }
            else{
                
                do {
                    let decompressedData: Data
                    if (data?.isGzipped)! {
                        decompressedData = (try! data?.gunzipped())!
                    } else {
                        decompressedData = data!
                    }
                    if let dictionary = try JSONSerialization.jsonObject(with: decompressedData, options: .mutableContainers) as? [Any]
                    {
                        self.arrHomeCare.removeAll(keepingCapacity: false)
                        DispatchQueue.main.async {
                            print(dictionary)
                            self.arrHomeCare = HomeCareSubModel.modelsFromDictionaryArray(array: dictionary as! Array<Dictionary<String,Any>>)
                            self.setData()
                            self.tbllist.reloadData()
                        }
                        
                        
                        
                        
                        SVProgressHUD.dismiss()
                    }
                }
                catch let exeptionError as Error? {
                    print(exeptionError!.localizedDescription)
                    SVProgressHUD.dismiss()
                }
            }
            
        })
        task.resume()
    }

    func apicall()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        SVProgressHUD.show()

        let url = URL(string: K.URL.getphysiotherapyservicetypes)
        var request = NSMutableURLRequest(url: url!)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")  // the request is JSON
        request.setValue("Bearer \(User.loggedInUser()?.accessToken ?? "")", forHTTPHeaderField: "Authorization")        // the expected response is also JSON

        request.httpMethod = "GET"

        request.httpBody = try? JSONSerialization.data(withJSONObject: [:], options: .prettyPrinted)

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)

        let task = session.dataTask(with: request as URLRequest) {(data, response, error) in

            //self.arrLabs.removeAll(keepingCapacity: false)

            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                self.arrHomeCare.removeAll(keepingCapacity: false)
                let decompressedData: Data
                if (data?.isGzipped)! {
                    decompressedData = (try! data?.gunzipped())!
                } else {
                    decompressedData = data!
                }
                if decompressedData.count != 0 {
                    
                    let peoplesArray = try! JSONSerialization.jsonObject(with: Data(decompressedData), options: JSONSerialization.ReadingOptions()) as? [Any]

                    if peoplesArray != nil {
                        print(peoplesArray!)
                        self.arrHomeCare = HomeCareSubModel.modelsFromDictionaryArray(array: peoplesArray as! Array<Dictionary<String,Any>>)
                        self.setData()
                    }
                }

                SVProgressHUD.dismiss()
            })

        }

        task.resume()
    }
}

//MARK:- UITextField Delegate
extension HomeCareSubVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        setData()
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        setData()
    }
}


//MARK:- Tableview Delegate Method
extension HomeCareSubVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrHomeCareDisplay.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        let mainview = cell.contentView.viewWithTag(1) as! UIView
        let imgmain = cell.contentView.viewWithTag(2) as! UIImageView
        let lbltitle = cell.contentView.viewWithTag(3) as! UILabel
        let lblsubtitle = cell.contentView.viewWithTag(4) as! UILabel

        Common.SetShadowwithCorneronView(view: mainview, cornerradius: 10.0,color:UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))

        let obj = arrHomeCareDisplay[indexPath.row]
        lbltitle.text = obj.name
        lblsubtitle.text = obj.HomeDescription
        imgmain.image = #imageLiteral(resourceName: "bannerPlaceholder")
        
        AppUtility.shared.getImage(imageID: obj.ImageWebUrl.toInt()!, completion: { (success, error, image) in
            if success == true {
                if image != nil {
                    imgmain.image = image
                }
            }
        })
        //imgmain.setImageWithURL(strUrl: obj.ImageWebUrl, placeHolderImage: #imageLiteral(resourceName: "bannerPlaceholder"), activityIndicatorViewStyle: .white)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let obj = arrHomeCareDisplay[indexPath.row]
        let physioTherapySubServiceVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "PhysiotherapySubServiceVC") as! PhysiotherapySubServiceVC
        physioTherapySubServiceVC.madicalId = obj.ID; AppUtility.shared.mainNavController.pushViewController(physioTherapySubServiceVC, animated: true)
    }

//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return UITableViewAutomaticDimension
//
//    }
}

