//
//  HomeCareVC.swift
//  AppDesign
//
//  Created by sachin on 15/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class HomeCareVC: BaseViewController
{
    @IBOutlet weak var tbllist: UITableView!
    @IBOutlet weak var viewsearch: UIView!
    @IBOutlet weak var txtsearch: UITextField!

    var arrHomeCare: Array<Dictionary<String,Any>> = []
    var arrHomeCareDisplay: Array<Dictionary<String,Any>> = []

    override func viewDidLoad()
    {
        super.viewDidLoad()

        addCartButton()


        Common.setCornerOnView(view: viewsearch, cornerradius: viewsearch.frame.size.height/2.0)
        Common.SetShadowwithCorneronView(view: viewsearch, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        arrHomeCare.append(["title" : "Physiotherapy",
                            "sortDesc" : "Physiotherapy treatment helps in relieving pain, improving muscles strength and maintaining and restoring maximum functionality of the body. On Shifa4u online healthcare portal, you can request an appointment to get our in-home physiotherapy services that include customized home-care packages as per individual needs based on American Standard of Health Care. Our certified physiotherapists are specialized in their training as well as compassionate with their patients’ needs.",
                            "image" : "https://www.shifa4u.com/Content/images/home-physio.jpg"])
        arrHomeCare.append(["title" : "Speech therapy",
                            "sortDesc" : "We provide evaluation and treatment for all ages who are suffering  with  speech and language disorders due to developmental delays or other neurological diseases. Speech and language therapy also provides treatment from birth to the elderly suffering from cognition impairment, such as Traumatic Brain Injury, Parkinson's Disease, and Multiple Sclerosis. Speech and language therapy is not just for eating! ",
                            "image" : "https://www.shifa4u.com/Content/images/speech-therapy.jpg"])
        arrHomeCare.append(["title" : "Dietitian",
                            "sortDesc" : "Everything you eat and drink plays a role in your recovery when suffering from an acute or long term life event. We offer Nutrition and Dietician services that can be tailored to your body composition to aid in healing. It is vital for those individuals and their families to have full support services when the patient is trying to overcome long term conditions that require dietary and nutrition support such as diabetes, obesity  kidney and/or heart disease. ",
                            "image" : "https://www.shifa4u.com/Content/images/home-dienticins-banner.jpg"])
        arrHomeCare.append(["title" : "Nursing",
                            "sortDesc" : "Effective Home Nursing Care facility is vital to achieve good health recovery plan. At Shifa4u , our nurses are highly qualified, compassionate and supervised by internationally trained physicians to provide your loved ones with the best available nursing care at home. Book an appointment for your preferred service and time slot, and ",
                            "image" : "https://www.shifa4u.com/Content/images/home-care.jpg"])
        arrHomeCare.append(["title" : "Home Doctor",
                            "sortDesc" : "Shifa4u brings you the services of top tier American Healthcare Professionals via Shifa4u Online Consultancy Sessions & Offline recommendations. If you want an expert 2nd opinion in making an informed decision regarding a complicated disease or a critical medical decision, for instance a bypass surgery, dialysis or chronic obesity, you may access them via Shifa4u Tele Clinic portal, and have online and offline consultancy sessions with doctors, dieticians, physiotherapists, speech therapists and psychologists.",
                            "image" : "https://www.shifa4u.com/Content/images/home-doctors-banner.jpg"])
        setData()
    }

    override func viewDidLayoutSubviews()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                removeView()
                addCartButton()
            }
            else
            {
                removeView()

                addCartButton()
            }

        } else {
            print("Portrait")
            //Image Height
            removeView()
            addCartButton()
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    @IBAction func btnback(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func textFieldDidChange(_ sender: UITextField) {
        setData()
    }


    //MARK: - Other Action
    func setData() {
        arrHomeCareDisplay.removeAll(keepingCapacity: false)
        if txtsearch.text?.isValid == true {
            arrHomeCareDisplay = self.arrHomeCare.filter({($0["title"] as? String ?? "").contains((txtsearch.text?.trim())!, compareOption: NSString.CompareOptions.caseInsensitive)})
        }
        else {
            arrHomeCareDisplay.append(contentsOf: self.arrHomeCare)
        }
        tbllist.reloadData()
    }
}


//MARK:- UITextField Delegate
extension HomeCareVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        setData()
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        setData()
    }
}


//MARK:- Tableview Delegate Method
extension HomeCareVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrHomeCareDisplay.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        let mainview = cell.contentView.viewWithTag(1) as! UIView
        let imgmain = cell.contentView.viewWithTag(2) as! UIImageView
        let lbltitle = cell.contentView.viewWithTag(3) as! UILabel
        let lblsubtitle = cell.contentView.viewWithTag(4) as! UILabel

        Common.SetShadowwithCorneronView(view: mainview, cornerradius: 10.0,color:UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))

        let obj = arrHomeCareDisplay[indexPath.row]
        lbltitle.text = obj["title"] as? String ?? ""
        lblsubtitle.text = obj["sortDesc"] as? String ?? ""
        imgmain.image = nil
        imgmain.setImageWithURL(strUrl: obj["image"] as? String ?? "", placeHolderImage: #imageLiteral(resourceName: "bannerPlaceholder"), activityIndicatorViewStyle: .white)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let obj = arrHomeCareDisplay[indexPath.row]
        if obj["title"] as? String == "Physiotherapy"
        {
            let homecareSubVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "HomeCareSubVC") as! HomeCareSubVC
            AppUtility.shared.mainNavController.pushViewController(homecareSubVC, animated: true)
        }
        else if obj["title"] as? String == "Dietitian"
        {
            let dieticianVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "DieticianVC") as! DieticianVC
            AppUtility.shared.mainNavController.pushViewController(dieticianVC, animated: true)
        }
        else if obj["title"] as? String == "Speech therapy"
        {
            let speechTherapyVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "SpeechtherapyVC") as! SpeechtherapyVC
            AppUtility.shared.mainNavController.pushViewController(speechTherapyVC, animated: true)
        }
        else if obj["title"] as? String == "Nursing"
        {
            let nursingVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "NursingVC") as! NursingVC
            AppUtility.shared.mainNavController.pushViewController(nursingVC, animated: true)
        }
        else if obj["title"] as? String == "Home Doctor"
        {
            let homeDoctorVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "HomeDoctorVC") as! HomeDoctorVC
            AppUtility.shared.mainNavController.pushViewController(homeDoctorVC, animated: true)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension

    }
}
