//
//  LatestBLogVC.swift
//  AppDesign
//
//  Created by sachin on 10/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class LatestBLogVC: BaseViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var CategoryBtn: UIButton!
    @IBOutlet weak var CategoryView: UIView!
    @IBOutlet weak var MyTableView: UITableView!
    
    var arrBlogList = NSArray()
    
    var arrCategoriesList:NSMutableArray = NSMutableArray()
    var CategoryId : Int = 0
    
    var TotalRecords:Int = 0
    var page_Limit: Int = 0
    
    var activityIndicator: UIActivityIndicatorView?
    
    let chooseArticleDropDown = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown
        ]
    }()
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }

  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Common.SetShadowwithCorneronView(view: CategoryView, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))

        //Add menu button
        addSlideMenuButton()
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        getCategories()
        
        if UserDefaults.standard.object(forKey: "arrBlogList") != nil
        {
            self.arrBlogList = NSMutableArray(array: UserDefaults.standard.object(forKey: "arrBlogList") as! NSArray)
                self.MyTableView.reloadData()
            
            if self.arrBlogList.count != 0
            {
                GetBLogsFromAP(is_firsttime: "")
            }
            else
            {
                GetBLogsFromAP(is_firsttime: "1")
            }
        }
        else
        {
            GetBLogsFromAP(is_firsttime: "1")
        }
        MyTableView.tableFooterView = UIView()
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView == MyTableView
        {
            let visibleRows = MyTableView.indexPathsForVisibleRows
            let indexPath: IndexPath? = visibleRows?.last
            if indexPath?.row == arrBlogList.count-1
            {
                if arrBlogList.count < TotalRecords
                {
                    page_Limit = page_Limit + 1
                    self.GetBLogsFromAP(is_firsttime: "")
                }
            }
        }
    }

    override func viewDidLayoutSubviews()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                removeView()

            }
            else
            {
                removeView()
        
            }
            
        } else {
            print("Portrait")
            //Image Height
            removeView()        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrBlogList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let mainview = cell.contentView.viewWithTag(1)
        var imgmain = cell.contentView.viewWithTag(2) as! UIImageView
        let lbltitle = cell.contentView.viewWithTag(3) as! UILabel
        let lbldate = cell.contentView.viewWithTag(4) as! UILabel
        var lbldescription = cell.contentView.viewWithTag(5) as! UILabel
        var lblauthor = cell.contentView.viewWithTag(6) as! UILabel
        var readMore = cell.contentView.viewWithTag(7) as! UIButton
        var readonImage = cell.contentView.viewWithTag(8) as! UIButton
        var readonLabel = cell.contentView.viewWithTag(9) as! UIButton
        
        Common.SetShadowwithCorneronView(view: mainview!, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: imgmain, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        
        let homeCare = NSMutableDictionary(dictionary: arrBlogList[indexPath.row] as! NSDictionary)
        let date = (homeCare.object(forKey: "PublishDateTime") as! String)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
        let dateFromString : NSDate = dateFormatter.date(from: date)! as NSDate
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let datenew = dateFormatter.string(from: dateFromString as Date)
        lbltitle.text = (homeCare.object(forKey: "Title") as! String)
        lbldate.text = datenew + " |"
        lblauthor.text = (homeCare.object(forKey: "Author") as! String)
        lbldescription.text = (homeCare.object(forKey: "ShortDescription") as! String).htmlToString
        
        imgmain.image = #imageLiteral(resourceName: "bannerPlaceholder")
        
        AppUtility.shared.getImageByNameDownload(imageName: (homeCare.object(forKey: "ImageUrl") as! String), completion: { (success, error, image) in
            if success == true {
                if image != nil {
                    imgmain.image = image
                }
            }
        })
    
        readMore.addTapGesture { (gesture) in
            
            let storyBoard : UIStoryboard =  UIStoryboard(name: "Main", bundle: nil)
            let vc : DetailVC = storyBoard.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
            vc.arrBlogList = NSMutableDictionary(dictionary: self.arrBlogList[indexPath.row] as! NSDictionary)
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        readonLabel.addTapGesture { (gesture) in
            
            let storyBoard : UIStoryboard =  UIStoryboard(name: "Main", bundle: nil)
            let vc : DetailVC = storyBoard.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
            vc.arrBlogList = NSMutableDictionary(dictionary: self.arrBlogList[indexPath.row] as! NSDictionary)
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        readonImage.addTapGesture { (gesture) in
            
            let storyBoard : UIStoryboard =  UIStoryboard(name: "Main", bundle: nil)
            let vc : DetailVC = storyBoard.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
            vc.arrBlogList = NSMutableDictionary(dictionary: self.arrBlogList[indexPath.row] as! NSDictionary)
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
       
        return cell
       
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
        
    }

    
 func GetBLogsFromAP(is_firsttime: String)

 {
    if Utility.isInterNetConnectionIsActive() == false
    {
        let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
        let later = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(later)
        self.present(alert, animated: true, completion: nil)
        return;
    }
    if is_firsttime == "1" {
        SVProgressHUD.show()
    }
    do
    {
        let parameters = ["BlogCategoryId": ""]

        let url = URL(string: K.URL.GetAllBlog)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        
        print(parameters)
        
        request.httpBody = httpBody
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            
            
            if error != nil {
                print(error ?? "Error Found")
                
            }
            else
            {
                do
                {
                    // gunzip
                    let decompressedData: Data
                    if (data?.isGzipped)! {
                        decompressedData = (try! data?.gunzipped())!
                    } else {
                        decompressedData = data!
                    }
                    
                    let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
                    
                    if let arrData = dictionary as? NSArray {
                        print(dictionary!)
                        
                        DispatchQueue.main.async {
                            self.arrBlogList = NSArray(array: arrData)
                            self.MyTableView.reloadData()
                            UserDefaults.standard.set(self.arrBlogList, forKey: "arrBlogList")
                            UserDefaults.standard.synchronize()
                        }
                    }
                    else {
                        ISMessages.show("Blogs not found")
                    }
                }
                SVProgressHUD.dismiss()
            }
        });
        task.resume()
        
    }
    
    }
    
    
    func getCategories()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        let parameters = ["": ""]
        
        let url = URL(string: K.URL.GetAllBlogCategories)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            SVProgressHUD.dismiss()
            // gunzip
            let decompressedData: Data
            if (data?.isGzipped)! {
                decompressedData = (try! data?.gunzipped())!
            } else {
                decompressedData = data!
            }
            
            let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
            
            if let arrData = dictionary as? NSArray {
                print(dictionary!)
                self.arrCategoriesList = NSMutableArray(array: arrData)
            }
            else {
                ISMessages.show("Categories not found")
            }
        });
        task.resume()
        
    }
    
    func GetFilteredBLogsFromAP(is_firsttime: String)
        
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        if is_firsttime == "1" {
            SVProgressHUD.show()
        }
        do
        {
            let parameters = ["BlogCategoryId": "\(CategoryId)"]
            
            let url = URL(string: K.URL.GetAllFilteredBlog)
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
            request.httpBody = httpBody
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            
            let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
                
                
                if error != nil {
                    print(error ?? "Error Found")
                    
                }
                else
                {
                    do
                    {
                        // gunzip
                        let decompressedData: Data
                        if (data?.isGzipped)! {
                            decompressedData = (try! data?.gunzipped())!
                        } else {
                            decompressedData = data!
                        }
                        
                        let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
                        
                        if let arrData = dictionary as? NSArray {
                            print(dictionary!)
                            
                            DispatchQueue.main.async {
                                self.arrBlogList = NSArray(array: arrData)
                                self.MyTableView.reloadData()
                                UserDefaults.standard.set(self.arrBlogList, forKey: "arrBlogList")
                                UserDefaults.standard.synchronize()
                            }
                        }
                        else {
                            ISMessages.show("Categories not found")
                        }
                    }
                    SVProgressHUD.dismiss()
                }
            });
            task.resume()
            
        }
        
    }
    
    
    @IBAction func CategoryBtn(_ sender: Any)
    {
        
        
        let arrLabsTemp = NSMutableArray()
        for i in 0..<arrCategoriesList.count
        {
            let labname = (arrCategoriesList.object(at: i) as! NSDictionary).object(forKey: "BlogCategoryName")
            arrLabsTemp.add(labname as Any)
        }
        
        
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            lblNotificationText?.setTitle(item, for: .normal)
            self?.CategoryId = (self?.arrCategoriesList.object(at: index) as! NSDictionary).object(forKey: "BlogCategoryId") as! Int
            
        
            self?.MyTableView.reloadData()
            self?.GetFilteredBLogsFromAP(is_firsttime: "1")
            
            self?.chooseArticleDropDown.hide()
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            if items.isEmpty {
                lblNotificationText?.setTitle("", for: .normal)
    
            }
        }
        
        chooseArticleDropDown.show()
        
    }
    
    @IBAction func ReadMoreBtn(_ sender: Any) {
    }
    
}
