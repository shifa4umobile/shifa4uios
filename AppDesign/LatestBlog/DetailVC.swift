//
//  DetailVC.swift
//  AppDesign
//
//  Created by UDHC on 17/09/2018.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {
    @IBOutlet weak var MainTitle: UILabel!
    @IBOutlet weak var TextView: UITextView!
    @IBOutlet weak var Author: UILabel!
    @IBOutlet weak var Date: UILabel!
    @IBOutlet weak var TopLabel: UILabel!
    @IBOutlet weak var ImageView: UIImageView!
    
   var arrBlogList = NSMutableDictionary()
    
    
    
    static func instantiate() -> DetailVC? {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "\(DetailVC.self)") as? DetailVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        let date = (arrBlogList.object(forKey: "PublishDateTime") as! String)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
        let dateFromString : NSDate = dateFormatter.date(from: date)! as NSDate
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let datenew = dateFormatter.string(from: dateFromString as Date)
        ImageView.image = #imageLiteral(resourceName: "bannerPlaceholder")
        AppUtility.shared.getImageByName(imageName: (arrBlogList.object(forKey: "ImageUrl") as! String), completion: { (success, error, image) in
            if success == true {
                if image != nil {
                    self.ImageView.image = image
                }
            }
        })
        ImageView.layer.cornerRadius = 8.0
        ImageView.clipsToBounds = true
        TextView.attributedText = (arrBlogList.object(forKey: "Descrption") as! String).htmlToAttributedString
        Date.text = datenew + " |"
        Author.text = (arrBlogList.object(forKey: "Author") as! String)
        MainTitle.text = (arrBlogList.object(forKey: "Title") as! String)
        self.TextView.scrollRangeToVisible(NSMakeRange(0, 0))
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
     @IBAction func BackBtn(_ sender: Any) {
       AppUtility.shared.mainNavController.popViewController(animated: true)
     }
     

}
