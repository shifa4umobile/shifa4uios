//
//  ContactUsVC.swift
//  AppDesign
//
//  Created by sachin on 10/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class ContactUsVC: BaseViewController {

    @IBOutlet weak var viewmain: UIView!
    @IBOutlet weak var txtname: UITextField!
    @IBOutlet weak var txtemail: UITextField!
    @IBOutlet weak var txtphone: UITextField!
    @IBOutlet weak var viewtextview: UIView!
    @IBOutlet weak var txtmessage: UITextView!
    @IBOutlet weak var btnsubmit: UIButton!

    override func viewDidLoad()
    {
        super.viewDidLoad()

        //Add menu button
        addSlideMenuButton()
        addCartButton()
        
        Common.SetShadowwithCorneronView(view: viewmain, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: btnsubmit, cornerradius: 10.0, color: UIColor.clear)
        
        Common.setCornerOnView(view: viewtextview, cornerradius: 10.0)

    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnsubmit(_ sender: Any)
    {
        
        if txtname.text?.isValid == false
        {
            ISMessages.show(K.Message.enterName)
            return;
        }
        else if txtemail.text?.isValid == false {
            ISMessages.show(K.Message.enterEmail)
            return;
        }
        else if txtemail.text?.isEmail == false {
            ISMessages.show(K.Message.enterValidEmail)
            return;
        }
        else if txtphone.text?.isValid == false {
            ISMessages.show(K.Message.mobile)
            return;
        }
        
        self.apiCall_uploadprescription()
    }

    override func viewDidLayoutSubviews()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                removeView()
                addCartButton()
            }
            else
            {
                removeView()
                
                addCartButton()
            }
            
        } else {
            print("Portrait")
            //Image Height
            removeView()
            addCartButton()
        }
    }
    
    //MARK: - Webservice
    func apiCall_uploadprescription()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        SVProgressHUD.show()
        
        let strUrl = K.URL.uploadprescription
        print(strUrl)
        let dic = NSMutableDictionary()
        
        dic.setValue("", forKey: "ContactId")
        dic.setValue(txtname.text, forKey: "Name")
        dic.setValue(txtphone.text, forKey: "ContactNo")
        dic.setValue(txtemail.text, forKey: "Email")
        dic.setValue(txtmessage.text, forKey: "Message")
        dic.setValue("CNTCUS", forKey: "ContactUsType")
        
        print(dic)
    
        Utility.apicallforParams_Image(strUrl  as String?, imgParam: "", image: nil, authorization_Header: "", parms: dic) { (response: NSMutableDictionary?,error:Error?) in
            print(error)
            print("Error is")
            SVProgressHUD.dismiss()
            ISMessages.show("Sent successfully!")
            self.txtname.text = nil
            self.txtemail.text = nil
            self.txtphone.text = nil
            self.txtmessage.text = nil
        
    }
}
}
