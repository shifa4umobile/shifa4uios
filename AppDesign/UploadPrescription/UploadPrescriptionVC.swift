//
//  UploadPrescriptionVC.swift
//  AppDesign
//
//  Created by sachin on 16/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class UploadPrescriptionVC: BaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate
{
    @IBOutlet weak var ImgView: UIView!
    @IBOutlet weak var ImageName: UILabel!
    @IBOutlet weak var NameofImage: UILabel!
    @IBOutlet weak var btnsend: UIButton!
    @IBOutlet weak var viewmain: UIView!
    @IBOutlet weak var txtname: UITextField!
    @IBOutlet weak var txtemail: UITextField!
    @IBOutlet weak var txtphone: UITextField!
    @IBOutlet weak var viewtextview: UIView!
    @IBOutlet weak var txtmessage: UITextView!
    @IBOutlet weak var btnupload: UIButton!
    
    var selectImage = UIImage()
    var ImageDetails = String()
    var Img:String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        addSlideMenuButton()
        Common.SetShadowwithCorneronView(view: viewmain, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: btnsend, cornerradius: 10.0, color: UIColor.clear)
        Common.SetShadowwithCorneronView(view: btnupload, cornerradius: 10.0, color: UIColor.clear)
        Common.SetShadowwithCorneronView(view: ImgView, cornerradius: 10.0, color: K.Color.blue)
        Common.setCornerOnView(view: viewtextview, cornerradius: 10.0)
    
        Common.setBorderOnView(view: viewtextview, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Webservice
    func apiCall_uploadprescription()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        SVProgressHUD.show()
        
        let strUrl = K.URL.uploadprescription
        print(strUrl)
        let dic = NSMutableDictionary()
        
        dic.setValue("", forKey: "ContactId")
        dic.setValue(txtname.text, forKey: "Name")
        dic.setValue(txtphone.text, forKey: "ContactNo")
        dic.setValue(txtemail.text, forKey: "Email")
        dic.setValue(txtmessage.text, forKey: "Message")
        dic.setValue("", forKey: "ContactUsType")
        
    
        let when = DispatchTime.now() + 10
        DispatchQueue.main.asyncAfter(deadline: when){
            Utility.apicallforParams_Image(strUrl  as String?, imgParam: "PrescriptionUrl", image: self.selectImage, authorization_Header: "", parms: dic) { (response: NSMutableDictionary?,error:Error?) in
                print(error)
                SVProgressHUD.dismiss()
                ISMessages.show("Upload Prescription successfully!")
                self.txtname.text = nil
                self.txtemail.text = nil
                self.ImageName.text = nil
                self.txtphone.text = nil
                self.txtmessage.text = nil
                
            }
            ISMessages.show("Internet Connection is slow , Still Uploading...")
        }
    }
    
    // MARK:- actionsheet delegate
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        if buttonIndex == 2
        {
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                let pickerView = UIImagePickerController()
                pickerView.allowsEditing = true
                pickerView.delegate = self
                pickerView.sourceType = .camera
                ImageName.isHidden = true
                self.present(pickerView, animated: true, completion: {
                    
                });
            }
        }
        else if buttonIndex == 3
        {
            let pickerView = UIImagePickerController()
            pickerView.allowsEditing = true
            pickerView.delegate = self
            pickerView.sourceType = .photoLibrary
            ImageName.isHidden = false;
            self.present(pickerView, animated: true, completion: {
                
            });
        }
    }
    
    // MARK: - PickerDelegates
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if picker.sourceType == .camera
        {
           ImageName.text = "CDt6-89766-okp09.jpeg"
        }
        else
    {
        if #available(iOS 11.0, *) {
            let imageUrl          = info[UIImagePickerControllerImageURL] as! NSURL
            let imgName         = imageUrl.lastPathComponent
        print("Here is the image name")
        print(imgName)
        ImageName.text = imgName
        }
        else {
            // Fallback on earlier versions
        }
    }
        dismiss(animated: true, completion: {() -> Void in
        })
//        selectImage = (info[UIImagePickerControllerEditedImage] as? UIImage)!
//        let alert = UIAlertController(title: "Successful", message:"Photo selected", preferredStyle: .alert)
//        let later = UIAlertAction(title: "OK", style: .default, handler: nil)
//        alert.addAction(later)
//        self.present(alert, animated: true, completion: nil)
    }
    
   
    @IBAction func btnsend(_ sender: Any)
    {
        if txtname.text?.isValid == false
        {
            ISMessages.show(K.Message.enterName)
            return;
        }
        else if txtemail.text?.isValid == false {
            ISMessages.show(K.Message.enterEmail)
            return;
        }
        else if txtemail.text?.isEmail == false {
            ISMessages.show(K.Message.enterValidEmail)
            return;
        }
        else if txtphone.text?.isValid == false {
            ISMessages.show(K.Message.mobile)
            return;
        }
        
        self.apiCall_uploadprescription()
    }
    
    @IBAction func btnupload(_ sender: Any)
    {
    
        let alertController = UIAlertController(title: "Choose Photo", message: "", preferredStyle: .actionSheet)
        
        let sendButton = UIAlertAction(title: "Take photo", style: .default, handler: { (action) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                let pickerView = UIImagePickerController()
                pickerView.allowsEditing = true
                pickerView.delegate = self
                pickerView.sourceType = .camera
                self.present(pickerView, animated: true, completion: {
                });
            }
            print("Ok button tapped")
        })
        
        let  deleteButton = UIAlertAction(title: "Choose from library", style: .destructive, handler: { (action) -> Void in
            let pickerView = UIImagePickerController()
            pickerView.allowsEditing = true
            pickerView.delegate = self
            pickerView.sourceType = .photoLibrary
            self.present(pickerView, animated: true, completion: {
                
            });
            print("Delete button tapped")
            self.ImageName.text = self.Img
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
        })
        
        
        alertController.addAction(sendButton)
        alertController.addAction(deleteButton)
        alertController.addAction(cancelButton)
        
        let popPresenter: UIPopoverPresentationController? = alertController.popoverPresentationController
        popPresenter?.sourceView = self.btnupload
        popPresenter?.sourceRect = self.btnupload.bounds
        present(alertController, animated: true)

    }
}
