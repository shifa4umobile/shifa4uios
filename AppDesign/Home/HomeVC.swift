//
//  HomeVC.swift
//  AppDesign
//
//  Created by sachin on 10/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class HomeVC: BaseViewController {

    @IBOutlet weak var viewlab: UIView!
    @IBOutlet weak var viewradiology: UIView!
    @IBOutlet weak var viewmedicalpackage: UIView!
    @IBOutlet weak var viewpharmacy: UIView!
    @IBOutlet weak var viewamericanteleclinic: UIView!
    @IBOutlet weak var viewhomecare: UIView!
    @IBOutlet weak var heightimage: NSLayoutConstraint!
    override func viewDidLoad()
    {
        super.viewDidLoad()
//        viewlab.isUserInteractionEnabled = false
//        viewhomecare.isUserInteractionEnabled = false
//        viewpharmacy.isUserInteractionEnabled = false
//        viewradiology.isUserInteractionEnabled = false
//        viewmedicalpackage.isUserInteractionEnabled = false
//        viewamericanteleclinic.isUserInteractionEnabled = false
        
        Common.SetShadowwithCorneronView(view: viewlab, cornerradius: 10.0, color: UIColor.lightGray)
        Common.SetShadowwithCorneronView(view: viewradiology, cornerradius: 10.0, color: UIColor.lightGray)
        Common.SetShadowwithCorneronView(view: viewmedicalpackage, cornerradius: 10.0, color: UIColor.lightGray)
        Common.SetShadowwithCorneronView(view: viewpharmacy, cornerradius: 10.0, color: UIColor.lightGray)
        Common.SetShadowwithCorneronView(view: viewamericanteleclinic, cornerradius: 10.0, color: UIColor.lightGray)
        Common.SetShadowwithCorneronView(view: viewhomecare, cornerradius: 10.0, color: UIColor.lightGray)


        if UIDevice.current.orientation.isLandscape
        {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                heightimage.constant = ((self.view.frame.size.height-60)*30)/100
            }
            else
            {
                heightimage.constant = 0
            }
        }
        else
        {
            print("Portrait")
            //Image Height
            heightimage.constant = ((self.view.frame.size.height-60)*30)/100
        }
        
        //Add menu button
        addSlideMenuButton()
        addCartButton()
        
        
        if UserDefaults.standard.object(forKey: "token") != nil
        {
            self.getMyProfileWebservice()
//            viewlab.isUserInteractionEnabled = true
//            viewhomecare.isUserInteractionEnabled = true
//            viewpharmacy.isUserInteractionEnabled = true
//            viewradiology.isUserInteractionEnabled = true
//            viewmedicalpackage.isUserInteractionEnabled = true
//            viewamericanteleclinic.isUserInteractionEnabled = true
        }
    }
    //MARK: - Webservice
    func getMyProfileWebservice() {
        
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        SVProgressHUD.show()
        UIApplication.shared.beginIgnoringInteractionEvents()

        
        let url = URL(string: K.URL.GET_MY_PROFILE)
        var urlRequest = URLRequest(url: url!)
        urlRequest.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        urlRequest.timeoutInterval = 60 * 5
        urlRequest.httpMethod = "GET"
        //urlRequest.allHTTPHeaderFields = headers
        urlRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let token = UserDefaults.standard.object(forKey: "token") as! String
        urlRequest.addValue(NSString(format: "Bearer %@", token) as String, forHTTPHeaderField: "Authorization")
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, errorResponse) in
            SVProgressHUD.dismiss()
            UIApplication.shared.endIgnoringInteractionEvents()
            
            if errorResponse != nil {
                print(errorResponse ?? "Erroer Found")
                
            }
            else{
                
                do {
                    let httpResponse = response as? HTTPURLResponse
                    
                    if httpResponse?.statusCode == 200
                    {
                        // gunzip
                        let decompressedData: Data
                        if (data?.isGzipped)! {
                            decompressedData = (try! data?.gunzipped())!
                        } else {
                            decompressedData = data!
                        }
                        //if  let json = try! JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves) as? NSDictionary
                        //{
                        if let json = try! JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves) as? NSDictionary
                        {
                            // self.arrLabs.removeAll(keepingCapacity: false)
                            DispatchQueue.main.async {
                                // print(dictionary)
                                let data1: Dictionary<String,Any> = (json as! Dictionary<String,Any>)
                                let user = User(dict: data1)
                                user.save()
                                
                            }
                        }
                    }                    
                }
                catch let exeptionError as Error? {
                    print(exeptionError!.localizedDescription)
                    SVProgressHUD.dismiss()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            }
        })
        task.resume()
    }

    override func viewWillAppear(_ animated: Bool) {
        addCartButton()
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
   
    @IBAction func btnlabs(_ sender: Any)
    {
        let labVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "LabsVC") as! LabsVC
        AppUtility.shared.mainNavController.pushViewController(labVC, animated: true)
    }
    
    @IBAction func btnRadiology(_ sender: Any)
    {
        let radiologyVC = AppUtility.STORY_BOARD_SECOND().instantiateViewController(withIdentifier: "Radiology_MainCategoryVC") as! Radiology_MainCategoryVC
        AppUtility.shared.mainNavController.pushViewController(radiologyVC, animated: true)
    }
    
    @IBAction func btnMedicalPackage(_ sender: Any)
    {
        let madicalVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "MedicalPackagesVC") as! MedicalPackagesVC
        AppUtility.shared.mainNavController.pushViewController(madicalVC, animated: true)
    }
    
    @IBAction func btnPharmacy(_ sender: Any)
    {
//let pharmacyVC = AppUtility.STORY_BOARD_SECOND().instantiateViewController(withIdentifier: "TeleclinicVC") as! TeleclinicVC
       // AppUtility.shared.mainNavController.pushViewController(pharmacyVC, animated: true)
    
     let pharmacyVC = AppUtility.STORY_BOARD_SECOND().instantiateViewController(withIdentifier: "LaunchingSoonVC") as! LaunchingSoonVC
     AppUtility.shared.mainNavController.pushViewController(pharmacyVC, animated: true)
        
    }
    
    @IBAction func btnamericanTeleclinic(_ sender: Any)
    {
        let pharmacyVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "AmericanTeleclinicVC") as! AmericanTeleclinicVC
        AppUtility.shared.mainNavController.pushViewController(pharmacyVC, animated: true)
    }
    
    @IBAction func btnhomecare(_ sender: Any)
    {
        let homecareVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "HomeCareVC") as! HomeCareVC
        AppUtility.shared.mainNavController.pushViewController(homecareVC, animated: true)

    }
    
    override func viewDidLayoutSubviews()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                heightimage.constant = ((self.view.frame.size.height-60)*30)/100
                removeView()
                addCartButton()
            }
            else
            {
                heightimage.constant = 0
                removeView()
                addCartButton()
            }
            
        } else {
            print("Portrait")
            //Image Height
            heightimage.constant = ((self.view.frame.size.height-60)*30)/100
            removeView()
            addCartButton()
        }
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                heightimage.constant = ((self.view.frame.size.height-60)*30)/100
            }
            else
            {
                heightimage.constant = 0
            }
        } else {
            print("Portrait")
            //Image Height
            heightimage.constant = ((self.view.frame.size.height-60)*30)/100
        }
    }
}

