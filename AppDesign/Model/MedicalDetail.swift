//
//  MedicalDetail.swift
//  AppDesign
//
//  Created by Amul on 10/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class MedicalDetail: NSObject {
    
    var ID          : Int! = 0
    var LabPackageVendorLocationId : Int! = 0
    var Ranking:Int! = 0
    var name        : String!
    var shortDesc   : String!
    var otherNames  : String!
    var PackageType  : String!
    var arrLabTests     : Array<LabTests> = []
    var arrLabPackagesComparison : Array<LabPackagesComparison> = []

    public class func modelsFromDictionaryArray(array:Array<Any>) -> [MedicalDetail]
    {
        var models:[MedicalDetail] = []
        for item in array
        {
            models.append(MedicalDetail(dict: item as! Dictionary<String, Any>))
        }
        return models
    }

    init(dict : Dictionary<String, Any>?) {

        ID          = dict?["LabPackageId"] as? Int
        LabPackageVendorLocationId          = dict?["LabPackageVendorLocationId"] as? Int
        Ranking = dict?["Ranking"] as? Int
        name        = dict?["Name"] as? String
        shortDesc   = dict?["ShortDescription"] as? String ?? ""
        otherNames  = dict?["OtherNames"] as? String ?? ""
        PackageType  = dict?["PackageType"] as? String ?? ""
        if let arrLabData = dict!["LabTests"] as? Array<Dictionary<String,Any>> {
            arrLabTests = LabTests.modelsFromDictionaryArray(array: arrLabData)
        }
        if let arrLabData = dict!["LabPackagesComparison"] as? Array<Dictionary<String,Any>> {
            arrLabPackagesComparison = LabPackagesComparison.modelsFromDictionaryArray(array: arrLabData)
        }
    }
}

