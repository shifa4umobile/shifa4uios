//
//  RadiologyLabsComparison.swift
//  AppDesign
//
//  Created by Amul on 11/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class RadiologyLabsComparison: NSObject {
    
    
    var LabId          : Int! = 0
    var labName     : String!
    var ImageUrlWeb    : String!
    var ProductLineId      : Int! = 0
    var ImagingId : Int! = 0
    var YourPrice : Int! = 0
    var VendorPrice : Int! = 0
    var Discount : Int! = 0
    
    
    public class func modelsFromDictionaryArray(array:Array<Any>) -> [RadiologyLabsComparison]
    {
        var models:[RadiologyLabsComparison] = []
        for item in array
        {
            models.append(RadiologyLabsComparison(dict: item as! Dictionary<String, Any>))
        }
        return models
    }
    
    init(dict : Dictionary<String, Any>?) {
        LabId          = dict?["LabId"] as? Int ?? 0
        labName     = dict?["LabName"] as? String
        ImageUrlWeb    = dict?["ImageUrlWeb"] as? String
        ProductLineId      = dict?["ProductLineId"] as? Int ?? 0
        ImagingId      = dict?["ImagingId"] as? Int ?? 0
        YourPrice      = dict?["YourPrice"] as? Int ?? 0
        VendorPrice      = dict?["VendorPrice"] as? Int ?? 0
        Discount      = dict?["Discount"] as? Int ?? 0
    }
}
