//
//  CartItems.swift
//  AppDesign
//
//  Created by Amul on 11/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class CartItems: NSObject
{
    var ID          : Int! = 0
    var labID       : Int! = 0
    var quantity    : Int! = 0
    var isHome      : Bool!

    public class func modelsFromDictionaryArray(array:Array<Any>) -> [CartItems]
    {
        var models:[CartItems] = []
        for item in array
        {
            models.append(CartItems(dict: item as! Dictionary<String, Any>))
        }
        return models
    }
    
    init(dict : Dictionary<String, Any>?) {
        ID               = dict?["ProductLineId"] as? Int ?? 0
        labID            = dict?["LabId"] as? Int ?? 0
        quantity         = dict?["quatity"] as? Int ?? 0
        isHome           = dict?["IsHomeSampleCollection"] as? Bool ?? false
    }
}
