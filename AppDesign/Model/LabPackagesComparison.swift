//
//  LabPackagesComparison.swift
//  AppDesign
//
//  Created by Amul on 10/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class LabPackagesComparison: NSObject {
    var labId : Int! = 0
    var LabPackageVendorLocationId  : Int! = 0
    var LabName     : String!
    var PackageName :String!
    var PackageId :Int! = 0
    var VendorPrice : Int! = 0
    var YourPrice :Int! = 0
    var Discount : Int! = 0
    
    
    public class func modelsFromDictionaryArray(array:Array<Any>) -> [LabPackagesComparison]
    {
        var models:[LabPackagesComparison] = []
        for item in array
        {
            models.append(LabPackagesComparison(dict: item as! Dictionary<String, Any>))
        }
        return models
    }
    
    init(dict : Dictionary<String, Any>?) {
        LabPackageVendorLocationId          = dict?["LabPackageVendorLocationId"] as? Int ?? 0
        LabName     = dict?["LabName"] as? String
         PackageName     = dict?["PackageName"] as? String
        PackageId        = dict?["PackageId"] as? Int ?? 0
        labId            = dict?["labId"] as? Int ?? 0
        VendorPrice          = dict?["VendorPrice"] as? Int ?? 0
        YourPrice        = dict?["YourPrice"] as? Int ?? 0
        Discount            = dict?["Discount"] as? Int ?? 0
       
        
    }

}
