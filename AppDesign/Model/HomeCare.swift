//
//  HomeCare.swift
//  AppDesign
//
//  Created by Amul on 11/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class HomeCare: NSObject
{
    var ID          : Int! = 0
    var name        : String!
    var price       : Int!
    var otherName   : String!

    
    public class func modelsFromDictionaryArray(array:Array<Any>) -> [HomeCare]
    {
        var models:[HomeCare] = []
        for item in array
        {
            models.append(HomeCare(dict: item as! Dictionary<String, Any>))
        }
        return models
    }
    
    init(dict : Dictionary<String, Any>?) {
        ID               = dict?["VendorLocationId"] as? Int ?? 0
        name             = dict?["ProductLine_ProductName"] as? String ?? ""
        price            = dict?["ProductLine_DiscountedPrice"] as? Int ?? 0
        otherName        = dict?["MedicalProductOtherNames"] as? String ?? ""
    }
}
