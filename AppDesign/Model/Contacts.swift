//
//  Contacts.swift
//  AppDesign
//
//  Created by Amul on 11/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class Contacts: NSObject
{
    var ID          : Int! = 0
    var TypeId      : Int! = 0
    var Name     : String!
    var contactsDesc : String!
    
    public class func modelsFromDictionaryArray(array:Array<Any>) -> [Contacts]
    {
        var models:[Contacts] = []
        for item in array
        {
            models.append(Contacts(dict: item as! Dictionary<String, Any>))
        }
        return models
    }
    
    init(dict : Dictionary<String, Any>?) {
        ID          = dict?["VendorLocationId"] as? Int ?? 0
        Name     = dict?["TypeName"] as? String
        contactsDesc     = dict?["Description"] as? String
         TypeId          = dict?["TypeId"] as? Int ?? 0
        
    }
}
