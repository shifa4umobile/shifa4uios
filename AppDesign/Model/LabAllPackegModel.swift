//
//  LabAllPackegModel.swift
//  AppDesign
//
//  Created by Amul on 11/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class LabAllPackegModel: NSObject
{
    var ImagingId          : Int! = 0
    var BodyLocationId          : Int! = 0
    var ImagingCategoryId : Int! = 0
    var Ranking:Int! = 0
    var ImagingName        : String!
    var ImagingCategoryName : String!
    var shortDesc   : String!
    var BodyLocationName  : String!
    var BodyLocationPartTypeName  : String!
    var BodyLocationPartId          : Int! = 0
    var BodyLocationPartTypeId          : Int! = 0
    var BodyLocationPartName        : String!
    var arrLocations     : Array<Locations> = []
    var arrRadiologyLabsComparison : Array<RadiologyLabsComparison> = []
    
    public class func modelsFromDictionaryArray(array:Array<Any>) -> [LabAllPackegModel]
    {
        var models:[LabAllPackegModel] = []
        for item in array
        {
            models.append(LabAllPackegModel(dict: item as! Dictionary<String, Any>))
        }
        return models
    }
    
    init(dict : Dictionary<String, Any>?) {
        
        ImagingId          = dict?["ImagingId"] as? Int
        BodyLocationId          = dict?["BodyLocationId"] as? Int
        ImagingCategoryId          = dict?["ImagingCategoryId"] as? Int
        Ranking = dict?["Ranking"] as? Int
        ImagingName        = dict?["ImagingName"] as? String
        ImagingCategoryName        = dict?["ImagingCategoryName"] as? String
        shortDesc   = dict?["ShortDescription"] as? String ?? ""
        BodyLocationName  = dict?["BodyLocationName"] as? String ?? ""
        BodyLocationPartTypeName  = dict?["BodyLocationPartTypeName"] as? String ?? ""
        BodyLocationPartId          = dict?["BodyLocationPartId"] as? Int
        BodyLocationPartTypeId          = dict?["BodyLocationPartTypeId"] as? Int
        BodyLocationPartName        = dict?["BodyLocationPartName"] as? String
        
        
        if let arrLabData = dict!["Locations"] as? Array<Dictionary<String,Any>> {
            arrLocations = Locations.modelsFromDictionaryArray(array: arrLabData)
        }
        if let arrLabData = dict!["RadiologyLabsComparison"] as? Array<Dictionary<String,Any>> {
            arrRadiologyLabsComparison = RadiologyLabsComparison.modelsFromDictionaryArray(array: arrLabData)
        }
    }


}
