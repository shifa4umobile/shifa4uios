//
//  RadiologyLab.swift
//  Connect
//
//  Created by Mahavir Makwana on 04/05/18.
//  Copyright © 2018 Shift Boolean. All rights reserved.
//

import UIKit

class RadiologyLab: NSObject {
    var ID          : Int! = 0
    var labName     : String!
    var labID       : Int!
    var price       : Double!

    /*"ImagingId": 564,
    "ProductLineId": 1612,
    "LabName": "Al-Razi",
    "LabId": 8,
    "YourPrice": 2375,
    "VendorPrice": 2500,
    "Discount": 0,
    "ImageUrlWeb": "63"*/

    public class func modelsFromDictionaryArray(array:Array<Any>) -> [RadiologyLab]
    {
        var models:[RadiologyLab] = []
        for item in array
        {
            models.append(RadiologyLab(dict: item as! Dictionary<String, Any>))
        }
        return models
    }

    init(dict : Dictionary<String, Any>?) {
        ID          = dict?["ImagingId"] as? Int ?? 0
        labName     = dict?["LabName"] as? String
        labID       = dict?["LabId"] as? Int ?? 0
        price       = dict?["VendorPrice"] as? Double ?? 0.0
    }
}
