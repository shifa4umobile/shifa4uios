//
//  PharmacyModel.swift
//  AppDesign
//
//  Created by Amul on 10/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class PharmacyModel: NSObject {

    var id          :Int?
    var icon       : UIImage?
    var LinkUrl      : String?
    
    
    public class func modelsFromDictionaryArray(array:Array<Any>) -> [PharmacyModel]
    {
        var models:[PharmacyModel] = []
        for item in array
        {
            models.append(PharmacyModel(dict: item as? Dictionary<String, Any>))
        }
        return models
    }
    
    
    init(dict : Dictionary<String, Any>?) {
        id       = dict?["id"] as? Int
        icon       = dict?["icon"] as? UIImage
        LinkUrl      = dict?["LinkUrl"] as? String ?? ""
        
    }
}
