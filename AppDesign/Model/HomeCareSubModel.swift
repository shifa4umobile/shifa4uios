//
//  HomeCareSubModel.swift
//  AppDesign
//
//  Created by Amul on 11/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class HomeCareSubModel: NSObject {
    var ID          : Int! = 0
    var name        : String!
    var ImageWebUrl   : String!
    var HomeDescription  : String!
   
    public class func modelsFromDictionaryArray(array:Array<Any>) -> [HomeCareSubModel]
    {
        var models:[HomeCareSubModel] = []
        for item in array
        {
            models.append(HomeCareSubModel(dict: item as! Dictionary<String, Any>))
        }
        return models
    }
    
    init(dict : Dictionary<String, Any>?) {
        
        ID          = dict?["MedicalSpecialityId"] as? Int
        name        = dict?["MedicalSpecialityName"] as? String
        ImageWebUrl   = dict?["ImageUrlWeb"] as? String ?? ""
        HomeDescription  = dict?["Description"] as? String ?? ""
        
    }
}
