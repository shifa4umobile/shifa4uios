//
//  Locations.swift
//  AppDesign
//
//  Created by Amul on 11/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class Locations: NSObject
{
    var ID          : Int! = 0
    var Name     : String!
    var arrContacts     : Array<Contacts> = []
    
    public class func modelsFromDictionaryArray(array:Array<Any>) -> [Locations]
    {
        var models:[Locations] = []
        for item in array
        {
            models.append(Locations(dict: item as! Dictionary<String, Any>))
        }
        return models
    }
    
    init(dict : Dictionary<String, Any>?) {
        ID          = dict?["VendorLocationId"] as? Int ?? 0
        Name     = dict?["Name"] as? String
        if let arrLabData = dict!["Contacts"] as? Array<Dictionary<String,Any>> {
            arrContacts = Contacts.modelsFromDictionaryArray(array: arrLabData)
        }
    }
}
