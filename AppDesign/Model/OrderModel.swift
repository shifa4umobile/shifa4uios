//
//  OrderModel.swift
//  AppDesign
//
//  Created by Amul on 15/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
import ObjectMapper
class OrederModel1: Mappable {
    var arrOpenOrders :[OrderDetailModel1]?
    var arrCanceledOrders :[OrderDetailModel1]?
    var arrClosedOrders  :[OrderDetailModel1]?
    required init?(map: Map) {
        
            }
            func mapping(map: Map)
            {
               arrOpenOrders  <- map["OpenOrders"]
                arrClosedOrders  <- map["CanceledOrders"]
                arrCanceledOrders  <- map["ClosedOrders"]
            }
}
class OrderDetailModel1: Mappable
{
    var Sr          : Int! = 0
    var OrderDetailId      : Int! = 0
    var OrderId     : String?
    var OrderDate    : String?
    var PaymentMethod : String?
    var OrderDateTime :String?
    var TotalAmount :String?
    var Zone :String?
    var ItemName :String?
    var PaymentStatus :String?
    var Price :String?
    var PaymentMethodPrefix :String?
    var arrOrderDetailsVM : [OrderDetailModel1]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        Sr  <- map["Sr"]
        OrderDetailId  <- map["OrderDetailId"]
        OrderId  <- map["OrderId"]
        OrderDate  <- map["OrderDate"]
        PaymentMethod  <- map["PaymentMethod"]
        OrderDateTime  <- map["OrderDateTime"]
        TotalAmount  <- map["TotalAmount"]
        Zone  <- map["Zone"]
        ItemName  <- map["ItemName"]
        PaymentStatus  <- map["PaymentStatus"]
        Price  <- map["Price"]
        PaymentMethodPrefix  <- map["PaymentMethodPrefix"]
        arrOrderDetailsVM  <- map["OrderDetailsVM"]
    }
    
    
}
























