//
//  User.swift
//  Heuro
//
//  Created by Harry on 22/05/17.
//
//

import UIKit


private let kUserID                 = "UserId"
private let kUserFullName               = "FullName"
private let kUserAccessToken        = "access_token"

private let kUserSex                = "Sex"
private let kUserCountry               = "Country"
private let kUserEmail              = "Email"
private let kUserArea               = "Area"
private let kUserGender        = "Gender"

private let kUserFirstName                = "FirstName"
private let kUserLastName               = "LastName"
private let kUserFatherName              = "FatherName"
private let kUserTitle               = "Title"
private let kUserCityId        = "CityId"

private let kUserLoginName                = "LoginName"
private let kUserMaritalStatus               = "MaritalStatus"
private let kUserAreaId              = "AreaId"
private let kUserCity               = "City"

private let kUserCountryId                = "CountryId"
private let kUserMobileNo               = "MobileNo"
private let kUserCNIC              = "CNIC"
private let kUserDateofBirth               = "DateofBirth"
private let kUserAddress        = "Address"


class User: NSObject, NSCoding {

    var ID                 : String! = "0"
    var fullName           : String! = ""
    var accessToken        : String! = ""

    var sex                : String! = ""
    var country            : String! = ""
    var email              : String! = ""
    var area               : String! = ""
    var gender             : String! = ""

    var firstName           : String! = ""
    var lastName            : String! = ""
    var fatherName          : String! = ""
    var title               : String! = ""
    var cityId              : Int! = 0

    var loginName               : String! = ""
    var maritalStatus           : String! = ""
    var areaId              : Int! = 0
    var city               : String! = ""

    var countryId                : Int! = 0
    var mobileNo               : String! = ""
    var CNIC              : String! = ""
    var dateofBirth         : String! = ""
    var address        : String! = ""

    public class func modelsFromDictionaryArray(array:Array<Any>) -> [User]
    {
        var models:[User] = []
        for item in array
        {
            models.append(User(dict: item as! Dictionary<String, Any>))
        }

        return models
    }

    

    public init(dict : [String : Any]?) {

        self.ID                 = dict?[kUserID] as? String ?? "0"
        self.fullName           = dict?[kUserFullName] as? String ?? ""
        self.accessToken        = dict?[kUserAccessToken] as? String ?? ""

        self.sex                = dict?[kUserSex] as? String ?? ""
        self.country            = dict?[kUserCountry] as? String ?? ""
        self.email              = dict?[kUserEmail] as? String ?? ""
        self.area               = dict?[kUserArea] as? String ?? ""
        self.gender             = dict?[kUserGender] as? String ?? ""

        self.firstName           = dict?[kUserFirstName] as? String ?? ""
        self.lastName            = dict?[kUserLastName] as? String ?? ""
        self.fatherName          = dict?[kUserFatherName] as? String ?? ""
        self.title               = dict?[kUserTitle] as? String ?? ""
        self.cityId              = dict?[kUserCityId] as? Int ?? 0

        self.loginName               = dict?[kUserLoginName] as? String ?? ""
        self.maritalStatus           = dict?[kUserMaritalStatus] as? String ?? ""
        self.areaId              = dict?[kUserAreaId] as? Int ?? 0
        self.city               = dict?[kUserCity] as? String ?? ""

        self.countryId                = dict?[kUserCountryId] as? Int ?? 0
        self.mobileNo               = dict?[kUserMobileNo] as? String ?? ""
        self.CNIC              = dict?[kUserCNIC] as? String ?? ""
        self.dateofBirth         = dict?[kUserDateofBirth] as? String ?? ""
        self.address        = dict?[kUserAddress] as? String ?? ""
    }

    //required public
    required init?(coder aDecoder: NSCoder) {

        self.ID                 = aDecoder.decodeObject(forKey: kUserID) as? String ?? ""
        self.fullName           = aDecoder.decodeObject(forKey: kUserFullName) as? String ?? ""
        self.accessToken        = aDecoder.decodeObject(forKey: kUserAccessToken) as? String ?? ""

        self.sex                = aDecoder.decodeObject(forKey: kUserSex) as? String ?? ""
        self.country            = aDecoder.decodeObject(forKey: kUserCountry) as? String ?? ""
        self.email              = aDecoder.decodeObject(forKey: kUserEmail) as? String ?? ""
        self.area               = aDecoder.decodeObject(forKey: kUserArea) as? String ?? ""
        self.gender             = aDecoder.decodeObject(forKey: kUserGender) as? String ?? ""

        self.firstName           = aDecoder.decodeObject(forKey: kUserFirstName) as? String ?? ""
        self.lastName            = aDecoder.decodeObject(forKey: kUserLastName) as? String ?? ""
        self.fatherName          = aDecoder.decodeObject(forKey: kUserFatherName) as? String ?? ""
        self.title               = aDecoder.decodeObject(forKey: kUserTitle) as? String ?? ""
        self.cityId              = aDecoder.decodeObject(forKey: kUserCityId) as? Int ?? 0

        self.loginName               = aDecoder.decodeObject(forKey: kUserLoginName) as? String ?? ""
        self.maritalStatus           = aDecoder.decodeObject(forKey: kUserMaritalStatus) as? String ?? ""
        self.areaId              = aDecoder.decodeObject(forKey: kUserAreaId) as? Int ?? 0
        self.city               = aDecoder.decodeObject(forKey: kUserCity) as? String ?? ""

        self.countryId                = aDecoder.decodeObject(forKey: kUserCountryId) as? Int ?? 0
        self.mobileNo               = aDecoder.decodeObject(forKey: kUserMobileNo) as? String ?? ""
        self.CNIC              = aDecoder.decodeObject(forKey: kUserCNIC) as? String ?? ""
        self.dateofBirth         = aDecoder.decodeObject(forKey: kUserDateofBirth) as? String ?? ""
        self.address        = aDecoder.decodeObject(forKey: kUserAddress) as? String ?? ""

    }
    

    func encode(with aCoder: NSCoder) {

        aCoder.encode(self.ID, forKey: kUserID)
        aCoder.encode(self.fullName, forKey: kUserFullName)
        aCoder.encode(self.accessToken, forKey: kUserAccessToken)

        aCoder.encode(self.sex, forKey: kUserSex)
        aCoder.encode(self.country, forKey: kUserCountry)
        aCoder.encode(self.email, forKey: kUserEmail)
        aCoder.encode(self.area , forKey: kUserArea)
        aCoder.encode(self.gender , forKey: kUserGender)

        aCoder.encode(self.firstName , forKey: kUserFirstName)
        aCoder.encode(self.lastName , forKey: kUserLastName)
        aCoder.encode(self.fatherName , forKey: kUserFatherName)
        aCoder.encode(self.title , forKey: kUserTitle)
        aCoder.encode(self.cityId , forKey: kUserCityId)

        aCoder.encode(self.loginName , forKey: kUserLoginName)
        aCoder.encode(self.maritalStatus , forKey: kUserMaritalStatus)
        aCoder.encode(self.areaId , forKey: kUserAreaId)
        aCoder.encode(self.city , forKey: kUserCity)

        aCoder.encode(self.countryId , forKey: kUserCountryId)
        aCoder.encode(self.mobileNo , forKey: kUserMobileNo)
        aCoder.encode(self.CNIC , forKey: kUserCNIC)
        aCoder.encode(self.dateofBirth , forKey: kUserDateofBirth)
        aCoder.encode(self.address , forKey: kUserAddress)
    }

//    func userType() -> UserType {
//        return UserType(rawValue:type) ?? .player
//    }

    func save() -> Void {
        StandardUserDefaults.setCustomObject(obj: self, key: K.Key.LoggedInUser)
        User.user = nil
    }
    
    class func delete() -> Void {
        StandardUserDefaults.removeObject(forKey: K.Key.LoggedInUser)
        StandardUserDefaults.synchronize()
        User.user = nil
    }

    private static var user : User! = nil
    public static func loggedInUser() -> User? {
        if user == nil {
            user = StandardUserDefaults.getCustomObject(key: K.Key.LoggedInUser) as? User
        }
        return user
    }
}
