//
//  LabDetails.swift
//  Connect
//
//  Created by Mahavir Makwana on 04/05/18.
//  Copyright © 2018 Shift Boolean. All rights reserved.
//

import UIKit

class LabDetails: NSObject {
    var ID          : Int! = 0
    var name        : String!
    var shortDesc   : String!
    var otherNames  : String!
    var sampleType  : String!
    var arrLabs     : Array<Lab> = []

    public class func modelsFromDictionaryArray(array:Array<Any>) -> [LabDetails]
    {
        var models:[LabDetails] = []
        for item in array
        {
            models.append(LabDetails(dict: (item as! Dictionary<String, Any>)))
        }
        return models
    }

    init(dict : Dictionary<String, Any>?) {

        ID          = dict?["LabTestId"] as? Int
        name        = dict?["Name"] as? String
        shortDesc   = dict?["ShortDescription"] as? String ?? ""
        otherNames  = dict?["OtherNames"] as? String ?? ""
        sampleType  = dict?["SampleType"] as? String ?? ""
        if let arrLabData = dict!["Labs"] as? Array<Dictionary<String,Any>> {
            arrLabs = Lab.modelsFromDictionaryArray(array: arrLabData)
        }
    }
}
