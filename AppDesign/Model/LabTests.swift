//
//  LabTests.swift
//  AppDesign
//
//  Created by Amul on 10/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class LabTests: NSObject {
    
    var ID          : Int! = 0
    var name        : String!
    
    
    public class func modelsFromDictionaryArray(array:Array<Any>) -> [LabTests]
    {
        var models:[LabTests] = []
        for item in array
        {
            models.append(LabTests(dict: item as! Dictionary<String, Any>))
        }
        return models
    }
    
    init(dict : Dictionary<String, Any>?) {
        ID          = dict?["LabTestId"] as? Int ?? 0
        name        = dict?["LabTestName"] as? String
        
    }

}
