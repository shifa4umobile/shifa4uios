//
//  Radiology.swift
//  Connect
//
//  Created by Mahavir Makwana on 04/05/18.
//  Copyright © 2018 Shift Boolean. All rights reserved.
//

import UIKit

class Radiology: NSObject {
    var ID           : Int! = 0
    var name         : String!
    var sortDesc     : String!
    var categoryID   : Int!
    var categoryName : String!
    var locationID   : Int!
    var locationName : String!
    var locationPartTypeName : String!
    var locationPartName : String!
    var arrLabs     : Array<RadiologyLab> = []


    public class func modelsFromDictionaryArray(array:Array<Any>) -> [Radiology]
    {
        var models:[Radiology] = []
        for item in array
        {
            models.append(Radiology(dict: item as! Dictionary<String, Any>))
        }
        return models
    }

    init(dict : Dictionary<String, Any>?) {
        ID           = dict?["ImagingId"] as? Int ?? 0
        name         = dict?["ImagingName"] as? String ?? ""
        sortDesc     = dict?["ShortDescription"] as? String ?? ""
        categoryID   = dict?["ImagingCategoryId"] as? Int ?? 0
        categoryName = dict?["ImagingCategoryName"] as? String ?? ""
        locationID   = dict?["BodyLocationId"] as? Int ?? 0
        locationName = dict?["BodyLocationName"] as? String ?? ""
        locationPartTypeName = dict?["BodyLocationPartTypeName"] as? String ?? ""
        locationPartName = dict?["BodyLocationPartName"] as? String ?? ""
        if let arrLabData = dict!["RadiologyLabsComparison"] as? Array<Dictionary<String,Any>> {
            arrLabs = RadiologyLab.modelsFromDictionaryArray(array: arrLabData)
        }
    }
}
