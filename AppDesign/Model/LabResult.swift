//
//  LabResult.swift
//  AppDesign
//
//  Created by Amul on 15/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class LabResult: NSObject {
    
    var Sr          : Int! = 0
    var PatientResultId      : Int!
    var OrderId     : String!
    var OrderDate    : String!
    var ReportDetail : String!
    var ReportName :String!
    var ReportHeading :String!
    var Paths :String!
    var VendorName :String!
    var IsRecommended:Bool! = false
    
    public class func modelsFromDictionaryArray(array:Array<Any>) -> [LabResult]
    {
        var models:[LabResult] = []
        for item in array
        {
            models.append(LabResult(dict: item as! Dictionary<String, Any>))
        }
        return models
    }
    
    init(dict : Dictionary<String, Any>?) {
        Sr          = dict?["Sr"] as? Int ?? 0
        PatientResultId      = dict?["PatientResultId"] as? Int ?? 0
        OrderId     = dict?["OrderId"] as? String
        OrderDate    = dict?["OrderDate"] as? String
        ReportName    = dict?["ReportName"] as? String
        ReportHeading  = dict?["ReportHeading"] as? String
        ReportDetail = dict?["ReportDetail"] as? String
        Paths = dict?["Paths"] as? String
        VendorName = dict?["VendorName"] as? String
        IsRecommended = dict?["IsRecommended"] as? Bool
        
        
    }
}
