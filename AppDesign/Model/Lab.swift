//
//  Lab.swift
//  Connect
//
//  Created by Mahavir Makwana on 04/05/18.
//  Copyright © 2018 Shift Boolean. All rights reserved.
//

import UIKit

class Lab: NSObject {
    var ID          : Int! = 0
    var labName     : String!
    var testName    : String!
    var testID      : Int!
    var price       : Double!
    var isSelected  : Bool! = false
    var quantity    : Int! = 0

    public class func modelsFromDictionaryArray(array:Array<Any>) -> [Lab]
    {
        var models:[Lab] = []
        for item in array
        {
            models.append(Lab(dict: item as! Dictionary<String, Any>))
        }
        return models
    }

    init(dict : Dictionary<String, Any>?) {
        ID          = dict?["LabId"] as? Int ?? 0
        labName     = dict?["LabName"] as? String
        testName    = dict?["TestName"] as? String
        testID      = dict?["TestId"] as? Int ?? 0
        price       = dict?["YourPrice"] as? Double ?? 0.0
    }
}
