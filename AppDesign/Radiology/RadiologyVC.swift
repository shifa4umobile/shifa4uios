//
//  RadiologyVC.swift
//  AppDesign
//
//  Created by sachin on 20/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class RadiologyVC: BaseViewController
{
    
    @IBOutlet weak var lblComingSoon: UILabel!
    @IBOutlet weak var lblHeader_Title: UILabel!
    @IBOutlet weak var viewTypeBg: UIView!
    @IBOutlet weak var viewSubLocationBg: UIView!
    @IBOutlet weak var viewBoadyLocationBg: UIView!
    @IBOutlet weak var lblTitle_SearchViaCategory: UILabel!
    @IBOutlet var viewHeader_SearchViaCategory: UIView!
    
    @IBOutlet weak var btnClick_MainCategorySearch: UIButton!
    @IBOutlet weak var img_SearchViaCat_MainDropDown: UIImageView!
    @IBOutlet weak var view_SearchViaCategoris_Height: NSLayoutConstraint!
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tbllist: UITableView!
    @IBOutlet weak var viewbtnsearch: UIView!
    @IBOutlet weak var viewsearchviaCat: UIView!
    @IBOutlet weak var viewSearchTop: UIView!
    
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var btnSearchTop: UIButton!
    
    @IBOutlet weak var btnType: UIButton!
    @IBOutlet weak var btnSubLocation: UIButton!
    @IBOutlet weak var btnLocation: UIButton!
    
    var arrCategoryList = NSArray()
    var arrLocationList = NSArray()
    var arrSubLocationList = NSArray()
    var arrTypeList = NSArray()
    var arrSearchList = NSArray()
    
    
    var arrRadiologyList:NSMutableArray = NSMutableArray()
    var arrSearch_RadiologyList:NSMutableArray = NSMutableArray()
    
    var isOpen_searchViaCategory:Int = 0
    
    var SelectCategoryName = String()
    var Categoryid : Int = Int()
    var Locationid : Int =  0
    var SubLocationid : Int = 0
    var LastTypeid: Int = 0
    
    
    
    var TotalRecords:Int = 0
    var page_Limit: Int = 0
    var activityIndicator: UIActivityIndicatorView?
    
    let chooseArticleDropDown = DropDown()
    //var arrRadiologyList: Array<Radiology> = []
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown
        ]
    }()
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        
        
        viewSearchTop.layer.cornerRadius = 4
        viewSearchTop.clipsToBounds = true
        
        Common.SetShadowwithCorneronView(view: viewSearch, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: viewsearchviaCat, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: viewbtnsearch, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        
        lblHeader_Title.text = "Radiology - " + SelectCategoryName
        lblTitle_SearchViaCategory.text = SelectCategoryName
        tbllist.tableHeaderView = viewHeader_SearchViaCategory
        tbllist.tableHeaderView?.height = 80
        viewBoadyLocationBg.isHidden = true
        viewSubLocationBg.isHidden = true
        viewTypeBg.isHidden = true
        img_SearchViaCat_MainDropDown.image = #imageLiteral(resourceName: "dropdown_right")
        self.lblComingSoon.isHidden = true
        
        //self.getCategory()
        self.getLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addCartButton()
    }
    override func viewDidLayoutSubviews()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            //Image Height
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
            {
                removeView()
                addCartButton()
            }
            else
            {
                removeView()
                
                addCartButton()
            }
            
        } else {
            print("Portrait")
            //Image Height
            removeView()
            addCartButton()
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnClick_MainCategorySearch(_ sender: Any) {
        if isOpen_searchViaCategory == 0 {
            tbllist.tableHeaderView?.height = 228
            tbllist.layoutIfNeeded()
            viewBoadyLocationBg.isHidden = false
            viewSubLocationBg.isHidden = false
            viewTypeBg.isHidden = false
            img_SearchViaCat_MainDropDown.image = #imageLiteral(resourceName: "dropdown")
            isOpen_searchViaCategory = 1
        }else{
            tbllist.tableHeaderView?.height = 80
            tbllist.layoutIfNeeded()
            viewBoadyLocationBg.isHidden = true
            viewSubLocationBg.isHidden = true
            viewTypeBg.isHidden = true
            img_SearchViaCat_MainDropDown.image = #imageLiteral(resourceName: "dropdown_right")
            isOpen_searchViaCategory = 0
        }
        tbllist.reloadData()
        
    }
    
    @IBAction func btnfilter(_ sender: Any)
    {
        
    }
    
    @IBAction func btninfo(_ sender: Any)
    {
        
    }
    
    @IBAction func btnSearchTop(_ sender: Any) {
        page_Limit = 0
        arrRadiologyList = NSMutableArray()
        arrSearch_RadiologyList = NSMutableArray()
        tbllist.reloadData()
        apicall(is_firsttime: "1", SearchKeyWord: txtSearch.text!)
    }
    @IBAction func btnaddtocart(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tbllist)
        let indexPath = self.tbllist.indexPathForRow(at: buttonPosition)
        let radiology = NSMutableDictionary(dictionary: arrRadiologyList[(indexPath?.row)!] as! NSDictionary)
        
        if UserDefaults.standard.value(forKey: "arrradiologycart") == nil
        {
            let selectLabId = "\(radiology.object(forKey: "ImagingId") ?? 0)"
            getDetail(is_firsttime: "1", selectid: selectLabId, selectDic: radiology as NSDictionary, isDetail: "0")
        }
        else
        {
            let arrLabTestCart:NSMutableArray = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrradiologycart") as! NSArray)
            var istrue = "yes"
            for i in 0..<arrLabTestCart.count
            {
                let dicLab = arrLabTestCart[i] as! NSDictionary
                let labidtemp = dicLab.object(forKey: "ImagingId") as! Int
                let labidtemp11 = radiology.object(forKey: "ImagingId") as! Int
                
                if labidtemp11 == labidtemp
                {
                    istrue = "no"
                }
            }
            if istrue == "yes"
            {
                let selectLabId = "\(radiology.object(forKey: "ImagingId") ?? 0)"
                getDetail(is_firsttime: "1", selectid: selectLabId, selectDic: radiology as NSDictionary, isDetail: "0")
            }
            else
            {
                ISMessages.show("Product Already Exits in Cart")
            }
        }
    }
    
    @IBAction func btnsearch(_ sender: Any)
    {
        callSearchViaCategory()
    }
    func callSearchViaCategory()  {
        page_Limit = 0
        arrRadiologyList = NSMutableArray()
        arrSearch_RadiologyList = NSMutableArray()
        tbllist.reloadData()
        self.getSearchResut(is_firsttime: "1")
        //        btnLocation.setTitle("Body Location", for: .normal)
        //        Locationid = 0
        //        btnSubLocation.setTitle("Sub Location", for: .normal)
        //        SubLocationid = 0
        //        btnType.setTitle("Type", for: .normal)
        //        LastTypeid = 0
        //btnCategory.setTitle("Category", for: .normal)
        //Categoryid = 0
        chooseArticleDropDown.dataSource = [""]
    }
    
    @IBAction func btnBack(_ sender: Any) {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    
    
    func getCategory()
    {
        
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        SVProgressHUD.show()
        
        let url = URL(string: K.URL.GetAllCategories)
        let request = URLRequest(url: url!)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            SVProgressHUD.dismiss()
            // gunzip
            let decompressedData: Data
            if (data?.isGzipped)! {
                decompressedData = (try! data?.gunzipped())!
            } else {
                decompressedData = data!
            }
            
            let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
            
            if let arrData = dictionary as? NSArray {
                print(dictionary!)
                self.arrCategoryList = NSArray(array: arrData)
            }
            else {
                ISMessages.show("Category not found")
            }
        });
        task.resume()
    }
    
    
    @IBAction func btncategory(_ sender: Any)
    {
        /* let arrLabsTemp = NSMutableArray()
         for i in 0..<arrCategoryList.count
         {
         let labname = (arrCategoryList.object(at: i) as! NSDictionary).object(forKey: "Name")
         arrLabsTemp.add(labname as Any)
         }
         
         let lblNotificationText = sender as? UIButton
         chooseArticleDropDown.anchorView = lblNotificationText
         chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
         chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
         chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
         lblNotificationText?.setTitle(item, for: .normal)
         self?.Categoryid = (self?.arrCategoryList.object(at: index) as! NSDictionary).object(forKey: "ImagingCategoryId")
         as! Int
         self?.btnLocation.setTitle("Body Location", for: .normal)
         self?.Locationid = 0
         self?.btnSubLocation.setTitle("Sub Location", for: .normal)
         self?.SubLocationid = 0
         self?.btnType.setTitle("Type", for: .normal)
         self?.LastTypeid = 0
         self?.chooseArticleDropDown.hide()
         }
         
         chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
         print("Muti selection action called with: \(items)")
         self?.getLocation()
         
         if items.isEmpty {
         lblNotificationText?.setTitle("", for: .normal)
         
         }
         }
         
         chooseArticleDropDown.show()
         */
    }
    func getLocation()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        let parameters = ["ImagingCategoryId": "\(Categoryid)"]
        
        let url = URL(string: K.URL.GetAllLocations)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            SVProgressHUD.dismiss()
            // gunzip
            let decompressedData: Data
            if (data?.isGzipped)! {
                decompressedData = (try! data?.gunzipped())!
            } else {
                decompressedData = data!
            }
            
            let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
            
            if let arrData = dictionary as? NSArray {
                print(dictionary!)
                self.arrLocationList = NSArray(array: arrData)
            }
            else {
                ISMessages.show("Category not found")
            }
            self.callSearchViaCategory()
        });
        task.resume()
        
    }
    
    @IBAction func btnbodylocation(_ sender: Any)
    {
       
        
        let arrLabsTemp = NSMutableArray()
        for i in 0..<arrLocationList.count
        {
            let labname = (arrLocationList.object(at: i) as! NSDictionary).object(forKey: "Name")
            arrLabsTemp.add(labname as Any)
        }
        
        if arrLabsTemp.count == 0
        {
            ISMessages.show("Location not Exist")
        }
        
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            //lblNotificationText?.setTitle(item, for: .normal)
            self?.btnLocation.setTitle(item, for: .normal)
            self?.Locationid = (self?.arrLocationList.object(at: index) as! NSDictionary).object(forKey: "BodyLocationId") as! Int
            self?.btnSubLocation.setTitle("Sub Location", for: .normal)
            self?.SubLocationid = 0
            self?.btnType.setTitle("Type", for: .normal)
            self?.LastTypeid = 0
            
            self?.getSubLocation()
            
            self?.chooseArticleDropDown.hide()
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            
            
            if items.isEmpty {
                //lblNotificationText?.setTitle("", for: .normal)
                
            }
        }
        
        chooseArticleDropDown.show()
    }
    func getSubLocation()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        let parameters = ["ImagingCategoryId": "\(Categoryid)" , "BodyLocationId": "\(Locationid)"]
        
        let url = URL(string: K.URL.GetAllSubLocations)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            SVProgressHUD.dismiss()
            // gunzip
            let decompressedData: Data
            if (data?.isGzipped)! {
                decompressedData = (try! data?.gunzipped())!
            } else {
                decompressedData = data!
            }
            
            let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
            
            if let arrData = dictionary as? NSArray {
                print(dictionary!)
                self.arrSubLocationList = NSArray(array: arrData)
                self.callSearchViaCategory()
            }
            else {
                ISMessages.show("SubLocation not found")
            }
        });
        task.resume()
        
    }
    @IBAction func btnsublocation(_ sender: Any)
    {
        
        
        let arrLabsTemp = NSMutableArray()
        for i in 0..<arrSubLocationList.count
        {
            let labname = (arrSubLocationList.object(at: i) as! NSDictionary).object(forKey: "Name")
            arrLabsTemp.add(labname as Any)
        }
        if arrLabsTemp.count == 0
        {
            ISMessages.show("Sub Location not Exist")
        }
        
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            //lblNotificationText?.setTitle(item, for: .normal)
            self?.btnSubLocation.setTitle(item, for: .normal)
            self?.SubLocationid = (self?.arrSubLocationList.object(at: index) as! NSDictionary).object(forKey: "BodyLocationPartId") as! Int
            self?.btnType.setTitle("Type", for: .normal)
            self?.LastTypeid = 0
            
            self?.getType()
            
            self?.chooseArticleDropDown.hide()
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            
            if items.isEmpty {
                //lblNotificationText?.setTitle("", for: .normal)
                
            }
        }
        
        chooseArticleDropDown.show()
    }
    func getType()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        
        let parameters = ["ImagingCategoryId": "\(Categoryid)" , "BodyLocationId": "\(Locationid)", "BodyLocationPartId": "\(SubLocationid)" ]
        
        let url = URL(string: K.URL.GetAllType)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            SVProgressHUD.dismiss()
            // gunzip
            let decompressedData: Data
            if (data?.isGzipped)! {
                decompressedData = (try! data?.gunzipped())!
            } else {
                decompressedData = data!
            }
            
            let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
            
            if let arrData = dictionary as? NSArray {
                print(dictionary!)
                self.arrTypeList = NSArray(array: arrData)
                self.callSearchViaCategory()
            }
            else {
                ISMessages.show("Type not found")
             }
        });
        task.resume()
        
    }
    @IBAction func btntype(_ sender: Any)
    {
       
        let arrLabsTemp = NSMutableArray()
        for i in 0..<arrTypeList.count
        {
            let labname = (arrTypeList.object(at: i) as! NSDictionary).object(forKey: "Name")
            arrLabsTemp.add(labname as Any)
        }
        if arrLabsTemp.count == 0
        {
            ISMessages.show("Type not Exist")
        }
        
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            //lblNotificationText?.setTitle(item, for: .normal)
            self?.btnType.setTitle(item, for: .normal)
            self?.LastTypeid = (self?.arrTypeList.object(at: index) as! NSDictionary).object(forKey: "BodyLocationPartTypeId") as! Int
            self?.callSearchViaCategory()
            self?.chooseArticleDropDown.hide()
            
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            if items.isEmpty {
                //lblNotificationText?.setTitle("", for: .normal)
                
            }
        }
        
        chooseArticleDropDown.show()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView == tbllist
        {
            let visibleRows = tbllist.indexPathsForVisibleRows
            let indexPath: IndexPath? = visibleRows?.last
            if indexPath?.row == arrRadiologyList.count-1
            {
                if TotalRecords == 10
                {
                    TotalRecords = 0
                    page_Limit = page_Limit + 1
                    self.getSearchResut(is_firsttime: "0")
                }
            }
        }
    }
    
    
    func getSearchResut(is_firsttime: String)
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        if is_firsttime == "1"
        {
            SVProgressHUD.show()
        }
        else
        {
            activityIndicator?.startAnimating()
            tbllist.tableFooterView = activityIndicator
        }
        
        let CityId = UserDefaults.standard.object(forKey: "selectCityid") as! String
        
        let parameters = ["ImagingCategoryId": "\(Categoryid)" , "BodyLocationId": "\(Locationid)", "BodyLocationPartId": "\(SubLocationid)" , "BodyLocationPartTypeID": "\(LastTypeid)" , "CityId" : "\(CityId)" , "SkipPages" : "\(page_Limit)" , "Records": "10"]
        
        let postData = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        
        var strUrl = String()
        
        strUrl = K.URL.GetAllSearch
        
        strUrl = strUrl.replacing(" ", with: "")
        let request = NSMutableURLRequest(url: NSURL(string: strUrl)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 120.0)
        request.httpMethod = "POST"
        let headers = [
            "content-type": "application/json"
        ]
        request.allHTTPHeaderFields = headers
        
        request.httpMethod = "POST"
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            DispatchQueue.main.async {
                self.activityIndicator?.stopAnimating()
                self.tbllist.tableFooterView = nil
                SVProgressHUD.dismiss()
                if let httpResponse = response as? HTTPURLResponse {
                    print("Status code: (\(httpResponse.statusCode))")
                    if httpResponse.statusCode == 200
                    {
                        do {
                            // gunzip
                            let decompressedData: Data
                            if (data?.isGzipped)! {
                                decompressedData = (try! data?.gunzipped())!
                            } else {
                                decompressedData = data!
                            }
                            
                            //let json1 = try JSONSerialization.jsonObject(with: decompressedData) as? [String: Any]
                            //print(json1 as Any)
                            
                            if let json = try! JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves) as? NSDictionary
                            {
                                
                                
                                self.TotalRecords = Int("\(json["TotalRecords"] ?? 0)")!
                                if let data11 = json["Radiologies"]
                                {
                                    if (!(data11 as AnyObject).isKind(of: NSNull.self))
                                    {
                                        let arrLabTestListTemp = NSMutableArray(array: data11 as! NSArray)
                                        
                                        let labArray:NSMutableArray = NSMutableArray()
                                        for i in 0..<arrLabTestListTemp.count
                                        {
                                            let dictemp = arrLabTestListTemp.object(at: i) as! Dictionary<AnyHashable, Any>
                                            let labDetails11 =  dictemp.nullKeyRemoval()
                                            labArray.add(labDetails11)
                                        }
                                        
                                        if labArray.count != 0
                                        {
                                            for dic in labArray
                                            {
                                                let dicTemp = NSMutableDictionary(dictionary: dic as! NSDictionary)
                                                let strUserID = dicTemp.object(forKey: "ImagingId")
                                                let arrUserID = self.arrRadiologyList.value(forKey: "ImagingId") as! NSArray
                                                if arrUserID.contains(strUserID ?? "")
                                                {
                                                    //nothing
                                                }
                                                else
                                                {
                                                    self.arrRadiologyList.add(dic)
                                                    self.arrSearch_RadiologyList.add(dic)
                                                }
                                            }
                                            self.tbllist.reloadData()
                                        }
                                    }
                                    if self.arrRadiologyList.count == 0
                                    {
                                        ISMessages.show("Error Found")
                                    }
                                }
                            }
                            else if let json2 = try! JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves) as? NSArray
                            {
                                
                                let arrLabTestListTemp = NSMutableArray(array: json2)
                                
                                let labArray:NSMutableArray = NSMutableArray()
                                for i in 0..<arrLabTestListTemp.count
                                {
                                    let dictemp = arrLabTestListTemp.object(at: i) as! Dictionary<AnyHashable, Any>
                                    let labDetails11 =  dictemp.nullKeyRemoval()
                                    labArray.add(labDetails11)
                                }
                                self.TotalRecords = labArray.count
                                if labArray.count != 0
                                {
                                    for dic in labArray
                                    {
                                        let dicTemp = NSMutableDictionary(dictionary: dic as! NSDictionary)
                                        let strUserID = dicTemp.object(forKey: "ImagingId")
                                        let arrUserID = self.arrRadiologyList.value(forKey: "ImagingId") as! NSArray
                                        if arrUserID.contains(strUserID ?? "")
                                        {
                                            //nothing
                                        }
                                        else
                                        {
                                            self.arrRadiologyList.add(dic)
                                            self.arrSearch_RadiologyList.add(dic)
                                        }
                                    }
                                    self.tbllist.reloadData()
                                }
                                if self.arrRadiologyList.count == 0
                                {
                                    ISMessages.show("Nothing Found")
                                }
                            }
                            
                            if self.arrRadiologyList.count == 0
                            {
                                self.lblComingSoon.isHidden = false
                                self.tbllist.isHidden = true
                            }
                            else
                            {
                                self.lblComingSoon.isHidden = true
                                self.tbllist.isHidden = false
                            }
                        }
                        catch let exeptionError as Error? {
                            print(exeptionError!.localizedDescription)
                            SVProgressHUD.dismiss()
                        }
                    }
                    else
                    {
                        ISMessages.show("Error Found")
                    }
                }
            }
        })
        dataTask.resume()
    }
    
    //MARK:- Api Call
    
    func getDetail(is_firsttime: String, selectid:String, selectDic:NSDictionary, isDetail:String)
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        if is_firsttime == "1" {
            SVProgressHUD.show()
        }
        let parameters:NSMutableDictionary = NSMutableDictionary()
        parameters.setValue(selectid, forKeyPath: "MedicalProductId")//medicalProductId
        
        if UserDefaults.standard.object(forKey: "selectCityid") != nil {
            let CityId = UserDefaults.standard.object(forKey: "selectCityid") as! String
            parameters.setValue(CityId, forKeyPath: "CityId")
        }
        print (parameters)
        let postData = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        
        let request = NSMutableURLRequest(url: NSURL(string: K.URL.radiologydetailbyid)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 120.0)
        request.httpMethod = "POST"
        let headers = [
            "content-type": "application/json"
        ]
        request.allHTTPHeaderFields = headers
        
        request.httpMethod = "POST"
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            SVProgressHUD.dismiss()
            if error != nil {
                print(error ?? "Error Found")
            }
            else
            {
                do {
                    // gunzip
                    let decompressedData: Data
                    if (data?.isGzipped)! {
                        decompressedData = (try! data?.gunzipped())!
                    } else {
                        decompressedData = data!
                    }
                    let json1 = try JSONSerialization.jsonObject(with: decompressedData) as? [String: Any]
                    print(json1 as Any)
                    
                    if let json = try! JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves) as? NSDictionary
                    {
                        DispatchQueue.main.async {
                            let arrdRadiologiesTemp =  NSMutableArray(array: json["Radiologies"] as! NSArray)
                            print(arrdRadiologiesTemp)
                            if arrdRadiologiesTemp.count != 0
                            {
                                let dicTemp = arrdRadiologiesTemp.object(at: 0) as! NSDictionary
                                
                                let RadiologyLabsComparison = NSMutableArray(array: dicTemp.object(forKey: "RadiologyLabsComparison") as! NSArray)
                                let Vendors = NSMutableArray(array:dicTemp.object(forKey: "Vendors") as! NSArray)
                                
                                for j in 0..<Vendors.count
                                {
                                    let dictemp1 = Vendors.object(at: j) as! Dictionary<AnyHashable, Any>
                                    let labDetails111 =  dictemp1.nullKeyRemoval()
                                    Vendors.removeObject(at: j)
                                    Vendors.insert(labDetails111, at: j)
                                }
                                
                                for j in 0..<RadiologyLabsComparison.count
                                {
                                    let dictemp1 = RadiologyLabsComparison.object(at: j) as! Dictionary<AnyHashable, Any>
                                    let labDetails111 =  dictemp1.nullKeyRemoval()
                                    RadiologyLabsComparison.removeObject(at: j)
                                    RadiologyLabsComparison.insert(labDetails111, at: j)
                                }
                                if isDetail == "1"
                                {
                                    let mutableDelectDic = NSMutableDictionary(dictionary: selectDic)
                                    mutableDelectDic.setValue(RadiologyLabsComparison, forKey: "RadiologyLabsComparison")
                                    mutableDelectDic.setValue(Vendors, forKey: "Vendors")
                                    
                                    let radiologyDetailVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "RadiologyDetailVC") as! RadiologyDetailVC
                                    radiologyDetailVC.dicSelectRadiologyTest = NSMutableDictionary(dictionary: mutableDelectDic)
                                    AppUtility.shared.mainNavController.pushViewController(radiologyDetailVC, animated: true)
                                    
                                }
                                else
                                {
                                    if UserDefaults.standard.value(forKey: "arrradiologycart") == nil
                                    {
                                        
                                        let arrLabTestCart:NSMutableArray = NSMutableArray()
                                        
                                        let mutableDelectDic = NSMutableDictionary(dictionary: selectDic)
                                        mutableDelectDic.setValue(RadiologyLabsComparison, forKey: "RadiologyLabsComparison")
                                        mutableDelectDic.setValue(Vendors, forKey: "Vendors")
                                        arrLabTestCart.add(mutableDelectDic)
                                        UserDefaults.standard.set(arrLabTestCart, forKey: "arrradiologycart")
                                        UserDefaults.standard.synchronize()
                                        
                                    }
                                    else
                                    {
                                        let arrLabTestCart:NSMutableArray = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrradiologycart") as! NSArray)
                                        
                                        let mutableDelectDic = NSMutableDictionary(dictionary: selectDic)
                                        mutableDelectDic.setValue(RadiologyLabsComparison, forKey: "RadiologyLabsComparison")
                                        mutableDelectDic.setValue(Vendors, forKey: "Vendors")
                                        arrLabTestCart.add(mutableDelectDic)
                                        UserDefaults.standard.set(arrLabTestCart, forKey: "arrradiologycart")
                                        UserDefaults.standard.synchronize()
                                    }
                                    self.onCartbuttonClicked(nil)
                                }
                            }
                        }
                    }
                }
                catch let exeptionError as Error? {
                    print(exeptionError!.localizedDescription)
                    SVProgressHUD.dismiss()
                }
            }
        })
        dataTask.resume()
    }
    
    
    func apicall(is_firsttime: String,SearchKeyWord:String)
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        if is_firsttime == "1"
        {
            SVProgressHUD.show()
        }
        else
        {
            activityIndicator?.startAnimating()
            tbllist.tableFooterView = activityIndicator
        }
        let parameters:NSMutableDictionary = NSMutableDictionary()
        parameters.setValue(page_Limit, forKeyPath: "SkipPages")
        parameters.setValue("10", forKeyPath: "Records")
        parameters.setValue(SearchKeyWord, forKeyPath: "SearchKeyWord")
        
        if UserDefaults.standard.object(forKey: "selectCityid") != nil {
            let CityId = UserDefaults.standard.object(forKey: "selectCityid") as! String
            parameters.setValue(CityId, forKeyPath: "CityId")
        }
        
        let postData = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        
        var strUrl = String()
        if SearchKeyWord.length != 0
        {
            strUrl = K.URL.GetLabImagingsBySearchKeyWord
        }
        else
        {
            strUrl = K.URL.GetAllLabImagings
        }
        strUrl = strUrl.replacing(" ", with: "")
        let request = NSMutableURLRequest(url: NSURL(string: strUrl)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 120.0)
        request.httpMethod = "POST"
        let headers = [
            "content-type": "application/json"
        ]
        request.allHTTPHeaderFields = headers
        
        request.httpMethod = "POST"
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            self.activityIndicator?.stopAnimating()
            self.tbllist.tableFooterView = nil
            SVProgressHUD.dismiss()
            if let httpResponse = response as? HTTPURLResponse {
                print("Status code: (\(httpResponse.statusCode))")
                if httpResponse.statusCode == 200
                {
                    do {
                        // gunzip
                        let decompressedData: Data
                        if (data?.isGzipped)! {
                            decompressedData = (try! data?.gunzipped())!
                        } else {
                            decompressedData = data!
                        }
                        
                        let json1 = try JSONSerialization.jsonObject(with: decompressedData) as? [String: Any]
                        print(json1 as Any)
                        
                        if let json = try! JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves) as? NSDictionary
                        {
                            DispatchQueue.main.async {
                                
                                self.TotalRecords = Int("\(json["TotalRecords"] ?? 0)")!
                                if let data11 = json["Radiologies"]
                                {
                                    if (!(data11 as AnyObject).isKind(of: NSNull.self))
                                    {
                                        let arrLabTestListTemp = NSMutableArray(array: data11 as! NSArray)
                                        
                                        let labArray:NSMutableArray = NSMutableArray()
                                        for i in 0..<arrLabTestListTemp.count
                                        {
                                            let dictemp = arrLabTestListTemp.object(at: i) as! Dictionary<AnyHashable, Any>
                                            let labDetails11 =  dictemp.nullKeyRemoval()
                                            labArray.add(labDetails11)
                                        }
                                        
                                        if labArray.count != 0
                                        {
                                            for dic in labArray
                                            {
                                                let dicTemp = NSMutableDictionary(dictionary: dic as! NSDictionary)
                                                let strUserID = dicTemp.object(forKey: "ImagingId")
                                                let arrUserID = self.arrRadiologyList.value(forKey: "ImagingId") as! NSArray
                                                if arrUserID.contains(strUserID ?? "")
                                                {
                                                    //nothing
                                                }
                                                else
                                                {
                                                    self.arrRadiologyList.add(dic)
                                                    self.arrSearch_RadiologyList.add(dic)
                                                }
                                            }
                                            self.tbllist.reloadData()
                                        }
                                    }
                                }
                                if self.arrRadiologyList.count == 0
                                {
                                    ISMessages.show("This service is not available in your current selected city")
                                }
                            }
                        }
                        else if let json2 = try! JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves) as? NSArray
                        {
                            DispatchQueue.main.async {
                                
                                self.TotalRecords = 0
                                
                                let arrLabTestListTemp = NSMutableArray(array: json2)
                                
                                let labArray:NSMutableArray = NSMutableArray()
                                for i in 0..<arrLabTestListTemp.count
                                {
                                    let dictemp = arrLabTestListTemp.object(at: i) as! Dictionary<AnyHashable, Any>
                                    let labDetails11 =  dictemp.nullKeyRemoval()
                                    labArray.add(labDetails11)
                                }
                                
                                if labArray.count != 0
                                {
                                    for dic in labArray
                                    {
                                        let dicTemp = NSMutableDictionary(dictionary: dic as! NSDictionary)
                                        let strUserID = dicTemp.object(forKey: "ImagingId")
                                        let arrUserID = self.arrRadiologyList.value(forKey: "ImagingId") as! NSArray
                                        if arrUserID.contains(strUserID ?? "")
                                        {
                                            //nothing
                                        }
                                        else
                                        {
                                            self.arrRadiologyList.add(dic)
                                            self.arrSearch_RadiologyList.add(dic)
                                        }
                                    }
                                    self.tbllist.reloadData()
                                }
                                if self.arrRadiologyList.count == 0
                                {
                                    ISMessages.show("This service is not available in your current selected city")
                                }
                            }
                        }
                    }
                    catch let exeptionError as Error? {
                        print(exeptionError!.localizedDescription)
                        SVProgressHUD.dismiss()
                    }
                }
                else
                {
                    ISMessages.show("This service is not available in your current selected city")
                }
            }
        })
        dataTask.resume()
    }
    
}

//MARK:- UITextField Delegate
extension RadiologyVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // arrRadiologyList = NSMutableArray(array: arrSearch_RadiologyList)
        // tbllist.reloadData()
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let newString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        if textField == txtSearch
        {
            if newString?.count == 0
            {
                arrRadiologyList = NSMutableArray()
                arrSearch_RadiologyList = NSMutableArray()
                page_Limit = 0
                tbllist.reloadData()
                apicall(is_firsttime: "1", SearchKeyWord: "")
                
                //                arrRadiologyList = NSMutableArray(array: arrSearch_RadiologyList)
                //                tbllist.reloadData()
            }
            else
            {
                //                var tmpary = [AnyHashable]()
                //                let predicate = NSPredicate(format: "SELF.ImagingName contains[c] %@", newString!)
                //                tmpary = arrSearch_RadiologyList.filtered(using: predicate) as! [AnyHashable]
                //                arrRadiologyList = NSMutableArray(array: tmpary)
                //                tbllist.reloadData()
            }
        }
        return true
    }
}



//MARK:- Tableview Delegate Method
extension RadiologyVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrRadiologyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let viewmain = cell.contentView.viewWithTag(1)
        let lbltitle = cell.contentView.viewWithTag(2) as! UILabel
        let lblrecommended = cell.contentView.viewWithTag(3) as! UILabel
        let lblCategory = cell.contentView.viewWithTag(4) as! UILabel
        let lblLocation = cell.contentView.viewWithTag(5) as! UILabel
        let lblSubLocation = cell.contentView.viewWithTag(6) as! UILabel
        let lblType = cell.contentView.viewWithTag(7) as! UILabel
        
        let viewbtnaddtocart = cell.contentView.viewWithTag(8)
        let btnaddtocart = cell.contentView.viewWithTag(9)
        
        let viewbtndetail = cell.contentView.viewWithTag(10)
        //let btndetail = cell.contentView.viewWithTag(11)
        
        
        var radiology = NSMutableDictionary(dictionary: arrRadiologyList[indexPath.row] as! NSDictionary)
        
        var dicradiologytemp = Dictionary<AnyHashable, Any>()
        dicradiologytemp = radiology as! [AnyHashable : Any]
        radiology = NSMutableDictionary(dictionary: dicradiologytemp.nullKeyRemoval() as NSDictionary)
        
        
        lbltitle.text = (radiology.object(forKey: "ImagingName") as! String)
        lblCategory.text = (radiology.object(forKey: "ImagingCategoryName") as! String)
        
        if let BodyLocationName =  radiology.object(forKey: "BodyLocationName") {
            lblLocation.text = (BodyLocationName as! String)
        }
        else
        {
            lblLocation.text = ""
        }
        
        
        if let BodyLocationPartName =  radiology.object(forKey: "BodyLocationPartName") {
            lblSubLocation.text = (BodyLocationPartName as! String)
        }
        else
        {
            lblSubLocation.text = ""
        }
        
        if let BodyLocationPartTypeName =  radiology.object(forKey: "BodyLocationPartTypeName") {
            lblType.text = (BodyLocationPartTypeName as! String)
        }
        else
        {
            lblType.text = ""
        }
        
        
        Common.SetShadowwithCorneronView(view: viewmain!, cornerradius: 10.0, color:UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.setCornerOnView(view: lblrecommended, cornerradius: 4.0)
        Common.SetShadowwithCorneronView(view: viewbtnaddtocart!, cornerradius: 10.0, color:UIColor.clear)
        Common.SetShadowwithCorneronView(view: viewbtndetail!, cornerradius: 10.0, color:UIColor.clear)
        
        /* btnaddtocart?.addTapGesture { (gesture) in
         //AppUtility.shared.addProductToCart(type: K.Key.CartLab, product: labDetails)
         // AppUtility.shared.addProductToRadiology(item: radiology as! Radiology)
         if UserDefaults.standard.value(forKey: "arrradiologycart") == nil
         {
         //var labDetails1 = Dictionary<AnyHashable, Any>()
         //labDetails1 = radiology as! [AnyHashable : Any]
         //let labDetails11 =  labDetails1.nullKeyRemoval()
         
         let arrLabTestCart:NSMutableArray = NSMutableArray()
         arrLabTestCart.add(radiology)
         UserDefaults.standard.set(arrLabTestCart, forKey: "arrradiologycart")
         UserDefaults.standard.synchronize()
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
         self.onCartbuttonClicked(nil)
         })
         }
         else
         {
         //var arrLabTestCart = NSArray()
         let arrLabTestCart:NSMutableArray = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrradiologycart") as! NSArray)
         var istrue = "yes"
         for i in 0..<arrLabTestCart.count
         {
         let dicLab = arrLabTestCart[i] as! NSDictionary
         let labidtemp = dicLab.object(forKey: "ImagingId") as! Int
         let labidtemp11 = radiology.object(forKey: "ImagingId") as! Int
         
         if labidtemp11 == labidtemp
         {
         istrue = "no"
         }
         }
         if istrue == "yes"
         {
         //                    var labDetails1 = Dictionary<AnyHashable, Any>()
         //                    labDetails1 = radiology as! [AnyHashable : Any]
         //                    let labDetails11 =  labDetails1.nullKeyRemoval()
         //
         arrLabTestCart.add(radiology)
         UserDefaults.standard.set(arrLabTestCart, forKey: "arrradiologycart")
         UserDefaults.standard.synchronize()
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
         self.onCartbuttonClicked(nil)
         })
         }
         else
         {
         ISMessages.show("Product Already Exits in Cart")
         }
         }
         }*/
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let radiology = NSMutableDictionary(dictionary: arrRadiologyList[(indexPath.row)] as! NSDictionary)
        
        let selectLabId = "\(radiology.object(forKey: "ImagingId") ?? 0)"
        getDetail(is_firsttime: "1", selectid: selectLabId, selectDic: radiology as NSDictionary, isDetail: "1")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
        
    }
}


