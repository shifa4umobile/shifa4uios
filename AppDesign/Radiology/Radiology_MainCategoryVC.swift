//
//  Radiology_MainCategoryVC.swift
//  AppDesign
//
//  Created by Bilal Iqbal on 23/10/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD


class Radiology_MainCategoryVC: UIViewController {
    
    var arrCategoryList = NSArray()
    
    
    
    @IBOutlet weak var collectionList: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCategory()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getCategory()
    {
        
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        SVProgressHUD.show()
        
        let url = URL(string: K.URL.GetAllCategories)
        let request = URLRequest(url: url!)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                // gunzip
                let decompressedData: Data
                if (data?.isGzipped)! {
                    decompressedData = (try! data?.gunzipped())!
                } else {
                    decompressedData = data!
                }
                
                let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
                
                if let arrData = dictionary as? NSArray {
                    print(dictionary!)
                    self.arrCategoryList = NSArray(array: arrData)
                    self.collectionList.reloadData()
                }
                else {
                    ISMessages.show("Category not found")
                }
            }
        });
        task.resume()
    }
    
    
    @IBAction func btnBack(_ sender: Any) {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
}

extension Radiology_MainCategoryVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCategoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_row", for: indexPath)
        
        let viewbg = cell.contentView.viewWithTag(101)
        
        Common.SetShadowwithCorneronView(view: viewbg!, cornerradius: 10.0, color: UIColor.lightGray)
        
        let imgCatIcon = cell.contentView.viewWithTag(102) as! UIImageView
        
        let lblCatName = cell.contentView.viewWithTag(103) as! UILabel
        lblCatName.text = (self.arrCategoryList.object(at: indexPath.row) as! NSDictionary).object(forKey: "Name") as! String
        if indexPath.row == 0 {
            imgCatIcon.image = #imageLiteral(resourceName: "X-Ray")
            
        }else if indexPath.row == 1{
            imgCatIcon.image = #imageLiteral(resourceName: "Ultrasound")
            
        }else if indexPath.row == 2{
            imgCatIcon.image = #imageLiteral(resourceName: "CT Scan")
            
        }else if indexPath.row == 3{
            imgCatIcon.image = #imageLiteral(resourceName: "MRI")
            
        }else if indexPath.row == 4{
            imgCatIcon.image = #imageLiteral(resourceName: "CardiacImaging")
            
        }else if indexPath.row == 5{
            imgCatIcon.image = #imageLiteral(resourceName: "Mammography")
            
        }else if indexPath.row == 6{
            imgCatIcon.image = #imageLiteral(resourceName: "dexascan")
            
        }else if indexPath.row == 7{
            imgCatIcon.image = #imageLiteral(resourceName: "fluoroscopy")
            
        }else if indexPath.row == 8{
            imgCatIcon.image = #imageLiteral(resourceName: "Stress Test")
            
        }else if indexPath.row == 9{
            imgCatIcon.image = #imageLiteral(resourceName: "Nuclear-Medicine")
            
        }
        
        return cell
        
    }
}

extension Radiology_MainCategoryVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width / 2
        //let height = 120 //width * 0.92
        return CGSize(width: width, height: CGFloat(140))
    }
}

extension Radiology_MainCategoryVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let radiologyVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "RadiologyVC") as! RadiologyVC
        radiologyVC.Categoryid = (self.arrCategoryList.object(at: indexPath.row) as! NSDictionary).object(forKey: "ImagingCategoryId")
            as! Int
        radiologyVC.SelectCategoryName = (self.arrCategoryList.object(at: indexPath.row) as! NSDictionary).object(forKey: "Name")
            as! String
        AppUtility.shared.mainNavController.pushViewController(radiologyVC, animated: true)
    }
}


