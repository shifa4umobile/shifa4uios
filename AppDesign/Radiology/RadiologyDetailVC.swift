//
//  RadiologyDetailVC.swift
//  AppDesign
//
//  Created by sachin on 20/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class RadiologyDetailVC: BaseViewController
{
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var viewdetail: UIView!
    @IBOutlet weak var viewpric: UIView!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var lbldescription: UILabel!
    @IBOutlet weak var lblimaginename: UILabel!
    @IBOutlet weak var lblcategory: UILabel!
    @IBOutlet weak var lbllocation: UILabel!
    @IBOutlet weak var lblsublocation: UILabel!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var vieworder: UIView!
    @IBOutlet weak var btnchooselab: UIButton!
    
    let chooseArticleDropDown = DropDown()
    var radiology: Radiology!
    var selectedLab: RadiologyLab? = nil
    
    var dicSelectRadiologyTest = NSMutableDictionary()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown
        ]
    }()
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
        
        Common.SetShadowwithCorneronView(view: viewdetail, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: vieworder, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: viewpric, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        
        lbltitle.text = (dicSelectRadiologyTest.object(forKey: "ImagingName") as! String)
        lblsubtitle.text = (dicSelectRadiologyTest.object(forKey: "ImagingName") as! String)
        lbldescription.text = (dicSelectRadiologyTest.object(forKey: "ShortDescription") as! String)
        lblimaginename.attributedText = Common.setattributetextColor(strtext:"Imaging Name : \((dicSelectRadiologyTest.object(forKey: "ImagingName") as! String))",rangeString:"Imaging Name :")
        lblcategory.attributedText = Common.setattributetextColor(strtext:"Category : \((dicSelectRadiologyTest.object(forKey: "ImagingCategoryName") as! String))",rangeString:"Category :")
        if let BodyLocationName =  dicSelectRadiologyTest.object(forKey: "BodyLocationName") {
            lbllocation.attributedText = Common.setattributetextColor(strtext:"Location : \((dicSelectRadiologyTest.object(forKey: "BodyLocationName") as! String))",rangeString:"Location :")
        }
        else
        {
            
        }
    
        if let BodyLocationPartName =  dicSelectRadiologyTest.object(forKey: "BodyLocationPartName") {
            lblsublocation.attributedText = Common.setattributetextColor(strtext:"Sub Location : \((dicSelectRadiologyTest.object(forKey: "BodyLocationPartName") as! String))",rangeString:"Sub Location :")
        }
        else
        {
            
        }

//        lbllocation.attributedText = Common.setattributetextColor(strtext:"Location : \((dicSelectRadiologyTest.object(forKey: "BodyLocationName") as! String))",rangeString:"Location :")
//        lblsublocation.attributedText = Common.setattributetextColor(strtext:"Sub Location : \((dicSelectRadiologyTest.object(forKey: "BodyLocationPartName") as! String))",rangeString:"Sub Location :")
        
        btnchooselab.setTitle("Select Lab", for: .normal)
        lblprice.text = "0"
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: - IBAction
    @IBAction func btnback(_ sender: Any)
    {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    
    @IBAction func btnchooselab(_ sender: Any)
    {
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        
        let arrLabsTemp = NSMutableArray()
        for i in 0..<(self.dicSelectRadiologyTest.object(forKey: "RadiologyLabsComparison") as! NSArray).count
        {
            let labname = ((self.dicSelectRadiologyTest.object(forKey: "RadiologyLabsComparison") as! NSArray).object(at: i) as! NSDictionary).object(forKey: "LabName")
            arrLabsTemp.add(labname as Any)
        }
        
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            
            //self?.selectedLab = self?.radiology.arrLabs[index]
            lblNotificationText?.setTitle(item, for: .normal)
            
            let arr = self?.dicSelectRadiologyTest.object(forKey: "RadiologyLabsComparison") as! NSMutableArray
            let dic = arr.object(at: index) as! NSDictionary
            self?.lblprice.text = NSString(format: "Rs. %@",Utility.setCurrencyFormate(NSString(format: "%d",dic.object(forKey: "YourPrice") as! Int) as String)) as String
            
            for i in 0..<arr.count
            {
                let dictemp = NSMutableDictionary(dictionary: arr.object(at: i) as! NSDictionary)
                if i == index
                {
                    dictemp.setValue("1", forKey: "isselect")
                }
                else
                {
                    dictemp.setValue("0", forKey: "isselect")
                }
                arr.removeObject(at: i)
                arr.insert(dictemp, at: i)
            }
            self?.dicSelectRadiologyTest.setValue(arr, forKey: "RadiologyLabsComparison")
            
            self?.chooseArticleDropDown.hide()
            
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            //            if items.isEmpty {
            //                lblNotificationText?.setTitle("", for: .normal)
            //            }
        }
        
        chooseArticleDropDown.show()
    }
    
    @IBAction func btnorder(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "arrradiologycart") == nil
        {
            let arrLabTestCart:NSMutableArray = NSMutableArray()
            arrLabTestCart.add(dicSelectRadiologyTest)
            UserDefaults.standard.set(arrLabTestCart, forKey: "arrradiologycart")
            UserDefaults.standard.synchronize()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                self.onCartbuttonClicked(nil)
            })
        }
        else
        {
            //var arrLabTestCart = NSArray()
            let arrLabTestCart:NSMutableArray = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrradiologycart") as! NSArray)
            var istrue = "yes"
            for i in 0..<arrLabTestCart.count
            {
                let dicLab = arrLabTestCart[i] as! NSDictionary
                let labidtemp = dicLab.object(forKey: "ImagingId") as! Int
                let labidtemp11 = dicSelectRadiologyTest.object(forKey: "ImagingId") as! Int
                
                if labidtemp11 == labidtemp
                {
                    istrue = "no"
                }
            }
            if istrue == "yes"
            {
                arrLabTestCart.add(dicSelectRadiologyTest)
                UserDefaults.standard.set(arrLabTestCart, forKey: "arrradiologycart")
                UserDefaults.standard.synchronize()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    self.onCartbuttonClicked(nil)
                })
            }
            else
            {
                ISMessages.show("Product Already Exits in Cart")
            }
        }
    }
    
}
