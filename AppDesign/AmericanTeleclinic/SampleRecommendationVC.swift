//
//  SampleRecommendationVC.swift
//  AppDesign
//
//  Created by UDHC on 17/08/2018.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class SampleRecommendationVC: UIViewController , UITableViewDataSource , UITableViewDelegate {

    @IBOutlet weak var MyVideoView: UIView!
    @IBOutlet weak var MyViewForPDF: UIView!
    @IBOutlet weak var MyTableView: UITableView!
    
    let kHeaderSectionTag: Int = 6900;
    
    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Common.SetShadowwithCorneronView(view: MyViewForPDF, cornerradius: 10.0, color: UIColor(red: 254.0/255.0, green: 87.0/255.0, blue: 32.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: MyViewForPDF, cornerradius: 10.0, color: UIColor(red: 254.0/255.0, green: 87.0/255.0, blue: 32.0/255.0, alpha: 1.0))
        
        Common.SetShadowwithCorneronView(view: MyVideoView, cornerradius: 10.0, color: UIColor(red: 237.0/255.0, green: 200.0/255.0, blue: 4.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: MyVideoView, cornerradius: 10.0, color: UIColor(red: 237.0/255.0, green: 200.0/255.0, blue: 4.0/255.0, alpha: 1.0))
        // Do any additional setup after loading the view.
        sectionNames = ["Can I access my 2nd opinion recommendation online??",
                         "Will Shifa4U provide transcripts and Urdu translation?",
                         "Who can view my data?"
        ];
        sectionItems = [["Yes, your recommendations are available in your account and can be accessed from anywhere in the world. Your account is secured to ensure only you may access your data using your account ID and password."],
                         ["Yes, transcripts are provided with every recommendation. An Urdu translation will be provided upon request free of charge. Moreover, you are welcome to consult Shifa4U physician to discuss your case after consultation."],
                         ["Your data is securely stored on Shifa4U servers and is visible to your account ID only. We encourage you to keep your account ID and password secure and not share with anyone you don’t trust."],
            
        ];
        self.MyTableView!.tableFooterView = UIView()
        self.MyTableView.backgroundColor = UIColor.clear
        
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    @IBAction func btnsend(_ sender: Any) {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    
    @IBAction func PDFViewer(_ sender: Any) {
        if let url = NSURL(string: "https://www.shifa4u.com/Content/images/doctors/sample-rec.pdf") {
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sectionNames.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            let arrayOfItems = self.sectionItems[section] as! NSArray
            return arrayOfItems.count;
        } else {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sectionNames[section] as? String
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

             return 42

    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //recast your view as a UITableViewHeaderFooterView
        
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor.white
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 12.5)
        header.textLabel?.textColor = K.Color.blue
        Common.SetShadowwithCorneronView(view: header, cornerradius: 1.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
            viewWithTag.removeFromSuperview()
        }
        let headerFrame = self.view.frame.size
        let theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 20, y: 20, width: 14, height: 14));
        theImageView.image = UIImage(named: "DropdownBlue")
        theImageView.tag = kHeaderSectionTag + section
        header.addSubview(theImageView)
        
        // make headers touchable
        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(FaqVC.sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as UITableViewCell
        let section = self.sectionItems[indexPath.section] as! NSArray
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor.black
          cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.textLabel?.text = section[indexPath.row] as? String
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.lineBreakMode = .byWordWrapping
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Expand / Collapse Methods
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section, imageView: eImageView!)
            } else {
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
                tableViewExpandSection(section, imageView: eImageView!)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.MyTableView!.beginUpdates()
            self.MyTableView!.deleteRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.MyTableView!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.MyTableView!.beginUpdates()
            self.MyTableView!.insertRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.MyTableView!.endUpdates()
        }
    }
    


}
