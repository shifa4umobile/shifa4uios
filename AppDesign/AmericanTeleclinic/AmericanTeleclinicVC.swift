//
//  AmericanTeleclinicVC.swift
//  AppDesign
//
//  Created by Bilal Iqbal on 26/07/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class AmericanTeleclinicVC: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var mytable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mytable.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnsend(_ sender: Any) {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }

 var temp = ["What is TeleMedicine" , "How it works" , "Meet our Providers" , "Pricing" , "Frequently asked Questions" , "Sample Recommendation" , "Submit Your Request"]
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (temp.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = mytable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if (indexPath.row == 6 )
        {
            cell.textLabel?.textColor = UIColor.red
        }
        
        cell.textLabel?.text = temp[indexPath.row]
        cell.accessoryType = .disclosureIndicator

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
         if (temp[indexPath.row] == "What is TeleMedicine")
        
        {
            let telemedicine = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "TeleMedicineVC") as! TeleMedicineVC
            AppUtility.shared.mainNavController.pushViewController(telemedicine, animated: true)
        }
         else if (temp[indexPath.row] == "How it works")
            
        {
        let howItWorks = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "HowItWorksVC") as! HowItWorksVC
        AppUtility.shared.mainNavController.pushViewController(howItWorks, animated: true)
        }
         else if (temp[indexPath.row] == "Meet our Providers")
            
        {
            let meetProvidor = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "MeetOurProvidorVC") as! MeetOurProvidorVC
            AppUtility.shared.mainNavController.pushViewController(meetProvidor, animated: true)
        }
        else if (temp[indexPath.row] == "Pricing")
            
        {
            let pricevc = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "PricingVC") as! PricingVC
            AppUtility.shared.mainNavController.pushViewController(pricevc, animated: true)
        }
         else if (temp[indexPath.row] == "Frequently asked Questions")
            
        {
            let faq = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "FaqVC") as! FaqVC
            AppUtility.shared.mainNavController.pushViewController(faq, animated: true)
        }
         else if (temp[indexPath.row] == "Sample Recommendation")
            
        {
            let sampleVc = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "SampleRecommendationVC") as! SampleRecommendationVC
            AppUtility.shared.mainNavController.pushViewController(sampleVc, animated: true)
            
        }
         else if (temp[indexPath.row] == "Submit Your Request")
            
        {
            let request = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "RequestViewController") as! RequestViewController
            AppUtility.shared.mainNavController.pushViewController(request, animated: true)
        }
        else
        {
            
        }
    
    }
    
    
}


