//
//  MeetOurProvidorVC.swift
//  AppDesign
//
//  Created by UDHC on 15/08/2018.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//rogressHUD
import EzPopup

class MeetOurProvidorVC: BaseViewController , UITableViewDataSource , UITableViewDelegate {
    
    @IBOutlet weak var SearchField: UITextField!
    @IBOutlet weak var btnSearchonFilter: UIView!
    @IBOutlet weak var specialitybtn: UIButton!
    @IBOutlet weak var genderbtn: UIButton!
    @IBOutlet weak var CategoryView: UIView!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mytextView: UITextView!
    @IBOutlet weak var viewsearch: UIView!
    
    var arrProvidorsList = NSArray()
    var arrSearch_PackageList:NSMutableArray = NSMutableArray()
    var arrBranchesList = NSArray()
    
    var Gender : String = ""
    var GenderId : Int = 0
    var SpecialityId : Int = 0
    
    
    
    var activityIndicator: UIActivityIndicatorView?
    
    let chooseArticleDropDown = DropDown()
    //var arrRadiologyList: Array<Radiology> = []
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown
        ]
    }()
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }

    var arrSpeechList:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.mytextView.scrollRangeToVisible(NSMakeRange(0, 0))
        self.getPhysicians()
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        
        if UserDefaults.standard.object(forKey: "arrProvidorsList") != nil
        {
            self.arrProvidorsList = NSMutableArray(array: UserDefaults.standard.object(forKey: "arrProvidorsList") as! NSArray)
            self.arrSearch_PackageList = NSMutableArray(array:UserDefaults.standard.object(forKey: "arrProvidorsList") as! NSArray)
            
            self.tableView.reloadData()
            
            if self.arrProvidorsList.count != 0
            {
                GetProvidors(is_firsttime: "")
            }
            else
            {
                GetProvidors(is_firsttime: "1")
            }
        }
        else
        {
            GetProvidors(is_firsttime: "1")
        }
        
        Common.SetShadowwithCorneronView(view: viewsearch, cornerradius: 1.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: viewsearch, cornerradius: 10.0, color: UIColor.clear)
        
        
        Common.SetShadowwithCorneronView(view: CategoryView, cornerradius: 1.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: CategoryView, cornerradius: 10.0, color: UIColor.clear)

        Common.SetShadowwithCorneronView(view: btnSearchonFilter, cornerradius: 10.0, color: UIColor.clear)
        tableView.tableFooterView = UIView()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnsend(_ sender: Any) {
        
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    
    @IBAction func textfieldchangeediting(_ sender: UITextField) {
        setData()
        
    }
    @IBAction func btnviewProfile(_ sender: Any) {
        
    }
    
    @IBAction func btnSubmittRequest(_ sender: Any) {
        
    }
    @IBAction func btnsearch(_ sender: Any)
    {
        tableView.reloadData()
        self.getSearchResut(is_firsttime: "1")
    }
    @IBAction func btnGender(_ sender: Any)
    {
    
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        chooseArticleDropDown.dataSource = ["Male" , "Female" , "All"]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            lblNotificationText?.setTitle(item, for: .normal)
            self?.Gender = item
            if self?.Gender == "Male"
                {
                  self?.GenderId = 10
                 }
            else if self?.Gender == "Female"
                {
                  self?.GenderId = 11
                 }
            else if self?.Gender == "All"
            {
                self?.GenderId = 0
            }
            self?.specialitybtn.setTitle("Select Speciality", for: .normal)
            self?.SpecialityId = 0
            self?.chooseArticleDropDown.hide()
        }
        
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            //self?.getLocation()
            
            if items.isEmpty {
                lblNotificationText?.setTitle("", for: .normal)
                
            }
        }
        
        chooseArticleDropDown.show()
        
    }
    @IBAction func btnSpeciality(_ sender: Any)
    {
        let arrLabsTemp = NSMutableArray()
        for i in 0..<arrBranchesList.count
        {
            let labname = (arrBranchesList.object(at: i) as! NSDictionary).object(forKey: "Name")
            arrLabsTemp.add(labname as Any)
        }
        
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            lblNotificationText?.setTitle(item, for: .normal)
            self?.SpecialityId = (self?.arrBranchesList.object(at: index) as! NSDictionary).object(forKey: "MedicalBranchId")
                as! Int
            self?.chooseArticleDropDown.hide()
        }
        
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            //self?.getLocation()
            
            if items.isEmpty {
                lblNotificationText?.setTitle("", for: .normal)
                
            }
        }
        
        chooseArticleDropDown.show()
    }
    
    func setData() {
        tableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrProvidorsList.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let mainview = cell.contentView.viewWithTag(1)
        var imgmain = cell.contentView.viewWithTag(2) as! UIImageView
        let lbltitle = cell.contentView.viewWithTag(3) as! UILabel
        let lblprice = cell.contentView.viewWithTag(4) as! UILabel
        let lblothername = cell.contentView.viewWithTag(5) as! UILabel
        let btnorder = cell.contentView.viewWithTag(6) as! UIView
        let btnordernew = cell.contentView.viewWithTag(7) as! UIButton
        let submittView = cell.contentView.viewWithTag(8) as! UIView
        let btnsubmitt = cell.contentView.viewWithTag(9) as! UIButton
      Common.SetShadowwithCorneronView(view: mainview!, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
         Common.SetShadowwithCorneronView(view: imgmain, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
         Common.SetShadowwithCorneronView(view: btnorder, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: btnordernew, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: submittView, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: btnsubmitt, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        imgmain.layer.borderWidth = 2
        imgmain.layer.masksToBounds = false
        imgmain.layer.cornerRadius = imgmain.frame.size.height/2
        imgmain.clipsToBounds = true
    
        let homeCare = NSMutableDictionary(dictionary: arrProvidorsList[indexPath.row] as! NSDictionary)
        lbltitle.text = (homeCare.object(forKey: "PhysicianName") as! String)
        lblprice.text = (homeCare.object(forKey: "Specialist") as! String).htmlToString
        lblothername.text = (homeCare.object(forKey: "CurrentPracticeLocation") as! String)
        
        imgmain.image = #imageLiteral(resourceName: "bannerPlaceholder")
        
        AppUtility.shared.getImage(imageID: (homeCare.object(forKey: "ImageWebId") as! Int) , completion: { (success, error, image) in
            if success == true {
                if image != nil {
                    imgmain.image = image
                }
            }
        })
        btnordernew.addTapGesture { (gesture) in
            let screenSize: CGRect = UIScreen.main.bounds
            let screenWidth = screenSize.width
            let customAlertVC = CustomAlertViewController.instantiate()
            customAlertVC?.titleString = (homeCare.object(forKey: "PhysicianName") as! String)
            customAlertVC?.messageString = (homeCare.object(forKey: "ProfileDetail") as! String).htmlToString
            let popupVC = PopupViewController(contentController: customAlertVC!, popupWidth: screenWidth)
            popupVC.cornerRadius = 5
            self.present(popupVC, animated: true, completion: nil)
            
        }
        btnsubmitt.addTapGesture { (gesture) in
            let screenSize: CGRect = UIScreen.main.bounds
            let screenWidth = screenSize.width
        
            let SubmittVC = submitRequestVC.instantiate()
            SubmittVC?.physicianString = (homeCare.object(forKey: "PhysicianName") as! String)
            SubmittVC?.PhysicianId = 13
            SubmittVC?.SpecialityId = 35
            SubmittVC?.ProductLineID = 3378
            SubmittVC?.specialityString = (homeCare.object(forKey: "Specialist") as! String)
            let popupVC = PopupViewController(contentController: SubmittVC!, popupWidth: screenWidth)
            popupVC.cornerRadius = 5
            self.present(popupVC, animated: true, completion: nil)
            
        }
        

            return cell
        }
        
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 150
    }
    func getPhysicians()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        let parameters = ["": ""]
        
        let url = URL(string: K.URL.GetAllBranches)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            SVProgressHUD.dismiss()
            // gunzip
            let decompressedData: Data
            if (data?.isGzipped)! {
                decompressedData = (try! data?.gunzipped())!
            } else {
                decompressedData = data!
            }
            
            let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
            
            if let arrData = dictionary as? NSArray {
                print(dictionary!)
                self.arrBranchesList = NSArray(array: arrData)
            }
            else {
                ISMessages.show("Branches not found")
            }
        });
        task.resume()
        
    }
    
    
    
    func GetProvidors(is_firsttime: String)
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        if is_firsttime == "1" {
            SVProgressHUD.show()
        }
        do
        {
        let parameters = [" ": " "]
        
        let url = URL(string: K.URL.GetAllProvidors)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
        
            
            if error != nil {
                print(error ?? "Error Found")
                
            }
            else
            {
                do
                {
            // gunzip
            let decompressedData: Data
            if (data?.isGzipped)! {
                decompressedData = (try! data?.gunzipped())!
            } else {
                decompressedData = data!
            }
            
            let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
            
            if let arrData = dictionary as? NSArray {
                print(dictionary!)
                
                DispatchQueue.main.async {
                    self.arrProvidorsList = NSArray(array: arrData)
                    self.arrSearch_PackageList = NSMutableArray(array: arrData)
                    self.tableView.reloadData()
                    //UserDefaults.standard.set(self.arrProvidorsList, forKey: "arrProvidorsList")
                    //UserDefaults.standard.set(self.arrSearch_PackageList, forKey: "arrProvidorsList")
                    //UserDefaults.standard.synchronize()
                }
            }
            else {
                ISMessages.show("Providor not found")
            }
                }
            SVProgressHUD.dismiss()
            }
        });
        task.resume()
        
    }
    
}
    func getSearchResut(is_firsttime: String)
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        if is_firsttime == "1" {
            SVProgressHUD.show()
        }
        do
        {
            
            let parameters = ["GenderProfileId": "\(GenderId)" , "MedicalBranchId": "\(SpecialityId)"]
            print(parameters)
            
            let url = URL(string: K.URL.GetAllProvidors)
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
            request.httpBody = httpBody
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            
            let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
                
                
                if error != nil {
                    print(error ?? "Error Found")
                    
                }
                else
                {
                    do
                    {
                        // gunzip
                        let decompressedData: Data
                        if (data?.isGzipped)! {
                            decompressedData = (try! data?.gunzipped())!
                        } else {
                            decompressedData = data!
                        }
                        
                        let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
                        
                        if let arrData = dictionary as? NSArray {
                            print(dictionary!)
                            
                            DispatchQueue.main.async {
                                self.arrProvidorsList = NSArray(array: arrData)
                                if(self.arrProvidorsList.count == 0)
                                {
                                    ISMessages.show("Nothing found")
                                    self.tableView.reloadData()

                                }
                                else
                                {
                                   self.tableView.reloadData()
                                }
                            }
                        }
                        else {
                            ISMessages.show("Providor not found")
                        }
                    }
                    SVProgressHUD.dismiss()
                }
            });
            task.resume()
            
        }
        
    }
}

extension MeetOurProvidorVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }

    func textFieldDidEndEditing(_ textField: UITextField) {

    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let newString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        if textField == SearchField
        {
            if newString?.count == 0
            {
                arrProvidorsList = NSMutableArray(array: arrSearch_PackageList)
                tableView.reloadData()
            }
            else
            {
                var tmpary = [AnyHashable]()
                let predicate = NSPredicate(format: "SELF.Name contains[c] %@", newString!)
                tmpary = arrSearch_PackageList.filtered(using: predicate) as! [AnyHashable]
                arrProvidorsList = NSMutableArray(array: tmpary)
                tableView.reloadData()
            }
        }
        return true
    }
}
