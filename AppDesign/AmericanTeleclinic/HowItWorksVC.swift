//
//  HowItWorksVC.swift
//  
//
//  Created by UDHC on 10/09/2018.
//

import UIKit

class HowItWorksVC: UIViewController , UITableViewDataSource , UITableViewDelegate{

    
    @IBOutlet weak var MytextView: UITextView!
    @IBOutlet weak var myTable: UITableView!
    
    let kHeaderSectionTag: Int = 6900;
    
    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
          self.MytextView.scrollRangeToVisible(NSMakeRange(0, 0))
        
        sectionNames = ["Step-by-step guide",
                         "What is included in 2nd Opinion Service?",
                         "After consultation support"
                         
        ];
        sectionItems = [[""],
            ["You submit your case accompanied by medical documents, Shifa4U physician team reviews your case and if required, request feature tests/radiology. Once completed, your case is forwarded to the board of American TelePhysicians group and referred to the relevant American consultant physicians. The consultant reviews the case and provides thorough consultation in written as well as video format."],
                         ["Similar to offline consultation, you submit your case and after the review of Shifa4U physicians and board of American TelePhysicians, an online appointment with relevant American consultant physician is scheduled to be taken place at Shifa4U facility or Shifa4U partnered facility. You are assisted by a Shifa4U local physician during your appointment to overcome any language barriers and ensure thorough understanding of queries and recommendations without anything being lost in transition. Thorough recommendation is also provided in written form after consultation."]
            
        ];
        self.myTable!.tableFooterView = UIView()
        self.myTable.backgroundColor = UIColor.clear
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Tableview Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sectionNames.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            let arrayOfItems = self.sectionItems[section] as! NSArray
            return arrayOfItems.count;
        } else {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sectionNames[section] as? String
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 40
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //recast your view as a UITableViewHeaderFooterView
        
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor.white
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 12.5)
        header.textLabel?.textColor = K.Color.blue
        Common.SetShadowwithCorneronView(view: header, cornerradius: 1.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
            viewWithTag.removeFromSuperview()
        }
        let headerFrame = self.view.frame.size
        let theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 20, y: 20, width: 14, height: 14));
        theImageView.image = UIImage(named: "DropdownBlue")
        theImageView.tag = kHeaderSectionTag + section
        header.addSubview(theImageView)
        
        // make headers touchable
        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(FaqVC.sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as UITableViewCell
        
        
        let section = self.sectionItems[indexPath.section] as! NSArray
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor.black
          cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.textLabel?.text = section[indexPath.row] as? String
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.lineBreakMode = .byWordWrapping
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Expand / Collapse Methods
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section, imageView: eImageView!)
            } else {
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
                tableViewExpandSection(section, imageView: eImageView!)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.myTable!.beginUpdates()
            self.myTable!.deleteRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.myTable!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.myTable!.beginUpdates()
            self.myTable!.insertRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.myTable!.endUpdates()
        }
    }
    
    @IBAction func btnsend(_ sender: Any) {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    
}
