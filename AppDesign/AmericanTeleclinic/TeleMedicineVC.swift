//
//  TeleMedicineVC.swift
//  AppDesign
//
//  Created by UDHC on 06/08/2018.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class TeleMedicineVC: UIViewController {

    @IBOutlet weak var textview: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textview.scrollRangeToVisible(NSMakeRange(0, 0))

    }
    
    @IBAction func btnsend(_ sender: Any) {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
