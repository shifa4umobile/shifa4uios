//
//  RequestViewController.swift
//  
//
//  Created by UDHC on 10/09/2018.
//

import UIKit
import SVProgressHUD

class RequestViewController: UIViewController {

    @IBOutlet weak var SecondryView: UIView!
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var RequestBtn: UIButton!
    @IBOutlet weak var MyTextView: UITextView!
    @IBOutlet weak var PhysicianButton: UIButton!
    @IBOutlet weak var SpecialityButton: UIButton!
    @IBOutlet weak var MyName: UITextField!
    @IBOutlet weak var MyNumber: UITextField!
    
    var arrProvidorsList = NSArray()
    var arrBranchesList = NSArray()
    
    var strName:String = ""
    var strmobile:String = ""
    var strTextView:String = ""
    
     var SpecialityId : Int = 0
     var PhysicianId : Int = 0
     var ProductLineID : Int = 0
    
     var Name: Array<Any> = []
     var ContactNumber: Array<Any> = []
     var TextView: Array<Any> = []
    
    var activityIndicator: UIActivityIndicatorView?
    
    let chooseArticleDropDown = DropDown()
    //var arrRadiologyList: Array<Radiology> = []
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown
        ]
    }()
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //MyNumber.delegate = self
        Common.SetShadowwithCorneronView(view: MainView, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: RequestBtn, cornerradius: 10.0, color: UIColor.clear)
        
        Common.setCornerOnView(view: SecondryView, cornerradius: 10.0)
        
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        self.getPhysicians()
        
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        let maxLength = 16
//        let currentString: NSString = textField.text! as NSString
//        let newString: NSString =
//            currentString.replacingCharacters(in: range, with: string) as NSString
//        return newString.length <= maxLength
//    }
    
    
    @IBAction func btnPhysician(_ sender: Any)
        {
            let arrLabsTemp = NSMutableArray()
            for i in 0..<arrProvidorsList.count
            {
                let labname = (arrProvidorsList.object(at: i) as! NSDictionary).object(forKey: "PhysicianName")
                arrLabsTemp.add(labname as Any)
            }
            
            let lblNotificationText = sender as? UIButton
            chooseArticleDropDown.anchorView = lblNotificationText
            chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
            chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
            chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
                lblNotificationText?.setTitle(item, for: .normal)
                self?.PhysicianId = (self?.arrProvidorsList.object(at: index) as! NSDictionary).object(forKey: "PhysicianId") as! Int
                self?.ProductLineID = (self?.arrProvidorsList.object(at: index) as! NSDictionary).object(forKey: "ProductLineId") as! Int
                self?.chooseArticleDropDown.hide()
            }
            
            chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
                print("Muti selection action called with: \(items)")
                //self?.getLocation()
                
                if items.isEmpty {
                    lblNotificationText?.setTitle("", for: .normal)
                    
                }
            }
            
            chooseArticleDropDown.show()
        }
    @IBAction func btnSpecialist(_ sender: Any)
    {
        let arrLabsTemp = NSMutableArray()
        for i in 0..<arrBranchesList.count
        {
            let labname = (arrBranchesList.object(at: i) as! NSDictionary).object(forKey: "Name")
            arrLabsTemp.add(labname as Any)
        }
        
        let lblNotificationText = sender as? UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            lblNotificationText?.setTitle(item, for: .normal)
            self?.SpecialityId = (self?.arrBranchesList.object(at: index) as! NSDictionary).object(forKey: "MedicalBranchId")
                as! Int
            self?.PhysicianButton.setTitle("Select Physician", for: .normal)
            self?.PhysicianId = 0
            self?.chooseArticleDropDown.hide()
        }
        
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            self?.GetProvidors(is_firsttime: "")
            
            if items.isEmpty {
                lblNotificationText?.setTitle("", for: .normal)
                
            }
        
        }
        
        chooseArticleDropDown.show()
        
    }
    @IBAction func btnRequestSubmitt(_ sender: Any)
    {
        if MyName.text?.isValid == false
        {
            ISMessages.show("Enter Name")
            return;
        }
        else if MyNumber.text?.isValid == false {
            ISMessages.show("Enter Contact Number")
            return;
        }
        else if MyTextView.text?.isValid == false {
            ISMessages.show("Enter the Medical Issues")
            return;
        }
        else if SpecialityButton.titleLabel?.text == "Select Speciality" {
            ISMessages.show("Select Speciality")
            return;
        }
        else if PhysicianButton.titleLabel?.text == "Select Physician" {
            ISMessages.show("Select Physician")
            return;
        }
        
        self.apiCall_uploadprescription()
    }
    
    
    @IBAction func btnsend(_ sender: Any) {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    func GetProvidors(is_firsttime: String)
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        if is_firsttime == "1" {
            SVProgressHUD.show()
        }
        do
        {
            let parameters = ["MedicalBranchId": "\(self.SpecialityId) "]
            
            let url = URL(string: K.URL.GetAllProvidors)
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
            request.httpBody = httpBody
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            
            let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
                
                
                if error != nil {
                    print(error ?? "Error Found")
                    
                }
                else
                {
                    do
                    {
                        // gunzip
                        let decompressedData: Data
                        if (data?.isGzipped)! {
                            decompressedData = (try! data?.gunzipped())!
                        } else {
                            decompressedData = data!
                        }
                        
                        let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
                        
                        if let arrData = dictionary as? NSArray {
                            print(dictionary!)
                                self.arrProvidorsList = NSArray(array: arrData)
                    
                        }
                        else {
                            ISMessages.show("Providor not found")
                        }
                    }
                    SVProgressHUD.dismiss()
                }
            });
            task.resume()
            
        }

}
    func getPhysicians()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        let parameters = ["": ""]
        
        let url = URL(string: K.URL.GetAllBranches)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            SVProgressHUD.dismiss()
            // gunzip
            let decompressedData: Data
            if (data?.isGzipped)! {
                decompressedData = (try! data?.gunzipped())!
            } else {
                decompressedData = data!
            }
            
            let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)
            
            if let arrData = dictionary as? NSArray {
                print(dictionary!)
                self.arrBranchesList = NSArray(array: arrData)
            }
            else {
                ISMessages.show("Branches not found")
            }
        });
        task.resume()
        
    }
    
    func apiCall_uploadprescription()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        SVProgressHUD.show()
        
        if User.loggedInUser() == nil
        {
            ISMessages.show("Login for Submitt request")
            let loginVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            loginVC.is_comefromcartvc = "1"
            SVProgressHUD.dismiss()
            AppUtility.shared.mainNavController.pushViewController(loginVC, animated: true)
            return;
        }
        else
        {
        let token = UserDefaults.standard.object(forKey: "token") as! String
        let headers = [
            "content-type": "application/json",
            "authorization": "Bearer \(token)"
        ]
        strName = MyName.text!
        strmobile = MyNumber.text!
        strTextView = MyTextView.text
        
        let parameters = ["ContactNumber": "\(strmobile)", "ProductLineId" : "\(ProductLineID)" , "PatientName": "\(strName)" , "MedicalIssues": "\(strTextView)" , "MedicalSpecialityId" : "\(SpecialityId)" , "PhysicianId": "\(PhysicianId)" ,  ]
        
        print(parameters)
        print(token)
        
        
        let url = URL(string: K.URL.SubmittRequest)
        var request = URLRequest(url: url!)
        request.allHTTPHeaderFields = headers
        request.httpMethod = "POST"

        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            
            if error != nil {
                print(error ?? "Error Found")
            }
            else
            {
         let httpResponse = response as? HTTPURLResponse
        print("Status code: (\(String(describing: httpResponse?.statusCode)))")
        SVProgressHUD.dismiss()
        ISMessages.show("Sent successfully!")
               
            }
        });
        task.resume()
        }
        self.MyTextView.text = nil
        self.MyName.text = nil
        self.MyNumber.text = nil
        self.SpecialityButton.setTitle("Select Speciality", for: .normal)
        self.PhysicianButton.setTitle("Select Physician", for: .normal)
        self.SpecialityId = 0
        self.PhysicianId = 0
        
}
}
