//
//  PricingVC.swift
//  AppDesign
//
//  Created by UDHC on 17/08/2018.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class PricingVC: UIViewController , UITableViewDelegate , UITableViewDataSource{
    

    @IBOutlet weak var MyTableView: UITableView!
    
    @IBOutlet weak var MyView1: UIView!
    @IBOutlet weak var MyView2: UIView!
    @IBOutlet weak var MyView3: UIView!
    
    
    let kHeaderSectionTag: Int = 6900;
    
    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MyTableView.tableFooterView = UIView()
        Common.SetShadowwithCorneronView(view: MyView1, cornerradius: 1.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: MyView1, cornerradius: 10.0, color: UIColor(red: 0/255, green: 40/255, blue: 104/255, alpha: 1))

        Common.SetShadowwithCorneronView(view: MyView2, cornerradius: 1.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: MyView2, cornerradius: 10.0, color: UIColor(red: 0/255, green: 40/255, blue: 104/255, alpha: 1))

        Common.SetShadowwithCorneronView(view: MyView3, cornerradius: 1.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: MyView3, cornerradius: 10.0, color: UIColor(red: 0/255, green: 40/255, blue: 104/255, alpha: 1))
        
        sectionNames = ["Payment modes",
                        "Refunds",
                        "Exchange rates"
        ];
        sectionItems = [["Payment modes include","On-site payment","All major credit cards",
            "Telenor Easypaisa"],
                        ["We will do every effort to meet your expectations. But, if for some reason you are not satisfied with your case we will refund your fee after deducting a very nominal administration fee."],
                        ["The American TeleClinic services are charged in US Dollars, hence the interbank rate on the date of payment will be charged in case the user may want to pay in Pak Rupees. For online payments your bank may charge a different exchange rate, Kindly confirm with your bank before making the payment. With easypaisa, the vendor may charge a minimal fee for transaction.."],
                        
        ];
        self.MyTableView.backgroundColor = UIColor.clear
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnsend(_ sender: Any) {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sectionNames.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            let arrayOfItems = self.sectionItems[section] as! NSArray
            return arrayOfItems.count;
        } else {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sectionNames[section] as? String
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 40
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //recast your view as a UITableViewHeaderFooterView
        
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor.white
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 12.5)
        header.textLabel?.textColor = K.Color.blue
        Common.SetShadowwithCorneronView(view: header, cornerradius: 1.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
            viewWithTag.removeFromSuperview()
        }
        let headerFrame = self.view.frame.size
        let theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 20, y: 20, width: 14, height: 14));
        theImageView.image = UIImage(named: "DropdownBlue")
        theImageView.tag = kHeaderSectionTag + section
        header.addSubview(theImageView)
        
        // make headers touchable
        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(FaqVC.sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as UITableViewCell
        let section = self.sectionItems[indexPath.section] as! NSArray
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor.black
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.textLabel?.text = section[indexPath.row] as? String
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.lineBreakMode = .byWordWrapping
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Expand / Collapse Methods
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section, imageView: eImageView!)
            } else {
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
                tableViewExpandSection(section, imageView: eImageView!)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.MyTableView!.beginUpdates()
            self.MyTableView!.deleteRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.MyTableView!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.MyTableView!.beginUpdates()
            self.MyTableView!.insertRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.MyTableView!.endUpdates()
        }
    }
}
