//
//  FaqVC.swift
//  AppDesign
//
//  Created by UDHC on 28/08/2018.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class FaqVC: UIViewController , UITableViewDataSource , UITableViewDelegate{

   
    @IBOutlet weak var tableView: UITableView!
    let kHeaderSectionTag: Int = 6900;
    
    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sectionNames = ["How are American TeleClinic doctors different from US qualified doctors working in              Pakistan?",
                         "I don’t speak English, how will I communicate with American doctors?",
                        "Who can view the information I share with Shifa4U/American TeleClinic team?",
                        "How long does it usually take to get an opinion?",
                        "Who decides which consultant will provide   my 2nd Opinion service?",
                        "What if I have questions regarding the 2nd opinion service provided to me?",
                        "How can I be sure that the offline 2nd opinion is actually being provided by an American specialist?",
                        "What is the refund policy?",
                     "I am not in Pakistan, can I still avail your 2nd Opinion service?",
                     "What are the payment modes?",
                     "Why do I need American TeleClinic 2nd Opinion service?"
        ];
        sectionItems = [
                         ["American TeleClinic doctors are actively practicing in US/UK with current and up to date qualification to treat and diagnose patients with complex medical conditions. American TeleClinic doctors can also guide you and facilitate the process of getting you advance and cutting-edge treatment if you choose to travel to these destinations for your medical care."],
                         ["Shifa4U Medical Officer will be taking your detailed history and doing clinical examination. If you feel you cannot effectively communicate with American doctors then the Shifa4U Medical Officer can interpret and convey your concerns to the American doctors in a more effective way."],
                         ["As an international organization, we take the confidentiality of your information very seriously. Only designated and credentialed shifa4U/American TeleClinic staff members will be allowed to access your information after your permission. Your information will not be shared with non Shifa4U/American TeleClinic staff without your written consent."],
                         ["We understand the frustration of waiting especially when you are diagnosed with any health condition. At Shifa4U/American TeleClinic we work effectively to get your questions answered in a timely fashion. For most second opinion cases we are able to provide recommendations on your case within 4-5 business days but it can sometime vary depending on the complexity of the case."],
                         ["You have an option to decide which consultant will review your case. American TeleClinic physicians along with their brief profiles are listed on our website to help you make this decision. You can also ask our Shifa4U Medical Officer who can always guide you in making an informed decision."],
                         ["We will always strive to make your experience satisfying and free of any confusion. If you feel you have questions regarding the 2nd opinion you have the option of using our follow up appointment at no charge to ask those questions."],
                         ["Your American Specialist will also be providing a brief video with the recommendations. The video will be uploaded to your electronic medical record. You can always request a copy of the video if you want to discuss your case with any other consultant."],
                         ["We will do every effort to meet your expectations. But, if for some reason you are not satisfied with your case we will refund your fee after deducting a very nominal administration fee."],
                         ["Yes, even if you are not in Pakistan you can take the advantage of our 2nd Opinion service and take care of your loved ones back in Pakistan by using our shifa4U portal."],
                         ["Payment modes include:",
                          "On-site payment",
                          "All major credit cards",
                          "Telenor Easypaisa"],
                         ["There are a number of reasons you might consider seeking American TeleClinic second opinion service. If you are not comfortable with the doctor’s level of experience treating your problem or feel that your physician is not providing enough information for you to make an informed decision. Some of the common reasons for which patient seek second opinion include but not limited to:",
                          "If you are diagnosed with a chronic illness and want to confirm the proposed treatment plan.",
                          "If you are offered more than one treatment alternative and does not know which one to choose.",
                          "If you have multiple medical problems that places you at greater risk of suffering complications as a result of surgery or other medical treatment."],
            
        ];
        self.tableView!.tableFooterView = UIView()
        self.tableView.backgroundColor = UIColor.clear

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Tableview Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {

            return sectionNames.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            let arrayOfItems = self.sectionItems[section] as! NSArray
            return arrayOfItems.count;
        } else {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
            return self.sectionNames[section] as? String
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 55
    
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //recast your view as a UITableViewHeaderFooterView
        
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor.white
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 12.5)
        header.textLabel?.textColor = K.Color.blue
        Common.SetShadowwithCorneronView(view: header, cornerradius: 1.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        
        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
            viewWithTag.removeFromSuperview()
        }
        let headerFrame = self.view.frame.size
        let theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 20, y: 20, width: 14, height: 14));
        theImageView.image = UIImage(named: "DropdownBlue")
        theImageView.tag = kHeaderSectionTag + section
        header.addSubview(theImageView)
        
        // make headers touchable
        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
       headerTapGesture.addTarget(self, action: #selector(FaqVC.sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as UITableViewCell
        

        let section = self.sectionItems[indexPath.section] as! NSArray
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.textLabel?.textColor = UIColor.black
        cell.textLabel?.text = section[indexPath.row] as? String
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.lineBreakMode = .byWordWrapping
       
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Expand / Collapse Methods
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section, imageView: eImageView!)
            } else {
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
                tableViewExpandSection(section, imageView: eImageView!)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.tableView!.beginUpdates()
            self.tableView!.deleteRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.tableView!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.tableView!.beginUpdates()
            self.tableView!.insertRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.tableView!.endUpdates()
        }
    }
    
    @IBAction func btnsend(_ sender: Any) {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }

}
