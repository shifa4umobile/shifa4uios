//
//  submitRequestVC.swift
//  AppDesign
//
//  Created by UDHC on 06/08/2018.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import SVProgressHUD

class submitRequestVC: UIViewController{


    
    @IBOutlet weak var mycontactview: UIView!
    @IBOutlet weak var MytextView: UITextView!
    @IBOutlet weak var contactnumber: UITextField!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var medicalissue: UIView!
    @IBOutlet weak var btnSubmittNew: UIButton!

    @IBOutlet weak var speciality: UITextField!
    @IBOutlet weak var physician: UITextField!
    
    var specialityString: String?
    var physicianString: String?
    var SpecialityId: Int = 0
    var ProductLineID: Int = 0
    var PhysicianId : Int = 0
    
    
    var strName:String = ""
    var strmobile:String = ""
    var strTextView:String = ""

    
    
    static func instantiate() -> submitRequestVC? {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "\(submitRequestVC.self)") as? submitRequestVC
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //contactnumber.delegate = self
        //name.delegate = self
        speciality.text = specialityString
        physician.text = physicianString


        Common.SetShadowwithCorneronView(view: mycontactview, cornerradius: 10.0, color: UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0))
        Common.SetShadowwithCorneronView(view: btnSubmittNew, cornerradius: 10.0, color: UIColor.clear)
        
        Common.setCornerOnView(view: medicalissue, cornerradius: 10.0)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        let maxLength = 16
//        let currentString: NSString = textField.text! as NSString
//        let newString: NSString =
//            currentString.replacingCharacters(in: range, with: string) as NSString
//        return newString.length <= maxLength
//    }
    
    
    @IBAction func btnsend(_ sender: Any) {
       dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnsubmit(_ sender: Any)
    {
        
        if name.text?.isValid == false
        {
            ISMessages.show("Enter Name")
            return;
        }
        else if contactnumber.text?.isValid == false {
            ISMessages.show("Enter Contact Number")
            return;
        }
        else if (contactnumber.text?.length)! < 11 {
            ISMessages.show("Enter Valid Contact Number")
            return;
        }
        else if MytextView.text?.isValid == false {
            ISMessages.show("Enter the Medical Issues")
            return;
        }
        
        self.apiCall_uploadprescription()
    }
    
    
    func apiCall_uploadprescription()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        SVProgressHUD.show()
        
        if User.loggedInUser() == nil
        {
            ISMessages.show("Login for Submitt request")
            let loginVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            loginVC.is_comefromcartvc = "1"
            SVProgressHUD.dismiss()
            dismiss(animated: true, completion: nil)
            AppUtility.shared.mainNavController.pushViewController(loginVC, animated: true)
            return;
        }
        else
        {
            let token = UserDefaults.standard.object(forKey: "token") as! String
            let headers = [
                "content-type": "application/json",
                "authorization": "Bearer \(token)"
            ]
        strName = name.text!
        strmobile = contactnumber.text!
        strTextView = MytextView.text
        
        let parameters = ["ContactNumber": "\(strmobile)", "ProductLineId" : "\(ProductLineID)" , "PatientName": "\(strName)" , "MedicalIssues": "\(strTextView)" , "MedicalSpecialityId" : "\(SpecialityId)" , "PhysicianId": "\(PhysicianId)"]
            
            
            print("HERE are")
            print(parameters)
        
        let url = URL(string: K.URL.SubmittRequest)
        var request = URLRequest(url: url!)
        request.allHTTPHeaderFields = headers
        request.httpMethod = "POST"
        
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            
            if error != nil {
                print(error ?? "Error Found")
            }
            else
            {
                let httpResponse = response as? HTTPURLResponse
                print("Status code: (\(String(describing: httpResponse?.statusCode)))")
                SVProgressHUD.dismiss()
                ISMessages.show("Sent successfully!")
            }
        });
        task.resume()
       self.MytextView.text = nil
       self.contactnumber.text = nil
       self.name.text = nil
        }
    }

}
