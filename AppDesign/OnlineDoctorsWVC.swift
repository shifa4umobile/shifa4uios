//
//  OnlineDoctorsWVC.swift
//  AppDesign
//
//  Created by UDHC on 30/10/2018.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class OnlineDoctorsWVC: UIViewController , MobileRTCMeetingServiceDelegate {
    
    var rtc_userid = "";
    var rtc_username = "";
    var rtc_token = "";
    var rtc_meeting = "335756756";

    @IBOutlet weak var SubVIew: UIView!
    @IBOutlet weak var MainView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Common.SetShadowwithCorneronView(view: MainView, cornerradius: 10.0, color: UIColor.lightGray)
        Common.SetShadowwithCorneronView(view: SubVIew, cornerradius: 10.0, color: UIColor.lightGray)
        // Do any additional setup after loading the view.
        let when = DispatchTime.now() + 15
        SVProgressHUD.show()
        DispatchQueue.main.asyncAfter(deadline: when){
            let service = MobileRTC.shared().getMeetingService();
            if (service == nil){
                return;
            }
            
            service?.delegate = self;
            
            let dic = [kMeetingParam_Username: self.rtc_username,
                       kMeetingParam_MeetingNumber: self.rtc_meeting,
                       ];
            
            let ret = service?.joinMeeting(with: dic);
            print("join meeting ret: \(ret)");
        }
        SVProgressHUD.dismiss()
    }

}
