//
//  MedicinesPopupVC.swift
//  AppDesign
//
//  Created by UDHC on 11/26/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class MedicinesPopupVC: BaseViewController {

    @IBOutlet weak var SecondView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var MainVIew: UIView!
    @IBOutlet weak var SendBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        Common.SetShadowwithCorneronView(view: MainVIew, cornerradius: 10.0, color: UIColor.gray)
        Common.SetShadowwithCorneronView(view: SecondView, cornerradius: 10.0, color: UIColor.gray)
        Common.SetShadowwithCorneronView(view: SendBtn, cornerradius: 10.0, color: UIColor.clear)
    
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func SendBtn(_ sender: Any) {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        else{
            SVProgressHUD.show()
          if textView.text?.isValid == false {
            ISMessages.show("Enter the Medicines names")
            return;
        }
        else
          {
            ISMessages.show("Send successfully")
            textView.text = nil
            SVProgressHUD.dismiss()
            return;
          }
            
        }
    }
    
    @IBAction func BackBtn(_ sender: Any) {
        dismiss(animated: true , completion: nil)
    }
}
