//
//  OrderPlacedVC.swift
//  AppDesign
//
//  Created by sachin on 16/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class OrderPlacedVC: UIViewController
{
    @IBOutlet weak var btntrackOrder: UIButton!
    @IBOutlet weak var lblordernumber: UILabel!
    
    var dicOrderFinal = NSMutableDictionary()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Common.SetShadowwithCorneronView(view: btntrackOrder, cornerradius: 10.0, color: UIColor.clear)
        let OrderId = dicOrderFinal.object(forKey: "OrderId")
        lblordernumber.text = NSString(format: "Your Order # %@", OrderId as Any as! String) as String
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnback(_ sender: Any)
    {
        AppUtility.sharedAppDelegate.setMainNavigationController()
    }
    
    @IBAction func btntrackorder(_ sender: Any)
    {
        AppUtility.sharedAppDelegate.setMainNavigationController()
    }
}
