//
//  LaunchingSoonVC.swift
//  AppDesign
//
//  Created by UDHC on 12/5/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class LaunchingSoonVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func back(_ sender: Any) {
        AppUtility.shared.mainNavController.popViewController(animated: true)
    }
}
