//
//  RegisterViewController.swift
//  AppDesign
//
//  Created by Mahavir Makwana on 31/05/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import ZSWTappableLabel

class RegisterViewController: UIViewController, ZSWTappableLabelTapDelegate, UITextFieldDelegate {

    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var txtBirthday: UITextField!
    @IBOutlet weak var lblTerms: ZSWTappableLabel!

    var dictUserData: Dictionary<String,Any> = [:]
    let arrGender = ["Male","Female"]

    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if dictUserData.keys.count > 0 {
            setData()
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }


    //MARK: - IBActions
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        //AppUtility.shared.loginNavController.popViewController(animated: true)
    }

    @IBAction func btnSignupSubmitTapped(_ sender: UIButton) {
        let email = isValidEmail(testStr: txtEmail.text!)
        if txtFirstName.text?.isValid == false {
            view.endEditing(true)
            ISMessages.show(K.Message.enterFirstName)
        }
        else if txtLastName.text?.isValid == false {
            view.endEditing(true)
            ISMessages.show(K.Message.enterLastName)
        }
        else if txtEmail.text?.isValid == false {
            view.endEditing(true)
            ISMessages.show(K.Message.enterEmail)
        }
        else if email == false {
            view.endEditing(true)
            ISMessages.show(K.Message.enterValidEmail)
        }
        else if txtPassword.text?.isValid == false {
            view.endEditing(true)
            ISMessages.show(K.Message.enterPassword)
        }
        else if (txtPassword.text?.length)! < 6
        {
            view.endEditing(true)
            ISMessages.show(K.Message.enterminimumsixdigitpwd)
        }
        else if txtConfirmPassword.text?.isValid == false {
            view.endEditing(true)
            ISMessages.show(K.Message.enterConfirmPassword)
        }
        else if txtPassword.text != txtConfirmPassword.text {
            view.endEditing(true)
            ISMessages.show(K.Message.passwordDoesNotMatch)
        }
        else if txtGender.text?.isValid == false {
            view.endEditing(true)
            ISMessages.show(K.Message.selectGender)
        }
        else if txtBirthday.text?.isValid == false {
            view.endEditing(true)
            ISMessages.show(K.Message.selectBirthday)
        }
        else {
            dictUserData[K.Key.FirstName]  = txtFirstName.text?.trimmed()
            dictUserData[K.Key.LastName]   = txtLastName.text?.trimmed()
            dictUserData[K.Key.Email]      = txtEmail.text?.trimmed()
            dictUserData[K.Key.Password]   = txtPassword.text?.trimmed()
            dictUserData[K.Key.Gender]     = txtGender.text?.trimmed()
            dictUserData[K.Key.Birthday]   = txtBirthday.text?.trimmed()
            dictUserData[K.Key.LoginName]   = txtEmail.text?.trimmed()
            
            let destViewController : RegisterSubmitViewController = self.storyboard!.instantiateViewController(withIdentifier: "RegisterSubmitViewController") as! RegisterSubmitViewController
            destViewController.dictUserData = dictUserData
            let topViewController : UIViewController = self.navigationController!.topViewController!
            if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
                print("Same VC")
            } else {
                self.navigationController!.pushViewController(destViewController, animated: true)
            }
            
//            let registerVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "RegisterSubmitViewController") as! RegisterSubmitViewController
//            registerVC.dictUserData = dictUserData
//            AppUtility.shared.loginNavController.pushViewController(registerVC, animated: true)
        }
    }

    @IBAction func btnTermsAndPrivacyTapped(_ sender: UIButton) {
        let registerVC = AppUtility.STORY_BOARD().instantiateViewController(withIdentifier: "TermsAndConditoinVC") as! TermsAndConditoinVC
        AppUtility.shared.loginNavController.pushViewController(registerVC, animated: true)
    }



    //MARK: - Other Actions
    func initialSetup() {
        setupActiveTappableLabel()
    }

    func showCountryPicker(arrList: Array<String>, textField: UITextField) {
        var selectedIndex = 0
        if txtGender.text?.isValid == true {
            let indexes = arrList.filter({$0 == txtGender.text})
            if indexes.count > 0 {
                selectedIndex = 0
            }
        }
        RMPickerViewController.show(withTitle: nil, selectedIndex:selectedIndex, array: arrList, doneBlock: { (index, value) in
            textField.text = value!
        }, cancel: {

        })
    }

    func showDatePicker() {
        var selectedDate = Date(timeIntervalSinceReferenceDate: 0)
        if txtBirthday.text?.isValid == true {
            let date = Date(fromString: (txtBirthday.text?.trimmed())!, format: K.DateFormat.dateFormatMMDDYYY)
            if date != nil {
                selectedDate = date!
            }
        }

        let selectAction = RMAction<UIDatePicker>(title: "Select", style: RMActionStyle.done) { controller in
            print("Successfully selected date: ", controller.contentView.date)

            self.txtBirthday.text = controller.contentView.date.toString(format: K.DateFormat.dateFormatMMDDYYY)
        }

        let cancelAction = RMAction<UIDatePicker>(title: "Cancel", style: RMActionStyle.cancel) { _ in
            print("Date selection was canceled")
        }

        let actionController = RMDateSelectionViewController(style: .white, title: "Test", message: "This is a test message.\nPlease choose a date and press 'Select' or 'Cancel'.", select: selectAction, andCancel: cancelAction)!;

        //You can access the actual UIDatePicker via the datePicker property
        actionController.datePicker.datePickerMode = .date
        //actionController.datePicker.minuteInterval = 5
        actionController.datePicker.maximumDate = Date()
        actionController.datePicker.date = selectedDate

        present(actionController, animated: true, completion: nil)
    }

    func setData() {
        if let firstName = dictUserData[K.Key.FirstName] as? String {
            txtFirstName.text = firstName
        }
        if let lastName = dictUserData[K.Key.LastName] as? String {
            txtLastName.text = lastName
        }
        if let email = dictUserData[K.Key.Email] as? String {
            txtEmail.text = email
        }
        if let password = dictUserData[K.Key.Password] as? String {
            txtPassword.text = password
            txtConfirmPassword.text = password
        }
        if let gender = dictUserData[K.Key.Gender] as? String {
            txtGender.text = gender
        }
        if let birthday = dictUserData[K.Key.Birthday] as? String {
            txtBirthday.text = birthday
        }

    }


    // MARK: - ZSWTappableLabelTap
    func setupActiveTappableLabel() {
        if let attributedText = lblTerms.attributedText?.mutableCopy() as? NSMutableAttributedString {
            let range1 = (attributedText.string as NSString).range(of: K.TappableValueType.TermsAndCondition)
            let range2 = (attributedText.string as NSString).range(of: K.TappableValueType.Privacy)

            if (range1.location != NSNotFound) || (range2.location != NSNotFound) {
                attributedText.addAttributes([
                    .tappableRegion: true,
                    .tappableHighlightedBackgroundColor: UIColor.clear,
                    NSAttributedStringKey(rawValue: "title"): K.TappableTitleType.TermsAndCondition
                    ], range: range1)
                attributedText.addAttributes([
                    .tappableRegion: true,
                    .tappableHighlightedBackgroundColor: UIColor.clear,
                    NSAttributedStringKey(rawValue: "title"): K.TappableTitleType.Privacy
                    ], range: range2)
            }
            lblTerms.attributedText = attributedText
        }
    }


    func tappableLabel(_ tappableLabel: ZSWTappableLabel, tappedAt idx: Int, withAttributes attributes: [NSAttributedStringKey : Any]) {
        switch attributes[NSAttributedStringKey("title")] as? String {
        case K.TappableTitleType.TermsAndCondition:
            AppUtility.redirectToTermsAndCondition(from: self, type: K.TappableValueType.TermsAndCondition, isTerms: true)
            break
        case K.TappableTitleType.Privacy:
            AppUtility.redirectToTermsAndCondition(from: self, type: K.TappableValueType.Privacy, isTerms: false)
            break
        default:
            break
        }
        //print("index: \(idx)")
    }

    //MARK: - UITextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtGender {
            self.view.endEditing(true)
            showCountryPicker(arrList: arrGender, textField: textField)
            return false
        }
        else if textField == txtBirthday {
            self.view.endEditing(true)
            showDatePicker()
            return false
        }
        return true
    }

    //MARK: - Webservice


}
