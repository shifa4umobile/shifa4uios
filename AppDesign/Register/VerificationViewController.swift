//
//  VerificationViewController.swift
//  AppDesign
//
//  Created by Mahavir Makwana on 02/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit

class VerificationViewController: UIViewController {

    @IBOutlet weak var lblDescription: UILabel!


    var dictUserData: Dictionary<String,Any> = [:]
    
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }


    //MARK: - IBActions
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        //AppUtility.shared.loginNavController.popViewController(animated: true)
    }

    @IBAction func btnReSendCodeTapped(_ sender: UIButton) {

    }

    @IBAction func btnLoginTapped(_ sender: UIButton) {
        let destViewController : LoginViewController = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        let topViewController : UIViewController = self.navigationController!.topViewController!
        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
            print("Same VC")
        } else {
            self.navigationController!.pushViewController(destViewController, animated: true)
        }
    }


    //MARK: - Other Actions
    func initialSetup() {
        lblDescription.text = "Congratulation! your account is created you can log in using  your email now."
    }


    //MARK: - UITextField Delegate


    //MARK: - Webservice

}
