//
//  RegisterSubmitViewController.swift
//  AppDesign
//
//  Created by Mahavir Makwana on 01/06/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
//import ZSWTappableLabel
//import SVProgressHUD

class RegisterSubmitViewController: UIViewController, ZSWTappableLabelTapDelegate
{

    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtArea: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var lblTerms: ZSWTappableLabel!

    var dictUserData: Dictionary<String,Any> = [:]

    let chooseArticleDropDown = DropDown()

    var arrCityList = NSArray()
    var cityID: Int = -1

    var arrAreaList = NSArray()
    var areaID: Int = -1

    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        getCity()
    }

    //MARK: - IBActions

    @IBAction func btnSelectCity(_ sender: Any)
    {
        let lblNotificationText = sender as! UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText.bounds.height))

        let arrLabsTemp = NSMutableArray()
        for i in 0..<arrCityList.count
        {
            let labname = (arrCityList.object(at: i) as! NSDictionary).object(forKey: "CityName")
            arrLabsTemp.add(labname as Any)
        }

        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in

            //lblNotificationText.setTitle(item, for: .normal)

            if let value = (self?.arrCityList.object(at: index) as! NSDictionary).object(forKey: "CityName") as? String {
                self?.cityID = (self?.arrCityList.object(at: index) as! NSDictionary).object(forKey: "CityId") as! Int
                self?.txtCity.text = value
            }
            self?.getArea()
            self?.chooseArticleDropDown.hide()
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
        }
        chooseArticleDropDown.show()
    }

    @IBAction func btnSelectArea(_ sender: Any)
    {
        let lblNotificationText = sender as! UIButton
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText.bounds.height))

        let arrLabsTemp = NSMutableArray()
        for i in 0..<arrAreaList.count
        {
            let labname = (arrAreaList.object(at: i) as! NSDictionary).object(forKey: "AreaName")
            arrLabsTemp.add(labname as Any)
        }

        chooseArticleDropDown.dataSource = arrLabsTemp as! [String]
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in

            //lblNotificationText.setTitle(item, for: .normal)

            if let value = (self?.arrAreaList.object(at: index) as! NSDictionary).object(forKey: "AreaName") as? String {
                self?.areaID = (self?.arrAreaList.object(at: index) as! NSDictionary).object(forKey: "AreaId") as! Int
                self?.txtArea.text = value
            }
            self?.chooseArticleDropDown.hide()
        }
        chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
        }
        chooseArticleDropDown.show()
    }

    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        //AppUtility.shared.loginNavController.popViewController(animated: true)
    }

    @IBAction func btnSignupTapped(_ sender: UIButton) {
        if txtCity.text?.isValid == true {
            //dictUserData[K.Key.City] = txtCity.text?.trimmed()
        }
        if txtArea.text?.isValid == true {
            UserDefaults.standard.set(self.txtCity.text, forKey: K.Key.City)
            UserDefaults.standard.synchronize()

            //dictUserData[K.Key.AreaID] = txtArea.text?.trimmed()
        }
        if txtAddress.text?.isValid == true
        {
            dictUserData[K.Key.Address] = txtAddress.text?.trimmed()
        }
        else
        {
            dictUserData[K.Key.Address] = ""
        }
        if txtMobileNumber.text?.isValid == true
        {
            dictUserData[K.Key.PhoneNumber] = txtMobileNumber.text?.trimmed()
        }
        else
        {
            dictUserData[K.Key.PhoneNumber] = ""
        }
        registerWebservice()
    }

    @IBAction func btnSkipTapped(_ sender: UIButton) {
        if txtCity.text?.isValid == true {
            //dictUserData[K.Key.City] = txtCity.text?.trimmed()
        }
        if txtArea.text?.isValid == true {
            UserDefaults.standard.set(self.txtCity.text, forKey: K.Key.City)
            UserDefaults.standard.synchronize()

            //dictUserData[K.Key.AreaID] = txtArea.text?.trimmed()
        }
        if txtAddress.text?.isValid == true {
            dictUserData[K.Key.Address] = txtAddress.text?.trimmed()
        }
        if txtMobileNumber.text?.isValid == true {
            dictUserData[K.Key.PhoneNumber] = txtMobileNumber.text?.trimmed()
        }
        registerWebservice()
    }


    //MARK: - Other Actions
    func initialSetup() {
        if (UserDefaults.standard.object(forKey: "selectCityid") != nil)
        {
            cityID = Int(UserDefaults.standard.object(forKey: "selectCityid") as! String)!
            let val = UserDefaults.standard.value(forKey: K.Key.City) as? String
            self.txtCity.text = val
        }

        dictUserData[K.Key.City] = ""
        dictUserData[K.Key.AreaID] = ""
        dictUserData[K.Key.Address] = ""
        dictUserData[K.Key.PhoneNumber] = ""
       // dictUserData[K.Key.LoginName] = ""
        setupActiveTappableLabel()
    }


    // MARK: - ZSWTappableLabelTap
    func setupActiveTappableLabel() {
        if let attributedText = lblTerms.attributedText?.mutableCopy() as? NSMutableAttributedString {
            let range1 = (attributedText.string as NSString).range(of: K.TappableValueType.TermsAndCondition)
            let range2 = (attributedText.string as NSString).range(of: K.TappableValueType.Privacy)

            if (range1.location != NSNotFound) || (range2.location != NSNotFound) {
                attributedText.addAttributes([
                    .tappableRegion: true,
                    .tappableHighlightedBackgroundColor: UIColor.clear,
                    NSAttributedStringKey(rawValue: "title"): K.TappableTitleType.TermsAndCondition
                    ], range: range1)
                attributedText.addAttributes([
                    .tappableRegion: true,
                    .tappableHighlightedBackgroundColor: UIColor.clear,
                    NSAttributedStringKey(rawValue: "title"): K.TappableTitleType.Privacy
                    ], range: range2)
            }
            lblTerms.attributedText = attributedText
        }
    }


    func tappableLabel(_ tappableLabel: ZSWTappableLabel, tappedAt idx: Int, withAttributes attributes: [NSAttributedStringKey : Any]) {
        switch attributes[NSAttributedStringKey("title")] as? String {
        case K.TappableTitleType.TermsAndCondition:
            AppUtility.redirectToTermsAndCondition(from: self, type: K.TappableValueType.TermsAndCondition, isTerms: true)
            break
        case K.TappableTitleType.Privacy:
            AppUtility.redirectToTermsAndCondition(from: self, type: K.TappableValueType.Privacy, isTerms: false)
            break
        default:
            break
        }
        //print("index: \(idx)")
    }

    //MARK: - UITextField Delegate


    //MARK: - Webservice
    func getCity()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        SVProgressHUD.show()
        let  selectcountryid = UserDefaults.standard.object(forKey: "selectcountryid")
        let url = URL(string: K.URL.GET_CITY + "countryId=\(selectcountryid ?? "")")
        let request = URLRequest(url: url!)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)

        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            SVProgressHUD.dismiss()
            // gunzip
            let decompressedData: Data
            if (data?.isGzipped)! {
                decompressedData = (try! data?.gunzipped())!
            } else {
                decompressedData = data!
            }

            let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)

            if let arrData = dictionary as? NSArray {
                print(dictionary!)
                self.arrCityList = NSArray(array: arrData)
            }
            else {
                ISMessages.show("City not found, try again later")
            }
            self.getArea()
        });
        task.resume()
    }
    func getArea()
    {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        SVProgressHUD.show()

        let url = URL(string: K.URL.GET_AREA + "\(self.cityID)")
        let request = URLRequest(url: url!)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)

        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            SVProgressHUD.dismiss()
            // gunzip
            let decompressedData: Data
            if (data?.isGzipped)! {
                decompressedData = (try! data?.gunzipped())!
            } else {
                decompressedData = data!
            }

            let dictionary = try? JSONSerialization.jsonObject(with: decompressedData, options: .mutableLeaves)

            if let arrData = dictionary as? NSArray {
                print(dictionary!)
                self.arrAreaList = NSArray(array: arrData)
            }
            else {
                ISMessages.show("Area not found, try again later")
            }
        });
        task.resume()
    }
    func registerWebservice() {
        if Utility.isInterNetConnectionIsActive() == false
        {
            let alert = UIAlertController(title: "Alert", message:"There IS NO internet connection", preferredStyle: .alert)
            let later = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(later)
            self.present(alert, animated: true, completion: nil)
            return;
        }
        SVProgressHUD.show()
        do {

            let headers = [
                "content-type": "application/json"
            ]
            //dictUserData[K.Key.AreaID] as! String
            var strAreID = String()
            if self.areaID > 0 {
                strAreID = "\(self.areaID)"
            }
            let reqParam = ["FirstName" : dictUserData[K.Key.FirstName] as! String,
                            "LastName" : dictUserData[K.Key.LastName] as! String,
                            "Password" : dictUserData[K.Key.Password] as! String,
                            "ConfirmPassword" : dictUserData[K.Key.Password] as! String,
                            "Gender" : dictUserData[K.Key.Gender] as! String,
                            "Email" : dictUserData[K.Key.Email] as! String,
                            "AreaId" : strAreID,
                            "PhoneNumber" : dictUserData[K.Key.PhoneNumber] as! String,
                            "DateOfBirth" : dictUserData[K.Key.Birthday] as! String,
                            "Address" : dictUserData[K.Key.Address] as! String,
                            "LoginName" : dictUserData[K.Key.LoginName] as! String,
                            "FatherName" :  dictUserData[K.Key.LastName] as! String] as Dictionary<String,Any>
            
            
            print((reqParam))

            

            let postData = try! JSONSerialization.data(withJSONObject: reqParam, options: [])

            let request = NSMutableURLRequest(url: NSURL(string: K.URL.REGISTRATION)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data

            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                SVProgressHUD.dismiss()
                if (error != nil) {
                    print(error as Any as Any)
                    ISMessages.show("Error occurred, Please try again")
                } else {
                    let httpResponse = response as? HTTPURLResponse
                 print(httpResponse?.statusCode)
                    if httpResponse?.statusCode == 200
                    {
//                        let user = User(dict: nil)
//                        user.accessToken = token
//                        user.save()
//
//                        UserDefaults.standard.setValue(token, forKey: "token")
//                        UserDefaults.standard.set(true, forKey: K.Key.IsLoggedInUser)
//                        UserDefaults.standard.synchronize()
//                        AppUtility.sharedAppDelegate.setMainNavigationController()

                        ISMessages.show("User has been successfully registered.. and then take him to the login page to login to his account..")

                        DispatchQueue.main.async {
                        let destViewController : VerificationViewController = self.storyboard!.instantiateViewController(withIdentifier: "VerificationViewController") as! VerificationViewController
                        destViewController.dictUserData = self.dictUserData
                        let topViewController : UIViewController = self.navigationController!.topViewController!
                        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
                            print("Same VC")
                        } else {
                            self.navigationController!.pushViewController(destViewController, animated: true)
                            }

                        }
                    }
                    else if httpResponse?.statusCode == 409
                    {
                        ISMessages.show("User already exist.")
                    }
                    else
                    {
                        ISMessages.show("Error Occured please try again later.")
                    }
                }
            })
            dataTask.resume()
        }
        catch
        {
            print(error)
        }
    }
    /*    let reqParam = ["FirstName" : dictUserData[K.Key.FirstName] as! String,
                        "LastName" : dictUserData[K.Key.LastName] as! String,
                        "Password" : dictUserData[K.Key.Password] as! String,
                        "ConfirmPassword" : dictUserData[K.Key.Password] as! String,
                        "Gender" : dictUserData[K.Key.Gender] as! String,
                        "Email" : dictUserData[K.Key.Email] as! String,
                        "AreaId" : dictUserData[K.Key.AreaID] as! String,
                        "PhoneNumber" : dictUserData[K.Key.PhoneNumber] as! String,
                        "DateOfBirth" : dictUserData[K.Key.Birthday] as! String,
                        "Address" : dictUserData[K.Key.Address] as! String,
                        "LoginName" : dictUserData[K.Key.LoginName] as! String,
                        "FatherName" : ""] as Dictionary<String,Any>


        _ = WebClient.requestWithUrl(url: K.URL.REGISTRATION, parameters: reqParam, method: "POST") { (responseObject, error) in
            if error == nil {
                //let dict = responseObject as! Dictionary<String, Any>
                print("Data: \(responseObject!)")

                //Store User Data In It's Model And
                //let user = User(dict: responseObject?[K.Key.Data] as! Dictionary)

                if let token = (responseObject as! Dictionary<String,Any>)["access_token"] as? String {

                    let user = User(dict: nil)
                    user.accessToken = token
                    user.save()

                    UserDefaults.standard.setValue(token, forKey: "token")
                    UserDefaults.standard.set(true, forKey: K.Key.IsLoggedInUser)
                    UserDefaults.standard.synchronize()
                    AppUtility.sharedAppDelegate.setMainNavigationController()
                }
                else {
                    ISMessages.show("Error occurred, Please try again")
                }

            }else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        }
    }*/
}
