//
//  constantMessages.swift
//  PallaPay
//
//  Created by sachin on 14/02/18.
//  Copyright © 2018 Bilal. All rights reserved.
//

import UIKit
import Foundation

enum screenName : String
{
    case Task = "F130"
    case Notification = "F131"
    case Event = "F132"
    case MobileDashboard = "F136"
    case MobileProfile = "F137"
    case MobileCalendar = "F138"
}

class constantMessages
{
    class AppColor
    {
        static let pinkColor :UIColor = UIColor(red: 250.0/255.0, green: 0.0/255.0, blue: 89.0/255.0, alpha: 1.0)
        static let AppTheamColor :UIColor = UIColor(red: 54.0/255.0, green: 80.0/255.0, blue: 108.0/255.0, alpha: 1.0)
        static let selectedSegment :UIColor = UIColor(red: 39.0/255.0, green: 71.0/255.0, blue: 99.0/255.0, alpha: 1.0)
        static let noramalSegmenttext :UIColor = UIColor(red: 126.0/255.0, green: 144.0/255.0, blue: 172.0/255.0, alpha: 1.0)
        static let orenge :UIColor = UIColor(red: 255.0/255.0, green: 179.0/255.0, blue: 81.0/255.0, alpha: 1.0)
        
        
        static let themeColor :UIColor = UIColor (red: 54.0/255.0, green: 80.0/255.0, blue: 108.0/255.0, alpha: 1.0)
        static let deselectedColor :UIColor = UIColor (red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        static let greenColor :UIColor = UIColor (red: 37.0/255.0, green: 238.0/255.0, blue: 98.0/255.0, alpha: 1.0)
        static let GrayColor :UIColor = UIColor (red: 227.0/255.0, green: 229.0/255.0, blue: 233.0/255.0, alpha: 1.0)
        static let leadTextColor :UIColor = UIColor (red: 37.0/255.0, green: 238.0/255.0, blue: 98.0/255.0, alpha: 1.0)

        static let slidercolor :UIColor = UIColor (red: 37.0/255.0, green: 190.0/255.0, blue: 239.0/255.0, alpha: 1.0)
        static let slidercolorAnother :UIColor = UIColor (red: 54.0/255.0, green: 80.0/255.0, blue: 108.0/255.0, alpha: 1.0)

        static let addImageBack :UIColor = UIColor (red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1.0)

        static let reviewlabelColor :UIColor = UIColor (red: 119.0/255.0, green: 119.0/255.0, blue: 119.0/255.0, alpha: 1.0)

    }
    
    class userDefault
    {
        static let auth_Token = "auth_token"
        static let user_detail = "user_detail"
        static let auto_login = "autoLogin"
    }
    
    class TypeAndStatus
    {
        //TYpe
        static let Type1 = "Deposit"
        static let Type2 = "Withdrawal"
        static let Type3 = "Transfer"
        static let Type4 = "Exchange"
        static let Type5 = "External deposit"
        static let Type6 = "Virtual Terminal"

        //Status
        static let Status0 = "anonymous"
        static let Status1 = "verified"
        static let Status2 = "business"
        static let Status3 = "pending verification"
    }

    class Currency
    {
        //TYpe
        static let currencyRUB = "RUB"
        static let currencyEUR = "EUR"
        static let currencyAED = "AED"
        static let currencyCNY = "CNY"
        static let currencyCCC = "CCC"
        static let currencyUSD = "USD"
    }

    /*
     // MARK: Date Time Formate
     */
    class TimeFormate {
        
        static let common_dateTime_format = "yyyy-MM-dd\'T\'HH:mm:ss Z"
        static let common_dateTime_format_withouttimeZone = "yyyy-MM-dd\'T\'HH:mm:ss"
        static let common_dateformat_picker = "yyyy-MM-dd\' \'HH:mm:ss Z"
        static let common_date_format = "yyyy-MM-dd"
        static let common_date = "yyyy/MM/dd"
        static let common_date_format_yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss"
        static let common_date_format_event = "yyyy-MM-dd HH:mm:ss Z"
        static let common_date_format_chat = "yyyy-MM-dd hh:mm aa"
        static let common_date_format_reminder = "EEE d MMM, yyyy 'at' hh:mm aa"
        static let common_date_format_list = "EEE d MMM 'at' hh:mm a"
        static let common_MMMdhhmma = "MMM d 'at' hh:mm a"
        static let common_dMMMhhmma = "dd MMM' at' hh:mm a"
        static let common_Time_formate_HHMM_A = "hh:mm a"
        static let common_dateTime_format_app = "dd-MM-yyyy HH:mm:ss ZZZ"
        static let common_dateformate_DDMMMYYYY = "dd MMM, yyyy 'at' hh:mm a"
        static let dateformate_YYYY = "yyyy"
        static let common_Time_formate_HHMMSS_A = "HH:mm:ss"
        static let common_Time_formate_HH_MM_A = "HH-mm a"
        static let common_Time_formate_DDMMM = "dd MMM"
        static let common_Time_formate_HH_mm = "HH:mm"
        static let common_Time_formate_DD_MMM = "dd MMM"
        static let common_Time_formate_hh_mm_dd_MM_yyyy = "hh:mm a dd MMM yyyy"
        static let common_Time_formate_dd_MM_yyyy_hh_mm_ss = "MM/dd/yyyy HH:mm:ss a"
        static let common_Time_formate_EEE_MMM_yy = "EEEE, MMMM dd"
        static let common_Time_formate_EEE_MMM_yy_HHMM_A = "EEEE, MMMM dd hh:mm a"
        static let common_Time_formate_dd_MMM_yyyy = "dd MMMM yyyy"
    }
    
    class Message
    {
        static let reminderdelete =  "Are you sure you want to delete all reminders?"
        static let notinternetconnection = "Please make sure you have connected to Internet."
        static let SomethingWentwrong = "Something went wrong Please try again later."
        static let insertUsernameorEmail = "Please Insert Username or email."
        static let insertPassword = "Please Insert Password."
        static let insertUsername = "Please Insert Username."
        static let insertConfirmPassword = "Please Insert Confirm Password."
        static let insertName = "Please Insert Name."
        static let insertEmail = "Please Insert Email."
        static let NotvalidEmail = "Please Insert Valid Email."
        static let notmatchPassword = "Password mismatch"
        static let selecttermsandcondition = "Select Terms and Conditions"
        static let Insertmessage = "Insert Message."
        static let UsernotAvailable = "User not Available."
        static let errorMessage = "Failed to store data please fill up all details"
        static let requestTimeout = "Request time out please try again later"

        static let PaaswordMustBeSixCharavcter = "Password must be six or more character."
        static let InsertMobile = "Please Insert Mobile Number"
        static let InsertMobileNumberCount = "Please Insert atlease 10 digit Mobile number"
        static let PincodeCharacter = "Please Insert Pincode atleast 6 character"
        static let InsertAddress = "Please Insert Address"
        static let InsertGSTNo = "Please Insert GST No."
        static let Bronze = "Bronze"
        static let Silver = "Silver"
        static let Gold = "Gold"
        static let NotYouTubeURL = "Only Youtube URL is allowed."
        static let DeleteMessage = "Are you sure you want to delete?"
        static let selectyear = "Please Select Year."
        static let selectCost = "Please Select Project Cost."
        static let selectCarprtArea = "Please Insert Carpet Area."
        static let Pincode = "Please Insert Pincode."
        static let MemberShipOpenFromMyStory = "My Story"
        static let MembershipOpenFromDashboard = "DashBoard"
        static let addressType = "Please Select Address Type"
        static let OldpasswordLength = "Old Password length must be 6 to 15 character."
        static let passwordLength = "Password length must be 6 to 15 character."
        static let ConfirmpasswordLength = "Confirm Password length must be 6 to 15 character."
        static let NotificationPurchase = "Purchase"
        static let Selectpincode = "This will clear the Cities details you have entered. Do you wish to continue?"
        static let selectCity = "This will clear the Pincode details you have entered. Do you wish to continue?"
        static let selectimage = "Please select atleast one Image."

        static let insertdesc = "Please Insert Description"

        static let selectPlan = "Please Select Membership Plan"
        static let youtubeUrl = "Only Youtube URL is allowed."
        static let MemberShipForBuyLead = "MemberShipForBuyLead"
        static let ratingAvalable = "For Hide Rating you need to upgrade your plan"
        static let selectCategory = "Please select atleast one category."

        static let selectcountryorPincode = "Please Insert detail either Country or Pincode."
        static let selectState = "Please Select State"
        static let selectLeadcity = "Please Select City"
        static let selectMinbudget = "Please Select Preferred Minimum Budget"
        static let selectPreProType = "Please Select Preferred Project Type"
        static let cantaddMedialink = "You can't add more Media Link"
        static let cantaddaward = "You can't add more Award"
        static let cantaddcasestudy = "You can't add more Case Study"


    }
    
    class notificationObserver
    {
        static let RegisterHeight = "heightContainer"
        static let ProLeadHeight = "heightContainerProLead"
        static let HeightmembershipPlan = "HeightmembershipPlan"
        static let alakartPrice = "alakartPrice"
        static let addressRefresh = "address"

    }
    
    class Variables
    {
        static let swipebuttonwidth: CGFloat = 80
    }
}

extension UIImage{
    convenience init(view: UIView) {
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: false)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage)!)
        
    }
}


