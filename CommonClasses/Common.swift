//
//  Common.swift
//  MediaBox
//
//  Created by sachin on 21/01/18.
//  Copyright © 2018 Sachin Patoliya. All rights reserved.
//

import UIKit
import AssetsLibrary
import Photos
import AVKit
import AVFoundation


let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate


class Common
{
    static let shared = Common()

    //MARK:- Check Empty or nil string
    class func CheckEmptyString(strString : String) -> Bool
    {
        if strString.isEmpty
        {
            return true
        }
        else
        {
            return false
        }
    }
    //MARK:-

    
    //MARK:- Check Valid Email
    class func isEmailValidate(email : String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9._-]+\\.[A-Za-z]{1,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    //MARK:-


    //MARK:- Alert Controller for One Button
    class func alertControllerForOneButton(viewcontroller:UIViewController , message:String)
    {
        let actionSheetController: UIAlertController = UIAlertController(title: "IDprop", message: message, preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        viewcontroller.present(actionSheetController, animated: true, completion: nil)
    }
    //MARK:-

    //MARK:- GEt Day,Month,Year
    class func getDayMonthYear() -> Int
    {
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        return year
    }
    
    //MARK:- Alert Controller for Two Button
    class func alertControllerForTwoButton(viewcontroller:UIViewController , message:String)
    {
        let actionSheetController: UIAlertController = UIAlertController(title: "IDprop", message: message, preferredStyle: .alert)
        let Yes: UIAlertAction = UIAlertAction(title: "YES", style: .default) { action -> Void in
      
        }
        let cancelAction: UIAlertAction = UIAlertAction(title: "NO", style: .cancel) { action -> Void in

        }
        actionSheetController.addAction(Yes)
        actionSheetController.addAction(cancelAction)
        viewcontroller.present(actionSheetController, animated: true, completion: nil)
    }
    //MARK:-

    
    //MARK:- Play A Video from PHAsset or Local File
    class func playVideo (view:UIViewController, assetVideo:PHAsset)
    {
        let imageManager = PHCachingImageManager()
        
        let options: PHVideoRequestOptions = PHVideoRequestOptions()
        options.version = .current
        imageManager.requestAVAsset(forVideo: assetVideo, options: nil, resultHandler: { (asset, audioMix, info) in
            
            if asset as? AVURLAsset != nil
            {
                let asset = asset as! AVURLAsset
                DispatchQueue.main.async(execute: {
                    let player = AVPlayer (url: asset.url)
                    let playerViewController = AVPlayerViewController()
                    player.volume = 5.0
                    playerViewController.player = player
                    view.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                })

            }
            else
            {
                var composition = AVComposition()
                composition = asset as! AVComposition
               // let videoComposition = AVMutableVideoComposition()
                let item = AVPlayerItem(asset: composition)
             //   item.videoComposition = videoComposition
                let player = AVPlayer (playerItem: item)
                let playerViewController = AVPlayerViewController()
                player.volume = 5.0
                playerViewController.player = player
                view.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }

            }
            
        })
    }
    //MARK:-

    
    //MARK:- Open a Image
    class func OpenImage (view:UIViewController, image:UIImage, frame : CGRect)
    {
//        let imageInfo = JTSImageInfo()
//        imageInfo.image = image
//        imageInfo.referenceRect = frame
//        imageInfo.referenceView = view.view
//        // Setup view controller
//        let imageViewer = JTSImageViewController(imageInfo: imageInfo, mode: JTSImageViewControllerMode.image, backgroundStyle: JTSImageViewControllerBackgroundOptions.scaled)
//        // Present the view controller.
//        imageViewer?.show(from: view, transition: JTSImageViewControllerTransition.fromOriginalPosition)
    }
    //MARK:-
    
    
    //MARK:- Set Border On View
    class func setBorderOnView(view:UIView, color:UIColor)
    {
        view.layer.borderWidth = 1.0
        view.layer.borderColor = color.cgColor
        view.layer.masksToBounds = true
    }
    //MARK:-
    
    //MARK:- Set Corner On View
    class func setCornerOnView(view:UIView, cornerradius:CGFloat)
    {
        view.layer.cornerRadius = cornerradius
        view.layer.masksToBounds = true
    }
    //MARK:-

    //MARK:- Set Corner and Shadow On View
    class func SetShadowwithCorneronView(view:UIView, cornerradius:CGFloat, color:UIColor)
    {
        view.layer.cornerRadius = cornerradius
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.borderWidth = 1.0
        view.layer.borderColor = color.cgColor
        view.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        view.layer.shadowOpacity = 0.15
        view.layer.shadowRadius = 5.0
        //view.clipsToBounds = true
    }
    //MARK:-
    
    
    //MARK:- Set Corner and Shadow On Buttton
    class func SetShadowwithCorneronButton(view:UIButton, cornerradius:CGFloat)
    {
        view.layer.cornerRadius = cornerradius
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        view.layer.shadowOpacity = 0.15
        view.layer.shadowRadius = 5.0
    }
    //MARK:-

    //MARK:- Set Corner and Shadow On View
    class func SetShadowwithCorneronImageView(view:UIImageView)
    {
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        view.layer.shadowOpacity = 0.15
        view.layer.shadowRadius = 5.0
    }
    //MARK:-

    
    //MARK:- Set Corner On ImageView
    class func SetCorneronImageView(view:UIImageView, cornerradius:CGFloat)
    {
        view.layer.cornerRadius = cornerradius
        view.layer.masksToBounds = true
    }
    //MARK:-


    //MARK:- Set Border On Buttons
    class func setBorderOnButton(buttton:UIButton, color:UIColor)
    {
        buttton.layer.borderWidth = 1.5
        buttton.layer.borderColor = color.cgColor
        buttton.layer.masksToBounds = true
    }
    //MARK:-
    
    //MARK:- Set Corner On Buttons
    class func setCornerOnButton(buttton:UIButton, cornerradius:CGFloat)
    {
        buttton.layer.cornerRadius = cornerradius
        buttton.layer.masksToBounds = true
    }
    //MARK:-


    
//    //MARK:- Change textField Placeholder COlor
//    class func SetPlaceholderCOlorTextField(myTextField:UITextField, strtext:String)
//    {
//        myTextField.attributedPlaceholder = NSAttributedString(string: strtext,
//                                                               attributes: [NSForegroundColorAttributeName: UIColor.white])
//        myTextField.tintColor = .white
//
//    }
//    //MARK:-

    //MARK:- Change textField Placeholder with COlor
//    class func SetPlaceholderCOlorTextFieldWithColor(myTextField:UITextField, strtext:String, color: UIColor)
//    {
//        myTextField.attributedPlaceholder = NSAttributedString(string: strtext,
//                                                               attributes: [NSForegroundColorAttributeName: color])
//        myTextField.tintColor = color
//    }
//    //MARK:-
//
//    //MARK:- Change textField Placeholder COlor with Color
//    class func SetPlaceholderCOlorTextFieldSelectedColor(myTextField:UITextField, strtext:String, color:UIColor)
//    {
//        myTextField.attributedPlaceholder = NSAttributedString(string: strtext,
//                                                               attributes: [NSForegroundColorAttributeName: color])
//        myTextField.tintColor = color
//    }
    //MARK:-

    
    //Convert Date Format
    class func convertdateFormat(date: Date, format:String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return  dateFormatter.string(from: date)
    }
        
    //Get Days Count between two days
    class func getDaysbetweentwodates(FromDate: Date, count: Int) -> NSInteger
    {
        let date = Calendar.current.date(byAdding: .month, value: count, to: FromDate)

        let calendar = NSCalendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: FromDate)
        let date2 = calendar.startOfDay(for: date!)
        
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        return components.day!
    }

//    //Calcuate Height
//    class func calculateHeight(inString:String,fontSize:CGFloat,frame:CGFloat) -> CGFloat {
//        let messageString = inString
//        let attributes : [String : Any] = [NSFontAttributeName : UIFont.systemFont(ofSize: fontSize)]
//
//        let attributedString : NSAttributedString = NSAttributedString(string: messageString, attributes: attributes)
//
//        let rect : CGRect = attributedString.boundingRect(with: CGSize(width: frame - 20, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
//
//        let requredSize:CGRect = rect
//        return requredSize.height
//    }
//
    //MARK:- REmoves space from url
    class func removespacefromUrl(strurl: String) -> String
    {
        let urlString = strurl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        return urlString!
    }
    
    //MARK:- Gradiant COlor
    class func gradiantcolor(slider: UISlider, color1: UIColor, color2: UIColor) -> UIImage
    {
        let tgl = CAGradientLayer()
        let frame = CGRect.init(x:0, y:0, width:slider.frame.size.width, height:slider.frame.size.height)
        tgl.cornerRadius = 5.0
        tgl.masksToBounds = true
        tgl.frame = frame
        tgl.colors = [color1.cgColor, color2.cgColor]
        tgl.startPoint = CGPoint.init(x:0.0, y:0.5)
        tgl.endPoint = CGPoint.init(x:1.0, y:0.5)
        
        UIGraphicsBeginImageContextWithOptions(tgl.frame.size, tgl.isOpaque, 0.0);
        tgl.render(in: UIGraphicsGetCurrentContext()!)
        let image1 = UIImage()
        if let image = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            
            image.resizableImage(withCapInsets: UIEdgeInsets.zero)
            
            return image
        }
        return image1
    }
    
    class func setattributetextColor(strtext:String,rangeString: String) -> NSMutableAttributedString
    {
        let main_string = strtext
        let string_to_color = rangeString
        
        let range = (main_string as NSString).range(of: string_to_color)
        let attributedString = NSMutableAttributedString(string:main_string)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor (red: 8.0/255.0, green: 29.0/255.0, blue: 106.0/255.0, alpha: 1.0) , range: range)
        return attributedString
    }
    
    
    //MARK:- Attributed String
    class func stringFromHtml(string: String) -> NSAttributedString? {
        do {
            let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
            if let d = data {
                let str = try NSAttributedString(data: d,
                                                 options: [.documentType: NSAttributedString.DocumentType.html],
                                                 documentAttributes: nil)
                return str
            }
        } catch {
        }
        return nil
    }
    
    //Calcuate Height
    class func calculateHeight(inString:String,fontSize:CGFloat,frame:CGFloat) -> CGFloat {
        let messageString = inString
       // let attributes : [String : Any] = [NSAttributedStringKey.font.rawValue : UIFont.systemFont(ofSize: fontSize)]
        
        let attributedString : NSAttributedString = NSAttributedString(string: messageString, attributes: [.font:UIFont.systemFont(ofSize: fontSize)])
        
        let rect : CGRect = attributedString.boundingRect(with: CGSize(width: frame - 20, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        let requredSize:CGRect = rect
        return requredSize.height
    }


}

//MARK: Date
extension Date {
    func getsixMonth() -> Date? {
        return Calendar.current.date(byAdding: .month, value: 6, to: self)
    }
    
    func getyear() -> Date? {
        return Calendar.current.date(byAdding: .year, value: 1, to: self)
    }
}


//MARK:- Check Type
class TransactionType
{
    class func checTransactionType(strtype: String) -> String
    {
        var str = String()
        if strtype == "1"
        {
            str = constantMessages.TypeAndStatus.Type1
        }
        else if strtype == "2"
        {
            str = constantMessages.TypeAndStatus.Type2
        }
        else if strtype == "3"
        {
            str = constantMessages.TypeAndStatus.Type3
        }
        else if strtype == "4"
        {
            str = constantMessages.TypeAndStatus.Type4
        }
        else if strtype == "5"
        {
            str = constantMessages.TypeAndStatus.Type5
        }
        else if strtype == "6"
        {
            str = constantMessages.TypeAndStatus.Type6
        }
        return str
    }
}
//MARK:-

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}


//MARK:- Check Currency
class CurrencyType
{
    class func checkCurrencyType(strtype: String) -> String
    {
        var str = String()
        if strtype == "debit_base"
        {
            str = constantMessages.Currency.currencyUSD
        }
        else if strtype == "debit_extra1"
        {
            str = constantMessages.Currency.currencyRUB
        }
        else if strtype == "debit_extra2"
        {
            str = constantMessages.Currency.currencyEUR
        }
        else if strtype == "debit_extra3"
        {
            str = constantMessages.Currency.currencyAED
        }
        else if strtype == "debit_extra4"
        {
            str = constantMessages.Currency.currencyCNY
        }
        else if strtype == "debit_extra5"
        {
            str = constantMessages.Currency.currencyCCC
        }
        return str
    }
    
    class func checkCurrencyTypeUSDToTag(strtype: String) -> String
    {
        var str = String()
        if strtype ==  constantMessages.Currency.currencyUSD
        {
            str = "debit_base"
        }
        else if strtype == constantMessages.Currency.currencyRUB
        {
            str = "debit_extra1"
        }
        else if strtype == constantMessages.Currency.currencyEUR
        {
            str = "debit_extra2"
        }
        else if strtype == constantMessages.Currency.currencyAED
        {
            str = "debit_extra3"
        }
        else if strtype == constantMessages.Currency.currencyCNY
        {
            str = "debit_extra4"
        }
        else if strtype == constantMessages.Currency.currencyCCC
        {
            str = "debit_extra5"
        }
        return str
    }
}
//MARK:-



//MARK:- Check Type
class TransactionStatus
{
    class func checTransactionStatus(strtype: String) -> String
    {
        var str = String()
        if strtype == "0"
        {
            str = constantMessages.TypeAndStatus.Status0
        }
        else if strtype == "1"
        {
            str = constantMessages.TypeAndStatus.Status1
        }
        else if strtype == "2"
        {
            str = constantMessages.TypeAndStatus.Status2
        }
        else if strtype == "3"
        {
            str = constantMessages.TypeAndStatus.Status3
        }
        return str
    }
}
//MARK:-


//MARK:- REmove null values from dictionary

extension Dictionary {
    func nullKeyRemoval() -> [AnyHashable: Any] {
        var dict: [AnyHashable: Any] = self
        
        let keysToRemove = dict.keys.filter { dict[$0] is NSNull }
        let keysToCheck = dict.keys.filter({ dict[$0] is Dictionary })
        for key in keysToRemove {
            dict.removeValue(forKey: key)
        }
        for key in keysToCheck {
            if let valueDict = dict[key] as? [AnyHashable: Any] {
                dict.updateValue(valueDict.nullKeyRemoval(), forKey: key)
            }
        }
        return dict
    }
}

extension String {
//    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
//        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
//        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
//        
//        return ceil(boundingBox.height)
//    }
//    
//    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
//        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
//        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
//        
//        return ceil(boundingBox.width)
//    }
}


class AlbumModel {
    let name:String
    let count:Int
    let collection:PHAssetCollection
    init(name:String, count:Int, collection:PHAssetCollection) {
        self.name = name
        self.count = count
        self.collection = collection
    }
}


extension UIImage
{
    class func imageWithLabel(label: UILabel) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, 0.0)
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    public class func gifImageWithData(data: NSData) -> UIImage? {
        guard let source = CGImageSourceCreateWithData(data, nil) else {
            print("image doesn't exist")
            return nil
        }
        
        return UIImage.animatedImageWithSource(source: source)
    }
    
    public class func gifImageWithURL(gifUrl:String) -> UIImage? {
        guard let bundleURL = NSURL(string: gifUrl)
            else {
                print("image named \"\(gifUrl)\" doesn't exist")
                return nil
        }
        guard let imageData = NSData(contentsOf: bundleURL as URL) else {
            print("image named \"\(gifUrl)\" into NSData")
            return nil
        }
        
        return gifImageWithData(data: imageData)
    }
    
    public class func gifImageWithName(name: String) -> UIImage? {
        guard let bundleURL = Bundle.main
            .url(forResource: name, withExtension: "gif") else {
                print("SwiftGif: This image named \"\(name)\" does not exist")
                return nil
        }
        
        guard let imageData = NSData(contentsOf: bundleURL) else {
            print("SwiftGif: Cannot turn image named \"\(name)\" into NSData")
            return nil
        }
        
        return gifImageWithData(data: imageData)
    }
    
    class func delayForImageAtIndex(index: Int, source: CGImageSource!) -> Double {
        var delay = 0.1
        
        let cfProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil)
        let gifProperties: CFDictionary = unsafeBitCast(CFDictionaryGetValue(cfProperties, Unmanaged.passUnretained(kCGImagePropertyGIFDictionary).toOpaque()), to: CFDictionary.self)
        
        var delayObject: AnyObject = unsafeBitCast(CFDictionaryGetValue(gifProperties, Unmanaged.passUnretained(kCGImagePropertyGIFUnclampedDelayTime).toOpaque()), to: AnyObject.self)
        
        if delayObject.doubleValue == 0 {
            delayObject = unsafeBitCast(CFDictionaryGetValue(gifProperties, Unmanaged.passUnretained(kCGImagePropertyGIFDelayTime).toOpaque()), to: AnyObject.self)
        }
        
        delay = delayObject as! Double
        
        if delay < 0.1 {
            delay = 0.1
        }
        
        return delay
    }
    
    class func gcdForPair(a: Int?, _ b: Int?) -> Int {
        var a = a
        var b = b
        if b == nil || a == nil {
            if b != nil {
                return b!
            } else if a != nil {
                return a!
            } else {
                return 0
            }
        }
        
        if a! < b! {
            let c = a!
            a = b!
            b = c
        }
        
        var rest: Int
        while true {
            rest = a! % b!
            
            if rest == 0 {
                return b!
            } else {
                a = b!
                b = rest
            }
        }
    }
    
    class func gcdForArray(array: Array<Int>) -> Int {
        if array.isEmpty {
            return 1
        }
        
        var gcd = array[0]
        
        for val in array {
            gcd = UIImage.gcdForPair(a: val, gcd)
        }
        
        return gcd
    }
    
    class func animatedImageWithSource(source: CGImageSource) -> UIImage? {
        let count = CGImageSourceGetCount(source)
        var images = [CGImage]()
        var delays = [Int]()
        
        for i in 0..<count {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                images.append(image)
            }
            
            let delaySeconds = UIImage.delayForImageAtIndex(index: Int(i), source: source)
            delays.append(Int(delaySeconds * 1000.0)) // Seconds to ms
        }
        
        let duration: Int = {
            var sum = 0
            
            for val: Int in delays {
                sum += val
            }
            
            return sum
        }()
        
        let gcd = gcdForArray(array: delays)
        var frames = [UIImage]()
        
        var frame: UIImage
        var frameCount: Int
        for i in 0..<count {
            frame = UIImage(cgImage: images[Int(i)])
            frameCount = Int(delays[Int(i)] / gcd)
            
            for _ in 0..<frameCount {
                frames.append(frame)
            }
        }
        
        let animation = UIImage.animatedImage(with: frames, duration: Double(duration) / 1000.0)
        
        return animation
    }
}
